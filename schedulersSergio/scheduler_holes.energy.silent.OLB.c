// Scheduler of multiple DAGs using HEFT
// Parameters : <instance_JOB_file> <instance_CN_file> <num_jobs> <num_CNs> 
//	

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>

#define NO_ASIG -1
#define SIZE_NOM_ARCH 180

#define DEBUG 0
#define DEBUG_0 0 
#define DEBUG_2 0
#define DEBUG_3 0
#define INFT 999999.0

#define TOKEN_LENGTH 100
#define LINE_LENGTH 5000

#define SQRT 0
#define SQR 1
#define LIN 2

// Task node (HEFT)
typedef struct _nodeT_HEFT {
   int task_id;
   float ur_value;
   struct _nodeT_HEFT *next;
} NodeT_HEFT;

typedef struct _nodeH {
   float start;
   float end;
   int cores;
   struct _nodeH *next;
} NodeH;

typedef NodeT_HEFT *pNodeT_HEFT;
typedef NodeT_HEFT *ListT_HEFT;
typedef NodeH *ListH;

// Task node (scheduler)
typedef struct _nodeT {
   int task_id;
   float ex_time;
   int cores;
   int machine;
   float ST;
   ListT_HEFT DEP;
   ListT_HEFT SUCC;
   struct _nodeT *next;
} NodeT;

typedef NodeT *pNodeT;
typedef NodeT *ListT;

// Job type
typedef struct _TypeJob {
	int job_id;
	int NT;
	double deadline;
	int pen_type;
	double penalization;
	ListT tasks;
	ListT_HEFT ur_list;
} TypeJob;

// CN type
typedef struct _TypeCN {
	int CN_id;
	int NM;
	int cores;
	float speed;
	float comm_cost; /* Communication cost between different machines in the CN */
	float EIdle;
	float EMax;
	int jobs; 		/*jobs assigned, according to solution */
	float est_ET; 	/* estimated execution time, according to estimation*/
	float makespan; /* makespan, according to scheduler */
	float sum_penalizations; /* sum of penalizations for jobs assigned to the CN (for deadline violations)*/
} TypeCN;

typedef struct _type_asig{
   int mach;
   float ST;
} asig_type;

int main(int argc, char *argv[]){

// List of tasks

void printlist(ListT_HEFT l){
    // Print a list
    ListT_HEFT ln = l;
    printf("[");
    while (ln != NULL){
        printf("%d ",ln->task_id);
        ln = ln->next;
    }
    printf("] ");
}

void printranks(ListT_HEFT l){
    // Print ranks
	printf("Upward ranks\n");
    ListT_HEFT ln = l;
    while (ln != NULL){
        printf("%d->%.2f\n",ln->task_id,ln->ur_value);
        ln = ln->next;
    }
}

void printholes(ListH l){
    // Print a list
    ListH ln = l;
    printf("[");
    while (ln != NULL){
        printf("%.2f-%.2f (%dC) ",ln->start,ln->end,ln->cores);
        ln = ln->next;
    }
    printf("]\n");
}

void printlistT(ListT l){
    // Print a list
    ListT ln = l;
    while (ln != NULL){
    	printf("T%d (%.2f %dC) DEP: ",ln->task_id,ln->ex_time,ln->cores);
		printlist(ln->DEP);
		printf("SUCC: ");
		printlist(ln->SUCC);
        ln = ln->next;
    }
}

void deletehole(ListH *l, float st_hole, float et_hole, int cores_hole){
    // Delete hole with start st_hole and end et_hole in the list l.

    ListH pn, pant;

#if DEBUG_2
printf("+++++++\n");
printf("Delete hole: start:%.2f, end:%.2f, cores:%d\n",st_hole,et_hole,cores_hole);
printholes(*l);
printf(" -> ");
#endif

    pn = *l;
    pant = NULL;

    while ((pn != NULL) && (pn->start != st_hole || pn->end != et_hole || pn->cores != cores_hole)){
//printf("pn: start:%.2f, end:%.2f, cores:%d\n",pn->start,pn->end,pn->cores);
        pant = pn;
        pn = pn->next;
    }

	if (pn == NULL){
		// Such hole does not exist, it was previously compacted with another one
#if DEBUG_2
printf("Delete hole: DOES NOT EXIST, start:%.2f, end:%.2f, cores:%d\n",st_hole,et_hole,cores_hole);
#endif
	} else {
    	if (pant == NULL){
    	    // First element
    	    *l = pn->next;
    	} else {
    	    pant->next = pn->next;
    	}
    	free(pn);
	}


#if DEBUG_2
    printholes(*l);
printf("++++++\n");
#endif

    //free(phole);
}

void compactholes(ListH *l){
	// Compact holes

    ListH pn;

#if DEBUG_2
printf("+++ COMPACT HOLES +++\n");
printf("    ");
printholes(*l);
printf(" -> ");
#endif

    pn = *l;


    while ((pn != NULL) && (pn->next != NULL)) {
		if ( (pn->end == pn->next->start) && (pn->cores == pn->next->cores) ){
#if DEBUG_2
printf("compact: start:%.2f, end:%.2f, cores:%d\n",pn->start,pn->next->end,pn->next->cores);
#endif
			pn->end = pn->next->end;
			deletehole(l,pn->next->start,pn->next->end,pn->next->cores);
		}
        pn = pn->next;
    }

#if DEBUG_2
    printholes(*l);
printf("+++ COMPACT HOLES +++\n");
#endif

}

void inserthole(ListH *l, float st, float et, int c){
    // Insert hole with starting time st and end time et in list lh.
	// Sorted by finishing time (earliest first)

#if DEBUG_2
printf("Insert hole: start:%.2f, end:%.2f, cores:%d\n",st,et,c);
#endif

    ListH pnh;
    pnh = (ListH) malloc(sizeof(NodeH));
    pnh->start = st;
    pnh->end = et;
    pnh->cores = c;
    pnh->next = NULL;

    if ( (*l == NULL) || ((*l)->end > et) ){
		pnh->next = *l;
        *l = pnh;
    } else {
        ListH lt = *l;

        while ( (lt->next != NULL) && (lt->next->end < et) ){
           	lt = lt->next;
        }
		if (lt->next == NULL){
        	lt->next = pnh;
		} else {
			pnh->next = lt->next;
			lt->next = pnh;
		}
    }
	compactholes(l);
}

void insertlist(ListT_HEFT *l, int t){
    // Insert node with task_id t at the end of list l.

    pNodeT_HEFT pn;
    pn = (pNodeT_HEFT) malloc(sizeof(NodeT_HEFT));
    pn->task_id = t;
    pn->ur_value = 0.0;
    pn->next = NULL;

    if (*l == NULL){
        *l = pn;
    } else {
        ListT_HEFT lt = *l;

        while (lt->next != NULL){
            lt = lt->next;
        }
        lt->next = pn;
    }
}

#if 0
void insertlistTask(ListT *l, int id){
    // Insert node with task_id t at the begining of list l.

    pNodeT pn;
    pn = (pNodeT) malloc(sizeof(NodeT));
    pn->task_id = id;
    pn->ex_time = 0.0;
	pn->machine = NO_ASIG;
    pn->ST = 0.0;
    pn->next = NULL;
    pn->DEP  = NULL;
    pn->SUCC = NULL;

	pn->next = *l;
    *l = pn;
}
#endif

void insertlistTask(ListT *l, int id){
    // Insert node with task_id t at the end of list l.

    pNodeT pn;
    pn = (pNodeT) malloc(sizeof(NodeT));
    pn->task_id = id;
    pn->cores = 0;
    pn->ex_time = 0.0;
	pn->machine = NO_ASIG;
    pn->ST = 0.0;
    pn->next = NULL;
    pn->DEP  = NULL;
    pn->SUCC = NULL;

    if (*l == NULL){
        *l = pn;
    } else {
        ListT lt = *l;

        while (lt->next != NULL){
            lt = lt->next;
        }
        lt->next = pn;
    }
}

void insordlist(ListT_HEFT *l, int t, float ur){
    // Insert node with task_id t in list l, ordered by (decreasing) ur_value.

    pNodeT_HEFT pn;
    pn = (pNodeT_HEFT) malloc(sizeof(NodeT_HEFT));
    pn->task_id = t;
    pn->ur_value = ur;
    pn->next = NULL;

    if ( (*l == NULL) || ((*l)->ur_value < ur) ){
		pn->next = *l;
        *l = pn;
    } else { 
		ListT_HEFT lt = *l;

        while ( (lt->next != NULL) && (lt->next->ur_value > ur) ){
           	lt = lt->next;
        }
		if (lt->next == NULL){
        	lt->next = pn;
		} else {
			pn->next = lt->next;
			lt->next = pn;
		}
    }
}

void createlist(ListT *l, int NT){
    // Create list of tasks with id_task from NT-1 to 0.
	// Upward rank calculation requieres reverse order !!
    int i;
    for (i=NT-1;i>=0;i--){
        insertlistTask(l,i);
    }
}

int ready(TypeJob Job, int tid){
	ListT lt,lt2;
	ListT_HEFT ld;

	lt = Job.tasks;
	while (lt->task_id != tid) {
		lt = lt->next;
	}
	ld = lt->DEP;
	int search = 1;
	while ((ld != NULL) && (search)){
		lt2 = Job.tasks;
		while (lt2->task_id != ld->task_id) {
			lt2 = lt2->next;
		}
		search = (lt2->machine != NO_ASIG);
		ld = ld->next;
	}
	return(ld==NULL);
}

void delete(ListT_HEFT *l, int t){
    // Delete node with task_id t in the list l.

    pNodeT_HEFT pn, pant;

    pn = *l;
    pant = NULL;
    while (pn && pn->task_id != t){
        pant = pn;
        pn = pn->next;
    }

    if (pant == NULL){
        // First element
        *l = pn->next;
    } else {
        pant->next = pn->next;
    }
    free(pn);
//printf("l after: ");
//printlista(*l);
}


void deleteTL(ListT *l, int t){
    // Delete node with task_id t in the list l.

    pNodeT pn, pant;

    pn = *l;
    pant = NULL;
    while (pn && pn->task_id != t){
        pant = pn;
        pn = pn->next;
    }

    if (pant == NULL){
        // First element
        *l = pn->next;
    } else {
        pant->next = pn->next;
    }
    free(pn);
//printf("l after: ");
//printlista(*l);
}

double ComputePenalizations(double exceededTime, int pen_type) {
	double pen = 0.0;

	if (exceededTime > 0.0) // Just in case, ComputePenalizations should never be called for exceededTime <= 0
		switch(pen_type) {
			case SQRT:	pen = sqrt(exceededTime);		    break;
			case SQR:	pen = exceededTime*exceededTime;	break;
			case LIN:	pen = exceededTime;				break;
		}

#if DEBUG
printf("ComputePenalizations: exceeded time: %.2f, pen_type:%d, pen: %.2f\n",exceededTime,pen_type,pen);
#endif
		
	return pen;
}

// start main

if (argc < 6){
        printf("Sintaxis: %s <instance_JOB_file> <instance_CN_file> <solution_file> <num_jobs> <num_CNs> \n", argv[0]);
        exit(1);
}

int NJ, NC;
FILE *fc, *fs, *fj;
char *JOB_file, *CN_file, *sol_file;

char line[LINE_LENGTH];
char *token = (char *) malloc (sizeof(char)*TOKEN_LENGTH);

NJ = atoi(argv[4]);
NC = atoi(argv[5]);

JOB_file = (char *)malloc(sizeof(char)*1024);
strcpy(JOB_file,argv[1]);

CN_file = (char *)malloc(sizeof(char)*1024);
strcpy(CN_file,argv[2]);

sol_file = (char *)malloc(sizeof(char)*1024);
strcpy(sol_file,argv[3]);

#if DEBUG_0
fprintf(stdout,"NJ: %d, NC: %d, arch_JOB: %s, CN_file: %s.\n",NJ,NC,JOB_file,CN_file);
#endif

int i,j,h,k,r;
float avg_comm;

TypeJob JOBS[NJ];
TypeCN CN[NC];

// list of JOBS to CN assignments 
int **MA = (int **) malloc(sizeof(int *)*NC);

#if 0

float **COMM = (float **) malloc(sizeof(float *)*NM);
if (COMM == NULL){
    fprintf(stderr,"Error in malloc for ETC matrix, dimensions %dx%d\n",NM,NM);
    exit(2);
}

for (i=0;i<NM;i++){
    COMM[i] = (float *) malloc(sizeof(float)*NM);
    if (COMM[i] == NULL){
        fprintf(stderr,"Error in malloc, row %d in COMM\n",i);
        exit(2);
    }
}

// Read communication values
if((fc = fopen(COMM_file, "r"))==NULL){
    fprintf(stderr,"Can't read communication costs file: %s\n",ETC_file);
    exit(1);
}

for (j=0;j<NM;j++){
	for (k=0;k<NM;k++){
    	fscanf(fc,"%f\n",&COMM[j][k]);
		avg_comm += COMM[j][k];
	}
	avg_comm = avg_comm/NM;
} 

close(fc);

#if DEBUG
printf("COMMUNICATIONS matrix\n");
for (j=0;j<NM;j++){
    for (k=0;k<NM;k++){
		printf("%.2f ",COMM[j][k]);
	}
	printf("\n");
}
#endif

#endif 

// Check: communication costs ? (maybe read from CN file)
avg_comm = 0.0;

// Read CN values
if((fc = fopen(CN_file, "r"))==NULL){
    fprintf(stderr,"Can't read CN file: %s\n",CN_file);
    exit(1);
}

// Read solution values
if((fs = fopen(sol_file, "r")) == NULL){
    fprintf(stderr,"Can't read solution file: %s\n",sol_file);
    exit(1);
}

int max_cores = 0;

for (r=0;r<NC;r++){
	CN[r].CN_id = r;
	// parameters 4 and 5: energy consumption
	fscanf(fc,"%d %d %f %f %f\n",&(CN[r].NM),&(CN[r].cores),&(CN[r].speed),&(CN[r].EIdle),&(CN[r].EMax));

	if (CN[r].cores > max_cores){
		max_cores = CN[r].cores;
	}

	CN[r].comm_cost = 0.0;

    fgets(line,LINE_LENGTH,fs);
    token = strtok(line," ");
    CN[r].est_ET = atof(token);
	token = strtok(NULL," ");
    CN[r].jobs = atoi(token);

    MA[r] = (int *) malloc(sizeof(int)*CN[r].jobs);
    if (MA[r] == NULL){
        fprintf(stderr,"Error in malloc, row %d in MA (%d jobs)\n",r,CN[r].jobs);
        exit(2);
    }

	for(j=0;j<CN[r].jobs;j++){
        token = strtok(NULL," ");
		MA[r][j] = atoi(token);
	}

	CN[r].sum_penalizations = 0.0;

#if DEBUG_0
printf("**CN%d: %d machines %dC at %f, comm: %f, energy: (%.2f,%.2f), %d jobs, estimated: %.2f\n",CN[r].CN_id,CN[r].NM,CN[r].cores,CN[r].speed,CN[r].comm_cost,CN[r].EIdle,CN[r].EMax,CN[r].jobs,CN[r].est_ET);
#endif
}

fclose(fc);
fclose(fs);

// Read JOB values
if((fj = fopen(JOB_file, "r")) == NULL){
    fprintf(stderr,"Can't read job instance file: %s\n",JOB_file);
    exit(1);
}

ListT l = NULL;
ListT l2 = NULL;

ListH lhs = NULL;

for (i=0;i<NJ;i++){
	JOBS[i].job_id = i;
    fgets(line,LINE_LENGTH,fj);
	token = strtok(line," ");
	JOBS[i].NT = atoi(token);

	token = strtok(NULL," ");
	// deadline 
	JOBS[i].deadline = atof(token);
	token = strtok(NULL," ");
	// cost function for penalization
	JOBS[i].pen_type = atoi(token);
	JOBS[i].penalization = 0.0;
	JOBS[i].tasks = NULL;
	JOBS[i].ur_list = NULL;
	createlist(&(JOBS[i].tasks),JOBS[i].NT);
	l = JOBS[i].tasks;
	while(token != NULL){
		token = strtok(NULL," ");
		l->ex_time = atof(token);
		token = strtok(NULL," ");
		l->cores = atoi(token);

if (l->cores > max_cores){
#if DEBUG_0
	printf("\ntask %d (job %d) requires %d cores (max cores is %d)\n",l->task_id,i,l->cores,max_cores);	
l->cores = max_cores;
#endif
}

		token = strtok(NULL," ");
		while((token != NULL) && (atoi(token) != -1)){
			insertlist(&(l->DEP),atoi(token));
			l2 = JOBS[i].tasks;
			while(l2->task_id != atoi(token)){
				l2 = l2->next;
			}
			insertlist(&(l2->SUCC),l->task_id);
			token = strtok(NULL," ");
		}
		l = l->next;
	}

#if DEBUG_3
printf("\nJ%d (deadline: %.2f, pen. type: %d) ",JOBS[i].job_id,JOBS[i].deadline,JOBS[i].pen_type);
l = JOBS[i].tasks;
printlistT(l);
#endif

}

close(fj);

float makespan = 0.0;
int heavy;

for (r=0;r<NC;r++){
	// For each CN

	// Machine array, stores the MET.
	float *mach = (float *) malloc(sizeof(float)*CN[r].NM);
	if (mach == NULL){
		fprintf(stderr,"Error in malloc (machine array), dimension %d\n",CN[r].NM);
		exit(2);
	}

	for(j=0;j<CN[r].NM;j++){
		mach[j] = 0.0;
	}

	// Holes list (of lists)
	NodeH **HL = (NodeH **) malloc(sizeof(NodeH *)*CN[r].NM);
	if (HL == NULL){
	    fprintf(stderr,"Error in malloc for HL, dimensions %d\n",CN[r].NM);
	    exit(2);
	}

	for(j=0;j<CN[r].NM;j++){
		HL[j] = NULL;
	}

	// HEFT for each job (DAG) assigned to execute in CN[r]

	// Jobs (DAGS) are processed sequentially in the order specified by the solution
	for (k=0;k<CN[r].jobs;k++){

		int job_proc = MA[r][k];
		// Upward rank array
#if DEBUG
printf("Job %d, %d tasks\n",job_proc,JOBS[job_proc].NT);
#endif

		float *UR = (float *) malloc(sizeof(float)*JOBS[job_proc].NT);
		if (UR == NULL){
			fprintf(stderr,"Error in malloc (upward rank array), dimension %d\n",JOBS[job_proc].NT);
			exit(2);
		}
		
		int best_machine, assign_hole;
		ListT p_best_task;
		float min_ft, min_score;

		float max_ur, max_comm, max_ft_pred ;
		ListT_HEFT lh,lh2 = NULL;
		ListT lt,lt2 = NULL;

		// Calculate upward rank
		lt = JOBS[job_proc].tasks;
		while (lt!=NULL) {
			// Loop thru tasks
			max_ur = 0.0;
			lh = lt->SUCC;
			while (lh != NULL){
				if ((avg_comm + UR[lh->task_id]) > max_ur){
					max_ur = avg_comm + UR[lh->task_id];
				}
				lh = lh->next;
			}
			// definition: UR[task i] = max_ur(SUCC) + avg_time[task i];
			UR[lt->task_id] = max_ur + lt->ex_time/CN[r].speed;
			insordlist(&(JOBS[job_proc].ur_list),lt->task_id,UR[lt->task_id]);
			lt = lt->next;
		}
		
#if DEBUG
printranks(JOBS[job_proc].ur_list);
#endif
	
		// Scedule tasks : loop thru list lh
		lh = JOBS[job_proc].ur_list;
		while (lh != NULL){
			// Select ready task with maximun upward rank value
			best_machine = -1;
			lh2 = lh;
	
			// Search for ready task
			while(!ready(JOBS[job_proc],lh2->task_id)){
				lh2 = lh2->next;
			}

			// Ready task with highest UR is lh2->task_id
			p_best_task = JOBS[job_proc].tasks;
			while (p_best_task->task_id != lh2->task_id) {
				p_best_task = p_best_task->next;
			}

			min_ft = INFT;
			min_score = INFT;

			// Compute earliest starting time
			max_comm = 0.0;
			max_ft_pred = 0.0;
			lh2 = p_best_task->DEP;
	
			// Loop for dependencies of task p_best_task->task_id (list lh2)
	    	while (lh2 != NULL){
				lt2 = JOBS[job_proc].tasks;
				while (lt2->task_id != lh2->task_id) {
	        	    lt2 = lt2->next;
	        	}
	
				if (lt2->ST + lt2->ex_time/CN[r].speed > max_ft_pred){
					max_ft_pred = lt2->ST + lt2->ex_time/CN[r].speed;
				}
	
				lh2 = lh2->next;
			}	
			// max_ft_pred is the maximum finish time of predecessors
			// take care if communication is required !!

			// Search for hole to execute task p_best_task->task_id 
			best_machine = -1;
			NodeH *best_hole = NULL;
			
			// Greedy: search fittest hole with respect to finish time
			// Within each machine, holes are sorted by finishing time
			float best_ET_hole = INFT+1;
			NodeH *best_hole_local = NULL;

			for (j=0;j<CN[r].NM;j++){
				lhs = HL[j];
				best_hole_local = NULL;
				while ( (best_hole_local == NULL) && (lhs != NULL)) {
					if ((lhs->cores >= p_best_task->cores) && (lhs->end - lhs->start) >= p_best_task->ex_time/CN[r].speed) {
						if ( ( max_ft_pred + p_best_task->ex_time/CN[r].speed < lhs->end) || ( (lhs->end == mach[j]) && (lhs->end > max_ft_pred) ) ) {
#if DEBUG_2
printf("Hole found for T%d (MFT:%.2f,ET:%.2f): [%.2f,%.2f]i, M%d.mak = %.2f\n",p_best_task->task_id,max_ft_pred, max_ft_pred + p_best_task->ex_time/CN[r].speed,lhs->start,lhs->end,j,mach[j]);
#endif
							best_hole_local = lhs;
							// stop if hole found: holes are sorted by finishing time
						}
					}
					lhs = lhs->next;
				}
				if ( ( best_hole_local != NULL ) && ( best_hole_local->end < best_ET_hole ) ) {
						best_ET_hole = best_hole_local->end;
						best_machine = j;
						best_hole = best_hole_local;
						assign_hole = 1;
				}
			} // end of hole search

			// Search for machine with minimum finish time for task p_best_task->task_id
			float st_machine;
			min_ft = best_ET_hole;

			for (j=0;j<CN[r].NM;j++){
#if DEBUG_2
printf("M%d (mak:%.2f), FT: %.2f, min_ft: %.2f\n",j,mach[j],mach[j]+p_best_task->ex_time/CN[r].speed,min_ft);
#endif
				if (mach[j] > max_ft_pred){
					st_machine = mach[j];
				} else {
					st_machine = max_ft_pred;
				}
	
				if (st_machine+p_best_task->ex_time/CN[r].speed < min_ft){
					min_ft = mach[j]+p_best_task->ex_time/CN[r].speed;
					min_score = max_ft_pred - mach[j];
					best_machine = j;
					assign_hole = 0;
#if DEBUG_2
printf("Updated: best_machine: %d, min_ft: %.2f, min_score: %.2f\n",best_machine,min_ft,min_score);
#endif
				} else {
					if (st_machine+p_best_task->ex_time/CN[r].speed == min_ft){
					// Decide ties by score = max_ft_pred - mach[j]
#if DEBUG_3
printf("Tie: M%d con M%d, score: %.2f, min_score: %.2f\n",j,best_machine,max_ft_pred-mach[j],min_score);
#endif
						if (max_ft_pred - mach[j] < min_score){
							min_score = max_ft_pred - mach[j];
							best_machine = j;
#if DEBUG_3
printf("Tie: best machine: M%d, min_score: %.2f\n",best_machine,min_score);
#endif
						}
					}
				}
			}
			
			if (assign_hole) { // asssign task to hole (best_hole != NULL) 
				// Hole available

				// Communications !
				// Search predecessors, if any one executes in a different machine than the hole, add communication cost
				// TO DO !!

				if ( best_hole->start >= max_ft_pred + CN[r].comm_cost) {
					p_best_task->ST = best_hole->start;
				} else {
					p_best_task->ST = max_ft_pred + CN[r].comm_cost;
				}

#if DEBUG
printf("******* Assign task %d (requires %dC, MPT:%.2f,ST:%.2f,ET:%.2f,FT:%.2f) to hole [%f-%f] in machine %d\n",p_best_task->task_id,p_best_task->cores,max_ft_pred,p_best_task->ST,p_best_task->ex_time/CN[r].speed,p_best_task->ST+p_best_task->ex_time/CN[r].speed,best_hole->start,best_hole->end,best_machine);
#endif
				float st_hole = best_hole->start;
				float et_hole = best_hole->end;
				int cores_hole = best_hole->cores;

				// Two new holes may appear
				if (p_best_task->ST > best_hole->start) {
					// Hole before starting the task
#if DEBUG_2
printf("Insert hole before starting the task\n");
#endif
					inserthole(&(HL[best_machine]),best_hole->start,p_best_task->ST,p_best_task->cores);
				}
				if (p_best_task->ST + p_best_task->ex_time/CN[r].speed < best_hole->end) {
					// Hole after finishing the task
#if DEBUG_2
printf("Insert hole after finishing the task\n");
#endif
					inserthole(&(HL[best_machine]),p_best_task->ST+p_best_task->ex_time/CN[r].speed,best_hole->end,p_best_task->cores);
				}
				
				if (p_best_task->ST+p_best_task->ex_time/CN[r].speed > mach[best_machine]) {
					// Task inserted in hole "at the end".
#if DEBUG_2
printf("Insert hole: task in the end\n");
#endif
					inserthole(&(HL[best_machine]),mach[best_machine],p_best_task->ST+p_best_task->ex_time/CN[r].speed,CN[r].cores-p_best_task->cores);
#if DEBUG_2
printf("Task inserted in hole at the end: modify mach[%d] %.2f to %.2f\n",best_machine,mach[best_machine],p_best_task->ST+p_best_task->ex_time/CN[r].speed); 
#endif
					mach[best_machine] = p_best_task->ST+p_best_task->ex_time/CN[r].speed;
					et_hole = mach[best_machine];
				}

				// Modify hole
				if (p_best_task->cores < best_hole->cores){
					// Task requieres less cores than available: a new hole appears !
					// Simply done by modifying the number of cores available in the previous hole.
					// The finishing time of the hole remains the same
#if DEBUG_2
printf("Modify hole: %dC to %dC\n",best_hole->cores,best_hole->cores-p_best_task->cores);
#endif
					best_hole->cores -= p_best_task->cores;
					if (p_best_task->ST+p_best_task->ex_time/CN[r].speed > mach[best_machine]) {
#if DEBUG_2
printf("Task inserted in hole at the end: modify hole end to %.2f\n",p_best_task->ST+p_best_task->ex_time/CN[r].speed); 
#endif
						best_hole->end = mach[best_machine];
					}
				} else {
					// Task requires the number of cores available: the hole dissappears !
					//if (p_best_task->ST+p_best_task->ex_time/CN[r].speed != mach[best_machine]) {
						deletehole(&(HL[best_machine]),st_hole,et_hole,cores_hole);
					//}
				}
				
			} else { // Assign task to machine
				// Communications !
				// Search predecessors, if any one executes in a different machine than the best_machine, add communication cost
				// TO DO !!

				p_best_task->ST = max_ft_pred + CN[r].comm_cost;

#if DEBUG
printf("****** Assign task %d (requires %dC, MPT:%.2f,ST:%.2f,ET:%.2f,FT:%.2f) to machine %d (mak:%.2f)\n",p_best_task->task_id,p_best_task->cores,max_ft_pred,p_best_task->ST,p_best_task->ex_time/CN[r].speed,p_best_task->ST+p_best_task->ex_time/CN[r].speed,best_machine,mach[best_machine]);
#endif
	
				if ( mach[best_machine] >= max_ft_pred + CN[r].comm_cost ) {
					p_best_task->ST = mach[best_machine];
					if (p_best_task->cores < CN[r].cores) {
						// New hole: available cores
						inserthole(&(HL[best_machine]),mach[best_machine],p_best_task->ST+p_best_task->ex_time/CN[r].speed,CN[r].cores-p_best_task->cores);
						//inserthole(&(HL[best_machine]),mach[best_machine],INFT,CN[r].cores-p_best_task->cores);
					}
				} else {
					p_best_task->ST = max_ft_pred + CN[r].comm_cost;
                    if (p_best_task->cores < CN[r].cores) {
						// New hole: available cores
						inserthole(&(HL[best_machine]),mach[best_machine],p_best_task->ST+p_best_task->ex_time/CN[r].speed,CN[r].cores-p_best_task->cores);
						//inserthole(&(HL[best_machine]),mach[best_machine],INFT,CN[r].cores-p_best_task->cores);
					}
#if DEBUG
printf("Hole before starting task\n");
#endif
					// Hole before starting task
					inserthole(&(HL[best_machine]),mach[best_machine],p_best_task->ST,p_best_task->cores);
				}

				#if 0
				// NEW: Hole after insertion
				if (p_best_task->cores < CN[r].cores) {
					inserthole(&(HL[best_machine]),p_best_task->ST+p_best_task->ex_time/CN[r].speed,INFT,CN[r].cores-p_best_task->cores);
				} 
				#endif 
#if DEBUG
printf("J%dT%d - Task assigned: ST:%.2f, FT:%.2f, modify mach[%d] to %.2f\n",job_proc,p_best_task->task_id,p_best_task->ST,p_best_task->ST+p_best_task->ex_time/CN[r].speed,best_machine,p_best_task->ST+p_best_task->ex_time/CN[r].speed);
#endif

				mach[best_machine] = p_best_task->ST+p_best_task->ex_time/CN[r].speed;
			}

			p_best_task->machine = best_machine;
	
			delete(&(JOBS[job_proc].ur_list),p_best_task->task_id);
			lh = JOBS[job_proc].ur_list;

#if DEBUG_2
printf("Machine holes:\n");
for (j=0;j<CN[r].NM;j++){
    printf("%d: ",j);
    printholes(HL[j]);
}
#endif

			if (lh == NULL) {
				// Last task: evaluate deadline
				if (p_best_task->ST+p_best_task->ex_time/CN[r].speed > JOBS[job_proc].deadline) {
					JOBS[job_proc].penalization = ComputePenalizations(p_best_task->ST+p_best_task->ex_time/CN[r].speed-JOBS[job_proc].deadline,JOBS[job_proc].pen_type);
					CN[r].sum_penalizations += JOBS[job_proc].penalization;
#if DEBUG
printf("CN %d. J%d, penalization: %.2f. Sum penalization: %.2f\n",r,job_proc,JOBS[job_proc].penalization,CN[r].sum_penalizations);
#endif
				}
			}		

			// next task in ur_list
		} 

		// next JOB

		//UR = NULL;
		free(UR);

		makespan = 0.0;
		heavy = -1;
		float mak_local = 0.0;
		
		for (j=0;j<CN[r].NM;j++){
			mak_local = mach[j];
		    if (mak_local > makespan){
		        makespan = mak_local;
				heavy = j;
		    }
		}
	
#if DEBUG
printf("Machine loads:\n");
for (j=0;j<CN[r].NM;j++){
	printf("M%d: %.2f\n",j,mach[j]);
}

printf("Machine holes:\n");
for (j=0;j<CN[r].NM;j++){
    printf("%d: ",j);
    printholes(HL[j]);
}
#endif

#if DEBUG
fprintf(stdout,"Makespan: %f, heavy: %d\n",makespan,heavy);
#endif

	
#if DEBUG
printf("Solution (task, machine, starting-finish times)\n");
lt = JOBS[job_proc].tasks;
while (lt != NULL){
    printf("Assigned: J%dT%d: machine %d (%.2f - %.2f)\n",job_proc,lt->task_id,lt->machine,lt->ST,lt->ST+lt->ex_time/CN[r].speed);
	lt = lt->next;
}
printf("**********\n");
#endif
	} 

    double wasted = 0.0;
    double sum_time = 0.0;
    ListH lnw;
    double cont_core = 0.0;
    double energy_consumption = 0.0;
    cont_core = (CN[r].EMax - CN[r].EIdle)/CN[r].cores;

    for (j=0;j<CN[r].NM;j++){
        energy_consumption += CN[r].EMax * mach[j];
        lnw = HL[j];
        while (lnw != NULL){
            wasted = wasted + (lnw->end - lnw->start) * lnw->cores;
            energy_consumption -= (lnw->cores * cont_core);
            lnw = lnw->next;
        }
        sum_time += mach[j] * CN[r].cores;
        #if DEBUG_0
        fprintf(stdout,"CN mach[%d]: %f\n",j,mach[j]);
        #endif
    }

    #if DEBUG_0
    fprintf(stdout,"CN %d, Makespan: %f, heavy: %d, wasted(holes): %.2f, use (absolute): %.2f, effective use: %.2f% (estimated: %.2f), penalization: %.2f, energy_consumption (W/h): %.4f\n",r,makespan,heavy,wasted,sum_time-wasted,100*(sum_time-wasted)/(makespan*CN[r].cores*CN[r].NM),CN[r].est_ET,CN[r].sum_penalizations,energy_consumption/3600.0);
    #else
    //fprintf(stdout,"%.4f %.4f %.4f\n",makespan/3600.0,energy_consumption/3600.0,CN[r].sum_penalizations);
    fprintf(stdout,"%.4f %.4f %.4f %.2f\n",makespan/3600.0,energy_consumption/3600.0,CN[r].sum_penalizations,100*(sum_time-wasted)/(makespan*CN[r].cores*CN[r].NM));
    #endif
// next CN
}

exit(0);
}

