#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  sol_converter.py
#  
#  This program converts MOEA-style solutions to a second-level 
#  scheduler style of solution
#
#  Copyright 2014 Santiago Iturriaga <santiago@marvin>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys

def main():
	if len(sys.argv) != 2:
		print("Error. Uso: {0} <moea sol file>".format(sys.argv[0]))
		sys.exit(1);
		
	with open(sys.argv[1]) as f:
		for line in f:
			moea_sol = line.split(' ')
			
			curr_output_line = ""
			curr_count = 0
			
			for index_raw in moea_sol:
				if len(index_raw.strip()) > 0:
					index = int(index_raw.strip())
				
					if index < 1000:
						curr_count = curr_count + 1
						curr_output_line = curr_output_line + " " + str(index)
					else:
						print("0.0 {0}{1}".format(curr_count, curr_output_line))
						curr_count = 0
						curr_output_line = ""
						
			print("0.0 {0}{1}".format(curr_count, curr_output_line))
		
	return 0

if __name__ == '__main__':
	main()

