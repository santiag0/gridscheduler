/**
 * DifferentialEvolutionCrossover.java
 * Class representing the crossover operator used in differential evolution
 * @author Antonio J. Nebro
 * @version 1.0
 */
package jmetal.base.operator.crossover.de;

import java.util.Properties;

import jmetal.base.operator.crossover.Crossover;
import jmetal.base.solutionType.RealSolutionType;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.SolutionType;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

public class DE_PBX_alpha_SA extends Crossover {
  /**
   * DEFAULT_CR defines a default CR (crossover operation control) value
   */
  public static final double DEFAULT_CR = 0.1; 
  
  /**
   * DEFAULT_F defines the default F (Scaling factor for mutation) value
   */
  private static final double DEFAULT_F = 0.5;
  
  /**
   * REAL_SOLUTION represents class jmetal.base.solutionType.RealSolutionType
   */
  private static Class REAL_SOLUTION ; 
  
  public double CR_ = DEFAULT_CR ;
  public double F_  = DEFAULT_F ;
  protected static double nextNextGaussian = 0.0;
  protected static boolean hasNextNextGaussian = false;
  protected double ro = 2.0;
  protected double alpha = 0.8;
  
  /**
   * Constructor
   */
  public DE_PBX_alpha_SA() {
    try {
    	REAL_SOLUTION = Class.forName("jmetal.base.solutionType.RealSolutionType") ;
    } catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
  } // Constructor
  

  private double nextGaussian(double center, double deviation) 
  {
  	if (hasNextNextGaussian) {
  		hasNextNextGaussian = false;
  	    return center + nextNextGaussian * deviation;
  	} else {
  	    double v1, v2, s;
  	    do { 
  	            v1 = 2.0 * PseudoRandom.randDouble() - 1.0;   // between -1.0 and 1.0
  	            v2 = 2.0 * PseudoRandom.randDouble() - 1.0;   // between -1.0 and 1.0
  	            s = v1 * v1 + v2 * v2;
  	    } while (s >= 1.0 || s == 0.0);
  	    double multiplier = Math.sqrt(-2 * Math.log(s)/s);
  	    nextNextGaussian = v2 * multiplier;
  	    hasNextNextGaussian = true;
  	    return center + deviation * v1 * multiplier;
  	}
  }

  /**
   * Executes the operation
   * @param object An object containing an array of three parents
   * @return An object containing the offSprings
   */
   public Object execute(Object object) throws JMException {
	     
		 Object [] parameters = (Object [])object;
		 Solution current = (Solution)parameters[0];
	     int index = (Integer)parameters[1];
	     
	     Solution child ;
	     
	     Double CR = new Double(current.getCR());
//	     Double CR = current.getDistanceToSolutionSet();
	     if (CR != null) {
	       CR_ = CR ;
	     } // if
	     //Double F = (Double)getParameter("F");
//	     Double F = current.getKDistance();
	     Double F = new Double(current.getF());
	     if (F != null) {
	       F_ = F ;
	     } // if
	     
	     alpha = current.getAlpha();

	     
	     SolutionSet set = (SolutionSet)getParameter("neighbors");
	     
	     
	     int r1, r2, r3 ;
	     do {
	         r1 = (int)(PseudoRandom.randInt(0,set.size()-1));
	       } while( r1==index );
	       do {
	         r2 = (int)(PseudoRandom.randInt(0,set.size()-1));
	       } while( r2==index || r2==r1);
	       do {
		         r3 = (int)(PseudoRandom.randInt(0,set.size()-1));
		       } while( r3==index || r3==r1 || r2==r3 || r2==r1);
	       

	     
	     int jrand ;

	     Solution p1,p2,p3;
	     p1 = set.get(r1);
	     p2 = set.get(r2);
	     p3 = set.get(r3);

     

     int numberOfVariables = current.getDecisionVariables().length ;
     jrand = (int)(PseudoRandom.randInt(0, numberOfVariables - 1)) ;
     
     child = new Solution(current) ;
     for (int j=0; j < numberOfVariables; j++) {
        if (PseudoRandom.randDouble(0, 1) < CR_ || j == jrand) {
          double value ;
          
          double I = p2.getDecisionVariables()[j].getValue() - p3.getDecisionVariables()[j].getValue();
          double u=0.0, l = 0.0;
          
          l = Math.max(((Double)current.getDecisionVariables()[j].getLowerBound()), p1.getDecisionVariables()[j].getValue()-I*alpha);
          u = Math.min(((Double)current.getDecisionVariables()[j].getUpperBound()), p1.getDecisionVariables()[j].getValue()+I*alpha);

          value = nextGaussian(p1.getDecisionVariables()[j].getValue(), F_*(u-l));
          
          
//          value = current.getDecisionVariables()[j].getValue()  +
//                  F_ * (p1.getDecisionVariables()[j].getValue() -
//                       p2.getDecisionVariables()[j].getValue()) ;
          
          if (value < child.getDecisionVariables()[j].getLowerBound()) {
//            value =  child.getDecisionVariables()[j].getLowerBound() ;
            value = 2.0 * child.getDecisionVariables()[j].getLowerBound() - value;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
          if (value > child.getDecisionVariables()[j].getUpperBound()) {
//            value = child.getDecisionVariables()[j].getUpperBound() ;
        	  value = 2.0 * child.getDecisionVariables()[j].getUpperBound() - value;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
            
          child.getDecisionVariables()[j].setValue(value) ;
        }
        else {
          double value ;
          value = current.getDecisionVariables()[j].getValue();
          child.getDecisionVariables()[j].setValue(value) ;
        } // else
     }
     
     if (PseudoRandom.randDouble() <= 0.1)
    	 F_ = PseudoRandom.randDouble(0.1, 1.0);
     
     if (PseudoRandom.randDouble() <= 0.1)
    	 CR_ = PseudoRandom.randDouble();
     
     if(PseudoRandom.randDouble() <= 0.1)
  	   alpha = 0.2 + PseudoRandom.randDouble() * 0.6;   
     
     
//     child.setKDistance(F_);
//     child.setDistanceToSolutionSet(CR_);
     current.setCR(CR);
     current.setF(F);
     child.setAlpha(alpha);
     
     return child ;
   }
}
