/**
 * DifferentialEvolutionCrossover.java
 * Class representing the crossover operator used in differential evolution
 * @author Antonio J. Nebro
 * @version 1.0
 */
package jmetal.base.operator.crossover.de;

import java.util.Properties;
import jmetal.base.operator.crossover.Crossover;
import jmetal.base.solutionType.RealSolutionType;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.SolutionType;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

public class DE_current_1_binCambioOutbound extends Crossover {
  /**
   * DEFAULT_CR defines a default CR (crossover operation control) value
   */
  public static final double DEFAULT_CR = 0.1; 
  
  /**
   * DEFAULT_F defines the default F (Scaling factor for mutation) value
   */
  private static final double DEFAULT_F = 0.5;
  
  /**
   * REAL_SOLUTION represents class jmetal.base.solutionType.RealSolutionType
   */
  private static Class REAL_SOLUTION ; 
  
  public double CR_ = DEFAULT_CR ;
  public double F_  = DEFAULT_F ;
  
  /**
   * Constructor
   */
  public DE_current_1_binCambioOutbound() {
    try {
    	REAL_SOLUTION = Class.forName("jmetal.base.solutionType.RealSolutionType") ;
    } catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
  } // Constructor
  



  /**
   * Executes the operation
   * @param object An object containing an array of three parents
   * @return An object containing the offSprings
   */
   public Object execute(Object object) throws JMException {
	     
		 Object [] parameters = (Object [])object;
		 Solution current = (Solution)parameters[0];
	     int index = (Integer)parameters[1];
	     
	     Solution child ;
	     
	     Double CR = (Double)getParameter("CR");
	     if (CR != null) {
	       CR_ = CR ;
	     } // if
	     Double F = (Double)getParameter("F");
	     if (F != null) {
	       F_ = F ;
	     } // if
	     SolutionSet set = (SolutionSet)getParameter("neighbors");
	     
	     
	     int r1, r2, r3 ;
	     do {
	         r1 = (int)(PseudoRandom.randInt(0,set.size()-1));
	       } while( r1==index );
	       do {
	         r2 = (int)(PseudoRandom.randInt(0,set.size()-1));
	       } while( r2==index || r2==r1);
	       

	     
	     int jrand ;

	     Solution p1,p2,p3;
	     p1 = set.get(r1);
	     p2 = set.get(r2);

     

     int numberOfVariables = current.getDecisionVariables().length ;
     jrand = (int)(PseudoRandom.randInt(0, numberOfVariables - 1)) ;
     
     child = new Solution(current) ;
     for (int j=0; j < numberOfVariables; j++) {
        if (PseudoRandom.randDouble(0, 1) < CR_ || j == jrand) {
          double value ;
          value = current.getDecisionVariables()[j].getValue()  +
                  F_ * (p1.getDecisionVariables()[j].getValue() -
                       p2.getDecisionVariables()[j].getValue()) ;
          
          if (value < child.getDecisionVariables()[j].getLowerBound()) {
//            value =  child.getDecisionVariables()[j].getLowerBound() ;
            value = 2.0 * child.getDecisionVariables()[j].getLowerBound() - value;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
          if (value > child.getDecisionVariables()[j].getUpperBound()) {
//            value = child.getDecisionVariables()[j].getUpperBound() ;
        	  value = 2.0 * child.getDecisionVariables()[j].getUpperBound() - value;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
            
          child.getDecisionVariables()[j].setValue(value) ;
        }
        else {
          double value ;
          value = current.getDecisionVariables()[j].getValue();
          child.getDecisionVariables()[j].setValue(value) ;
        } // else
     }
     
     return child ;
   }
}
