/**
 * DifferentialEvolutionCrossover.java
 * Class representing the crossover operator used in differential evolution
 * @author Antonio J. Nebro
 * @version 1.0
 */
package jmetal.base.operator.crossover.de;

import java.util.Properties;
import jmetal.base.operator.crossover.Crossover;
import jmetal.base.solutionType.RealSolutionType;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.SolutionType;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import jmetal.util.Ranking;

public class DEGLX extends Crossover {
  /**
   * DEFAULT_CR defines a default CR (crossover operation control) value
   */
  public static final double DEFAULT_CR = 0.1; 
  
  /**
   * DEFAULT_F defines the default F (Scaling factor for mutation) value
   */
  private static final double DEFAULT_F = 0.5;
  
  /**
   * REAL_SOLUTION represents class jmetal.base.solutionType.RealSolutionType
   */
  private static Class REAL_SOLUTION ; 
  
  public double CR_ = DEFAULT_CR ;
  public double F_  = DEFAULT_F ;
  
  /**
   * Constructor
   */
  public DEGLX() {
    try {
    	REAL_SOLUTION = Class.forName("jmetal.base.solutionType.RealSolutionType") ;
    } catch (ClassNotFoundException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
  } // Constructor
  



  /**
   * Executes the operation
   * @param object An object containing an array of three parents
   * @return An object containing the offSprings
   */
   public Object execute(Object object) throws JMException {
     
	 Object [] parameters = (Object [])object;
	 Solution current = (Solution)parameters[0];
     int index = (Integer)parameters[1];
   

     
     Double CR = (Double)getParameter("CR");
     if (CR != null) {
       CR_ = CR ;
     } // if
     Double F = (Double)getParameter("F");
     if (F != null) {
       F_ = F ;
     } // if
     SolutionSet set       = (SolutionSet)getParameter("population");
     SolutionSet neighbors = (SolutionSet)getParameter("neighbors");
     SolutionSet archive   = (SolutionSet)getParameter("archive");
     
     
     Solution p, q , p1, p2, lbest, gbest;
     
     // Selecting the best of the neighborhood
     Ranking r = new Ranking(neighbors);
     r.getSubfront(0).sort(new jmetal.base.operator.comparator.CrowdingComparator());
     lbest = r.getSubfront(0).get(0);
     
               
     int r1, r2, r3, r4, r5 ;
     do {
         r1 = (int)(PseudoRandom.randInt(0,neighbors.size()-1));
       } while( r1==index || neighbors.get(r1).getLocation() == lbest.getLocation());
	 do {
	     r2 = (int)(PseudoRandom.randInt(0,neighbors.size()-1));
	 } while( r2==index || r2==r1 || neighbors.get(r1).getLocation() == lbest.getLocation());
     p = neighbors.get(r1);
     q = neighbors.get(r2);
       
     
     gbest = archive.get(PseudoRandom.randInt(0,archive.size()-1));
       
     do {
       r3 = (int)(PseudoRandom.randInt(0,set.size()-1));
     } while (r3==index || set.get(r3).getLocation() == p.getLocation() || set.get(r3).getLocation() == q.getLocation() );
       
     do {
       r4 = (int)PseudoRandom.randInt(0,set.size()-1);
     } while (r4 == r3 || r4 == index || set.get(r3).getLocation() == p.getLocation() || set.get(r4).getLocation() == q.getLocation());
       
     
     
     p1 = set.get(r3);
     p2 = set.get(r4);
     
         
     int jrand ;

     int numberOfVariables = current.getDecisionVariables().length ;
     jrand = (int)(PseudoRandom.randInt(0, numberOfVariables - 1)) ;
     
     Solution child = new Solution(current) ;
     for (int j=0; j < numberOfVariables; j++) {
        if (PseudoRandom.randDouble(0, 1) < CR_ || j == jrand) {
        	
        	    
 		   double aux1 = F_ * (lbest.getDecisionVariables()[j].getValue() - current.getDecisionVariables()[j].getValue());
 		   double aux2 = F_ * (p.getDecisionVariables()[j].getValue() - q.getDecisionVariables()[j].getValue());
 		   double L = current.getDecisionVariables()[j].getValue()+aux1+aux2;
 		   
 		   aux1 = F_ * (gbest.getDecisionVariables()[j].getValue() - current.getDecisionVariables()[j].getValue());
 		   aux2 = F_ * (p1.getDecisionVariables()[j].getValue() - p2.getDecisionVariables()[j].getValue());
 		   double G = current.getDecisionVariables()[j].getValue() + aux1 + aux2;
 		   double value = CR_ * G + (1 - CR_) * L;
 		   
          
          if (value < child.getDecisionVariables()[j].getLowerBound()) {
            value =  child.getDecisionVariables()[j].getLowerBound() ;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
          if (value > child.getDecisionVariables()[j].getUpperBound()) {
            value = child.getDecisionVariables()[j].getUpperBound() ;
            //value = PseudoRandom.randDouble( child.getDecisionVariables()[j].getLowerBound(), child.getDecisionVariables()[j].getUpperBound());
          }
            
          child.getDecisionVariables()[j].setValue(value) ;
        }
        else {
          double value ;
          value = current.getDecisionVariables()[j].getValue();
          child.getDecisionVariables()[j].setValue(value) ;
        } // else
     }
     
     return child ;
   }
}
