package jmetal.base.operator.localSearch;


import java.util.Random;

import jmetal.base.Variable;
import jmetal.base.variable.Int;
import jmetal.base.variable.Permutation;
import jmetal.base.operator.localSearch.LocalSearch;
import jmetal.base.*;
import jmetal.problems.jobsScheduling.JobsScheduling;
import jmetal.util.Matrix;
import jmetal.util.PseudoRandom;
import jmetal.base.solutionType.*;

import jmetal.util.JMException;


/**
 * This class implements a local search operator that repairs solutions by removing those 
 * jobs that cannot be executed in the assigned CN (because they require more processors 
 * than the number of processors in the CN) and adding them to other CN with enough resources
 * 
 * @author Bernab� Dorronsoro
 * @version 1.0
 */
public class RepairSolutionLocalSearch extends LocalSearch {
	
	private JobsScheduling problem_ = null;

	/**
	 * Constructor
	 * Creates a new local search object.
	 */
	public RepairSolutionLocalSearch(){

		super();
	} // LMCTSLocalSearch


	/**
	 * Executes the local search. 
	 * @param object Object representing a package with a solution and an islandId
	 * @return An object containing the new improved solution
	 * @throws JMException 
	 */
	public Object execute( Object object ) throws JMException {
//		System.out.println("entro no cc!");

		// Recover the parameters (solution and island)
		Solution solution = (Solution) object;

		Permutation vars = (Permutation) solution.getDecisionVariables()[0];
		
		if ( problem_ == null ) problem_ = (JobsScheduling) getParameter( "Problem" );
		
		int split = problem_.getNumberJobs();
		int len = vars.getLength();
		int CN = 0;
		
		int capacityCN = problem_.numberProcs(CN);
		
		// Do the solution repair
		for (int i=0; i<len; i++) {
			
			if (vars.vector_[i] >= split) {
				CN++;
				capacityCN = problem_.numberProcs(CN);
			}
			else {
				int reqProcs = problem_.requiredProcs(vars.vector_[i]);
				if ( reqProcs > capacityCN) {
					// Mover este job a otro CN con capacidad suficiente
					int numberOfCandidates = numberOfCNsWithCapacity(reqProcs, problem_);
					
					// Move the solution to one of the CNs with capacity, chosen randomly
					// TODO: We could choose the one with lower CT instead 
					int candidate = PseudoRandom.randInt(0, numberOfCandidates);
					moveJobToCN(vars.vector_, i, candidate, split);
				}
			}
			
		}
		
//		 return new Solution(solution);
		return solution;
	} // execute 

	
	/**
	 * Moves job to the selected CN. The job will be located at the beginning of the 
	 * CN, since big jobs are normally better to be run first...
	 * @param vector
	 * @param i
	 * @param candidate
	 */
	private void moveJobToCN(int[] vector, int i, int candidate, int split) {
		int startCN = 0;
		int CN = 0;
		int len = vector.length;
		
		while ((CN < candidate) && (startCN<len)) {
			if (vector[startCN] >= split)
				CN++;
			startCN++;
		}
		
		if (startCN>1)
			startCN--;
		
		// StartCN is pointing to the position where the splitter for the candidate CN is in vector
		// move the job in i to position startCN, moving all the numbers between they two
		int job = vector[i];
		
		if (i < startCN) {
			for (int k=i; k<startCN; k++)
				vector[k] = vector[k+1];
			vector[startCN] = job;
		}
		else if (i > startCN) {
			for (int k=i; k>startCN; k--)
				vector[k] = vector[k-1];
			vector[startCN] = job;
		}
			
		
	}
	
	private int numberOfCNsWithCapacity(int reqProcs, JobsScheduling problem) {
		int number = 0;
		int numberCNs = problem.getNumberCNs();
		
		for (int i=0; i<numberCNs; i++) 
			if (problem_.numberProcs(i) >= reqProcs)
				number++;
			
		
		return number;
	}

	public int getEvaluations() {

		return 0;
	} // getEvaluations

} // LMCTSLocalSearch
