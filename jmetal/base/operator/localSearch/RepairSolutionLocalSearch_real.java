package jmetal.base.operator.localSearch;

import java.util.Random;

import jmetal.base.Variable;
import jmetal.base.variable.Int;
import jmetal.base.variable.Permutation;
import jmetal.base.operator.localSearch.LocalSearch;
import jmetal.base.*;
import jmetal.problems.jobsScheduling.JobsScheduling;
import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.util.Matrix;
import jmetal.util.PseudoRandom;
import jmetal.base.solutionType.*;
import jmetal.util.JMException;

/**
 * This class implements a local search operator that repairs solutions by
 * removing those jobs that cannot be executed in the assigned CN (because they
 * require more processors than the number of processors in the CN) and adding
 * them to other CN with enough resources
 * 
 * @author Bernab� Dorronsoro
 * @version 1.0
 */
public class RepairSolutionLocalSearch_real extends LocalSearch {

	private JobsScheduling_real problem_ = null;

	/**
	 * Constructor Creates a new local search object.
	 */
	public RepairSolutionLocalSearch_real() {

		super();
	} // LMCTSLocalSearch

	/**
	 * Executes the local search.
	 * 
	 * @param object
	 *            Object representing a package with a solution and an islandId
	 * @return An object containing the new improved solution
	 * @throws JMException
	 */
	public Object execute(Object object) throws JMException {
		try {
			// System.out.println("entro no cc!");

			// Recover the parameters (solution and island)
			Solution solution = (Solution) object;

			Permutation vars = (Permutation) solution.getDecisionVariables()[0];

			if (problem_ == null)
				problem_ = (JobsScheduling_real) getParameter("Problem");

			int split = problem_.getNumberJobs();
			int len = vars.getLength();
			int CN = 0;

			// int capacityCN = problem_.numberProcs(CN);
			int capacityCN = problem_.numberCores(CN);

			// Do the solution repair
			for (int i = 0; i < len; i++) {
				if (vars.vector_[i] >= split) {
					CN++;
					capacityCN = problem_.numberCores(CN);
				} else {
					int reqProcs = problem_.requiredProcs(vars.vector_[i]);
					while (reqProcs > capacityCN) {
						int job;
						job = vars.vector_[i];

						int pre;
						pre = getCNofJob(solution, split, job);

						// Mover este job a otro CN con capacidad suficiente
						int numberOfCandidates = numberOfCNsWithCapacity(
								reqProcs, problem_);

						if (numberOfCandidates > 0) {
							// Move the solution to one of the CNs with
							// capacity,
							// chosen
							// randomly
							// TODO: We could choose the one with lower CT
							// instead
							int candidate = PseudoRandom.randInt(0,
									numberOfCandidates - 1);
							boolean reeval;
							reeval = moveJobToCN(solution, i, candidate, split);

							int post;
							post = getCNofJob(solution, split, job);

							/*
							 * if (pre == post) { System.out
							 * .println("ERROR!!! La solución sigue mal!!!");
							 * System.exit(1); }
							 */

							if (reeval) {
								if (vars.vector_[i] >= split) {
									CN++;
									capacityCN = problem_.numberCores(CN);
									reqProcs = 0;
								} else {
									reqProcs = problem_
											.requiredProcs(vars.vector_[i]);
								}
							} else {
								reqProcs = 0;
							}
						} else {
							reqProcs = 0;
						}
					}
				}
			}

			/*
			 * if (!assertCorrectness(solution)) {
			 * System.out.println("ERROR!!! La solución sigue mal!!!");
			 * System.exit(1); }
			 */

			// return new Solution(solution);
			return solution;
		} catch (JMException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(1);
			return null;
		}
	} // execute

	private int getCNofJob(Solution s, int split, int job) {
		boolean finished = false;
		int currCN = 0;
		int currPos = 0;

		Permutation vars = (Permutation) s.getDecisionVariables()[0];

		while (!finished) {
			finished = currPos >= vars.getLength();

			if (!finished) {
				finished = (vars.vector_[currPos] == job);

				if (!finished) {
					if (vars.vector_[currPos] >= split) {
						currCN++;
					}

					currPos++;
				}
			}
		}

		if (vars.vector_[currPos] == job) {
			return currCN;
		} else {
			return -1;
		}
	}

	private boolean assertCorrectness(Solution s) {
		int split = problem_.getNumberJobs();

		Permutation vars = (Permutation) s.getDecisionVariables()[0];
		int len = vars.getLength();

		int CN = 0;
		int capacityCN = problem_.numberCores(CN);

		// Do the solution repair
		for (int i = 0; i < len; i++) {
			if (vars.vector_[i] >= split) {
				CN++;
				capacityCN = problem_.numberCores(CN);
			} else {
				int reqProcs = 1;
				try {
					reqProcs = problem_.requiredProcs(vars.vector_[i]);
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
				if (reqProcs > capacityCN) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Moves job to the selected CN. The job will be located at the beginning of
	 * the CN, since big jobs are normally better to be run first...
	 * 
	 * @param vector
	 * @param i
	 * @param candidate
	 */
	private boolean moveJobToCN(Solution s, int i, int candidate, int split) {
		int startCN = 0;
		int CN = 0;

		Permutation vars = (Permutation) s.getDecisionVariables()[0];
		int len = vars.getLength();

		int reqcores = 1;
		try {
			reqcores = problem_.requiredProcs(vars.vector_[i]);
		} catch (JMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}

		int CNIndex = 0;
		int CNLength = problem_.getNumberCNs();
		int CNCurrCand = -1;

		while ((CNIndex < CNLength) && (CNCurrCand < candidate)) {
			if (problem_.numberCores(CNIndex) >= reqcores) {
				CNCurrCand++;

				if (CNCurrCand < candidate) {
					CNIndex++;
				}
			} else {
				CNIndex++;
			}
		}

		while ((CN < CNIndex) && (startCN < len)) {
			if (vars.vector_[startCN] >= split) {
				CN++;
			}
			startCN++;
		}

		// if (startCN > 1)
		// startCN--;

		// StartCN is pointing to the position where the splitter for the
		// candidate CN is in vector
		// move the job in i to position startCN, moving all the numbers between
		// they two
		int job = vars.vector_[i];

		if (i < startCN) {
			for (int k = i; k < startCN - 1; k++) {
				vars.vector_[k] = vars.vector_[k + 1];
			}
			vars.vector_[startCN - 1] = job;
			return true;
		} else if (i > startCN) {
			for (int k = i; k > startCN; k--) {
				vars.vector_[k] = vars.vector_[k - 1];
			}
			vars.vector_[startCN] = job;
			return false;
		}

		return false;
	}

	private int numberOfCNsWithCapacity(int reqProcs,
			JobsScheduling_real problem) {
		int number = 0;
		int numberCNs = problem.getNumberCNs();

		for (int i = 0; i < numberCNs; i++) {
			// if (problem_.numberProcs(i) >= reqProcs) {
			if (problem_.numberCores(i) >= reqProcs) {
				number++;
			}
		}

		return number;
	}

	public int getEvaluations() {

		return 0;
	} // getEvaluations

} // LMCTSLocalSearch
