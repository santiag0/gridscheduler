package jmetal.heuristics.jobscheduling;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import jmetal.base.Solution;
import jmetal.base.Variable;
import jmetal.base.variable.Permutation;
import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.util.Configuration;

public class SmartScheduler {
	// Tipos de sorting previo al second level scheduler
	public static final int SORTING_NONE = 0;
	public static final int SORTING_ESTIMATION = 1;
	public static final int SORTING_AVGCORES = 2;
	public static final int SORTING_NUMLVL = 3;
	public static final int SORTING_MAXCORES = 4;
	public static final int SORTING_NUMTASKS = 5;
	public static final int SORTING_CPATH = 6;
	public static final int SORTING_LOAD = 7;

	public static final int SORTING_ASC = 1;
	public static final int SORTING_DSC = -1;

	// Available first level scheduling heuristics
	public static final int ROUND_ROBIN = 0;
	public static final int LOAD_BALANCE = 1;
	public static final int Max_Min = 2;
	public static final int Max_MIN = 3;
	public static final int Min_MIN = 4;
	public static final int Max_MinCPath = 5;
	public static final int LongestFirst = 6;
	public static final int CoreAware = 7;
	public static final int MachineAware = 8;

	public static String[] firstLevelSched = new String[] {
			"jmetal.heuristics.jobscheduling.RoundRobin",
			"jmetal.heuristics.jobscheduling.LoadBalance",
			"jmetal.heuristics.jobscheduling.MaxMin",
			"jmetal.heuristics.jobscheduling.MaxMinEnergy",
			"jmetal.heuristics.jobscheduling.MinMinEnergy",
			"jmetal.heuristics.jobscheduling.MaxMinCPath",
			"jmetal.heuristics.jobscheduling.LongestFirst",
			"jmetal.heuristics.jobscheduling.CoreAware",
			"jmetal.heuristics.jobscheduling.MachineAware" };

	private static int _currentSorting = 0;
	private static int _currentDirection = -1;
	private static int _currentStrategy = 0;

	public static Solution ExecuteHeuristic(JobsScheduling_real problem)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		if (_currentStrategy == 9) {
			return new Solution(problem);
		} else {
			Solution sol = ExecuteHeuristic(problem, _currentStrategy,
					_currentSorting, _currentDirection);
			
			_currentSorting++;
			
			if (_currentSorting == 8) {
				_currentSorting = 0;
				
				if (_currentDirection == -1) {
					_currentDirection = 1;
				} else if (_currentDirection == 1) {
					_currentDirection = -1;
					_currentStrategy++;
				}
			}
			
			return sol;
		}
	}

	/**
	 * @param args
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public static Solution ExecuteHeuristic(JobsScheduling_real problem,
			int strategyIndex, int sorting, int direction)
			throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException,
			ClassNotFoundException {

		// System.out.println("Error. Try: smartScheduler NbInstances FirstLevelHeuristic LocalScheduler Sorting Direction");
		// System.out.println("FirstLevelHeuristic: ROUND_ROBIN  = 0");
		// System.out.println("                     LOAD_BALANCE = 1");
		// System.out.println("                     Max_Min      = 2");
		// System.out.println("                     Max_MIN      = 3");
		// System.out.println("                     Min_MIN      = 4");
		// System.out.println("                     Max_MinCPath = 5");
		// System.out.println("                     LongestFirst = 6");
		// System.out.println("                     CoreAware    = 7");
		// System.out.println("                     MachAware    = 8");
		// System.out.println();
		// System.out.println("Sorting:             NONE       = 0");
		// System.out.println("                     ESTIMATION = 1");
		// System.out.println("                     AVGCORES   = 2");
		// System.out.println("                     NUMLVL     = 3");
		// System.out.println("                     MAXCORES   = 4");
		// System.out.println("                     NUMTASKS   = 5");
		// System.out.println("                     CRIT PATH  = 6");
		// System.out.println("                     LOAD       = 7");
		// System.out.println("Direction:           ASC  = 1");
		// System.out.println("                     DESC = -1");

		Strategy firstLevelS = null;

		// Create an instance of the first level scheduler
		Class<?> schedClass = Class.forName(firstLevelSched[strategyIndex]);
		Constructor[] constructors = schedClass.getConstructors();

		{
			int i = 0;
			// find the constructor
			while ((i < constructors.length)
					&& (constructors[i].getParameterTypes().length != 0)
					&& !constructors[i].getParameterTypes()[2]
							.equals(String.class.getName())) {
				i++;
			}

			firstLevelS = (Strategy) constructors[i].newInstance();
		}

		// Map tasks into the clusters
		ArrayList<ArrayList<Integer>> mapping = firstLevelS.schedule(problem);

		int numberCNs = problem.numberCNs_;
		int numberJobs = problem.numberJobs_;

		// create the chromosome of the individual
		int[] variables = new int[numberCNs + numberJobs - 1];

		int varIndex = 0;
		int exceed = 0;

		for (int i = 0; i < numberCNs; i++) {
			int jobsInCN = mapping.get(i).size();
			for (int k = 0; k < jobsInCN; k++) {
				variables[varIndex] = mapping.get(i).get(k).intValue();
				varIndex++;
			}
			if (varIndex < numberCNs + numberJobs - 1)
				variables[varIndex] = numberJobs + exceed;
			varIndex++;
			exceed++;
		}

		Variable[] vars = new Variable[1];
		vars[0] = new Permutation(variables);

		// Build a solution with the Mapping
		Solution sol = new Solution(problem, vars);

		// Re-ordena los trabajos para el algoritmo de segundo nivel.
		JobSorting(problem, sol, sorting, direction);

		return sol;
	}

	/*
	 * Función que re-ordena los jobs de cáda CN según algún criterio para
	 * mejorar el desempeño de los planificadores de segundo nivel.
	 */
	public static void JobSorting(JobsScheduling_real probl, Solution sol,
			int tipoSorting, int direction) {
		int schedule[] = ((Permutation) sol.getDecisionVariables()[0]).vector_;

		// Índice de inicio y de fin del CN cuyas jobs están siendo re-ordenado
		// actualmente.
		int start_index = 0;
		int end_index = -1;
		int cn = 0;

		for (int i = 0; i < 1004; i++) {
			if (schedule[i] >= 1000) {
				// Número de job no válido usado como marca para separar CNs
				end_index = i - 1;

				System.out.println("[CN] start=" + start_index + " end="
						+ end_index);

				if (end_index > start_index) {
					JobSorting_CN(probl, schedule, cn, start_index, end_index,
							tipoSorting, direction);
				}

				start_index = i + 1;
				cn++;
			}
		}

		// Proceso el último CN
		end_index = 1003;

		System.out.println("[CN] start=" + start_index + " end=" + end_index);

		if (end_index > start_index) {
			JobSorting_CN(probl, schedule, cn, start_index, end_index,
					tipoSorting, direction);
		}
	}

	public static void JobSorting_CN(JobsScheduling_real probl, int schedule[],
			int cn, int start_index, int end_index, int tipoSorting,
			int direction) {

		try {
			Comparator<Integer> criterioSorting = new JobComparator(probl, cn,
					tipoSorting, direction);

			// Convierto los int a Integer para poder usar el custom comparator
			// (odio Java)
			Integer[] scheduleObj = new Integer[end_index - start_index + 1];
			System.out.print("Old order: ");
			for (int i = start_index; i <= end_index; i++) {
				System.out.print(schedule[i] + " ");
				scheduleObj[i - start_index] = Integer.valueOf(schedule[i]);
			}
			System.out.println("");

			// Arrays.sort(scheduleObj, start_index, end_index,
			// criterioSorting);
			Arrays.sort(scheduleObj, criterioSorting);

			// Actualizo el array original
			System.out.print("New order: ");
			for (int i = 0; i < scheduleObj.length; i++) {
				schedule[start_index + i] = scheduleObj[i].intValue();
				System.out.print(schedule[start_index + i] + " ");
			}
			System.out.println("");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}