package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class CoreAware extends Strategy {

	private double[] jobLength = new double[1000];
	private int[] jobMaxNCoresPerMachine = new int[1000];
	private int[] jobNCoresPerLevel = new int[1000];

	// RoundRobin taking into account that tasks can be executed in the
	// corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {

		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;

		// Uso el comparator que tenía para ordenar por camino crítico
		// El CN no importa para el camino crítico (solamente para el estimador
		// de Javid)
		Comparator<Integer> critPathSorting = new JobComparator(probl, -1,
				SmartScheduler.SORTING_MAXCORES, SmartScheduler.SORTING_DSC);

		Integer[] jobs = new Integer[1000];
		for (int job = 0; job < 1000; job++) {
			jobs[job] = job;

			jobLength[job] = probl.criticalPath(job);
			try {
				jobMaxNCoresPerMachine[job] = instance.requiredProcs(job);
				jobNCoresPerLevel[job] = instance.requiredProcsLevel(job);
				//jobNCoresPerLevel[job] = instance.requiredAvgCoresLevel(job);
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}

		// Ordeno los jobs por el largo de su camino crítico
		Arrays.sort(jobs, critPathSorting);

		ArrayList<ArrayList<Double>> estimatedCoreCT = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();

		// Estimation of the CT of the different CNs
		for (int i = 0; i < instance.getNumberCNs(); i++) {
			mapping.add(new ArrayList<Integer>());
			estimatedCoreCT.add(new ArrayList<Double>());

			for (int j = 0; j < instance.CNNbProc[i]; j++) {
				for (int h = 0; h < instance.CNCores[i]; h++) {
					estimatedCoreCT.get(i).add(0.0);
				}
			}
		}

		for (int pos = 0; pos < 1000; pos++) {
			Integer job;
			job = jobs[pos];

			int bestCN;
			bestCN = 0;

			double bestCT;
			bestCT = Double.MAX_VALUE;

			for (int i = 0; i < 5; i++) {
				if (jobMaxNCoresPerMachine[job] <= instance.CNCores[i]) {
					double jobLengthForCN;
					//jobLengthForCN = (jobLength[job] * jobNCoresPerLevel[job])
					//		/ (instance.CNNbProc[i] * instance.CNCores[i] * instance.CNSpeed[i]);

					jobLengthForCN = jobLength[job] / instance.CNSpeed[i];

					if (jobNCoresPerLevel[job] < estimatedCoreCT.get(i).size()) {
						if (estimatedCoreCT.get(i).get(
								jobNCoresPerLevel[job] - 1)
								+ jobLengthForCN < bestCT) {

							bestCT = estimatedCoreCT.get(i).get(
									jobNCoresPerLevel[job] - 1)
									+ jobLengthForCN;
							bestCN = i;
						}
					} else {
						if (estimatedCoreCT.get(i).get(
								estimatedCoreCT.get(i).size() - 1)
								+ jobLengthForCN < bestCT) {

							bestCT = estimatedCoreCT.get(i).get(
									estimatedCoreCT.get(i).size() - 1)
									+ jobLengthForCN;
							bestCN = i;
						}
					}
				}
			}

			for (int i = 0; i < jobNCoresPerLevel[job]; i++) {
				try {
					estimatedCoreCT
							.get(bestCN)
							.set(0,
									estimatedCoreCT.get(bestCN).get(0)
											+ (jobLength[job] / instance.CNSpeed[bestCN]));
					
					// Reordeno los CT de los cores para que los cores con menos trabajo
					// estén siempre al inicio de la lista
					Collections.sort(estimatedCoreCT.get(bestCN));
				} catch (Exception ex) {
					System.exit(1);
				}
			}

			mapping.get(bestCN).add(job);


		}

		return mapping;
	}
}
