package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class MinMinEnergy extends Strategy {

	// Regular RoundRobin 
//	@Override
//	public ArrayList<ArrayList<Integer>> schedule(JobSchedInstance instance) {
//		
//		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
//		
//		for (int i=0; i<instance.getNumberCNs(); i++)
//			mapping.add(new ArrayList<Integer>());
//		
//		// assign jobs to the existing clusters (namely CNs) in a round robin manner
//		for (int i=0; i<instance.getNumberJobs(); i++)
//		{
//			mapping.get(i%instance.getNumberCNs()).add(new Integer(i));
//		}
//		
//		return mapping;
//	}

	// RoundRobin taking into account that tasks can be executed in the corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {
		
		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;
		
		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
		
		// Estimation of the CT of the different CNs
		double [] estimatedCT = new double[instance.getNumberCNs()];
		
		for (int i=0; i<instance.getNumberCNs(); i++) {
			mapping.add(new ArrayList<Integer>());
			estimatedCT[i] = 0.0;
		}

		// List of the unassigned jobs		
		ArrayList<Integer> unassignedJ = new ArrayList<Integer>(instance.getNumberJobs());
				
		// List with the critical paths of every job
		double[] jobsCP = new double[instance.getNumberJobs()];
		
		// Calculate the critical path of all jobs
		for (int i=0; i<instance.getNumberJobs(); i++) {
			jobsCP[i] = probl.criticalPath(i);
			unassignedJ.add(new Integer(i));
		}
		
		// While there are unassigned jobs, assign them
		while (!unassignedJ.isEmpty()) {
			
			// FIRST-STEP: for each job, find the machine that can finish it with less energy requirements
			
			// the job-machine pairs will be stored here
			int[] assignments = new int[instance.getNumberJobs()]; 
			for (int i=0; i<instance.getNumberJobs(); i++) {
				assignments[i] = -1 ;
			}
			
			Iterator<Integer> it = unassignedJ.iterator();
			while (it.hasNext()) {
				int job = it.next().intValue();
				
				// EstEnergy = estTime * consumption of servers
				double estEnergy = 0.0;
				double estTime = 0.0;
				double minEnergy = Double.MAX_VALUE;
				
				try {
					if (instance.requiredProcs(job) <= instance.CNCores[0]) {
//						estTime = probl.instance.estimateTime(job, 0) + estimatedCT[0];
						estTime = probl.instance.estimateTime(job, 0) + estimatedCT[0];
						estEnergy = estTime * instance.CNNbProc[0] * instance.EnergyMax[0];
						minEnergy = estEnergy;
						assignments[job] = 0;
//						estimatedCT[0] += estTime;
					}
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (int c=1; c<instance.getNumberCNs(); c++) {
					
					try {
						if (instance.requiredProcs(job) <= instance.CNCores[c]) { 
//							estTime = probl.instance.estimateTime(job, c) + estimatedCT[c];
							estTime = probl.instance.estimateTimePonderatedResourcesUse(job, c) + estimatedCT[c];
							estEnergy = estTime * instance.CNNbProc[c] * instance.EnergyMax[c];
							if (minEnergy > estEnergy) {
//								estimatedCT[0] = estTime;
//								estimatedCT[c] += estTime;
								minEnergy = estEnergy;
								assignments[job] = c;
							}
						}
					} catch (JMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
			
			// SECOND-STEP: From all the pairs machine-job, choose the one with minimum completion time
			double minCT = Double.MAX_VALUE ;
			int job = 0;
					
			for (int j=0; j<assignments.length; j++) {
				
				if (assignments[j]>=0) {
//					double CT = probl.instance.estimateTime(j, assignments[j]) + estimatedCT[assignments[j]];
					double CT = probl.instance.estimateTimePonderatedResourcesUse(j, assignments[j]) + estimatedCT[assignments[j]];
					if (CT < minCT) {
						minCT = CT;
						job = j;
					}
				}
			}
			
			estimatedCT[assignments[job]] = minCT ;
			for (int i=0; i<unassignedJ.size();i++) {
				if (unassignedJ.get(i).intValue() == job) {
					unassignedJ.remove(i);
					break;
				}
			}
			
			mapping.get(assignments[job]).add(new Integer(job));
			
//			System.out.print(job + " ");
		}

		System.out.println();
		return mapping;
	}
}
