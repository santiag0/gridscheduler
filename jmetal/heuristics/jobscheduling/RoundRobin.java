package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class RoundRobin extends Strategy {

	// Regular RoundRobin 
//	@Override
//	public ArrayList<ArrayList<Integer>> schedule(JobSchedInstance instance) {
//		
//		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
//		
//		for (int i=0; i<instance.getNumberCNs(); i++)
//			mapping.add(new ArrayList<Integer>());
//		
//		// assign jobs to the existing clusters (namely CNs) in a round robin manner
//		for (int i=0; i<instance.getNumberJobs(); i++)
//		{
//			mapping.get(i%instance.getNumberCNs()).add(new Integer(i));
//		}
//		
//		return mapping;
//	}

	// RoundRobin taking into account that tasks can be executed in the corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {
		
		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;
		
		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
		
		for (int i=0; i<instance.getNumberCNs(); i++)
			mapping.add(new ArrayList<Integer>());
		
		int cn = 0;
		int job=0;
		// assign jobs to the existing clusters (namely CNs) in a round robin manner
		while (job<instance.getNumberJobs())
		{
			try {
				if (instance.requiredProcs(job) <= instance.CNCores[cn%instance.getNumberCNs()]) {
					mapping.get(cn%instance.getNumberCNs()).add(new Integer(job));
					job++;
				}
				
				cn++;
					
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mapping;
	}
}
