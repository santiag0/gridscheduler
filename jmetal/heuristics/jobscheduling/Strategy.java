package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;

public abstract class Strategy {

	public abstract ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real problem); // Returns the list of jobs assigned to every cluster

}