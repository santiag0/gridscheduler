package jmetal.heuristics.jobscheduling;

import java.util.Comparator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;

public class JobComparator implements Comparator<Integer> {
	// Instancia del problema
	private JobsScheduling_real _probl;

	// CN sobre la que se esta realizando al ordenación de trabajos
	private int _cn;

	// Tipo de sorting a usar
	private int _tipo;
	
	// Dirección del sorting (asc/dsc)
	private int _direction;

	public JobComparator(JobsScheduling_real probl, int cn, int tipoSorting, int direction) {
		_probl = probl;
		_cn = cn;
		_tipo = tipoSorting;
		_direction = direction;
	}

	public int compare(Integer job1, Integer job2) {
		try {
			double estimate1,estimate2;
			int avgcores1,avgcores2;
			int numlvl1,numlvl2;
			int maxcores1,maxcores2;	
			int totalTasks1,totalTasks2;
			double critpath1,critpath2;
			
			switch (_tipo) {
			case SmartScheduler.SORTING_NONE:
				return 0;
			case SmartScheduler.SORTING_ESTIMATION:
				estimate1 = _probl.instance.estimateTime(job1, _cn);
				estimate2 = _probl.instance.estimateTime(job2, _cn);
				
				// Ordena por el estimado.
				if (estimate1 < estimate2) {
					return -1 * _direction;
				} else if (estimate1 > estimate2) {
					return 1 * _direction;
				} else {
					return 0;
				}
			case SmartScheduler.SORTING_AVGCORES:
				avgcores1 = _probl.instance.requiredAvgCoresLevel(job1);
				avgcores2 = _probl.instance.requiredAvgCoresLevel(job2);
				
				// Ordena por el promedio de cores.
				if (avgcores1 < avgcores2) {
					return -1 * _direction;
				} else if (avgcores1 > avgcores2) {
					return 1 * _direction;
				} else {
					return 0;
				}
			case SmartScheduler.SORTING_NUMLVL:
				numlvl1 = _probl.instance.NumberLevels(job1);
				numlvl2 = _probl.instance.NumberLevels(job2);
				
				// Ordena por el numero de niveles.
				if (numlvl1 < numlvl2) {
					return -1 * _direction;
				} else if (numlvl1 > numlvl2) {
					return 1 * _direction;
				} else {
					return 0;
				}
			case SmartScheduler.SORTING_MAXCORES:
				maxcores1 = _probl.instance.requiredProcs(job1);
				maxcores2 = _probl.instance.requiredProcs(job2);
				
				// Ordena por el numero de niveles.
				if (maxcores1 < maxcores2) {
					return -1 * _direction;
				} else if (maxcores1 > maxcores2) {
					return 1 * _direction;
				} else {
					// Si son iguales, ordeno por camino crítico
					critpath1 = _probl.criticalPath(job1);
					critpath2 = _probl.criticalPath(job2);
					
					avgcores1 = _probl.instance.requiredAvgCoresLevel(job1) * _probl.instance.NumberLevels(job1);
					avgcores2 = _probl.instance.requiredAvgCoresLevel(job2) * _probl.instance.NumberLevels(job2);
					
					// Ordena por la cantidad de tareas.
					if ((critpath1 * avgcores1) < (critpath2 * avgcores2)) {
						return -1 * _direction;
					} else if ((critpath1 * avgcores1) > (critpath2 * avgcores2)) {
						return 1 * _direction;
					} else {
						return 0;
					}
				}
			case SmartScheduler.SORTING_NUMTASKS:
				totalTasks1 = 0;
				totalTasks2 = 0;
				
				// Suma la cantidad de tareas en todos los niveles
				for (int i = 0; i < _probl.instance.getLevels(job1).length; i++) {
					totalTasks1 += _probl.instance.getLevels(job1)[i].size();
				}
				for (int i = 0; i < _probl.instance.getLevels(job2).length; i++) {
					totalTasks2 += _probl.instance.getLevels(job2)[i].size();
				}
				
				// Ordena por la cantidad de tareas.
				if (totalTasks1 < totalTasks2) {
					return -1 * _direction;
				} else if (totalTasks1 > totalTasks2) {
					return 1 * _direction;
				} else {
					return 0;
				}
			case SmartScheduler.SORTING_CPATH:
				critpath1 = _probl.criticalPath(job1);
				critpath2 = _probl.criticalPath(job2);
				
				// Ordena por la cantidad de tareas.
				if (critpath1 < critpath2) {
					return -1 * _direction;
				} else if (critpath1 > critpath2) {
					return 1 * _direction;
				} else {
					return 0;
				}
			case SmartScheduler.SORTING_LOAD:
				critpath1 = _probl.criticalPath(job1);
				critpath2 = _probl.criticalPath(job2);
				
				avgcores1 = _probl.instance.requiredAvgCoresLevel(job1);
				avgcores2 = _probl.instance.requiredAvgCoresLevel(job2);
				
				// Ordena por la cantidad de tareas.
				if ((critpath1 * avgcores1) < (critpath2 * avgcores2)) {
					return -1 * _direction;
				} else if ((critpath1 * avgcores1) > (critpath2 * avgcores2)) {
					return 1 * _direction;
				} else {
					return 0;
				}
			default:
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}