package jmetal.heuristics.jobscheduling;

/*
 * 
 * This strategy tries to balance to the maximum the number of jobs assigned to the CNs
 * 
 * It starts assigning to the CN with highest number of cores those jobs that can only run there
 * then goes to the next CN and assigns those jobs that cannot run in the other CNs, balancing between the current CN and the previous one
 * Continues until all jobs are assigned
 * 
 * */

import java.util.ArrayList;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class LoadBalance extends Strategy {

	// Balance the number of jobs in the different clusters as much as possible
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {
		
		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;
		
//		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
//		
//		int [] numberOfAssignedJobsInCN = {0, 0, 0, 0, 0};
//		
//		// list of the CNs, ordered from higher number of cores to lower number of cores
//		int [] CNsOrderedByNbCores = new int[instance.getNumberCNs()];
//		
//		for (int i=0; i<instance.getNumberCNs(); i++)
//			mapping.add(new ArrayList<Integer>());
//		
//		int nbCores = 0;
//		for (int c=0; c<instance.getNumberCNs(); c++)
//			if (nbCores < instance.CNCores[c])
//				nbCores = instance.CNCores[c];
//		
//		int ind = 0;
//		while (ind<instance.getNumberCNs()) {
//			for (int aux=0; aux<instance.getNumberCNs(); aux++) {
//				if (nbCores == instance.CNCores[aux]) {
//					CNsOrderedByNbCores[ind] = aux;
//					ind++;
//				}
//			}
//			nbCores--;
//		}
//		
//		// At this point, we have in CNsOrderedByNbCores a list of CN ids, ordered by their number of cores in decreasing order
//		
//		int cn= 0;
//		
//		while ((cn<instance.getNumberCNs())) {
//			// Find the CN with the next higher number of cores with lowest index in CNsOrderedByNbCores that can execute
//			while ((cn<instance.getNumberCNs()-1) && (instance.CNCores[CNsOrderedByNbCores[cn]] == instance.CNCores[CNsOrderedByNbCores[cn+1]]))
//				cn++;
//			
//			int assignedJobs = 0;
//			
//			while (assignedJobs<=instance.getNumberJobs()) {
//			
//				for (int j=0; j<instance.getNumberJobs(); j++) {
//					try {
//						if (instance.requiredProcs(j) == instance.CNCores[CNsOrderedByNbCores[cn]]) {
//							
//							// find the machine that can execute job j, doing load balance between all machines that can execute it
//							int selectedCN = CNsOrderedByNbCores[cn];
//							for (int scn=cn; scn>=0; scn--) {
//								if (numberOfAssignedJobsInCN[selectedCN] > numberOfAssignedJobsInCN[scn])
//									selectedCN = scn;
//							}
//							
//							// assign job j in selectedCN CN
//							mapping.get(selectedCN).add(new Integer(j));
//							
//							numberOfAssignedJobsInCN[selectedCN]++;
//							assignedJobs++;
//						}
//					} catch (JMException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			
//			}
//		}
		
		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
		
		// list of the CNs, ordered from higher number of cores to lower number of cores
		int [] CNsOrderedByNbCores = new int[instance.getNumberCNs()];
		int [] numberOfAssignedJobsInCN = {0, 0, 0, 0, 0};
		
		for (int i=0; i<instance.getNumberCNs(); i++)
			mapping.add(new ArrayList<Integer>());
			
		int nbCores = 0;
		for (int c=0; c<instance.getNumberCNs(); c++)
			if (nbCores < instance.CNCores[c])
				nbCores = instance.CNCores[c];
		
		int ind = 0;
		while (ind<instance.getNumberCNs()) {
			for (int aux=0; aux<instance.getNumberCNs(); aux++) {
				if (nbCores == instance.CNCores[aux]) {
					CNsOrderedByNbCores[ind] = aux;
					ind++;
				}
			}
			nbCores--;
		}
		// At this point, we have in CNsOrderedByNbCores a list of CN ids, ordered by their number of cores in decreasing order
		
		// Separate jobs in terms of the number of cores they need into jobsByCore
		ArrayList<ArrayList<Integer>> jobsByCore = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i<instance.CNCores[CNsOrderedByNbCores[0]]; i++)
			jobsByCore.add(new ArrayList<Integer>());
		
		for (int job=0; job<instance.getNumberJobs(); job++) {
			try {
				int nc = instance.requiredProcs(job);
//				if (jobsByCore.get(nc-1) == null)
//					jobsByCore.add(new ArrayList<Integer>());
				
				jobsByCore.get(nc-1).add(new Integer(job));
				
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int cores = jobsByCore.size();
		
		while (cores > 0) {
			if (jobsByCore.get(cores-1) == null){
				cores--;
				continue;
			}
			
			Iterator<Integer> it = jobsByCore.get(cores-1).iterator();
			
			// Look for the smallest CN that can execute the job
			int maxIndexWithEnoughCores = 0;
			while ((maxIndexWithEnoughCores<instance.getNumberCNs()) && (instance.CNCores[CNsOrderedByNbCores[maxIndexWithEnoughCores]] >= cores)) 
				maxIndexWithEnoughCores++;
			
			while (it.hasNext()) {
				int selectedCN  = CNsOrderedByNbCores[0];
				int numberAssignedJobs = numberOfAssignedJobsInCN[selectedCN];
				
				// look for the CN with lowest number of assigned jobs (that can execute the current job)
				for (int cn=0; cn<maxIndexWithEnoughCores; cn++) {
					if (numberOfAssignedJobsInCN[CNsOrderedByNbCores[cn]] < numberAssignedJobs) {
						selectedCN  = CNsOrderedByNbCores[cn];
						numberAssignedJobs = numberOfAssignedJobsInCN[selectedCN];
					}
				}
				
				mapping.get(selectedCN).add(it.next());
				numberOfAssignedJobsInCN[selectedCN]++;
				
			}
			
			cores--;
		}
		
		
		return mapping;
	}
}
