package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class MaxMinCPath extends Strategy {
	static int estimator = 2;

	private int[] totalCores = new int[1000];
	private double[] totalOps = new double[1000];

	public double LengthMetric(JobsScheduling_real probl, int job) {
		double lengthValue = 0.0;

		int totalReqCores = 0;

		switch (estimator) {
		case 0:
			// simple
			lengthValue = probl.criticalPath(job);
			break;
		case 1:
			try {
				lengthValue = probl.criticalPath(job)
						* probl.instance.requiredAvgCoresLevel(job);
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(-1);
			}
			break;
		case 2:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalCores[job];

			break;
		case 3:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = totalCores[job] * totalCores[job];

			break;
		case 4:
			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = totalOps[job];

			break;
		case 5:
			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalOps[job];

			break;
		case 6:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalOps[job]
					* totalCores[job];

			break;
		case 7:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job)
					* probl.instance.getLevels(job).length;

			break;
		}

		return lengthValue;
	}

	public double EstimatorFunction(JobsScheduling_real probl, int job, int cn) {
		double estValue = 0.0;

		int totalReqCores = 0;

		switch (estimator) {
		case 0:
			// simple
			estValue = (LengthMetric(probl, job) / probl.instance.CNSpeed[cn]);
			break;
		case 1:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));
			break;
		case 2:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		case 3:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		case 4:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		case 5:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		case 6:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		case 7:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		}

		return estValue;
	}

	// RoundRobin taking into account that tasks can be executed in the
	// corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {

		// Uso el comparator que tenía para ordenar por camino crítico
		// El CN no importa para el camino crítico (solamente para el estimador
		// de Javid)
		Comparator<Integer> critPathSorting = new JobComparator(probl, -1,
				SmartScheduler.SORTING_MAXCORES, SmartScheduler.SORTING_DSC);
		
		Integer[] jobs = new Integer[1000];
		for (int job = 0; job < 1000; job++) {
			jobs[job] = job;
		}
			
		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;

		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();

		// Estimation of the CT of the different CNs
		double[] estimatedCT = new double[instance.getNumberCNs()];

		for (int i = 0; i < instance.getNumberCNs(); i++) {
			mapping.add(new ArrayList<Integer>());
			estimatedCT[i] = 0.0;
		}

		// List of the unassigned jobs
		ArrayList<Integer> unassignedJ = new ArrayList<Integer>(
				instance.getNumberJobs());

		// List with the critical paths of every job
		double[] jobsCP = new double[instance.getNumberJobs()];
	
		// Calculate the critical path of all jobs
		for (int i=0; i<instance.getNumberJobs(); i++) {
			int job;
			job = jobs[i];
			
			jobsCP[i] = probl.criticalPath(job);
			unassignedJ.add(new Integer(job));
		}

		// While there are unassigned jobs, assign them
		while (!unassignedJ.isEmpty()) {

			// FIRST-STEP: for each unassigned job, find the machine that can
			// finish it faster

			// the job-machine pairs will be stored here
			int[] assignments = new int[instance.getNumberJobs()];
			for (int i = 0; i < instance.getNumberJobs(); i++) {
				assignments[i] = -1;
			}

			Iterator<Integer> it = unassignedJ.iterator();
			while (it.hasNext()) {
				int job = it.next().intValue();

				double estTime = 0.0;
				double minTime = Double.MAX_VALUE;

				try {
					if (instance.requiredProcs(job) <= instance.CNCores[0]) {
						estTime = EstimatorFunction(probl, job, 0)
								+ estimatedCT[0];

						minTime = estTime;
						assignments[job] = 0;
					}
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(-1);
				}

				for (int c = 1; c < instance.getNumberCNs(); c++) {
					try {
						if (instance.requiredProcs(job) <= instance.CNCores[c]) {
							estTime = EstimatorFunction(probl, job, c)
									+ estimatedCT[c];

							if (minTime > estTime) {
								minTime = estTime;
								assignments[job] = c;
							}
						}
					} catch (JMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.exit(-1);
					}

				}
			}

			// SECOND-STEP: From all the pairs machine-job, choose the one with
			// maximum completion time
			double maxCT = 0.0;
			int job = 0;

			for (int j = 0; j < assignments.length; j++) {

				if (assignments[j] >= 0) {
					double CT = 0.0;
					CT = EstimatorFunction(probl, j, assignments[j])
							+ estimatedCT[assignments[j]];

					if (CT > maxCT) {
						maxCT = CT;
						job = j;
					}
				}
			}

			// if (job>=0) {
			estimatedCT[assignments[job]] = maxCT;
			for (int i = 0; i < unassignedJ.size(); i++) {
				if (unassignedJ.get(i).intValue() == job) {
					unassignedJ.remove(i);
					break;
				}
			}

			mapping.get(assignments[job]).add(new Integer(job));
			System.out.print(job + " ");
			// }
		}

		System.out.println();
		return mapping;
	}
}
