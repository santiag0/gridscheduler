package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class MachineAware extends Strategy {

	private static int ALGORITHM_TYPE = 0; // Best is 0!!!

	private double[] jobLength = new double[1000];
	private int[] jobMaxNCoresPerMachine = new int[1000];
	private int[] jobNCoresPerLevel = new int[1000];

	// RoundRobin taking into account that tasks can be executed in the
	// corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {

		JobSchedInstance instance = probl.instance;

		// Uso el comparator que tenía para ordenar por camino crítico
		// El CN no importa para el camino crítico (solamente para el estimador
		// de Javid)
		Comparator<Integer> critPathSorting = new JobComparator(probl, -1,
				SmartScheduler.SORTING_MAXCORES, SmartScheduler.SORTING_DSC);

		Integer[] jobs = new Integer[1000];
		for (int job = 0; job < 1000; job++) {
			jobs[job] = job;

			jobLength[job] = probl.criticalPath(job);
			try {
				jobMaxNCoresPerMachine[job] = instance.requiredProcs(job);
				jobNCoresPerLevel[job] = instance.requiredProcsLevel(job);
				// jobNCoresPerLevel[job] = instance.requiredAvgCoresLevel(job);
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}

		// Ordeno los jobs por el largo de su camino crítico
		Arrays.sort(jobs, critPathSorting);

		ArrayList<ArrayList<Double>> estimatedMachineCT = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();

		// Estimation of the CT of the different CNs
		for (int i = 0; i < instance.getNumberCNs(); i++) {
			mapping.add(new ArrayList<Integer>());
			estimatedMachineCT.add(new ArrayList<Double>());

			for (int j = 0; j < instance.CNNbProc[i]; j++) {
				estimatedMachineCT.get(i).add(0.0);
			}
		}

		for (int pos = 0; pos < 1000; pos++) {
			Integer job;
			job = jobs[pos];

			int bestCN;
			bestCN = 0;

			double bestCT;
			bestCT = Double.MAX_VALUE;

			for (int i = 0; i < 5; i++) {
				if (jobMaxNCoresPerMachine[job] <= instance.CNCores[i]) {
					double jobLengthForCN;
					
					// jobLengthForCN = (jobLength[job] *
					// jobNCoresPerLevel[job]) / (instance.CNNbProc[i] *
					// instance.CNCores[i] * instance.CNSpeed[i]);
					
					// jobLengthForCN = jobLength[job] / instance.CNSpeed[i];
					jobLengthForCN = instance.estimateTime(job, i);

					if (estimatedMachineCT.get(i).get(instance.CNNbProc[i] - 1)
							+ jobLengthForCN < bestCT) {
						bestCT = estimatedMachineCT.get(i).get(
								instance.CNNbProc[i] - 1)
								+ jobLengthForCN;
						bestCN = i;
					}
				}
			}

			if (ALGORITHM_TYPE == 0) {
				int numberOfTasks = instance.jobTasksOps[job].length;

				for (int i = 0; i < numberOfTasks; i++) {
					try {
						// Estimado actual de carga de cómputo de la máquina
						double currentEstimate;
						currentEstimate = estimatedMachineCT.get(bestCN).get(0);

						// Estimado de carga de cómputo de la tarea
						double taskEstimate;
						taskEstimate = (instance.jobTasksOps[job][i] * instance.jobTasksProc[job][i])
								/ (instance.CNSpeed[bestCN] * instance.CNCores[bestCN]);
						
						// taskEstimate = (instance.jobTasksOps[job][i]) /
						// (instance.CNSpeed[bestCN]);

						estimatedMachineCT.get(bestCN).set(0,
								currentEstimate + taskEstimate);

						// Reordeno las máquinas según su CT estimado para que
						// las máquinas
						// con menos trabajo estén siempre al inicio de la lista
						Collections.sort(estimatedMachineCT.get(bestCN));
					} catch (Exception ex) {
						ex.printStackTrace();
						System.exit(1);
					}
				}
			} else if (ALGORITHM_TYPE == 1) {
				int numberOfTasks = 0;
				int avgNumCores = 0;
				try {
					numberOfTasks = instance.requiredAvgProcsLevel(job, instance.CNCores[bestCN]);
					avgNumCores = instance.requiredAvgCores(job);
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}

				for (int i = 0; i < numberOfTasks; i++) {
					try {
						// Estimado actual de carga de cómputo de la máquina
						double currentEstimate;
						currentEstimate = estimatedMachineCT.get(bestCN).get(0);

						// Estimado de carga de cómputo de la tarea
						double taskEstimate;
						
						taskEstimate = (jobLength[job] * avgNumCores)
								/ (instance.CNSpeed[bestCN] * instance.CNCores[bestCN]);
						
						//taskEstimate = (jobLength[job]) / (instance.CNSpeed[bestCN]);

						estimatedMachineCT.get(bestCN).set(0, currentEstimate + taskEstimate);

						// Reordeno las máquinas según su CT estimado para que
						// las máquinas
						// con menos trabajo estén siempre al inicio de la lista
						Collections.sort(estimatedMachineCT.get(bestCN));
					} catch (Exception ex) {
						ex.printStackTrace();
						System.exit(1);
					}
				}
			}

			mapping.get(bestCN).add(job);
		}

		return mapping;
	}
}
