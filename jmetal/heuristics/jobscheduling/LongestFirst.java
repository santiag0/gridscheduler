package jmetal.heuristics.jobscheduling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import jmetal.problems.jobsScheduling.JobsScheduling_real;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class LongestFirst extends Strategy {

	static int estimator = 6;

	private int[] totalCores = new int[1000];
	private double[] totalOps = new double[1000];

	public double LengthMetric(JobsScheduling_real probl, int job) {
		double lengthValue = 0.0;

		int totalReqCores = 0;

		switch (estimator) {
		case 0:
			// simple
			lengthValue = probl.criticalPath(job);
			break;
		case 1:
			try {
				lengthValue = probl.criticalPath(job)
						* probl.instance.requiredAvgCoresLevel(job);
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(-1);
			}
			break;
		case 2:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalCores[job];

			break;
		case 3:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = totalCores[job] * totalCores[job];

			break;
		case 4:
			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = totalOps[job];

			break;
		case 5:
			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalOps[job];

			break;
		case 6:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job) * totalOps[job]
					* totalCores[job];

			break;
		case 7:
			if (totalCores[job] == 0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalCores[job] += probl.instance.jobTasksProc[job][task];
					}
				}
			}

			if (totalOps[job] == 0.0) {
				for (int i = 0; i < probl.instance.getLevels(job).length; i++) {
					for (int j = 0; j < probl.instance.getLevels(job)[i].size(); j++) {
						int task = ((Integer) probl.instance.getLevels(job)[i]
								.get(j)).intValue();
						totalOps[job] += probl.instance.jobTasksOps[job][task];
					}
				}
			}

			lengthValue = probl.criticalPath(job)
					* probl.instance.getLevels(job).length;

			break;
		}

		return lengthValue;
	}

	public double EstimatorFunction(JobsScheduling_real probl, int job, int cn) {
		double estValue = 0.0;

		int totalReqCores = 0;

		switch (estimator) {
		case 0:
			// simple
			estValue = (LengthMetric(probl, job) / probl.instance.CNSpeed[cn]);
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			estValue = (LengthMetric(probl, job) / (probl.instance.CNSpeed[cn]
					* probl.instance.CNNbProc[cn] * probl.instance.CNCores[cn]));

			break;
		}

		return estValue;
	}

	// RoundRobin taking into account that tasks can be executed in the
	// corresponding cluster
	@Override
	public ArrayList<ArrayList<Integer>> schedule(JobsScheduling_real probl) {
		
		JobSchedInstance instance = ((JobsScheduling_real) probl).instance;

		// Uso el comparator que tenía para ordenar por camino crítico
		// El CN no importa para el camino crítico (solamente para el estimados de Javid)
		Comparator<Integer> critPathSorting = new JobComparator(probl, -1,
				SmartScheduler.SORTING_CPATH, SmartScheduler.SORTING_DSC);
		
		Integer[] jobs = new Integer[1000];
		for (int job=0; job < 1000; job++) {
			jobs[job] = job;
		}
		
		Arrays.sort(jobs, critPathSorting);

		ArrayList<ArrayList<Integer>> mapping = new ArrayList<ArrayList<Integer>>();
		
		// Estimation of the CT of the different CNs
		double[] estimatedCT = new double[instance.getNumberCNs()];
		
		for (int i = 0; i < instance.getNumberCNs(); i++) {
			mapping.add(new ArrayList<Integer>());
			estimatedCT[i] = 0.0;
		}
		
		for (int pos=0; pos < 1000; pos++) {
			Integer job;
			job = jobs[pos];
		
			int bestCN;
			bestCN = 0;
			
			double bestCT;
			bestCT = Double.MAX_VALUE;
			
			try {			
				if (instance.requiredProcs(job) <= instance.CNCores[0]) {
					bestCT = EstimatorFunction(probl, job, 0) + estimatedCT[0];
				} else {
					bestCT = Double.MAX_VALUE;
				}
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(-1);
			}

			for (int c = 1; c < instance.getNumberCNs(); c++) {
				try {
					
					if (instance.requiredProcs(job) <= instance.CNCores[c]) {
						double newCT;
						newCT = EstimatorFunction(probl, job, c)
								+ estimatedCT[c];

						if (newCT < bestCT) {
							bestCT = newCT;
							bestCN = c;
						}
					}
				} catch (JMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(-1);
				}
			}
			
			mapping.get(bestCN).add(job);
			estimatedCT[bestCN] += bestCT;
		}
		
		return mapping;
	}
}
