package jmetal.problems.jobsScheduling;


//import jmetal.util.JMException;

//import jmetal.base.variable.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.ArrayList;

import jmetal.base.solutionType.*;
import jmetal.base.variable.Int;
import jmetal.base.*;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.Matrix;
import jmetal.util.PseudoRandom;
import jmetal.util.ScheduleStrategy;

public class GenerateInstanceSergio {
	
		protected int	numberCNs_						= 0 ;
		protected int	numberJobs_						= 0 ;
		protected int	numberOfObjectivesByDefault_	= 3 ;
		
		protected static final String path_      = "JobsScheduling/"; ///< Base directory of the matrices instances
		
		protected boolean 	verbose 					= false;
		protected boolean 	verboseRd 					= false;
		protected boolean 	verboseAssignments			= false;
		
		protected boolean	includeDeadlines			= true; // includes the info about deadlines and penalization functions in the instance
			
//		protected JobSchedInstance instance;  ///< Stores the copy of ETC values
		//protected double[][] Comp_; ///< Stores the copy computation time values
		
		public final static int PENALIZATION_NO_EXECUTION = 1000000;
		
		public final static int NUMBER_PEN_FUNCTIONS = 3;
		
		public final static int NUMBER_PROCESSORS = 5;
		
		public final double[] GFLOPS 		= {7.20, 10.40, 11.72, 13.32, 17.93};
		public final double[] EnergyIdle 	= {75, 68, 76, 74, 102};
		public final double[] EnergyMax 	= {94, 109, 214, 131, 210};
		public final int[] NbCores 			= {1, 2, 4, 2, 6};
		
		public final static int SQRT = 0;
		public final static int SQR  = 1;
		public final static int LIN  = 2;
		
		public final static double RefGFLOPS = 7.20; // The speed of the slowest considered processor

		
		
//		protected class JobSchedInstance {
//		
//			// Job data
////			private int[] jobImportance = new int[numberJobs];			// The importance of every job
//			private int[] jobWidth				= null ;	// The width of every job
//			private int[] jobHeight				= null ;	// The height of every job
//			private int[][] jobLevelsStart		= null ;	// The task ID where a new level starts
//			private int[][] jobTasksLevel		= null ;	// the level every task belongs to
//			private int[][] jobTasksProc 		= null ;	// the number of processors every task requires
//			// I'll only store the execution time on the slowest machine
////			private int[][][] jobTasksTimes 	= null ;	// the times to execute the task with the different speed levels
//			//private int[][] jobTasksTimes 		= null ;	// the times to execute the task with the different speed levels
//			private double[][] jobTasksOps 		= null ;	// the number of operations of the task
//			private int[][] jobTasksStart 		= null ;	// Earliest Start time for every task
//			private int[][] jobTasksFinish 		= null ;	// Earliest Finishing time for every task
//			private int[][][] jobTasksInTask 	= null ;	// the input dependencies from other tasks
//			private int[][][] jobTasksOutTask 	= null ;	// the tasks with dependencies on the output of this one
//			
//			private int[] jobPenalFunct 		= null;		// The penalization function for every job
//			private int[] jobDeadlines			= null ;	// The deadline assigned to every job
//			
////			private int[] jobRequiredProcs		= null;
//			            
//			// CN data
//			private int[] CNNbProc 				= null;		// The number of processors in every CN
//			private int[] CNSpeed 				= null;		// The speed of processors in every CN. It is the number of GFLOPS / number of cores
//			private int[] CNCores 				= null;		// The number of cores of processors in every CN - NEW FEATURE!!
//			private int[] EnergyIdle 			= null;		// The energy consumed by processors in idle state in every CN - NEW FEATURE!!
//			private int[] EnergyMax 			= null;		// The energy consumed by processors in max computing state in every CN - NEW FEATURE!!
//			
//			public JobSchedInstance(String fileName){
//				
//				// Read instance from fileName
//				
//			}
//			
//			// returns the level every task belongs to
//			private int[] getLevels(int[][] TaskInTasks) {
//				
//				int numberOfTasks = TaskInTasks.length;
//				int[] levels = new int[numberOfTasks];
//				
//				// Initialize values for levels to -1
//				for (int i=0; i<numberOfTasks; i++) {
//					levels[i] = -1;
//				}
//					
//				boolean finished = false;
//				int task=0;
//				
//				// Find the first task
//				while (!finished) {
//					if (TaskInTasks[task].length == 0) {
//						finished = true;
//						levels[task] = 0;
//					}
//					task++;
//				}
//				
//				finished = false;
//				int processedTasks = 1;
//							
//				// Assign levels for all the other tasks
//				while (!finished){
//					for (int i=0; i<numberOfTasks; i++) {
//						// Get highest level of inTasks
//						int numberInTasks = TaskInTasks[i].length;
//						int highestLevelInTasks = -1;
//						for (int it=0; it<numberInTasks; it++) {
////							int localHighestLevel = -1;
//							if (levels[TaskInTasks[i][it]] == -1)
//								break;
//							else {
//								if (levels[TaskInTasks[i][it]] > highestLevelInTasks)
//									highestLevelInTasks = levels[TaskInTasks[i][it]];
//							}
//						}
//						if (highestLevelInTasks >= 0) {
//							processedTasks++;
//							levels[i] = highestLevelInTasks + 1;
//						}
//					}
//									
//					// End after assigning the corresponding level to every task
//					if (processedTasks == numberOfTasks)
//						finished = true;
//				}
//					
//				return levels;
//			}
//			
//			public int getNumberCNs() {
//				return CNNbProc.length;
//			}
//			
//			public int getNumberJobs() {
//				return jobWidth.length;
//			}
//			
////			/**
////			 *  Returns the maximum number of processors required by
////			 *  any level in the job 
////			 * @param job
////			 * @return
////			 * @throws JMException 
////			 */
////			
////			public int requiredProcs(int jobID) throws JMException{
////				int reqProcs = 0;
////				
////				//int job = (int) jobID.getValue();
////				//int job = (int) ((Permutation)jobID[0]).vector_[jobID] ;//.getValue();
////				
////				// We cannot use jobLevelStart because the levels in the instances are not correct
//////				for (int level = 0; level < jobLevelsStart[jobID].length; level++) {
//////					int procsInLevel = 0;
//////					
//////					// Compute the number of processors required by this level
//////					for (int l=0; l < jobTasksLevel[jobID].length; l++){
//////						if (jobTasksLevel[jobID][l] == level)
//////							procsInLevel+= jobTasksProc[jobID][l];
//////					}
//	////
//////					if (procsInLevel > reqProcs)
//////						reqProcs = procsInLevel;
//////				}
////				
////				ArrayList<Integer> procsInLevel = new ArrayList<Integer>();
//////				for (int i=0; i<10; i++)
//////					procsInLevel.add(null);
////				
////				for (int i=0; i<jobTasksLevel[jobID].length; i++) {
////					if (jobTasksLevel[jobID][i] >= procsInLevel.size()) {
////						int count = jobTasksLevel[jobID][i];
////						while (count<procsInLevel.size()) {
////							procsInLevel.add(null);
////							count++;
////						}
////						
////						//procsInLevel.add(jobTasksLevel[jobID][i], procsInLevel.get(jobTasksLevel[jobID][i]) + new Integer(jobTasksProc[jobID][i]));
////						procsInLevel.add(new Integer(jobTasksProc[jobID][i]));
////					}
////					else if (procsInLevel.get(jobTasksLevel[jobID][i]) == null)
////						procsInLevel.set(jobTasksLevel[jobID][i], new Integer(jobTasksProc[jobID][i])); 
////					else {
////						procsInLevel.set(jobTasksLevel[jobID][i], procsInLevel.get(jobTasksLevel[jobID][i]) + new Integer(jobTasksProc[jobID][i]));
////					}
////				}
////				
////				// Get the level with highest number of processors
////				Iterator<Integer> it = procsInLevel.iterator();
////				while (it.hasNext()) {
////					Integer procs = it.next();
////					if (procs.intValue() > reqProcs)
////						reqProcs = procs.intValue();
////					
////				}
////								
////				return reqProcs;
////			}
//			
//			/**
//			 *  Returns the maximum number of processors required by
//			 *  any task in the job 
//			 * @param job
//			 * @return
//			 * @throws JMException 
//			 */
//			
//			public int requiredProcs(int jobID) throws JMException{
//				int reqProcs = 0;
//				
//				int numberTasks = jobTasksProc[jobID].length;
//				
//				for (int i=0; i<numberTasks;i++) {
//					if (reqProcs < jobTasksProc[jobID][i])
//						reqProcs = jobTasksProc[jobID][i];
//				}
//												
//				return reqProcs;
//			}
//			
//			/**
//			 * Estimates the time to compute job in CN cn according to Javid's algorithm
//			 * @param job
//			 * @param cn
//			 * @return
//			 */
//			public int estimateTime(int job, int cn){
//				double execTime = 0.0;
//				
//				//int numberOfTasks = jobTasksTimes[job].length;
//				int numberOfTasks = jobTasksOps[job].length;
//				
//				for (int k=0; k<numberOfTasks; k++) {
//					//execTime += (double)jobTasksTimes[job][k][CNSpeed[cn]-1] * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
//					//execTime += Math.ceil((double)jobTasksTimes[job][k] / (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
//					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
//				}
//				
//				execTime = Math.ceil(execTime/(double)CNNbProc[cn]);
////				try {
////					execTime = Math.ceil(execTime/(double)requiredProcs(job));
////				} catch (JMException e) {
////					e.printStackTrace();
////				}
//				return (int)execTime;
//			}
//			
//			
//			/**
//			 * Estimates the time to compute job in the slowest CN 
//			 * available in the problem instance according to Javid's algorithm
//			 * @param job
//			 * @param cn
//			 * @return
//			 */
//			public int estimateTimeSlowestAvailableMachine(int job){
//				double execTime = 0.0;
//				
//				int numberOfTasks = jobTasksOps[job].length;
//				
//				int slowestMachine = 0;
//				//int speedSlowestMachine = CNSpeed[0]-1; // Speed goes from 1 to 10
//				int speedSlowestMachine = CNSpeed[0]; // GFLOPS per core in machine 0
//				int len = CNSpeed.length;
//				
//				for (int i=0; i<len; i++) {
//					if (CNSpeed[i] < speedSlowestMachine) {
//						//speedSlowestMachine = CNSpeed[i]-1; // Speed goes from 1 to 10
//						speedSlowestMachine = CNSpeed[i];
//						slowestMachine = i;
//					}
//				}
//				
//				for (int k=0; k<numberOfTasks; k++) {
//					//execTime += (double)jobTasksTimes[job][k][speedSlowestMachine] * (double)jobTasksProc[job][k];
//					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[slowestMachine]) * (double)jobTasksProc[job][k];
//				}
//				
////				try {
////					// The time to compute a job in the slowest machine is computed as
////					// the time to sequentially run it in the slowest machine
////					// over the number of processors required by the job
////					execTime = Math.ceil(execTime/(double)requiredProcs(job));
////				} catch (JMException e) {
////					e.printStackTrace();
////				}
//				execTime = Math.ceil(execTime/(double)CNNbProc[slowestMachine]);
//				
//				return (int)execTime;
//			}
//			
//			
//			/**
//			 * Estimates the time to compute job in the fastest CN 
//			 * available in the problem instance according to Javid's algorithm
//			 * @param job
//			 * @param cn
//			 * @return
//			 */
//			public int estimateTimeFastestAvailableMachine(int job){
//				double execTime = 0.0;
//				
//				//int numberOfTasks = jobTasksTimes[job].length;
//				int numberOfTasks = jobTasksOps[job].length;
//				
//				int fastestMachine = 0;
//				//int speedFastestMachine = CNSpeed[0]-1; // Speed goes from 1 to 10
//				int speedFastestMachine = CNSpeed[0];
//				int len = CNSpeed.length;
//				
//				for (int i=0; i<len; i++) {
//					if (CNSpeed[i] > speedFastestMachine) {
//						//speedFastestMachine = CNSpeed[i]-1; // Speed goes from 1 to 10
//						speedFastestMachine = CNSpeed[i];
//						fastestMachine = i;
//					}
//				}
//				
//				for (int k=0; k<numberOfTasks; k++) {
////					execTime += (double)jobTasksTimes[job][k][speedFastestMachine] * (double)jobTasksProc[job][k];
//					//execTime += Math.ceil((double)jobTasksTimes[job][k] / (double)CNSpeed[fastestMachine]) * (double)jobTasksProc[job][k];
//					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[fastestMachine]) * (double)jobTasksProc[job][k];
//				}
//				
////				try {
////					// The time to compute a job in the slowest machine is computed as
////					// the time to sequentially run it in the slowest machine
////					// over the number of processors required by the job
////					execTime = Math.ceil(execTime/(double)requiredProcs(job));
////				} catch (JMException e) {
////					e.printStackTrace();
////				}
//				execTime = Math.ceil(execTime/(double)CNNbProc[fastestMachine]);
//				
//				return (int)execTime;
//			}
//		}

		
		// Job data
//		private int[] jobImportance = new int[numberJobs];			// The importance of every job
		private int[] jobWidth				= null ;	// The width of every job
		private int[] jobHeight				= null ;	// The height of every job
		private int[][] jobLevelsStart		= null ;	// The task ID where a new level starts
		private int[][] jobTasksLevel		= null ;	// the level every task belongs to
		private int[][] jobTasksProc 		= null ;	// the number of processors every task requires
		// I'll only store the execution time on the slowest machine
//		private int[][][] jobTasksTimes 	= null ;	// the times to execute the task with the different speed levels
		//private int[][] jobTasksTimes 		= null ;	// the times to execute the task with the different speed levels
		private double[][] jobTasksOps 		= null ;	// the number of operations of the task
//		private int[][] jobTasksStart 		= null ;	// Earliest Start time for every task
//		private int[][] jobTasksFinish 		= null ;	// Earliest Finishing time for every task
		private int[][][] jobTasksInTask 	= null ;	// the input dependencies from other tasks
		private int[][][] jobTasksOutTask 	= null ;	// the tasks with dependencies on the output of this one
		
		private int[] jobPriority 			= null;		// The priority function for every job
		private int[] jobPenalFunct			= null;		// The penalization function for every job
		private int[] jobDeadlines			= null ;	// The deadline assigned to every job
		
//		private int[] jobRequiredProcs		= null;
		            
		// Computing Node (CN) data
		private int[] CNNbProc 				= null;		// The number of processors in every CN
		private double[] CNSpeed			= null;		// The speed of processors in every CN. It is the number of GFLOPS / number of cores
		private int[] CNCores 				= null;		// The number of cores of processors in every CN - NEW FEATURE!!
		private double[] CNEnergyIdle 		= null;		// The energy consumed by processors in idle state in every CN - NEW FEATURE!!
		private double[] CNEnergyMax 			= null;		// The energy consumed by processors in max computing state in every CN - NEW FEATURE!!
		
		private void readInstance(String fileName){
			
			// Read instance from fileName
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			System.out.println("Reading instance: " + fileName);
			String aux;
			try {
				aux = br.readLine(); // Avoid first line
				aux = br.readLine();
				StringTokenizer st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f"); 
				
				st.nextToken();
//				st.nextToken();
				int numberJobs = (new Integer(st.nextToken())).intValue();
				
				if (verboseRd)
					System.out.println("number of jobs = " + numberJobs);
				
				// Job data
//				jobImportance = new int[numberJobs];			// The importance of every job
				jobWidth = new int[numberJobs];				// The width of every job
				jobHeight = new int[numberJobs];			// The height of every job
				jobLevelsStart = new int[numberJobs][];		// The task ID where a new level starts
				jobTasksLevel = new int[numberJobs][];		// the level every task belongs to
				jobTasksProc = new int[numberJobs][];		// the number of processors every task requires
				
				jobPriority = new int[numberJobs];			// The priority assigned to every job
				jobPenalFunct = new int[numberJobs];		// The penalization function of every job
				
				// I'll only store the execution time on the slowest machine
//				jobTasksTimes = new int[numberJobs][][];	// the times to execute the task with the different speed levels
				//jobTasksTimes = new int[numberJobs][];	// the times to execute the task with the different speed levels
				jobTasksOps = new double[numberJobs][];		// the number of operations of the task
//				jobTasksStart = new int[numberJobs][];		// Earliest Start time for every task
//				jobTasksFinish = new int[numberJobs][];		// Earliest Finishing time for every task
				jobTasksInTask = new int[numberJobs][][];	// the input dependencies from other tasks
				jobTasksOutTask = new int[numberJobs][][];	// the tasks with dependencies on the output of this one
				
				jobPenalFunct = new int[numberJobs];		// The penalization function associated to every job
				jobDeadlines  = new int[numberJobs];		// The deadline assigned to every job
				int [] jobImportance = new int[numberJobs];	// The importance assigned to every job
				
				// TODO Change jobPenalFunct and jobImportance; these values must be specified by the instance,
				//      but for the moment they are randomly set
//				for (int i=0; i<numberJobs; i++) {
//					jobPenalFunct[i] = PseudoRandom.randInt(0,NUMBER_PEN_FUNCTIONS);
//					jobImportance[i] = PseudoRandom.randInt(1, 10);
////					jobImportance[i] = 10;
//				}
				
				// Read the information of every job
				for (int i=0; i<numberJobs; i++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					st.nextToken(); // "Job"
					int id =  (new Integer(st.nextToken())).intValue(); // Job ID
					
					if (verboseRd)
						System.out.println("Reading Job " + id);

					st.nextToken(); // User Owner
					st.nextToken(); // Nb procs.
					st.nextToken(); // Job value
					st.nextToken(); // Job importance
					
					jobPriority[id]  = (new Integer(st.nextToken())).intValue(); // Job priority
					jobPenalFunct[id]  = (new Integer(st.nextToken())).intValue(); // Job penalization function					
					
//					jobImportance[id] = (new Integer(st.nextToken())).intValue(); // Job Importance
					
//					System.out.println("\t Importance " + jobImportance[id]);
					
					st.nextToken(); // TmsToExe Job
					String next = st.nextToken(); 
					while (!next.equalsIgnoreCase("TmsToExe"))
						next = st.nextToken();
					
					// 7 parameters we do not mind
					for (int j=0; j<7; j++) {
						st.nextToken();
					}
					jobWidth[id]  = (new Integer(st.nextToken())).intValue(); // Job Width
					jobHeight[id] = (new Integer(st.nextToken())).intValue(); // Job Height
					
					if (verboseRd) {
						System.out.println("\t Width " + jobWidth[id]);
						System.out.println("\t Height " + jobHeight[id]);
					}
					
					// Read TaskLevels
					next = st.nextToken();

					int levels = (new Integer(st.nextToken())).intValue();
					if (verboseRd)
						System.out.print("\t Task levels (" + levels+ "): ");
					jobLevelsStart[id] = new int[levels];
					for (int j=0; j<levels; j++) {
						jobLevelsStart[id][j] = (new Integer(st.nextToken())).intValue();
						if (verboseRd)
							System.out.print(jobLevelsStart[id][j] + ", ");
					}
					if (verboseRd)
						System.out.println();
					next = st.nextToken();
					
					// Read Tasks
					next = st.nextToken();
					int numberOfTasks = (new Integer(st.nextToken())).intValue(); // Number of Tasks in this Job
					if (verboseRd)
						System.out.println("\t Number of tasks: " + numberOfTasks);
					
					jobTasksLevel[id] = new int[numberOfTasks];
					jobTasksProc[id]  = new int[numberOfTasks];
					// I'll only store the execution time on the slowest machine
//					jobTasksTimes[id] = new int[numberOfTasks][];
					//jobTasksTimes[id] = new int[numberOfTasks];
					jobTasksOps[id] =  new double[numberOfTasks];
//					jobTasksStart[id] = new int[numberOfTasks];
//					jobTasksFinish[id] = new int[numberOfTasks];
					jobTasksInTask[id] = new int[numberOfTasks][];
					jobTasksOutTask[id] = new int[numberOfTasks][];
					
					// Read the information for every task
					for (int j=0; j<numberOfTasks; j++){
						next = st.nextToken();
						int taskId = (new Integer(st.nextToken())).intValue(); // Task ID
						// TODO: Forget about the task levels given by the instance, and compute them after the 
						//       loop when the input and output tasks for all the instances is known
//						jobTasksLevel[id][taskId] = (new Integer(st.nextToken())).intValue(); // Task Level
						st.nextToken();
						
						jobTasksProc[id][taskId] = (new Integer(st.nextToken())).intValue(); // Processors required by the task
						if (verboseRd) {
							System.out.println("\t Task: " + taskId);
//							System.out.println("\t\t Level: " + jobTasksLevel[id][taskId]);
							System.out.println("\t\t Parallel Processors: " + jobTasksProc[id][taskId]);
						}
						
						// Read Times to Execute the Task
						next = st.nextToken();
						int times = (new Integer(st.nextToken())).intValue();// - 1;
						times--;
						st.nextToken(); // Avoid first 0
						
						// Instead of storing in memory the times to execute the tasks in every machine,
						// I will compute it every time I need it
//						jobTasksTimes[id][taskId] = new int[times];
//						if (verboseRd)
//							System.out.print("\t\t Times to execute: ");
//						for (int k=0; k<times; k++) {
//							jobTasksTimes[id][taskId][k] = (new Integer(st.nextToken())).intValue();
//							if (verboseRd)
//								System.out.print(jobTasksTimes[id][taskId][k]+ ", ");
//						}
						//jobTasksTimes[id][taskId] = (new Integer(st.nextToken())).intValue();
						// The number of operations of the task is computed as the time at reference speed 1 (slowest processor in Javid's instance)
						//  multiplied by the GFLOPS of the slowest processor (among the ones I selected)
						jobTasksOps[id][taskId] = (new Integer(st.nextToken())).intValue() * RefGFLOPS; 
						for (int k=0; k<times-1; k++) 
							st.nextToken();
	
						
						if (verboseRd)
							System.out.println();
						st.nextToken();
						//jobTasksStart[id][taskId] = (new Integer(st.nextToken())).intValue();
						st.nextToken();
						
						st.nextToken();
						//jobTasksFinish[id][taskId] = (new Integer(st.nextToken())).intValue();
						st.nextToken();
						
//						if (verboseRd) {
//							System.out.println("\t\t Earliest Start time: " + jobTasksStart[id][taskId]);
//							System.out.println("\t\t Earliest Finishing time: " + jobTasksFinish[id][taskId]);
//						}
						
						st.nextToken(); // Procs
						st.nextToken();
						st.nextToken();
						
						// Read InTasks (tasks whose output are the input of this one)
						st.nextToken();
						int intasks = (new Integer(st.nextToken())).intValue();
						if (verboseRd)
							System.out.print("\t\t Tasks from which I need input: (" + intasks + "): ");

						jobTasksInTask[id][taskId] = new int[intasks];
						for (int k=0; k<intasks; k++) {
							jobTasksInTask[id][taskId][k] = (new Integer(st.nextToken())).intValue();
							if (verboseRd)
								System.out.print(jobTasksInTask[id][taskId][k] + ", ");
						}
						if (verboseRd)
							System.out.println();
						st.nextToken();
						
						// Read OutTasks
						st.nextToken();
						int outtasks = (new Integer(st.nextToken())).intValue();
						if (verboseRd)
							System.out.print("\t\t Tasks depending on the output: (" + outtasks + "): ");
						jobTasksOutTask[id][taskId] = new int[outtasks];
						for (int k=0; k<outtasks; k++) {
							jobTasksOutTask[id][taskId][k] = (new Integer(st.nextToken())).intValue();
							if (verboseRd)
								System.out.print(jobTasksOutTask[id][taskId][k] + ", ");
						}
						if (verboseRd)
							System.out.println();
						st.nextToken();
						st.nextToken();
						
					}
				}

				// Compute the task's levels
				for (int job=0; job<numberJobs; job++) {
//					int numberOfTasks = jobTasksLevel[job].length;
//					for (int task=0; task<numberOfTasks; task++) {
//						jobTasksLevel[job][task] = getHigestLevel(jobTasksInTask[job][task]);
//					}
					jobTasksLevel[job] = getLevels(jobTasksInTask[job]);
				}
								
				
				
				// All jobs have been read at this point
				
				// Now, read the information related to the CNs
				// Go to the "Computational Nodes" section, ignoring all the intermediate data
				do {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
				} while (!st.nextToken().equalsIgnoreCase("Computational"));
				
				st.nextToken();
				
				int numberCNs = (new Integer(st.nextToken())).intValue();
				if (verboseRd)
					System.out.println("Number of CNs : " + numberCNs);
				
				CNNbProc = new int[numberCNs];
				CNSpeed = new double[numberCNs];
				CNCores = new int[numberCNs];
				CNEnergyIdle = new double[numberCNs];
				CNEnergyMax = new double[numberCNs];
				
				for (int k=0; k<numberCNs; k++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					st.nextToken();
					int id = (new Integer(st.nextToken())).intValue();
					CNNbProc[id] = (new Integer(st.nextToken())).intValue();
					CNSpeed[id] = (new Double(st.nextToken())).doubleValue();
					if (verboseRd) {
						System.out.println("\t Computing Node "+id + ":");
						System.out.println("\t\t Number of Processors: "+CNNbProc[id]);
						System.out.println("\t\t Speed of Processors: " +CNSpeed[id]);
					}
					
					// 3 values we do not care about
					st.nextToken();
					st.nextToken();
					st.nextToken();
					
					CNCores[id] = (new Integer(st.nextToken())).intValue();
					CNEnergyIdle[id] = (new Double(st.nextToken())).doubleValue();
					CNEnergyMax[id] = (new Double(st.nextToken())).doubleValue();
				}
				 
				for(int i=0; i<numberJobs;i++) {
					boolean warning = true;
					try {
					
						for (int j=0; j<numberCNs;j++) {
								if (requiredProcs(i) < CNNbProc[j])
									warning = false;
						}
						
						if (warning)
								System.out.println("WARNING: None of the CNs can execute Job " + i + ". It requires " + requiredProcs(i) + " processors.");

					} catch (JMException e) {
						e.printStackTrace();
					}

				}
				
				// Compute the deadlines for the jobs
				for (int i=0; i<numberJobs; i++)
					// Using as reference speed 1
					// jobDeadlines[i] = jobImportance[i] * estimateTimeSlowestMachine(i);
					// Using as reference speed of the slowest machine in the system
					//jobDeadlines[i] = jobImportance[i] * estimateTimeSlowestAvailableMachine(i);
					// Using as reference speed of the fastest machine in the system, 1 is lowest importance
					//jobDeadlines[i] = (11-jobImportance[i]) * estimateTimeFastestAvailableMachine(i);
					jobDeadlines[i] = (11-jobPriority[i]) * estimateTimeSlowestAvailableMachine(i);
				
//				System.exit(-1);
				// System.out.println("Number of objectives: " +
				// numberObjectives);
//	
//				while (aux != null) {
//					
//					aux = br.readLine();
//				
//				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// returns the level every task belongs to
		private int[] getLevels(int[][] TaskInTasks) {
			
			int numberOfTasks = TaskInTasks.length;
			int[] levels = new int[numberOfTasks];
			
			// Initialize values for levels to -1
			for (int i=0; i<numberOfTasks; i++) {
				levels[i] = -1;
			}
				
			boolean finished = false;
			int task=0;
			
			// Find the first task
			while (!finished) {
				if (TaskInTasks[task].length == 0) {
					finished = true;
					levels[task] = 0;
				}
				task++;
			}
			
			finished = false;
			int processedTasks = 1;
						
			// Assign levels for all the other tasks
			while (!finished){
				for (int i=0; i<numberOfTasks; i++) {
					// Get highest level of inTasks
					int numberInTasks = TaskInTasks[i].length;
					int highestLevelInTasks = -1;
					for (int it=0; it<numberInTasks; it++) {
//						int localHighestLevel = -1;
						if (levels[TaskInTasks[i][it]] == -1)
							break;
						else {
							if (levels[TaskInTasks[i][it]] > highestLevelInTasks)
								highestLevelInTasks = levels[TaskInTasks[i][it]];
						}
					}
					if (highestLevelInTasks >= 0) {
						processedTasks++;
						levels[i] = highestLevelInTasks + 1;
					}
				}
								
				// End after assigning the corresponding level to every task
				if (processedTasks == numberOfTasks)
					finished = true;
			}
				
			return levels;
		}
		
		/**
		 * Estimates the time to compute job in the slowest CN 
		 * available in the problem instance according to Javid's algorithm
		 * @param job
		 * @param cn
		 * @return
		 */
		public int estimateTimeSlowestAvailableMachine(int job){
			double execTime = 0.0;
			
			int numberOfTasks = jobTasksOps[job].length;
			
			int slowestMachine = 0;
			//int speedSlowestMachine = CNSpeed[0]-1; // Speed goes from 1 to 10
			double speedSlowestMachine = CNSpeed[0]; // GFLOPS per core in machine 0
			int len = CNSpeed.length;
			
			for (int i=0; i<len; i++) {
				if (CNSpeed[i] < speedSlowestMachine) {
					//speedSlowestMachine = CNSpeed[i]-1; // Speed goes from 1 to 10
					speedSlowestMachine = CNSpeed[i];
					slowestMachine = i;
				}
			}
			
			for (int k=0; k<numberOfTasks; k++) {
				//execTime += (double)jobTasksTimes[job][k][speedSlowestMachine] * (double)jobTasksProc[job][k];
				execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[slowestMachine]) * (double)jobTasksProc[job][k];
			}
			
//			try {
//				// The time to compute a job in the slowest machine is computed as
//				// the time to sequentially run it in the slowest machine
//				// over the number of processors required by the job
//				execTime = Math.ceil(execTime/(double)requiredProcs(job));
//			} catch (JMException e) {
//				e.printStackTrace();
//			}
			execTime = Math.ceil(execTime/(double)CNNbProc[slowestMachine]);
			
			return (int)execTime;
		}
		
		/**
		 *  Returns the maximum number of processors required by
		 *  any task in the job 
		 * @param job
		 * @return
		 * @throws JMException 
		 */
		
		public int requiredProcs(int jobID) throws JMException{
			int reqProcs = 0;
			
			int numberTasks = jobTasksProc[jobID].length;
			
			for (int i=0; i<numberTasks;i++) {
				if (reqProcs < jobTasksProc[jobID][i])
					reqProcs = jobTasksProc[jobID][i];
			}
											
			return reqProcs;
		}
		
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		FileOutputStream fos = null;
		FileOutputStream fosrev = null;
		
		if (args.length != 2) {
			System.out.println("USE: java GenerateInstance InstanceName NumberInstances");
			System.exit(-1);
		}
		
		try {
			
			for (int inst=0; inst<(new Integer(args[1])).intValue(); inst++) {
				GenerateInstanceSergio gi = new GenerateInstanceSergio();
				
				String instance = "";
				
				if (inst+1>=10)
					instance = args[0] + "-"+ new Integer(inst+1).toString()+".ssfMod";
				else
					instance = args[0] + "-0"+ new Integer(inst+1).toString()+".ssfMod";
				
				gi.readInstance(instance);
				
				fos = new FileOutputStream(instance+"Sergio");
				fosrev = new FileOutputStream(instance+"Sergio.rev");
			
				PrintWriter res = new PrintWriter(fos);
				PrintWriter resrev = new PrintWriter(fosrev);
				
				int numberJobs = gi.jobDeadlines.length; 
				
	//			System.out.println("Numero de jobs: " + numberJobs);
				// Everything is read, now write the data in the required format:
				for (int i=0; i<numberJobs;i++) {
					
	//				System.out.println("Procesando job: " + i);
					
					
					DecimalFormat df = new DecimalFormat("0.00");
					DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
					dfs.setDecimalSeparator('.');
					df.setDecimalFormatSymbols(dfs);
					
					int numberOfTasks = gi.jobTasksOps[i].length;
					res.print(numberOfTasks); // Print number of tasks
					res.print(" " + gi.jobDeadlines[i]); // Print the deadline of the job
					res.print(" " + gi.jobPenalFunct[i]); // Print cost of job
					
					resrev.print(numberOfTasks); // Print number of tasks
					resrev.print(" " + gi.jobDeadlines[i]); // Print the deadline of the job
					resrev.print(" " + gi.jobPenalFunct[i]); // Print cost of job
					
	//				System.out.print(numberOfTasks + " "); // Print number of tasks
	//				System.out.print(gi.jobDeadlines[i] + " "); // Print the deadline of the job
	//				System.out.print(gi.jobPenalFunct[i] + " "); // Print cost of job
					
					for (int j=0; j<numberOfTasks; j++) {
						res.print(" " + df.format(new Double(gi.jobTasksOps[i][j]))); // Print tasks length
						res.print(" " + gi.jobTasksProc[i][j]); // Print required cores of the task
						
	//					System.out.print(gi.jobTasksOps[i][j] + " "); // Print tasks length
	//					System.out.print(gi.jobTasksProc[i][j] + " "); // Print required cores of the task
						
						int numberParents = gi.jobTasksInTask[i][j].length; // number of precedent tasks
						for(int k=0; k<numberParents; k++) {
							res.print(" " + gi.jobTasksInTask[i][j][k]); // Print the precedent tasks
	//						System.out.print(gi.jobTasksInTask[i][j][k] + " "); // Print the precedent tasks
						}
						if (j<numberOfTasks-1)
							res.print(" -1"); // print separator
	//					System.out.print("-1 "); // print separator
					}
					
					// Reverse order
					for (int j=numberOfTasks-1; j>=0; j--) {
						resrev.print(" " + df.format(new Double(gi.jobTasksOps[i][j]))); // Print tasks length
						resrev.print(" " + gi.jobTasksProc[i][j]); // Print required cores of the task
						
	//					System.out.print(gi.jobTasksOps[i][j] + " "); // Print tasks length
	//					System.out.print(gi.jobTasksProc[i][j] + " "); // Print required cores of the task
						
						int numberParents = gi.jobTasksInTask[i][j].length; // number of precedent tasks
						for(int k=0; k<numberParents; k++) {
							resrev.print(" " + gi.jobTasksInTask[i][j][k]); // Print the precedent tasks
	//						System.out.print(gi.jobTasksInTask[i][j][k] + " "); // Print the precedent tasks
						}
						if (j>0)
							resrev.print(" -1"); // print separator
	//					System.out.print("-1 "); // print separator
					}
					
					res.println();
					
					resrev.println();
	//				System.out.println();
				}
				
				res.close();
				resrev.close();
				
				fos = new FileOutputStream(instance+"CNsSergio");
				res = new PrintWriter(fos);
				
				int numberCNs = gi.CNCores.length;
				for (int i=0; i<numberCNs; i++) {
					res.print(gi.CNNbProc[i] + " "); // Number of processors in CN
					res.print(gi.CNCores[i] + " "); // Number of cores per processor in CN
					res.print(gi.CNSpeed[i] + " "); // Speed of processors in CN
					res.print(gi.CNEnergyIdle[i] + " "); // Idle energy consumption of processors in CN
					res.print(gi.CNEnergyMax[i] + "\n"); // Speed of processors in CN
				}
				
				/*
				 * // Job data
	//		private int[] jobImportance = new int[numberJobs];			// The importance of every job
			private int[] jobWidth				= null ;	// The width of every job
			private int[] jobHeight				= null ;	// The height of every job
			private int[][] jobLevelsStart		= null ;	// The task ID where a new level starts
			private int[][] jobTasksLevel		= null ;	// the level every task belongs to
			private int[][] jobTasksProc 		= null ;	// the number of processors every task requires
			// I'll only store the execution time on the slowest machine
	//		private int[][][] jobTasksTimes 	= null ;	// the times to execute the task with the different speed levels
			//private int[][] jobTasksTimes 		= null ;	// the times to execute the task with the different speed levels
			private double[][] jobTasksOps 		= null ;	// the number of operations of the task
			private int[][] jobTasksStart 		= null ;	// Earliest Start time for every task
			private int[][] jobTasksFinish 		= null ;	// Earliest Finishing time for every task
			private int[][][] jobTasksInTask 	= null ;	// the input dependencies from other tasks
			private int[][][] jobTasksOutTask 	= null ;	// the tasks with dependencies on the output of this one
			
			private int[] jobPriority 		= null;		// The penalization function for every job
			private int[] jobPenalFunct 		= null;		// The penalization function for every job
			private int[] jobDeadlines			= null ;	// The deadline assigned to every job
			
	//		private int[] jobRequiredProcs		= null;
			            
			// CN data
			private int[] CNNbProc 				= null;		// The number of processors in every CN
			private int[] CNSpeed 				= null;		// The speed of processors in every CN. It is the number of GFLOPS / number of cores
			private int[] CNCores 				= null;		// The number of cores of processors in every CN - NEW FEATURE!!
	//		private int[] EnergyIdle 			= null;		// The energy consumed by processors in idle state in every CN - NEW FEATURE!!
	//		private int[] EnergyMax 			= null;		// The energy consumed by processors in max computing state in every CN - NEW FEATURE!!
			
				 * */
				
				res.close();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
