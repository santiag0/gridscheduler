package jmetal.problems.jobsScheduling;


//import jmetal.base.Configuration.SolutionType_;
//import jmetal.base.Configuration.VariableType_;
import jmetal.base.Problem;
import jmetal.base.solutionType.*;
import jmetal.base.*;
import jmetal.util.JMException;


public class Mix extends JobsScheduling {

	private static final String path_      = "JobsScheduling/"; ///< Base directory of the matrices instances
	private static final String name_      = "Mix";               ///< Name of the problem
//	private static final double per_       = 0.3;                      ///< Constant used in the matrix

//	protected	int		numberCNs_		=   0 ;
//	protected	int		numberJobs_		=   0 ;
	
	private int instanceNo_ = 0;
	
	/** Constructor
	 * @param numberOfVariables Number of variables of the problem 
	 * @param solutionType The solution type must "Int"
	 * @param numberOfIslands Number of islands to use, must be greater than 1
	 * @throws ClassNotFoundException 
	 */
//	public Small( Integer numberOfVariables, String solutionType, Integer numberOfIslands ) throws ClassNotFoundException {
//
////		M_ = new Matrix();
//		//M_.recoverXMLData( xmlPath_+xmlMatrix_ , per_ );
////		M_.recoverData(512, 16, path_+name_+"."+instanceNo_ );
//		
////		ETC_  = M_.getETCmatrix();
//
//		int numberOfVars  = 100; //M_.getNumberOfTasks() ;
//		int numberOfMachs = 10; //M_.getNumberOfMachines();
//		
//		numberOfVariables_ = 110;
//		
////		numberOfTasks_       = numberOfVars;
////		numberOfMachines_    = ( numberOfMachs              > 0         )? numberOfMachs              : numberOfMachinesByDefault_  ;
////		numberOfVariables_   = ( numberOfVars               > 0         )? numberOfVars               : numberOfVariablesByDefault_ ;
//		
//		numberOfObjectives_  = numberOfObjectivesByDefault_ ;
//		numberOfConstraints_ = 0                            ;
//		problemName_         = name_                        ;
//
//		upperLimit_ = new double[ numberOfVariables_ ];
//		lowerLimit_ = new double[ numberOfVariables_ ];
//
//		for( int i=0; i<numberOfVariables_ ; i++ ) {
//			lowerLimit_[i] = 0;
//			upperLimit_[i] = 110; //numberOfMachines_;
//		} // for
//
////		solutionType_ = Enum.valueOf( SolutionType_.class , solutionType );
//		if (solutionType.compareTo("BinaryReal") == 0)
//		      solutionType_ = new BinaryRealSolutionType(this) ;
//		    else if (solutionType.compareTo("Real") == 0)
//		    	solutionType_ = new RealSolutionType(this) ;
//		    else if (solutionType.compareTo("Int") == 0)
//		    	solutionType_ = new IntSolutionType(this) ;
//		    else {
//		    	System.out.println("Error: solution type " + solutionType + " invalid") ;
//		    	System.exit(-1) ;
//		    }
//
//		// All the variables are of the same type, so the solutionType name is the
//		// same than the variableType name
////		variableType_ = new VariableType_[numberOfVariables_];
////		for( int var=0 ; var<numberOfVariables_ ; ++var )
////			variableType_[var] = Enum.valueOf( VariableType_.class , solutionType );    
//
//	} // Small

	/** Constructor.
	 * Creates a new instance of the MO_Scheduling problem.
	 * 
	 * @param numberOfVariables Number of variables of the problem 
	 * @param solutionType The solution type must "Int"
	 */
	public Mix( String solutionType , Integer instanceNumber) throws ClassNotFoundException {
		
		super();
		
		instance = new JobSchedInstance(path_+name_+"."+instanceNo_);
		
//		int numberOfTasks  = instance.getNumberOfTasks() ;
//		int numberOfCNs    = instance.getNumberOfMachines();
//		
//		numberOfVariables_	= numberOfTasks + numberOfCNs;
		problemName_         = name_                        ;

//		upperLimit_ = new double[ numberOfVariables_ ];
//		lowerLimit_ = new double[ numberOfVariables_ ];
//
//		for( int i=0; i<numberOfVariables_ ; i++ ) {
//			lowerLimit_[i] = 0;
//			upperLimit_[i] = numberOfVariables_;
//		} // for

//		solutionType_ = Enum.valueOf( SolutionType_.class , solutionType );
		if (solutionType.compareTo("Permutation") == 0)
		      solutionType_ = new PermutationSolutionType(this) ;
		    else {
		    	System.out.println("Error: solution type " + solutionType + " invalid") ;
		    	System.exit(-1) ;
		    }

	} // Small
	
	/** Constructor
	 * @param solutionType The solution type must "Int"
	 * @throws ClassNotFoundException 
	 * @throws JMException 
	 */
	public Mix( String solutionType ) throws ClassNotFoundException, JMException {

		//this( numberOfVariablesByDefault_ , solutionType , numberOfIslandsByDefault_ );
		//this( 110 , solutionType , 1 );
super();
		
		instance = new JobSchedInstance(path_+name_+".ssfMod");
		
		if (verbose)
			for (int i=0; i<instance.getNumberJobs(); i++)
				System.out.println("Job " + i + " requires " + instance.requiredProcs(i) + " processors");

		
		numberJobs_  = instance.getNumberJobs() ;
		numberCNs_    = instance.getNumberCNs();
		
//		int numberOfTasks  = 100 ;
//		int numberOfCNs    = 20 ;
//		
		numberOfVariables_	= 1;  // We have one single variable that is a permutation of numberOfJobs + numberOfCNs numbers
		length_ = new int[1];
		length_[0] = numberJobs_ + numberCNs_ - 1;
		
		problemName_         = name_                        ;

		upperLimit_ = new double[ numberOfVariables_ ];
		lowerLimit_ = new double[ numberOfVariables_ ];

		for( int i=0; i<numberOfVariables_ ; i++ ) {
			lowerLimit_[i] = 0;
			upperLimit_[i] = numberOfVariables_;
		} // for

//		solutionType_ = Enum.valueOf( SolutionType_.class , solutionType );
		if (solutionType.compareTo("Permutation") == 0)
		      solutionType_ = new PermutationSolutionType(this) ;
		    else {
		    	System.out.println("Error: solution type " + solutionType + " invalid") ;
		    	System.exit(-1) ;
		    }
	} // Small

//	public Small( String solutionType, Integer instanceNumber) throws ClassNotFoundException {
////		this( numberOfVariablesByDefault_ , solutionType , numberOfIslandsByDefault_ );
//		instanceNo_ = instanceNumber.intValue();
////		M_ = new Matrix();
//		//M_.recoverXMLData( xmlPath_+xmlMatrix_ , per_ );
////		M_.recoverData(512, 16, path_+name_+"."+instanceNo_ );
//		
////		ETC_  = M_.getETCmatrix();
//
//		System.out.println("Reading file_ " + path_+name_+"."+instanceNo_);
//
//		int numberOfVars  = 110;//M_.getNumberOfTasks() ;
//		int numberOfMachs = 10;//M_.getNumberOfMachines();
//		
//		numberOfVariables_ = 110;
//		
////		numberOfTasks_       = numberOfVars;
////		numberOfMachines_    = ( numberOfMachs              > 0         )? numberOfMachs              : numberOfMachinesByDefault_  ;
////		numberOfVariables_   = ( numberOfVars               > 0         )? numberOfVars               : numberOfVariablesByDefault_ ;
//		
//		numberOfObjectives_  = numberOfObjectivesByDefault_ ;
//		numberOfConstraints_ = 0                            ;
//		problemName_         = name_                        ;
//
//		upperLimit_ = new double[ numberOfVariables_ ];
//		lowerLimit_ = new double[ numberOfVariables_ ];
//
//		for( int i=0; i<numberOfVariables_ ; i++ ) {
//			lowerLimit_[i] = 0;
//			upperLimit_[i] = 110;//numberOfMachines_;
//		} // for
//
////		solutionType_ = Enum.valueOf( SolutionType_.class , solutionType );
//		if (solutionType.compareTo("BinaryReal") == 0)
//		      solutionType_ = new BinaryRealSolutionType(this) ;
//		    else if (solutionType.compareTo("Real") == 0)
//		    	solutionType_ = new RealSolutionType(this) ;
//		    else if (solutionType.compareTo("Int") == 0)
//		    	solutionType_ = new IntSolutionType(this) ;
//		    else {
//		    	System.out.println("Error: solution type " + solutionType + " invalid") ;
//		    	System.exit(-1) ;
//		    }
//
//		// All the variables are of the same type, so the solutionType name is the
//		// same than the variableType name
////		variableType_ = new VariableType_[numberOfVariables_];
////		for( int var=0 ; var<numberOfVariables_ ; ++var )
////			variableType_[var] = Enum.valueOf( VariableType_.class , solutionType ); 
//	} // Small
	
//	public void nextInstance()
//	{
//		instanceNo_++;
//	}

//	@Override
//	public Problem clone() {
//		Small newProblem = null;
//		try {
//			newProblem = new Small( "Int" );
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
////		newProblem.M_                = this.M_                ;
////		newProblem.numberOfMachines_ = this.numberOfMachines_ ;
////		newProblem.numberOfTasks_    = this.numberOfTasks_    ;
////		newProblem.ETC_              = this.ETC_              ;
//		
//
//		return( newProblem );
//	} // clone

} // Small
