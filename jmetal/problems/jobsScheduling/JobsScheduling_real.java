package jmetal.problems.jobsScheduling;

//import jmetal.base.Variable;
//import jmetal.base.variable.Int;
//import jmetal.base.Problem;
//import jmetal.base.Solution;

import jmetal.util.JMException;
import jmetal.base.variable.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.ArrayList;

import jmetal.base.operator.localSearch.RepairSolutionLocalSearch_real;
import jmetal.base.solutionType.*;
import jmetal.base.variable.Int;
import jmetal.base.*;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.Configuration;
import jmetal.util.Matrix;
import jmetal.util.PseudoRandom;
import jmetal.util.ScheduleStrategy;

import java.util.UUID;

public abstract class JobsScheduling_real extends Problem {
	public int numberCNs_ = 0;
	public int numberJobs_ = 0;
	protected int numberOfObjectivesByDefault_ = 3;

	// public static final String path_ =
	// "/run/media/santiago/Store/Facultad/Two-level_scheduler/jMetal3.1/jMetal3.1/JobsScheduling/";
	// ///< Base directory of the matrices instances
	
	public static String path_network_ = "JobsScheduling/"; // /< Base
																	// directory
																	// of the
																	// matrices
																	// instances
	//public static String path_network_ = "/mnt/store/Facultad/Two-level_scheduler/jMetal3.1/jMetal3.1/JobsScheduling/";
	
	public static final String path_ = "/tmp/"; // /< Base
												// directory
												// of
												// the
												// matrices
												// instances

	// Parameters to be set before evaluating a solution
	// public String scheduler =
	// "/home/santiago/schedulersSergio/scheduler_holes.energy.silent.EFT";
	// public File scheduler = new File(
	// "/home/users/siturriaga/schedulersSergio/scheduler_holes.energy.silent.EFT");
	public File scheduler = new File(
			"schedulersSergio/scheduler_holes.energy.silent.EFT");

	// public String scheduler =
	// "/Facultad/Two-level_scheduler/schedulersSergio/scheduler_holes.energy.silent.EFT.bat";
	// public String solFile =
	// "~/JobsScheduling/Bernabe-5-1000-0-0-0-100-01.solSergio";
	// public String instSchedJobs =
	// "~/JobsScheduling/Bernabe-5-1000-0-0-0-100-01.ssfModSergio";

	// directory of the matrices instances
	// protected String name_ = "Bernabe-5-1000-0-0-0-100-%02d"; // /< Name of
	// the problem
	// protected String name_ = "Bernabe-5-1000-0-0-100-0-%02d";
	// protected String name_ = "Bernabe-5-1000-0-100-0-0-%02d";
	// protected String name_ = "Bernabe-5-1000-100-0-0-0-%02d";
	// protected String name_ = "Bernabe-5-1000-30-30-30-10-%02d";

	public abstract String GetName();

	public abstract void SetName(String name);

	private UUID uuid_ = UUID.randomUUID();

	protected int instanceNo_ = 0;

	protected boolean verbose = false;
	protected boolean verboseRd = false;
	protected boolean verboseAssignments = false;

	public JobSchedInstance instance; // /< Stores the copy of ETC values

	public final static int PENALIZATION_NO_EXECUTION = 1000000;

	public final static int NUMBER_PEN_FUNCTIONS = 3;

	public final static int SQRT = 0;
	public final static int SQR = 1;
	public final static int LIN = 2;

	public final static double RefGFLOPS = 7.20; // The speed of the slowest
													// considered processor

	public double[] Usage = null;

	// Percentage of consumption of an idle processor with respect to when it is
	// fully used
	// private final static double IDLE_ = 0.6;
	/**
	 * Constructor. Creates a empty instance of the MO_Scheduling problem.
	 */
	protected JobsScheduling_real() {

		numberOfObjectives_ = numberOfObjectivesByDefault_;
		numberOfConstraints_ = 0;

	} // MO_Scheduling

	public static void copyFile(File source, File dest) throws JMException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			try {
				inputChannel = new FileInputStream(source).getChannel();
				outputChannel = new FileOutputStream(dest).getChannel();
				outputChannel
						.transferFrom(inputChannel, 0, inputChannel.size());
			} finally {
				inputChannel.close();
				outputChannel.close();
			}
		} catch (IOException e) {
			throw new JMException(e.getMessage());
		}
	}

	public void evaluate(Solution solution) throws JMException {

		RepairSolutionLocalSearch_real repair = new RepairSolutionLocalSearch_real();
		repair.setParameter("Problem", this);
		solution = (Solution) repair.execute(solution);
		
		String solFile = "/tmp/" + String.format(this.GetName(), instanceNo_)
				+ uuid_.toString() + ".ssfSolSergio";

		// get the parameters needed to run Sergio's scheduler
		int nbJobs = instance.getNumberJobs(); // Number of jobs

		// Print the solution into a file (DAGs go in reverse order)

		double[] CTs = new double[numberCNs_]; // Here, the completion time of
												// every CN will be stored
		double[] Energy = new double[numberCNs_]; // Here, the energy use of
													// every CN will be stored
		double[] Penalizations = new double[numberCNs_]; // Here, the
															// penalizations due
															// to missed
															// deadlines for
															// every CN will be
															// stored
		Usage = new double[numberCNs_]; // Here, the percentage of usage for
										// every CN will be stored

		Runtime r = Runtime.getRuntime();

		File solFileRef;
		solFileRef = new File(solFile);

		FileWriter f_writer = null;
		BufferedWriter writer = null;

		try {
			// System.out.println("Creando "+solFileRef.getPath());
			f_writer = new FileWriter(solFileRef);
			writer = new BufferedWriter(f_writer);

			Permutation values = (Permutation) solution.getDecisionVariables()[0];

			StringBuilder sb = new StringBuilder();
			int taskCount = 0;

			for (int i = 0; i < values.size_; i++) {
				if (values.vector_[i] < nbJobs) {
					taskCount++;
					sb.append(values.vector_[i] + " ");
				} else {
					// System.out.println("Escribiendo "+sb.toString());

					writer.write("0.0 ");
					writer.write(taskCount + " ");
					writer.write(sb.toString());
					writer.newLine();

					sb = new StringBuilder();
					taskCount = 0;
				}
			}

			// System.out.println("Escribiendo "+sb.toString());

			writer.write("0.0 ");
			writer.write(taskCount + " ");
			writer.write(sb.toString());
			writer.newLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			// System.out.println("ERORRRRRRRRRRRRRRRRRRRRRR");
			e1.printStackTrace();
		} finally {
			if (f_writer != null) {
				try {
					// System.out.println("Cerrando");
					writer.close();
					f_writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					// System.out.println("ERORRRRRRRRRRRRRRRRRRRRRR 2");
					e.printStackTrace();
				}
			}
		}

		// Call the scheduler
		InputStream err_in = null, in = null;
		BufferedInputStream buf = null;
		InputStreamReader inread = null;
		BufferedReader bufferedreader = null;
		BufferedInputStream err_buf = null;
		InputStreamReader err_inread = null;
		BufferedReader err_bufferedreader = null;
		
		String[] cmd = new String[6];
		try {
			// String[] env = new String[1];
			// env[0] = "LD_LIBRARY_PATH=.";
			cmd[0] = scheduler.getPath();
			cmd[1] = instance.instSchedJobs;
			cmd[2] = instance.instSchedCNs;
			cmd[3] = solFile;
			cmd[4] = (new Integer(nbJobs)).toString();
			cmd[5] = (new Integer(numberCNs_)).toString();

			// Process p = r.exec(cmd, env);
			Process p = r.exec(cmd);
			try {
				p.waitFor();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			// System.out.println("TERMINA LA EJECUCION");

			err_in = p.getErrorStream();
			err_buf = new BufferedInputStream(err_in);
			err_inread = new InputStreamReader(err_buf);
			err_bufferedreader = new BufferedReader(err_inread);

			String err_output = err_bufferedreader.readLine();
			if (err_output != null) {
				if (err_output.length() > 0) {
					System.out.println("ERROR: " + err_output);
				}
			}

			in = p.getInputStream();
			buf = new BufferedInputStream(in);
			inread = new InputStreamReader(buf);
			bufferedreader = new BufferedReader(inread);

			String output = bufferedreader.readLine();
			if (output == null) {
				System.out.println("EJECUTO... " + cmd[0] + " " + cmd[1] + " "
						+ cmd[2] + " " + cmd[3] + " " + cmd[4] + " " + cmd[5]);
				System.out.println("OUTPUT: " + output);
			}

			StringTokenizer token = null;
			token = new StringTokenizer(output);

			// System.out.println("LEO RESULTADOS");

			// The output of the scheduler is assumed to be makespan, energy,
			// and deadline penalizations for every computing node (every CN in
			// one line), one after the other, all double values and separated
			// by one space
			for (int nc = 0; nc < numberCNs_; nc++) {
				// System.out.println("LA SALIDA OBTENIDA ES: " + output);

				CTs[nc] = new Double(token.nextToken());
				Energy[nc] = new Double(token.nextToken());
				Penalizations[nc] = new Double(token.nextToken());

				String usageToken = token.nextToken();
				if (usageToken.equals("-nan")) {
					Usage[nc] = 0.0;
				} else {
					Usage[nc] = new Double(usageToken);
				}

				if (nc < numberCNs_ - 1) {
					// if (bufferedreader != null) {
					output = bufferedreader.readLine();
					if (output != null)
						token = new StringTokenizer(output);
				}
			}

			// System.out.println("TERMINO DE LEER LOS RESULTADOS");

			// System.exit(-1);
		} catch (IOException e) {
			System.out.println("El error: " + e.getMessage());
		} finally {
			if (err_in != null) {
				try {
					err_bufferedreader.close();
					err_inread.close();
					err_buf.close();
					err_in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// Close the InputStream

			
			if (in != null) {
				try {
					bufferedreader.close();
					inread.close();
					buf.close();
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		// put the results in the solution fitness values

		// 1 - Max of the completion times (Makespan)
		double mk = CTs[0];
		for (int i = 1; i < numberCNs_; i++)
			if (CTs[i] > mk)
				mk = CTs[i];

		solution.setObjective(0, mk);

		// 2 - Sum of energy consumed in every CN (idle times after completion
		// time are not considered)
		// add to the energy the difference between the Makespan and the CT *
		// E_IDLE
		double totalEnergy = 0.0;
		for (int i = 0; i < numberCNs_; i++)
			// totalEnergy += Energy[i] ;
			totalEnergy += Energy[i] + (mk - CTs[i]) * instance.CNNbProc[i]
					* instance.EnergyIdle[i];

		solution.setObjective(1, totalEnergy);

		// 3 - Sum of penalizations
		double totalPenalizations = 0.0;
		for (int i = 0; i < numberCNs_; i++)
			totalPenalizations += Penalizations[i];

		solution.setObjective(2, totalPenalizations);

		// evaluateJavid(solution);
		// evaluateImproved(solution);
	}

	/**
	 * Evaluates a solution using Javid's algorithm: - Parallel tasks can be
	 * split into several processors - Jobs are inserted in the CNs as a bag,
	 * without taking care of when and in which processor they will actually
	 * execute - In the estimation, the time is divided by the number of
	 * processors, instead of using the number of processors*nb cores
	 * 
	 * @param solution
	 *            The solution to evaluate
	 * @param loadingPosition
	 *            Loading position
	 * @throws JMException
	 */
	public void evaluateJavid(Solution solution) throws JMException {
		// Variable[] gen;

		int availableProcessors[] = new int[instance.getNumberCNs()];

		Variable[] vars = solution.getDecisionVariables();

		int initPos = 0, finalPos = 0; // Initial and final positions of the
										// jobs in a CN
		int cn = 0; // ID of a computer node

		// int len = solution.numberOfVariables();
		int len = ((Permutation) solution.getDecisionVariables()[0])
				.getLength();
		double CT[] = new double[numberCNs_];
		// double energy[] = new double[numberCNs_];
		double penalizations[] = new double[numberCNs_];
		instance.CNJobsAssigned = new int[numberCNs_][];
		// instance.CNCoresPerTasksRatio = new double[numberCNs_];

		// For every CN:
		// Print test Sample Assignments for Sergio:
		// System.out.print("\n\n\n\nNEXT SOLUTION\n\n\n\n");

		for (int i = 0; i < len; i++) {
			// Get the scheduled jobs in this CN
			while ((i < len) && (isJob(vars[0], i))) {
				finalPos++;
				i++;
			}

			// Here, we know that jobs from position initPos to finalPos are
			// assigned to CN cn
			int numberjobs = finalPos - initPos;
			instance.CNJobsAssigned[cn] = new int[numberjobs];
			for (int j = 0; j < numberjobs; j++) {
				instance.CNJobsAssigned[cn][j] = ((Permutation) vars[0]).vector_[j
						+ initPos];
			}

			if (initPos == finalPos) { // If no jobs are assigned to this CN
				// The energy used is computed after the for loop
				CT[cn] = 0.0;
				penalizations[cn] = 0.0;
				if (verboseAssignments)
					System.out.println("CN" + cn + ": Empty");
			} else {
				// Estimate the completion time
				if (verboseAssignments) {
					System.out.print("CN" + cn + ": ");
					for (int k = initPos; k < finalPos; k++)
						System.out.print(((Permutation) vars[0]).vector_[k]
								+ ", ");

					System.out.println();
				}

				CT[cn] = GetCompletionTime((Permutation) vars[0], initPos,
						finalPos, cn, penalizations);

				// Print test Sample Assignments for Sergio:
				// int numJobs=finalPos-initPos;
				// System.out.print("\n" + CT[cn] + " " + numJobs + " ");
				// for (int k=initPos; k<finalPos; k++)
				// System.out.print(((Permutation)vars[0]).vector_[k] + " ");

			}

			finalPos++;
			initPos = finalPos;
			i = initPos - 1;
			cn++;
		}

		// Compute the average number of cores in the jobs assigned to every CN
		// for(int i=0; i<numberCNs_; i++) {
		// instance.CNCoresPerTasksRatio[i] = 0.0;
		// int numberOfTasks = 0;
		// for(int j=0; j<instance.CNJobsAssigned[i].length; j++) {
		// for(int k=0;
		// k<instance.jobTasksProc[instance.CNJobsAssigned[i][j]].length; k++) {
		// instance.CNCoresPerTasksRatio[i] +=
		// instance.jobTasksProc[instance.CNJobsAssigned[i][j]][k];
		// numberOfTasks++;
		// }
		// }
		// instance.CNCoresPerTasksRatio[i] = instance.CNCoresPerTasksRatio[i] /
		// numberOfTasks;
		// }

		// Fitness
		// 1 - Max of the completion times
		double mk = CT[0];
		for (int i = 1; i < numberCNs_; i++)
			if (CT[i] > mk)
				mk = CT[i];

		solution.setObjective(0, mk);

		// 2 - Sum of energy

		// energy = EnergyComp + EnergyIdle
		double totalEnergy = 0.0;
		for (int i = 0; i < numberCNs_; i++) {
			// double consumption = Consumption(i);
			// totalEnergy += consumption * CT[i] * instance.CNNbProc[i] +
			// (mk-CT[i]) * SLEEP_ * consumption * instance.CNNbProc[i];
			double nbCPUs = (double) instance.CNNbProc[i]
					/ (double) instance.CNCores[i];
			// totalEnergy += consumption * CT[i] * nbCPUs + (mk-CT[i]) * IDLE_
			// * consumption * nbCPUs;
			totalEnergy += instance.EnergyMax[i] * CT[i] * nbCPUs
					+ (mk - CT[i]) * instance.EnergyIdle[i] * nbCPUs;
		}

		solution.setObjective(1, totalEnergy);

		// 3 - Sum of penalizations
		double totalPenalizations = 0.0;
		for (int i = 0; i < numberCNs_; i++)
			totalPenalizations += penalizations[i];

		solution.setObjective(2, totalPenalizations);

		//
		// // Recover the solution values
		// gen = solution.getDecisionVariables();
		//
		// int[] Schedule = new int[numberOfTasks_]; // Contains the solution
		// values
		//
		// // Recover solution parameters
		// for( int var=0 ; var<numberOfTasks_ ; ++var ) {
		// Schedule[var] = (int) gen[var].getValue();
		// } // for
		//
		// // Return the fitness values
		// setObjectives(solution, Fitness( Schedule ) );
	} // evaluateJavid

	// /**
	// * Evaluates a solution using Javid's algorithm:
	// * - Parallel tasks cannot be split into several processors, they must run
	// in the same processor
	// * - Jobs are inserted in the processor (or set of cores) that can start
	// them the earliest
	// * - In the estimation, the time is divided by the number of processors*nb
	// cores
	// * @param solution The solution to evaluate
	// * @param loadingPosition Loading position
	// * @throws JMException
	// */
	// public void evaluateImproved( Solution solution ) throws JMException {
	//
	// int availableProcessors[] = new int[instance.getNumberCNs()];
	//
	// Variable[] vars = solution.getDecisionVariables();
	//
	// int initPos=0, finalPos = 0; // Initial and final positions of the jobs
	// in a CN
	// int cn = 0; // ID of a computer node
	//
	// int nonExecutedJobs = 0;
	// //int len = solution.numberOfVariables();
	// int len = ((Permutation)solution.getDecisionVariables()[0]).getLength();
	// double CT[] = new double[numberCNs_];
	// // double energy[] = new double[numberCNs_];
	// double penalizations[] = new double[numberCNs_];
	// instance.CNJobsAssigned = new int[numberCNs_][];
	// // instance.CNCoresPerTasksRatio = new double[numberCNs_];
	//
	// // For every CN:
	// // Print test Sample Assignments for Sergio:
	// //System.out.print("\n\n\n\nNEXT SOLUTION\n\n\n\n");
	//
	// for (int i=0; i<len; i++) {
	// // Get the scheduled jobs in this CN
	// while ((i<len) && (isJob(vars[0],i))) {
	// finalPos++;
	// i++;
	// }
	//
	// // Here, we know that jobs from position initPos to finalPos are assigned
	// to CN cn
	// if (initPos != finalPos) { // If no jobs are assigned to this CN
	//
	// ArrayList<Integer> remainingJobs = new ArrayList<Integer>();
	// for (int h=initPos; h<finalPos; h++)
	// remainingJobs.add(new Integer(((Permutation)vars[0]).vector_[h]));
	//
	// // Get the completion time after executing all the tasks
	// while (!remainingJobs.isEmpty()) {
	// // Execute all tasks for which there are resources
	// Iterator<Integer> it = remainingJobs.iterator();
	// // while (it.hasNext() && (availableProcessors > 0)) {
	// while (it.hasNext()) {
	// int job = it.next().intValue();
	// int maxRequiredProc = instance.requiredProcs(job);
	//
	// //if (requiredProc > ((JobsScheduling)
	// problem).instance.CNCores[cn]*((JobsScheduling)
	// problem).instance.CNNbProc[cn]) {
	// if (maxRequiredProc > instance.CNCores[cn]) {
	// // TODO: How to handle this case?
	// //System.out.println("ATENCION: el job " + job + " necesita " +
	// maxRequiredProc + " procesadores, mientras que el CN " + cn +
	// " solo tiene " + ((JobsScheduling) problem).instance.CNCores[cn]);
	// nonExecutedJobs++;
	// it.remove();
	// }
	//
	// else {
	// // the number of procs of the biggest level
	// // requiredProc = ((JobsScheduling)
	// problem).instance.requiredProcsLevel(job);
	// int avgRequiredProc = instance.requiredAvgProcsLevel(job);
	//
	//
	// procsCN[cn] += avgRequiredProc;
	// numberJobs[cn]++;
	//
	// //if ((!executed[vars.vector_[i]]) && (requiredProc <=
	// availableProcessors)) {
	// // double finishTime = insertInEarliestAvailableTime(availableTime, cn,
	// (double)((JobsScheduling)
	// problem).instance.estimateTimeAtMachineLevel(job, cn));
	//
	// // Javid Estimation
	// // double [] penalizations = {0.0,0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	// 0.0, 0.0, 0.0};
	// // double finishTime1 = ((JobsScheduling) problem).GetCompletionTime(new
	// Permutation(vars), initPos, finalPos, cn, penalizations);
	// //
	// // System.out.println("Results of Javid's stimator: " + finishTime1);
	//
	// int reqMachines =
	// (int)Math.ceil((double)avgRequiredProc/(double)((JobsScheduling)
	// problem).instance.CNCores[cn]);
	//
	// // double finishTime = insertInEarliestAvailableTime(availableTime, cn,
	// estimateTime(job, cn, ((JobsScheduling) problem), reqMachines),
	// reqMachines);
	//
	// // CALCULANDO EL TIEMPO NIVEL POR NIVEL
	// // El ancho del grafo es el ancho del nivel m�s grande
	// double time = estimateTimeLevels(job, cn, ((JobsScheduling) problem),
	// reqMachines, avgRequiredProc);
	// AvgJobsLength[cn] += time;
	// AvgJobsLevel[cn] += ((JobsScheduling)
	// problem).instance.getLevels(job).length;
	//
	// for(int l=0; l<((JobsScheduling)
	// problem).instance.jobTasksOps[job].length;l++) {
	// AvgTaskLength[cn] += ((JobsScheduling)
	// problem).instance.jobTasksOps[job][l];
	// totalNbTasks[cn]++;
	// }
	//
	//
	//
	// double finishTime = insertInEarliestAvailableTime(availableTime, cn,
	// time, reqMachines);
	// // Se considera por separado el ancho de cada nivel (como la suma de los
	// procesadores que requieren las tareas del nivel)
	// // double finishTime = insertInEarliestAvailableTime(availableTime, cn,
	// estimateTimeInEachLevels(job, cn, ((JobsScheduling) problem)),
	// reqMachines);
	// // La tarea ocupa el procesador entero, pero durante el tiempo original
	// // double finishTime = insertInEarliestAvailableTime(availableTime, cn,
	// estimateTime(job, cn, ((JobsScheduling) problem), reqMachines),
	// reqMachines);
	//
	// if (finishTime > CT[cn])
	// CT[cn] = finishTime;
	//
	// it.remove();
	// }
	//
	// }
	// }
	// // Print test Sample Assignments for Sergio:
	// int numJobs=finalPos-initPos;
	// System.out.print("\n" + CT[cn] + " " + numJobs + " ");
	// for (int l=initPos; l<finalPos; l++)
	// // System.out.print(((Permutation)vars[0]).vector_[l] + " ");
	// System.out.print(vars[l] + " ");
	// }
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	// int numberjobs = finalPos-initPos;
	// instance.CNJobsAssigned[cn] = new int[numberjobs];
	// for (int j=0; j<numberjobs; j++) {
	// instance.CNJobsAssigned[cn][j] =
	// ((Permutation)vars[0]).vector_[j+initPos];
	// }
	//
	// if (initPos == finalPos) { // If no jobs are assigned to this CN
	// // The energy used is computed after the for loop
	// CT[cn] = 0.0;
	// penalizations[cn] = 0.0;
	// if (verboseAssignments)
	// System.out.println("CN" + cn + ": Empty");
	// } else {
	// // Estimate the completion time
	// if (verboseAssignments) {
	// System.out.print("CN" + cn + ": ");
	// for (int k=initPos; k<finalPos; k++)
	// System.out.print(((Permutation)vars[0]).vector_[k] + ", ");
	//
	// System.out.println();
	// }
	//
	// CT[cn] = GetCompletionTime((Permutation)vars[0], initPos, finalPos, cn,
	// penalizations);
	//
	// // Print test Sample Assignments for Sergio:
	// //int numJobs=finalPos-initPos;
	// //System.out.print("\n" + CT[cn] + " " + numJobs + " ");
	// //for (int k=initPos; k<finalPos; k++)
	// // System.out.print(((Permutation)vars[0]).vector_[k] + " ");
	//
	//
	// }
	//
	// finalPos++;
	// initPos = finalPos;
	// i=initPos-1;
	// cn++;
	// }
	//
	// // Compute the average number of cores in the jobs assigned to every CN
	// // for(int i=0; i<numberCNs_; i++) {
	// // instance.CNCoresPerTasksRatio[i] = 0.0;
	// // int numberOfTasks = 0;
	// // for(int j=0; j<instance.CNJobsAssigned[i].length; j++) {
	// // for(int k=0;
	// k<instance.jobTasksProc[instance.CNJobsAssigned[i][j]].length; k++) {
	// // instance.CNCoresPerTasksRatio[i] +=
	// instance.jobTasksProc[instance.CNJobsAssigned[i][j]][k];
	// // numberOfTasks++;
	// // }
	// // }
	// // instance.CNCoresPerTasksRatio[i] = instance.CNCoresPerTasksRatio[i] /
	// numberOfTasks;
	// // }
	//
	// // Fitness
	// // 1 - Max of the completion times
	// double mk = CT[0];
	// for (int i=1; i<numberCNs_; i++)
	// if (CT[i] > mk)
	// mk = CT[i];
	//
	// solution.setObjective(0,mk);
	//
	// // 2 - Sum of energy
	//
	// // energy = EnergyComp + EnergyIdle
	// double totalEnergy = 0.0;
	// for (int i=0; i<numberCNs_; i++){
	// //double consumption = Consumption(i);
	// //totalEnergy += consumption * CT[i] * instance.CNNbProc[i] + (mk-CT[i])
	// * SLEEP_ * consumption * instance.CNNbProc[i];
	// double nbCPUs = (double)instance.CNNbProc[i] /
	// (double)instance.CNCores[i];
	// //totalEnergy += consumption * CT[i] * nbCPUs + (mk-CT[i]) * IDLE_ *
	// consumption * nbCPUs;
	// totalEnergy += instance.EnergyMax[i] * CT[i] * nbCPUs + (mk-CT[i]) *
	// instance.EnergyIdle[i] * nbCPUs;
	// }
	//
	// solution.setObjective(1,totalEnergy);
	//
	// // 3 - Sum of penalizations
	// double totalPenalizations = 0.0;
	// for (int i=0; i<numberCNs_; i++)
	// totalPenalizations += penalizations[i];
	//
	// solution.setObjective(2, totalPenalizations);
	//
	// //
	// // // Recover the solution values
	// // gen = solution.getDecisionVariables();
	// //
	// // int[] Schedule = new int[numberOfTasks_]; // Contains the solution
	// values
	// //
	// // // Recover solution parameters
	// // for( int var=0 ; var<numberOfTasks_ ; ++var ) {
	// // Schedule[var] = (int) gen[var].getValue();
	// // } // for
	// //
	// // // Return the fitness values
	// // setObjectives(solution, Fitness( Schedule ) );
	// } // evaluateImproved

	// private double Consumption(int cn) {
	// double cons = 0.0;
	//
	// // double[] CONS_ = {1.75, 2.0, 2.15, 2.25, 2.35, 2.43, 2.55, 2.70, 3.0,
	// 3.4};
	//
	// // Processors list (sources: http://www.cpubenchmark.net/common_cpus.html
	// and http://www.techarp.com/showarticle.aspx?artno=337&pgno=0):
	// // 1x intel Pentium 4 3.0 - 491 - 81.9W
	// // 2x intel Core2 Duo T7250 2.0 - 1099 - 65W
	// // 3x intel Core2 Duo E6600 2.4 - 1507 - 65W
	// // 4x intel Core2 Duo E7500 2.93 - 2012 - 65W
	// // 5x intel Core2 Duo E8500 3.16 - 2417 - 65W
	// // 6x intel Core2 Quad Q6600 2.4 - 2981 - 95W
	// // 7x intel Core2 Quad Q8300 2.5 - 3557 - 95W
	// // 8x intel Core2 Quad Q9450 2.66 - 4033 - 95W
	// // 9x intel Core2 Quad Q9650 3.0 - 4619 - 65W
	// // 10x AMD Phenom II X6 1055T - 5198
	//
	// double[] CONS_ = {80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5};
	//
	// /*
	// *
	// * CONSUMPTION IN MULTI-CORE
	// *
	// * After talking with Young, he told me that the consumption of a
	// processor
	// * in idle time is 50-60% of the total consumption at full capacity,
	// * and then you can follow a linear pattern for the consumption when using
	// the different
	// * processors. Example for intel Core Quad:
	// * Idle: 60W
	// * 1 core: 70W
	// * 2 cores: 80W
	// * 3 cores: 90W
	// * 4 cores: 100W
	// *
	// * But, in our estimations we can consider that either all the procs are
	// working full capacity
	// * of they all are idle, therefore, we will consider levels 100W or 60W.
	// *
	// * */
	// cons = CONS_[cn];
	//
	// // TODO: assign different consumptions to different processor, for the
	// moment it is proportional to the speed ...
	// // double CONS_ = 1.75;
	// // cons = CONS_ * instance.CNSpeed[cn];
	//
	// /*************
	// * Websites with CPU energy consumption information
	// * http://en.wikipedia.org/wiki/Front_side_bus
	// * http://www.pcstats.com/articleview.cfm?articleid=2394&page=2
	// * http://mysite.verizon.net/pchardwarelinks/elec.htm
	// * http://www.anandtech.com/bench/Product/188
	// *
	// http://www.lostcircuits.com/mambo//index.php?option=com_content&task=view&id=70&Itemid=42&limit=1&limitstart=5
	// *
	// *
	// *
	// http://www.xbitlabs.com/articles/cpu/display/amd-energy-efficient_6.html#sect0
	// * http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=5611491&tag=1
	// * Athlon 64 X2 3800+ - 2.0GHz - 14W (11W with C'n'Q) - 165W
	// * Athlon 64 X2 4600+ - 2.4GHz - 20W (14W with C'n'Q) - 170W
	// * Core 2 Duo E6300 - 1.86GHz - 26W - 191W
	// *
	// *
	// http://www.tomshardware.com/reviews/intel-cpu-power-consumption,1750-9.html
	// * Core 2 Duo E6850 - 3.0GHz - 77W - 132W
	// * Pentium 4 630 - 3.0GHz - 86W - 155W
	// * Core 2 Extreme QX6850 - 3.0GHz - 94W - 195W
	// * Pentium D 830 - 3.0GHz - 104W - 203W
	// *
	// * http://www.anandtech.com/bench/CPU/51
	// * http://www.anandtech.com/bench/CPU/52
	// * AMD Athlon 2650e 1.6GHz - 84W - 93.1W - 1 Core
	// * - AMD Athlon X2 3250e 1.5GHz - 86.1W - 101.8W - 2 Cores
	// *
	// * Intel i5 750 2.66GHz (3.2 Turbo) - 83W - 184W - 4 Cores
	// * - AMD Athlon II X4 605e 2.3GHz - 110W - 137.7W - 4Cores
	// *
	// * - AMD Phenom II X6 1090T 3.2GHz (3.6GHz Turbo) - 122.5W - 201W -6 Cores
	// * Intel Core i7 970 3.2GHz - 105W - 186W - 6 Cores
	// *
	// *
	// *
	// * GFLOPS for Intel processors:
	// * http://www.intel.com/support/processors/sb/cs-017346.htm
	// * http://www.intel.com/support/processors/sb/cs-023143.htm
	// * http://www.intel.com/support/processors/sb/cs-028241.htm
	// * http://www.intel.com/support/processors/celeron/sb/CS-020865.htm
	// *
	// * http://www.tomshardware.com/reviews/intel-core-i5,2410-13.html
	// *
	// *
	// *
	// http://www.xbitlabs.com/articles/cpu/display/power-consumption-overclocking_16.html#sect1
	// *
	// * GPU power consumption
	// *
	// http://www.codinghorror.com/blog/2006/08/video-card-power-consumption.html
	// */
	//
	// return cons;
	// }
	public boolean isJob(Variable j, int id) throws JMException {
		return ((Permutation) j).vector_[id] < this.numberJobs_;
	}

	/**
	 * Estimates the time to compute job in CN cn as the sum of the time to
	 * compute every level
	 * 
	 * @param job
	 * @param cn
	 * @return
	 */
	public double estimateTimeLevels(int job, int cn,
			JobSchedInstance instance, int reqMachines, int reqProcs) {
		double execTime = 0.0;

		// int numberOfTasks = jobTasksTimes[job].length;
		int numberOfTasks = instance.jobTasksOps[job].length;
		ArrayList[] levels = instance.getLevels(job);
		int numberLevels = instance.getLevels(job).length;
		double[] timeLevel = new double[numberLevels];

		// The time to compute every level is the time of the longest task in
		// the level divided by the number of cores
		for (int i = 0; i < numberLevels; i++) {
			Iterator it = levels[i].iterator();
			double time = 0;
			while (it.hasNext()) {
				double t = Math.ceil(instance.jobTasksOps[job][((Integer) it
						.next()).intValue()] / instance.CNSpeed[cn]);
				if (t > time)
					time = t;
			}
			timeLevel[i] = time;
		}

		for (int i = 0; i < numberLevels; i++) {

			// La tarea ocupa el procesador entero, pero durante el tiempo
			// original
			execTime += (double) timeLevel[i];

		}

		return (int) execTime;
	}

	/**
	 * Inserts a job that is requiring 'time' time steps of 'reqMachines'
	 * machines in CN 'cn' in such a way that it starts the earliest possible
	 * 
	 * */
	private double insertInEarliestAvailableTime(double[] availableTimes,
			int cn, double time, int reqMachines) {

		int[] machines = new int[reqMachines];
		double[] earliestTime = new double[reqMachines];

		for (int i = 0; i < reqMachines; i++) {
			earliestTime[i] = Double.MAX_VALUE;
			machines[i] = Integer.MAX_VALUE;
		}

		for (int j = 0; j < reqMachines; j++)
			for (int i = 0; i < availableTimes.length; i++)
				if (earliestTime[j] > availableTimes[i]) {

					// if it was assigned it before, undo the previous
					// assignment
					if (machines[j] < Integer.MAX_VALUE) {
						availableTimes[machines[j]] -= time;
					}
					earliestTime[j] = availableTimes[i];
					machines[j] = i;
					availableTimes[i] += time;
				}

		return availableTimes[machines[reqMachines - 1]];
	}

	/**
	 * Returns the estimated completion time of executing jobs in vars betwen
	 * initPos (included) and finalPos (excluded) for CN cn
	 * 
	 * @param vars
	 * @param initPos
	 * @param finalPos
	 * @param cn
	 * @return The completion time
	 * @throws JMException
	 */
	public double GetCompletionTime(Permutation vars, int initPos,
			int finalPos, int cn, double[] penalizations) throws JMException {

		double CT = 0.0;
		// int availableProcessors = instance.CNNbProc[cn];

		Vector<Integer> releaseTime = new Vector();
		Vector<Integer> releaseProc = new Vector();
		int initTime = 0;

		int cores = instance.CNCores[cn];
		int procs = instance.CNNbProc[cn];
		double availableTime[] = new double[cores * procs];

		// Get the tasks to execute
		ArrayList<Integer> jobsToExecute = new ArrayList<Integer>();
		for (int i = initPos; i < finalPos; i++)
			jobsToExecute.add(new Integer(vars.vector_[i]));

		// Get the completion time after executing all the tasks
		while (!jobsToExecute.isEmpty()) {
			// Execute all tasks for which there are resources
			Iterator<Integer> it = jobsToExecute.iterator();
			// while (it.hasNext() && (availableProcessors > 0)) {
			while (it.hasNext()) {
				int job = it.next().intValue();
				// int requiredProc = instance.requiredProcs(job);

				int requiredProc = instance.requiredAvgProcsLevel(job, 3);

				// if (requiredProc > instance.CNNbProc[cn]) {
				if (requiredProc > instance.CNCores[cn]) {
					// TODO: How to handle this case?
					// return Double.MAX_VALUE;
					penalizations[cn] += PENALIZATION_NO_EXECUTION;
					it.remove();
				}

				// else if (requiredProc <= availableProcessors) {
				else {

					// int finishTime = initTime + instance.estimateTime(job,
					// cn);

					int reqMachines = (int) Math.ceil((double) requiredProc
							/ (double) instance.CNCores[cn]);
					double time = estimateTimeLevels(job, cn, instance,
							reqMachines, requiredProc);
					double finishTime = insertInEarliestAvailableTime(
							availableTime, cn, time, reqMachines);

					// Add penalizations
					penalizations[cn] += ComputePenalizations(job, finishTime
							- instance.jobDeadlines[job]);

					if (finishTime > CT)
						CT = finishTime;

					it.remove();

					// availableProcessors -= requiredProc;
					// int pos = insertOrder(releaseTime, finishTime);
					// releaseProc.add(pos, requiredProc);
					// tasksRun++;
				}

			}

			// All possible tasks were scheduled. Now release resources of the
			// first finishing tasks
			// if (!releaseTime.isEmpty()) {
			// initTime = releaseTime.remove(0).intValue(); // The first
			// finishing time finishes
			// availableProcessors += releaseProc.remove(0).intValue(); // The
			// processors used by this task are now free
			//
			// while ((!releaseTime.isEmpty()) && (initTime ==
			// releaseTime.elementAt(0))) {
			// // if (!releaseTime.isEmpty())
			// // while (initTime == releaseTime.elementAt(0)) {
			// releaseTime.remove(0); // All the tasks finishing at the same
			// time as the first one finish too
			// availableProcessors += releaseProc.remove(0).intValue(); // The
			// processors they used are now free
			// // if (releaseTime.isEmpty())
			// // break;
			// }
			// }

		}

		// CT+=releaseTime.lastElement().intValue();

		if (verboseAssignments)
			System.out.println("    Completion time = " + CT);

		return CT;
	}

	public double ComputePenalizations(int job, double exceededTime) {
		double time = 0.0;
		if (exceededTime > 0.0)
			switch (instance.jobPenalFunct[job]) {
			case SQRT:
				time = Math.sqrt(exceededTime);
				break;
			case SQR:
				time = exceededTime * exceededTime;
				break;
			case LIN:
				time = exceededTime;
				break;
			}

		return time;
	}

	public int getNumberJobs() {
		return instance.getNumberJobs();
	}

	public int getNumberCNs() {
		return instance.getNumberCNs();
	}

	public int numberProcs(int cn) {
		return instance.CNNbProc[cn];
	}

	public int numberCores(int cn) {
		return instance.CNCores[cn];
	}

	public int requiredProcs(int job) throws JMException {
		return instance.requiredProcs(job);
	}

	/**
	 * Inserts finish time in an ordered list (from low to high values)
	 * 
	 * @param releaseTime
	 * @param finishTime
	 * @return The position of the list where the element was inserted
	 */
	public int insertOrder(Vector<Integer> releaseTime, int finishTime) {
		int pos = 0;
		int len = releaseTime.size();

		Iterator<Integer> it = releaseTime.iterator();

		while (it.hasNext() && (it.next() < finishTime)) {
			// it.next();
			pos++;
		}

		releaseTime.add(pos, new Integer(finishTime));

		return pos;
	}

	/**
	 * Sets the value of all the objectives in the solution.
	 * 
	 * @param solution
	 *            The solution to modify.
	 * @param fitness
	 *            The fitness values to be stored.
	 */
	public void setObjectives(Solution solution, double[] fitness) {
		for (int i = 0; i < solution.numberOfObjectives(); i++) {
			solution.setObjective(i, fitness[i]);
		}
	}

	public class JobSchedInstance {

		// Job data
		// private int[] jobImportance = new int[numberJobs]; // The importance
		// of every job
		private int[] jobWidth = null; // The width of every job
		private int[] jobHeight = null; // The height of every job
		private int[][] jobLevelsStart = null; // The task ID where a new level
												// starts
		private int[][] jobTasksLevel = null; // the level every task belongs to
		public int[][] jobTasksProc = null; // the number of (processors) CORES
											// every task requires
		// I'll only store the execution time on the slowest machine
		// private int[][][] jobTasksTimes = null ; // the times to execute the
		// task with the different speed levels
		// private int[][] jobTasksTimes = null ; // the times to execute the
		// task with the different speed levels
		public double[][] jobTasksOps = null; // the number of operations of the
												// task
		// private int[][] jobTasksStart = null ; // Earliest Start time for
		// every task
		// private int[][] jobTasksFinish = null ; // Earliest Finishing time
		// for every task
		private int[][][] jobTasksInTask = null; // the input dependencies from
													// other tasks
		private int[][][] jobTasksOutTask = null; // the tasks with dependencies
													// on the output of this one

		private int[] jobPriority = null; // The priority function for every job
		private int[] jobPenalFunct = null; // The penalization function for
											// every job
		private int[] jobDeadlines = null; // The deadline assigned to every job

		// private int[] jobRequiredProcs = null;

		// Computing Node (CN) data
		public int[] CNNbProc = null; // The number of processors in every CN
		public double[] CNSpeed = null; // The speed of processors in every CN.
										// It is the number of GFLOPS / number
										// of cores
		public int[] CNCores = null; // The number of cores of processors in
										// every CN - NEW FEATURE!!
		public double[] EnergyIdle = null; // The energy consumed by processors
											// in idle state in every CN - NEW
											// FEATURE!!
		public double[] EnergyMax = null; // The energy consumed by processors
											// in max computing state in every
											// CN - NEW FEATURE!!

		public int[][] CNJobsAssigned = null; // This structure keeps all tasks
												// assigned to the different CNs
		// public double[] CNCoresPerTasksRatio = null; // Here, the average
		// number of cores of the tasks assigned to every CN is stored

		public String instSchedCNs; // =
									// "/cygdrive/d/Facultad/Two-level_scheduler/jMetal3.1/jMetal3.1/JobsScheduling/Bernabe-5-1000-0-0-0-100-01.ssfModCNsSergio";
		public String instSchedJobs; // =
										// "/cygdrive/d/Facultad/Two-level_scheduler/jMetal3.1/jMetal3.1/JobsScheduling/Bernabe-5-1000-0-0-0-100-01.ssfModSergio";

		private void initializeVars(int numberJobs) {
			// Job data
			// jobImportance = new int[numberJobs]; // The importance of every
			// job
			jobWidth = new int[numberJobs]; // The width of every job
			jobHeight = new int[numberJobs]; // The height of every job
			jobLevelsStart = new int[numberJobs][]; // The task ID where a new
													// level starts
			jobTasksLevel = new int[numberJobs][]; // the level every task
													// belongs to
			jobTasksProc = new int[numberJobs][]; // the number of processors
													// every task requires

			jobPriority = new int[numberJobs]; // The priority assigned to every
												// job
			jobPenalFunct = new int[numberJobs]; // The penalization function of
													// every job

			// I'll only store the execution time on the slowest machine
			// jobTasksTimes = new int[numberJobs][][]; // the times to execute
			// the task with the different speed levels
			// jobTasksTimes = new int[numberJobs][]; // the times to execute
			// the task with the different speed levels
			jobTasksOps = new double[numberJobs][]; // the number of operations
													// of the task
			// jobTasksStart = new int[numberJobs][]; // Earliest Start time for
			// every task
			// jobTasksFinish = new int[numberJobs][]; // Earliest Finishing
			// time for every task
			jobTasksInTask = new int[numberJobs][][]; // the input dependencies
														// from other tasks
			jobTasksOutTask = new int[numberJobs][][]; // the tasks with
														// dependencies on the
														// output of this one

			jobPenalFunct = new int[numberJobs]; // The penalization function
													// associated to every job
			jobDeadlines = new int[numberJobs]; // The deadline assigned to
												// every job
			int[] jobImportance = new int[numberJobs]; // The importance
														// assigned to every job
		}

		// public JobSchedInstance(String jobsFile, String CNsFile, int
		// numberJobs, int numberCNs){
		//
		// try {
		//
		// initializeVars(numberJobs);
		//
		//
		// CNNbProc = new int[numberCNs];
		// CNCores = new int[numberCNs];
		// CNSpeed = new double[numberCNs];
		// EnergyIdle = new double[numberCNs];
		// EnergyMax = new double[numberCNs];
		//
		//
		// //Read the CNs file
		// FileInputStream fis = new FileInputStream(CNsFile);
		// InputStreamReader isr = new InputStreamReader(fis);
		// BufferedReader br = new BufferedReader(isr);
		//
		// //System.out.println("Reading " + paretoFrontDirectory_ +
		// paretoFrontFile_[problemId]);
		//
		// String aux = br.readLine();
		// int cn = 0;
		// while (aux!=null){
		// StringTokenizer st = new StringTokenizer(aux);
		// CNNbProc[cn] = new Integer((String)st.nextElement()).intValue();
		// CNCores[cn] = new Integer((String)st.nextElement()).intValue();
		// CNSpeed[cn] = new Double((String)st.nextElement()).doubleValue();
		// EnergyIdle[cn] = new Double((String)st.nextElement()).doubleValue();
		// EnergyMax[cn] = new Double((String)st.nextElement()).doubleValue();
		// cn++;
		// aux = br.readLine();
		// }
		//
		//
		// // Read the jobs file
		// fis = new FileInputStream(jobsFile);
		// isr = new InputStreamReader(fis);
		// br = new BufferedReader(isr);
		//
		// //System.out.println("Reading " + paretoFrontDirectory_ +
		// paretoFrontFile_[problemId]);
		//
		// aux = br.readLine();
		// int job = 0;
		// while (aux!=null){
		// StringTokenizer st = new StringTokenizer(aux);
		// int nbTasks = new Integer((String)st.nextElement()).intValue();
		// jobTasksOps[job] = new double[nbTasks];
		// jobTasksProc[job] = new int[nbTasks];
		//
		// // jobWidth = new int[nbTasks]; // The width of every job
		// // jobHeight = new int[nbTasks]; // The height of every job
		// // jobLevelsStart = new int[nbTasks][]; // The task ID where a new
		// level starts
		// jobTasksLevel = new int[numberJobs][nbTasks]; // the level every task
		// belongs to
		//
		// for (int j=0; j<numberJobs; j++)
		// jobTasksLevel[j][0] = 0;
		// // no interest on the deadline
		// st.nextElement();
		// // no interest on the cost function
		// st.nextElement();
		//
		//
		// // llenar los tiempos de ejecuci�n de los jobs
		// for (int i=0; i<nbTasks; i++) {
		// jobTasksOps[job][i] = new
		// Double((String)st.nextElement()).doubleValue();
		// jobTasksProc[job][i] = new
		// Integer((String)st.nextElement()).intValue();
		// }
		// job++;
		// aux = br.readLine();
		// }
		//
		// }// try
		// catch (IOException e) {
		// e.printStackTrace();
		// }
		//
		// }

		public JobSchedInstance(String fileName) {

			// Read instance from fileName
			FileInputStream fis = null;
			InputStreamReader isr = null;
			BufferedReader br = null;
			
			try {
				fis = new FileInputStream(fileName);
				isr = new InputStreamReader(fis);
				br = new BufferedReader(isr);
				
				System.out.println("Reading instance: " + fileName);
				String aux;

				aux = br.readLine(); // Avoid first line
				aux = br.readLine();
				StringTokenizer st = new StringTokenizer(aux,
						"\\<>;,~ \t\n\r\f");

				st.nextToken();
				// st.nextToken();
				int numberJobs = (new Integer(st.nextToken())).intValue();

				if (verboseRd)
					System.out.println("number of jobs = " + numberJobs);

				// Job data
				initializeVars(numberJobs);

				// TODO Change jobPenalFunct and jobImportance; these values
				// must be specified by the instance,
				// but for the moment they are randomly set
				// for (int i=0; i<numberJobs; i++) {
				// jobPenalFunct[i] =
				// PseudoRandom.randInt(0,NUMBER_PEN_FUNCTIONS);
				// jobImportance[i] = PseudoRandom.randInt(1, 10);
				// // jobImportance[i] = 10;
				// }

				// Read the information of every job
				for (int i = 0; i < numberJobs; i++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					st.nextToken(); // "Job"
					int id = (new Integer(st.nextToken())).intValue(); // Job ID

					if (verboseRd)
						System.out.println("Reading Job " + id);

					st.nextToken(); // User Owner
					st.nextToken(); // Nb procs.
					st.nextToken(); // Job value
					st.nextToken(); // Job importance

					jobPriority[id] = (new Integer(st.nextToken())).intValue(); // Job
																				// priority
					jobPenalFunct[id] = (new Integer(st.nextToken()))
							.intValue(); // Job penalization function

					// jobImportance[id] = (new
					// Integer(st.nextToken())).intValue(); // Job Importance

					// System.out.println("\t Importance " + jobImportance[id]);

					st.nextToken(); // TmsToExe Job
					String next = st.nextToken();
					while (!next.equalsIgnoreCase("TmsToExe"))
						next = st.nextToken();

					// 7 parameters we do not mind
					for (int j = 0; j < 7; j++) {
						st.nextToken();
					}
					jobWidth[id] = (new Integer(st.nextToken())).intValue(); // Job
																				// Width
					jobHeight[id] = (new Integer(st.nextToken())).intValue(); // Job
																				// Height

					if (verboseRd) {
						System.out.println("\t Width " + jobWidth[id]);
						System.out.println("\t Height " + jobHeight[id]);
					}

					// Read TaskLevels
					next = st.nextToken();

					int levels = (new Integer(st.nextToken())).intValue();
					if (verboseRd)
						System.out.print("\t Task levels (" + levels + "): ");
					jobLevelsStart[id] = new int[levels];
					for (int j = 0; j < levels; j++) {
						jobLevelsStart[id][j] = (new Integer(st.nextToken()))
								.intValue();
						if (verboseRd)
							System.out.print(jobLevelsStart[id][j] + ", ");
					}
					if (verboseRd)
						System.out.println();
					next = st.nextToken();

					// Read Tasks
					next = st.nextToken();
					int numberOfTasks = (new Integer(st.nextToken()))
							.intValue(); // Number of Tasks in this Job
					if (verboseRd)
						System.out.println("\t Number of tasks: "
								+ numberOfTasks);

					jobTasksLevel[id] = new int[numberOfTasks];
					jobTasksProc[id] = new int[numberOfTasks];
					// I'll only store the execution time on the slowest machine
					// jobTasksTimes[id] = new int[numberOfTasks][];
					// jobTasksTimes[id] = new int[numberOfTasks];
					jobTasksOps[id] = new double[numberOfTasks];
					// jobTasksStart[id] = new int[numberOfTasks];
					// jobTasksFinish[id] = new int[numberOfTasks];
					jobTasksInTask[id] = new int[numberOfTasks][];
					jobTasksOutTask[id] = new int[numberOfTasks][];

					// Read the information for every task
					for (int j = 0; j < numberOfTasks; j++) {
						next = st.nextToken();
						int taskId = (new Integer(st.nextToken())).intValue(); // Task
																				// ID
						// TODO: Forget about the task levels given by the
						// instance, and compute them after the
						// loop when the input and output tasks for all the
						// instances is known
						// jobTasksLevel[id][taskId] = (new
						// Integer(st.nextToken())).intValue(); // Task Level
						st.nextToken();

						jobTasksProc[id][taskId] = (new Integer(st.nextToken()))
								.intValue(); // Processors required by the task
						if (verboseRd) {
							System.out.println("\t Task: " + taskId);
							// System.out.println("\t\t Level: " +
							// jobTasksLevel[id][taskId]);
							System.out.println("\t\t Parallel Processors: "
									+ jobTasksProc[id][taskId]);
						}

						// Read Times to Execute the Task
						next = st.nextToken();
						int times = (new Integer(st.nextToken())).intValue();// -
																				// 1;
						times--;
						st.nextToken(); // Avoid first 0

						// Instead of storing in memory the times to execute the
						// tasks in every machine,
						// I will compute it every time I need it
						// jobTasksTimes[id][taskId] = new int[times];
						// if (verboseRd)
						// System.out.print("\t\t Times to execute: ");
						// for (int k=0; k<times; k++) {
						// jobTasksTimes[id][taskId][k] = (new
						// Integer(st.nextToken())).intValue();
						// if (verboseRd)
						// System.out.print(jobTasksTimes[id][taskId][k]+ ", ");
						// }
						// jobTasksTimes[id][taskId] = (new
						// Integer(st.nextToken())).intValue();
						// The number of operations of the task is computed as
						// the time at reference speed 1 (slowest processor in
						// Javid's instance)
						// multiplied by the GFLOPS of the slowest processor
						// (among the ones I selected)
						jobTasksOps[id][taskId] = (new Integer(st.nextToken()))
								.intValue() * RefGFLOPS;
						for (int k = 0; k < times - 1; k++)
							st.nextToken();

						if (verboseRd)
							System.out.println();
						st.nextToken();
						// jobTasksStart[id][taskId] = (new
						// Integer(st.nextToken())).intValue();
						st.nextToken();

						st.nextToken();
						// jobTasksFinish[id][taskId] = (new
						// Integer(st.nextToken())).intValue();
						st.nextToken();

						// if (verboseRd) {
						// System.out.println("\t\t Earliest Start time: " +
						// jobTasksStart[id][taskId]);
						// System.out.println("\t\t Earliest Finishing time: " +
						// jobTasksFinish[id][taskId]);
						// }

						st.nextToken(); // Procs
						st.nextToken();
						st.nextToken();

						// Read InTasks (tasks whose output are the input of
						// this one)
						st.nextToken();
						int intasks = (new Integer(st.nextToken())).intValue();
						if (verboseRd)
							System.out
									.print("\t\t Tasks from which I need input: ("
											+ intasks + "): ");

						jobTasksInTask[id][taskId] = new int[intasks];
						for (int k = 0; k < intasks; k++) {
							jobTasksInTask[id][taskId][k] = (new Integer(
									st.nextToken())).intValue();
							if (verboseRd)
								System.out.print(jobTasksInTask[id][taskId][k]
										+ ", ");
						}
						if (verboseRd)
							System.out.println();
						st.nextToken();

						// Read OutTasks
						st.nextToken();
						int outtasks = (new Integer(st.nextToken())).intValue();
						if (verboseRd)
							System.out
									.print("\t\t Tasks depending on the output: ("
											+ outtasks + "): ");
						jobTasksOutTask[id][taskId] = new int[outtasks];
						for (int k = 0; k < outtasks; k++) {
							jobTasksOutTask[id][taskId][k] = (new Integer(
									st.nextToken())).intValue();
							if (verboseRd)
								System.out.print(jobTasksOutTask[id][taskId][k]
										+ ", ");
						}
						if (verboseRd)
							System.out.println();
						st.nextToken();
						st.nextToken();

					}
				}

				// Compute the task's levels
				for (int job = 0; job < numberJobs; job++) {
					// int numberOfTasks = jobTasksLevel[job].length;
					// for (int task=0; task<numberOfTasks; task++) {
					// jobTasksLevel[job][task] =
					// getHigestLevel(jobTasksInTask[job][task]);
					// }
					jobTasksLevel[job] = getLevels(jobTasksInTask[job]);
				}

				// All jobs have been read at this point

				// Now, read the information related to the CNs
				// Go to the "Computational Nodes" section, ignoring all the
				// intermediate data
				do {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
				} while (!st.nextToken().equalsIgnoreCase("Computational"));

				st.nextToken();

				int numberCNs = (new Integer(st.nextToken())).intValue();
				if (verboseRd)
					System.out.println("Number of CNs : " + numberCNs);

				CNNbProc = new int[numberCNs];
				CNSpeed = new double[numberCNs];
				CNCores = new int[numberCNs];
				EnergyIdle = new double[numberCNs];
				EnergyMax = new double[numberCNs];

				for (int k = 0; k < numberCNs; k++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					st.nextToken();
					int id = (new Integer(st.nextToken())).intValue();
					CNNbProc[id] = (new Integer(st.nextToken())).intValue();
					CNSpeed[id] = (new Double(st.nextToken())).doubleValue();
					if (verboseRd) {
						System.out.println("\t Computing Node " + id + ":");
						System.out.println("\t\t Number of Processors: "
								+ CNNbProc[id]);
						System.out.println("\t\t Speed of Processors: "
								+ CNSpeed[id]);
					}

					// 3 values we do not care about
					st.nextToken();
					st.nextToken();
					st.nextToken();

					CNCores[id] = (new Integer(st.nextToken())).intValue();
					EnergyIdle[id] = (new Double(st.nextToken())).doubleValue();
					EnergyMax[id] = (new Double(st.nextToken())).doubleValue();
				}

				for (int i = 0; i < numberJobs; i++) {
					boolean warning = true;
					try {

						for (int j = 0; j < numberCNs; j++) {
							if (requiredProcs(i) <= CNCores[j])
								warning = false;
						}

						if (warning) {
							System.out
									.println("WARNING: None of the CNs can execute Job "
											+ i
											+ ". It requires "
											+ requiredProcs(i) + " processors.");

							// System.exit(-1);
						}
					} catch (JMException e) {
						e.printStackTrace();
					}

				}

				// Compute the deadlines for the jobs
				for (int i = 0; i < numberJobs; i++)
					// Using as reference speed 1
					// jobDeadlines[i] = jobImportance[i] *
					// estimateTimeSlowestMachine(i);
					// Using as reference speed of the slowest machine in the
					// system
					// jobDeadlines[i] = jobImportance[i] *
					// estimateTimeSlowestAvailableMachine(i);
					// Using as reference speed of the fastest machine in the
					// system, 1 is lowest importance
					// jobDeadlines[i] = (11-jobImportance[i]) *
					// estimateTimeFastestAvailableMachine(i);
					jobDeadlines[i] = (11 - jobPriority[i])
							* estimateTimeSlowestAvailableMachine(i);

				// System.exit(-1);
				// System.out.println("Number of objectives: " +
				// numberObjectives);
				//
				// while (aux != null) {
				//
				// aux = br.readLine();
				//
				// }
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			} finally {
				if (fis != null) {
					try {
						br.close();
						isr.close();						
						fis.close();	
					} catch (Exception inner) {
						inner.printStackTrace();
					}					
				}
			}
		}

		// returns the level every task belongs to
		private int[] getLevels(int[][] TaskInTasks) {

			int numberOfTasks = TaskInTasks.length;
			int[] levels = new int[numberOfTasks];

			// Initialize values for levels to -1
			for (int i = 0; i < numberOfTasks; i++) {
				levels[i] = -1;
			}

			boolean finished = false;
			int task = 0;

			// Find the first task
			while (!finished) {
				if (TaskInTasks[task].length == 0) {
					finished = true;
					levels[task] = 0;
				}
				task++;
			}

			finished = false;
			int processedTasks = 1;

			// Assign levels for all the other tasks
			while (!finished) {
				for (int i = 0; i < numberOfTasks; i++) {
					// Get highest level of inTasks
					int numberInTasks = TaskInTasks[i].length;
					int highestLevelInTasks = -1;
					for (int it = 0; it < numberInTasks; it++) {
						// int localHighestLevel = -1;
						if (levels[TaskInTasks[i][it]] == -1)
							break;
						else {
							if (levels[TaskInTasks[i][it]] > highestLevelInTasks)
								highestLevelInTasks = levels[TaskInTasks[i][it]];
						}
					}
					if (highestLevelInTasks >= 0) {
						processedTasks++;
						levels[i] = highestLevelInTasks + 1;
					}
				}

				// End after assigning the corresponding level to every task
				if (processedTasks == numberOfTasks)
					finished = true;
			}

			return levels;
		}

		public int getNumberCNs() {
			return CNNbProc.length;
		}

		public int getNumberJobs() {
			return jobWidth.length;
		}

		// /**
		// * Returns the maximum number of processors required by
		// * any level in the job
		// * @param job
		// * @return
		// * @throws JMException
		// */
		//
		// public int requiredProcs(int jobID) throws JMException{
		// int reqProcs = 0;
		//
		// //int job = (int) jobID.getValue();
		// //int job = (int) ((Permutation)jobID[0]).vector_[jobID]
		// ;//.getValue();
		//
		// // We cannot use jobLevelStart because the levels in the instances
		// are not correct
		// // for (int level = 0; level < jobLevelsStart[jobID].length; level++)
		// {
		// // int procsInLevel = 0;
		// //
		// // // Compute the number of processors required by this level
		// // for (int l=0; l < jobTasksLevel[jobID].length; l++){
		// // if (jobTasksLevel[jobID][l] == level)
		// // procsInLevel+= jobTasksProc[jobID][l];
		// // }
		// //
		// // if (procsInLevel > reqProcs)
		// // reqProcs = procsInLevel;
		// // }
		//
		// ArrayList<Integer> procsInLevel = new ArrayList<Integer>();
		// // for (int i=0; i<10; i++)
		// // procsInLevel.add(null);
		//
		// for (int i=0; i<jobTasksLevel[jobID].length; i++) {
		// if (jobTasksLevel[jobID][i] >= procsInLevel.size()) {
		// int count = jobTasksLevel[jobID][i];
		// while (count<procsInLevel.size()) {
		// procsInLevel.add(null);
		// count++;
		// }
		//
		// //procsInLevel.add(jobTasksLevel[jobID][i],
		// procsInLevel.get(jobTasksLevel[jobID][i]) + new
		// Integer(jobTasksProc[jobID][i]));
		// procsInLevel.add(new Integer(jobTasksProc[jobID][i]));
		// }
		// else if (procsInLevel.get(jobTasksLevel[jobID][i]) == null)
		// procsInLevel.set(jobTasksLevel[jobID][i], new
		// Integer(jobTasksProc[jobID][i]));
		// else {
		// procsInLevel.set(jobTasksLevel[jobID][i],
		// procsInLevel.get(jobTasksLevel[jobID][i]) + new
		// Integer(jobTasksProc[jobID][i]));
		// }
		// }
		//
		// // Get the level with highest number of processors
		// Iterator<Integer> it = procsInLevel.iterator();
		// while (it.hasNext()) {
		// Integer procs = it.next();
		// if (procs.intValue() > reqProcs)
		// reqProcs = procs.intValue();
		//
		// }
		//
		// return reqProcs;
		// }

		/**
		 * Returns the maximum number of (processors) CORES required by any task
		 * in the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int requiredProcs(int jobID) throws JMException {
			int reqProcs = 0;

			int numberTasks = jobTasksProc[jobID].length;

			for (int i = 0; i < numberTasks; i++) {
				if (reqProcs < jobTasksProc[jobID][i])
					reqProcs = jobTasksProc[jobID][i];
			}

			return reqProcs;
		}

		/**
		 * Returns the maximum number of (processors) CORES required by any
		 * level in the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int requiredProcsLevel(int jobID) throws JMException {
			int reqProcs = 0;

			int numberTasks = jobTasksProc[jobID].length;
			int numberLevels = 0;
			for (int i = 0; i < jobTasksLevel[jobID].length; i++) {
				if (numberLevels < jobTasksLevel[jobID][i])
					numberLevels = jobTasksLevel[jobID][i];
			}
			numberLevels++;

			ArrayList[] levels = new ArrayList[numberLevels];
			for (int i = 0; i < numberTasks; i++) {

				if (levels[jobTasksLevel[jobID][i]] == null)
					levels[jobTasksLevel[jobID][i]] = new ArrayList();

				levels[jobTasksLevel[jobID][i]].add(new Integer(i));
			}

			for (int i = 0; i < numberLevels; i++) {

				Iterator it = levels[i].iterator();
				int procsInLevel = 0;
				while (it.hasNext()) {
					procsInLevel += jobTasksProc[jobID][((Integer) it.next())
							.intValue()];
				}
				if (reqProcs < procsInLevel)
					reqProcs = procsInLevel;
			}

			// if (reqProcs < jobTasksProc[jobID][i])
			// reqProcs = jobTasksProc[jobID][i];
			// }

			return reqProcs;
		}

		/**
		 * Returns the average number of CORES required by the levels of the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int requiredAvgCoresLevel(int jobID) throws JMException {
			int reqProcs = 0;

			int numberTasks = jobTasksProc[jobID].length;
			int numberLevels = 0;

			// Get the maximum number of levels in the DAG
			for (int i = 0; i < jobTasksLevel[jobID].length; i++) {
				if (numberLevels < jobTasksLevel[jobID][i])
					numberLevels = jobTasksLevel[jobID][i];
			}
			numberLevels++;

			// Compute the sum of processors required in all levels
			for (int i = 0; i < numberTasks; i++) {
				reqProcs += jobTasksProc[jobID][i];
			}

			return reqProcs / numberLevels;
		}
		
		/**
		 * Returns the average number of CORES required a job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int requiredAvgCores(int jobID) throws JMException {
			int reqProcs = 0;

			int numberTasks = jobTasksProc[jobID].length;

			// Compute the sum of processors required in all levels
			for (int i = 0; i < numberTasks; i++) {
				reqProcs += jobTasksProc[jobID][i];
			}

			return reqProcs / numberTasks;
		}

		/**
		 * Returns the average number of Processors required by the levels of
		 * the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int requiredAvgProcsLevel(int jobID, int coresPerProc)
				throws JMException {
			int reqProcs = 0;

			int numberTasks = jobTasksProc[jobID].length;
			int numberLevels = 0;

			// Get the maximum number of levels in the DAG
			for (int i = 0; i < jobTasksLevel[jobID].length; i++) {
				if (numberLevels < jobTasksLevel[jobID][i])
					numberLevels = jobTasksLevel[jobID][i];
			}
			numberLevels++;

			// Compute the sum of processors required in all levels
			for (int i = 0; i < numberTasks; i++) {
				reqProcs += Math.ceil(jobTasksProc[jobID][i]
						/ (double) coresPerProc);
			}

			return reqProcs / numberLevels;
		}

		/**
		 * Returns the number of levels in the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public int NumberLevels(int jobID) {
			int reqProcs = 0;

			int numberLevels = 0;
			for (int i = 0; i < jobTasksLevel[jobID].length; i++) {
				if (numberLevels < jobTasksLevel[jobID][i])
					numberLevels = jobTasksLevel[jobID][i];
			}
			numberLevels++;

			return numberLevels;
		}

		/**
		 * Returns the tasks composing every level in the job
		 * 
		 * @param job
		 * @return
		 * @throws JMException
		 */

		public ArrayList[] getLevels(int jobID) {
			int numberTasks = jobTasksProc[jobID].length;
			int numberLevels = 0;
			for (int i = 0; i < jobTasksLevel[jobID].length; i++) {
				if (numberLevels < jobTasksLevel[jobID][i])
					numberLevels = jobTasksLevel[jobID][i];
			}
			numberLevels++;

			ArrayList[] levels = new ArrayList[numberLevels];
			for (int i = 0; i < numberTasks; i++) {

				if (levels[jobTasksLevel[jobID][i]] == null)
					levels[jobTasksLevel[jobID][i]] = new ArrayList();

				levels[jobTasksLevel[jobID][i]].add(new Integer(i));
			}

			return levels;
		}

		/**
		 * Estimates the time to compute job in CN cn according to Javid's
		 * algorithm
		 * 
		 * @param job
		 * @param cn
		 * @return
		 */
		public double estimateTime(int job, int cn) {
			double execTime = 0.0;

			// int numberOfTasks = jobTasksTimes[job].length;
			int numberOfTasks = jobTasksOps[job].length;

			for (int k = 0; k < numberOfTasks; k++) {
				// execTime += (double)jobTasksTimes[job][k][CNSpeed[cn]-1] *
				// (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
				// execTime += Math.ceil((double)jobTasksTimes[job][k] /
				// (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed
				// goes from 1 to 10
				// execTime += Math.ceil((double)jobTasksOps[job][k] /
				// (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed
				// goes from 1 to 10
				execTime += ((double) jobTasksOps[job][k] / (double) CNSpeed[cn])
						* (double) jobTasksProc[job][k]; // Speed goes from 1 to
															// 10
				// taking into account the multi-cores processors
				// execTime += Math.ceil((double)jobTasksOps[job][k] /
				// (double)CNSpeed[cn]) * (double)jobTasksProc[job][k] *
				// (double)CNCores[cn]; // Speed goes from 1 to 10
			}

			// Not considering multi-cores
			// execTime = Math.ceil(execTime/(double)CNNbProc[cn]);
			// Considering the multi-cores case
			// execTime =
			// Math.ceil(execTime/((double)CNNbProc[cn]*(double)CNCores[cn]));
			execTime = execTime
					/ ((double) CNNbProc[cn] * (double) CNCores[cn]);
			// try {
			// execTime = Math.ceil(execTime/(double)requiredProcs(job));
			// } catch (JMException e) {
			// e.printStackTrace();
			// }
			return execTime;
		}

		/**
		 * Estimates the time to compute job in CN cn, ponderated by the % of
		 * resources used in that CN
		 * 
		 * @param job
		 * @param cn
		 * @return
		 */
		public double estimateTimePonderatedResourcesUse(int job, int cn) {
			double execTime = 0.0;

			int numberOfTasks = jobTasksOps[job].length;

			for (int k = 0; k < numberOfTasks; k++) {
				execTime += ((double) jobTasksOps[job][k] / (double) CNSpeed[cn])
						* (double) jobTasksProc[job][k]; // Speed goes from 1 to
															// 10
			}

			// execTime = execTime/((double)CNNbProc[cn]*(double)CNCores[cn]);
			try {
				execTime = execTime
						* (instance.requiredProcs(job) / (double) CNCores[cn]);
			} catch (JMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return execTime;
		}

		/**
		 * Estimates the time to compute job in CN cn according to Javid's
		 * algorithm, but instead of considering that the job can be run in the
		 * whole CN in parallel, we consider it runs in a single machine, using
		 * all cores
		 * 
		 * @param job
		 * @param cn
		 * @return
		 * @throws JMException
		 */
		public int estimateTimeAtMachineLevel(int job, int cn)
				throws JMException {
			double execTime = 0.0;

			// Get the number os tasks composing the job
			int numberOfTasks = jobTasksOps[job].length;
			// Get the maximum number of cores required by any task in the job
			int reqProcs = requiredProcs(job);

			for (int k = 0; k < numberOfTasks; k++) {
				// execTime += (double)jobTasksTimes[job][k][CNSpeed[cn]-1] *
				// (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
				// execTime += Math.ceil((double)jobTasksTimes[job][k] /
				// (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed
				// goes from 1 to 10
				execTime += Math.ceil((double) jobTasksOps[job][k]
						/ (double) CNSpeed[cn])
						* (double) jobTasksProc[job][k]; // Speed goes from 1 to
															// 10
			}

			// execTime = Math.ceil(execTime/(double)CNNbProc[cn]);
			execTime = Math.ceil(execTime / (double) CNCores[cn]);
			// execTime =
			// Math.ceil(execTime/((double)CNNbProc[cn]*(double)CNCores[cn]));
			// try {
			// execTime = Math.ceil(execTime/(double)requiredProcs(job));
			// } catch (JMException e) {
			// e.printStackTrace();
			// }
			return (int) execTime;
		}

		/**
		 * Estimates the time to compute job in the slowest CN available in the
		 * problem instance according to Javid's algorithm
		 * 
		 * @param job
		 * @param cn
		 * @return
		 */
		public int estimateTimeSlowestAvailableMachine(int job) {
			double execTime = 0.0;

			int numberOfTasks = jobTasksOps[job].length;

			int slowestMachine = 0;
			// int speedSlowestMachine = CNSpeed[0]-1; // Speed goes from 1 to
			// 10
			double speedSlowestMachine = CNSpeed[0]; // GFLOPS per core in
														// machine 0
			int len = CNSpeed.length;

			for (int i = 0; i < len; i++) {
				if (CNSpeed[i] < speedSlowestMachine) {
					// speedSlowestMachine = CNSpeed[i]-1; // Speed goes from 1
					// to 10
					speedSlowestMachine = CNSpeed[i];
					slowestMachine = i;
				}
			}

			for (int k = 0; k < numberOfTasks; k++) {
				// execTime +=
				// (double)jobTasksTimes[job][k][speedSlowestMachine] *
				// (double)jobTasksProc[job][k];
				execTime += Math.ceil((double) jobTasksOps[job][k]
						/ (double) CNSpeed[slowestMachine])
						* (double) jobTasksProc[job][k];
			}

			// try {
			// // The time to compute a job in the slowest machine is computed
			// as
			// // the time to sequentially run it in the slowest machine
			// // over the number of processors required by the job
			// execTime = Math.ceil(execTime/(double)requiredProcs(job));
			// } catch (JMException e) {
			// e.printStackTrace();
			// }
			execTime = Math.ceil(execTime / (double) CNNbProc[slowestMachine]);
			// execTime =
			// Math.ceil(execTime/((double)CNNbProc[slowestMachine]*(double)CNCores[slowestMachine]));

			return (int) execTime;
		}

		/**
		 * Estimates the time to compute job in the fastest CN available in the
		 * problem instance according to Javid's algorithm
		 * 
		 * @param job
		 * @param cn
		 * @return
		 */
		public int estimateTimeFastestAvailableMachine(int job) {
			double execTime = 0.0;

			// int numberOfTasks = jobTasksTimes[job].length;
			int numberOfTasks = jobTasksOps[job].length;

			int fastestMachine = 0;
			// int speedFastestMachine = CNSpeed[0]-1; // Speed goes from 1 to
			// 10
			double speedFastestMachine = CNSpeed[0];
			int len = CNSpeed.length;

			for (int i = 0; i < len; i++) {
				if (CNSpeed[i] > speedFastestMachine) {
					// speedFastestMachine = CNSpeed[i]-1; // Speed goes from 1
					// to 10
					speedFastestMachine = CNSpeed[i];
					fastestMachine = i;
				}
			}

			for (int k = 0; k < numberOfTasks; k++) {
				// execTime +=
				// (double)jobTasksTimes[job][k][speedFastestMachine] *
				// (double)jobTasksProc[job][k];
				// execTime += Math.ceil((double)jobTasksTimes[job][k] /
				// (double)CNSpeed[fastestMachine]) *
				// (double)jobTasksProc[job][k];
				execTime += Math.ceil((double) jobTasksOps[job][k]
						/ (double) CNSpeed[fastestMachine])
						* (double) jobTasksProc[job][k];
			}

			// try {
			// // The time to compute a job in the slowest machine is computed
			// as
			// // the time to sequentially run it in the slowest machine
			// // over the number of processors required by the job
			// execTime = Math.ceil(execTime/(double)requiredProcs(job));
			// } catch (JMException e) {
			// e.printStackTrace();
			// }
			// execTime =
			// Math.ceil(execTime/((double)CNNbProc[fastestMachine]*(double)CNCores[fastestMachine]));
			execTime = Math.ceil(execTime / (double) CNNbProc[fastestMachine]);

			return (int) execTime;
		}
	}

	// Returns the sum of the number of operations of the tasks in the critical
	// path
	public double criticalPath(int jobID) {
		double[] CPtoNode = new double[instance.jobTasksOps[jobID].length];

		// initialize the critical path length to arrive to any task in the job
		// for (int i=0; i<CPtoNode.length; i++) {
		// CPtoNode[i] = 0.0;
		// }

		// We compute the CP in terms of operations, then the time can be
		// computed for any CN just taking into account its speed
		CPtoNode[0] = instance.jobTasksOps[jobID][0];

		// for each level, compute the critical path of every task in the level
		// as its number of operations + the critical path of any precedent
		for (int level = 0; level < instance.jobLevelsStart[jobID].length - 1; level++) {
			for (int task = instance.jobLevelsStart[jobID][level]; task < instance.jobLevelsStart[jobID][level + 1]; task++) {
				// para todas las tareas precedentes, soger el m�ximo PC =>
				// maxPrec

				double maxCTPred = 0.0;
				for (int pred = 0; pred < instance.jobTasksInTask[jobID][task].length; pred++)
					if (CPtoNode[instance.jobTasksInTask[jobID][task][pred]] > maxCTPred)
						maxCTPred = CPtoNode[instance.jobTasksInTask[jobID][task][pred]];

				if (CPtoNode[task] < maxCTPred
						+ instance.jobTasksOps[jobID][task])
					CPtoNode[task] = maxCTPred
							+ instance.jobTasksOps[jobID][task];
			}
		}

		double maxCTPred = 0.0;
		for (int pred = 0; pred < instance.jobTasksInTask[jobID][instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]].length; pred++) {
			if (CPtoNode[instance.jobTasksInTask[jobID][instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]][pred]] > maxCTPred)
				maxCTPred = CPtoNode[instance.jobTasksInTask[jobID][instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]][pred]];
		}

		if (CPtoNode[instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]] < maxCTPred
				+ instance.jobTasksOps[jobID][instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]])
			CPtoNode[instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]] = maxCTPred
					+ instance.jobTasksOps[jobID][instance.jobLevelsStart[jobID][instance.jobLevelsStart[jobID].length - 1]];

		return CPtoNode[CPtoNode.length - 1];

	}

	/**
	 * Prints the solution into a file with the estimated time to compute for
	 * every CN and the jobs assigned
	 * 
	 * @param file
	 *            The file where the data will be written
	 * @throws JMException
	 * @throws IOException
	 */
	public void printSolutionToFile(Solution solution, String file)
			throws JMException, IOException {
		// Variable[] gen;

		int availableProcessors[] = new int[instance.getNumberCNs()];

		Variable[] vars = solution.getDecisionVariables();

		int initPos = 0, finalPos = 0; // Initial and final positions of the
										// jobs in a CN
		int cn = 0; // ID of a computer node

		// int len = solution.numberOfVariables();
		int len = ((Permutation) solution.getDecisionVariables()[0])
				.getLength();
		double CT[] = new double[numberCNs_];
		// double energy[] = new double[numberCNs_];
		double penalizations[] = new double[numberCNs_];

		// For every CN:
		// Print test Sample Assignments for Sergio:
		// System.out.print("\n\n\n\nNEXT SOLUTION\n\n\n\n");

		FileOutputStream fos = new FileOutputStream(file);
		PrintWriter res = new PrintWriter(fos);

		for (int i = 0; i < len; i++) {
			// Get the scheduled jobs in this CN
			while ((i < len) && (isJob(vars[0], i))) {
				finalPos++;
				i++;
			}

			// Here, we know that jobs from position initPos to finalPos are
			// assigned to CN cn
			if (initPos == finalPos) { // If no jobs are assigned to this CN
				// The energy used is computed after the for loop
				CT[cn] = 0.0;
				penalizations[cn] = 0.0;
				if (verboseAssignments)
					System.out.println("CN" + cn + ": Empty");
			} else {
				// Estimate the completion time
				if (verboseAssignments) {
					System.out.print("CN" + cn + ": ");
					for (int k = initPos; k < finalPos; k++)
						System.out.print(((Permutation) vars[0]).vector_[k]
								+ ", ");

					System.out.println();
				}

				CT[cn] = GetCompletionTime((Permutation) vars[0], initPos,
						finalPos, cn, penalizations);

				// Print test Sample Assignments for Sergio:
				int numJobs = finalPos - initPos;
				res.print(CT[cn] + " " + numJobs + " ");
				for (int k = initPos; k < finalPos; k++)
					res.print(((Permutation) vars[0]).vector_[k] + " ");

				res.println();

			}

			finalPos++;
			initPos = finalPos;
			i = initPos - 1;
			cn++;
		}

		res.close();

	} // evaluate

} // MO_Scheduling
