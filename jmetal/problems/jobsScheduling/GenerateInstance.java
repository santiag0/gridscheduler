package jmetal.problems.jobsScheduling;

import jmetal.util.JMException;
import jmetal.base.variable.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.ArrayList;

import jmetal.base.solutionType.*;
import jmetal.base.variable.Int;
import jmetal.base.*;
import jmetal.util.JMException;
import jmetal.util.Configuration.*;
import jmetal.util.Matrix;
import jmetal.util.PseudoRandom;
import jmetal.util.ScheduleStrategy;

public class GenerateInstance {
	
		protected int	numberCNs_						= 0 ;
		protected int	numberJobs_						= 0 ;
		protected int	numberOfObjectivesByDefault_	= 3 ;
		
		protected static final String path_      = "JobsScheduling/"; ///< Base directory of the matrices instances
		
		protected boolean 	verbose 					= false;
		protected boolean 	verboseRd 					= false;
		protected boolean 	verboseAssignments			= false;
			
		protected JobSchedInstance instance;  ///< Stores the copy of ETC values
		//protected double[][] Comp_; ///< Stores the copy computation time values
		
		public final static int PENALIZATION_NO_EXECUTION = 1000000;
		
		public final static int NUMBER_PEN_FUNCTIONS = 3;
		
		public final static int NUMBER_PROCESSORS = 5;
		
		public final double[] GFLOPS 		= {7.20, 10.40, 11.72, 13.32, 17.93};
		public final double[] EnergyIdle 	= {75, 68, 76, 74, 102};
		public final double[] EnergyMax 	= {94, 109, 214, 131, 210};
		public final int[] NbCores 			= {1, 2, 4, 2, 6};
		public final static int maxNbCores = 6; 
		
		public final static int SQRT = 0;
		public final static int SQR  = 1;
		public final static int LIN  = 2;
		
		public final static double RefGFLOPS = 7.20; // The speed of the slowest considered processor

		
		protected class JobSchedInstance {
		
			// Job data
//			private int[] jobImportance = new int[numberJobs];			// The importance of every job
			private int[] jobWidth				= null ;	// The width of every job
			private int[] jobHeight				= null ;	// The height of every job
			private int[][] jobLevelsStart		= null ;	// The task ID where a new level starts
			private int[][] jobTasksLevel		= null ;	// the level every task belongs to
			private int[][] jobTasksProc 		= null ;	// the number of processors every task requires
			// I'll only store the execution time on the slowest machine
//			private int[][][] jobTasksTimes 	= null ;	// the times to execute the task with the different speed levels
			//private int[][] jobTasksTimes 		= null ;	// the times to execute the task with the different speed levels
			private double[][] jobTasksOps 		= null ;	// the number of operations of the task
			private int[][] jobTasksStart 		= null ;	// Earliest Start time for every task
			private int[][] jobTasksFinish 		= null ;	// Earliest Finishing time for every task
			private int[][][] jobTasksInTask 	= null ;	// the input dependencies from other tasks
			private int[][][] jobTasksOutTask 	= null ;	// the tasks with dependencies on the output of this one
			
			private int[] jobPenalFunct 		= null;		// The penalization function for every job
			private int[] jobDeadlines			= null ;	// The deadline assigned to every job
			
//			private int[] jobRequiredProcs		= null;
			            
			// CN data
			private int[] CNNbProc 				= null;		// The number of processors in every CN
			private int[] CNSpeed 				= null;		// The speed of processors in every CN. It is the number of GFLOPS / number of cores
			private int[] CNCores 				= null;		// The number of cores of processors in every CN - NEW FEATURE!!
			private int[] EnergyIdle 			= null;		// The energy consumed by processors in idle state in every CN - NEW FEATURE!!
			private int[] EnergyMax 			= null;		// The energy consumed by processors in max computing state in every CN - NEW FEATURE!!
			
			public JobSchedInstance(String fileName){
				
				// Read instance from fileName
				
			}
			
			// returns the level every task belongs to
			private int[] getLevels(int[][] TaskInTasks) {
				
				int numberOfTasks = TaskInTasks.length;
				int[] levels = new int[numberOfTasks];
				
				// Initialize values for levels to -1
				for (int i=0; i<numberOfTasks; i++) {
					levels[i] = -1;
				}
					
				boolean finished = false;
				int task=0;
				
				// Find the first task
				while (!finished) {
					if (TaskInTasks[task].length == 0) {
						finished = true;
						levels[task] = 0;
					}
					task++;
				}
				
				finished = false;
				int processedTasks = 1;
							
				// Assign levels for all the other tasks
				while (!finished){
					for (int i=0; i<numberOfTasks; i++) {
						// Get highest level of inTasks
						int numberInTasks = TaskInTasks[i].length;
						int highestLevelInTasks = -1;
						for (int it=0; it<numberInTasks; it++) {
//							int localHighestLevel = -1;
							if (levels[TaskInTasks[i][it]] == -1)
								break;
							else {
								if (levels[TaskInTasks[i][it]] > highestLevelInTasks)
									highestLevelInTasks = levels[TaskInTasks[i][it]];
							}
						}
						if (highestLevelInTasks >= 0) {
							processedTasks++;
							levels[i] = highestLevelInTasks + 1;
						}
					}
									
					// End after assigning the corresponding level to every task
					if (processedTasks == numberOfTasks)
						finished = true;
				}
					
				return levels;
			}
			
			public int getNumberCNs() {
				return CNNbProc.length;
			}
			
			public int getNumberJobs() {
				return jobWidth.length;
			}
			
//			/**
//			 *  Returns the maximum number of processors required by
//			 *  any level in the job 
//			 * @param job
//			 * @return
//			 * @throws JMException 
//			 */
//			
//			public int requiredProcs(int jobID) throws JMException{
//				int reqProcs = 0;
//				
//				//int job = (int) jobID.getValue();
//				//int job = (int) ((Permutation)jobID[0]).vector_[jobID] ;//.getValue();
//				
//				// We cannot use jobLevelStart because the levels in the instances are not correct
////				for (int level = 0; level < jobLevelsStart[jobID].length; level++) {
////					int procsInLevel = 0;
////					
////					// Compute the number of processors required by this level
////					for (int l=0; l < jobTasksLevel[jobID].length; l++){
////						if (jobTasksLevel[jobID][l] == level)
////							procsInLevel+= jobTasksProc[jobID][l];
////					}
	////
////					if (procsInLevel > reqProcs)
////						reqProcs = procsInLevel;
////				}
//				
//				ArrayList<Integer> procsInLevel = new ArrayList<Integer>();
////				for (int i=0; i<10; i++)
////					procsInLevel.add(null);
//				
//				for (int i=0; i<jobTasksLevel[jobID].length; i++) {
//					if (jobTasksLevel[jobID][i] >= procsInLevel.size()) {
//						int count = jobTasksLevel[jobID][i];
//						while (count<procsInLevel.size()) {
//							procsInLevel.add(null);
//							count++;
//						}
//						
//						//procsInLevel.add(jobTasksLevel[jobID][i], procsInLevel.get(jobTasksLevel[jobID][i]) + new Integer(jobTasksProc[jobID][i]));
//						procsInLevel.add(new Integer(jobTasksProc[jobID][i]));
//					}
//					else if (procsInLevel.get(jobTasksLevel[jobID][i]) == null)
//						procsInLevel.set(jobTasksLevel[jobID][i], new Integer(jobTasksProc[jobID][i])); 
//					else {
//						procsInLevel.set(jobTasksLevel[jobID][i], procsInLevel.get(jobTasksLevel[jobID][i]) + new Integer(jobTasksProc[jobID][i]));
//					}
//				}
//				
//				// Get the level with highest number of processors
//				Iterator<Integer> it = procsInLevel.iterator();
//				while (it.hasNext()) {
//					Integer procs = it.next();
//					if (procs.intValue() > reqProcs)
//						reqProcs = procs.intValue();
//					
//				}
//								
//				return reqProcs;
//			}
			
			/**
			 *  Returns the maximum number of processors required by
			 *  any task in the job 
			 * @param job
			 * @return
			 * @throws JMException 
			 */
			
			public int requiredProcs(int jobID) throws JMException{
				int reqProcs = 0;
				
				int numberTasks = jobTasksProc[jobID].length;
				
				for (int i=0; i<numberTasks;i++) {
					if (reqProcs < jobTasksProc[jobID][i])
						reqProcs = jobTasksProc[jobID][i];
				}
												
				return reqProcs;
			}
			
			/**
			 * Estimates the time to compute job in CN cn according to Javid's algorithm
			 * @param job
			 * @param cn
			 * @return
			 */
			public int estimateTime(int job, int cn){
				double execTime = 0.0;
				
				//int numberOfTasks = jobTasksTimes[job].length;
				int numberOfTasks = jobTasksOps[job].length;
				
				for (int k=0; k<numberOfTasks; k++) {
					//execTime += (double)jobTasksTimes[job][k][CNSpeed[cn]-1] * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
					//execTime += Math.ceil((double)jobTasksTimes[job][k] / (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[cn]) * (double)jobTasksProc[job][k]; // Speed goes from 1 to 10
				}
				
				execTime = Math.ceil(execTime/(double)CNNbProc[cn]);
//				try {
//					execTime = Math.ceil(execTime/(double)requiredProcs(job));
//				} catch (JMException e) {
//					e.printStackTrace();
//				}
				return (int)execTime;
			}
			
			
			/**
			 * Estimates the time to compute job in the slowest CN 
			 * available in the problem instance according to Javid's algorithm
			 * @param job
			 * @param cn
			 * @return
			 */
			public int estimateTimeSlowestAvailableMachine(int job){
				double execTime = 0.0;
				
				int numberOfTasks = jobTasksOps[job].length;
				
				int slowestMachine = 0;
				//int speedSlowestMachine = CNSpeed[0]-1; // Speed goes from 1 to 10
				int speedSlowestMachine = CNSpeed[0]; // GFLOPS per core in machine 0
				int len = CNSpeed.length;
				
				for (int i=0; i<len; i++) {
					if (CNSpeed[i] < speedSlowestMachine) {
						//speedSlowestMachine = CNSpeed[i]-1; // Speed goes from 1 to 10
						speedSlowestMachine = CNSpeed[i];
						slowestMachine = i;
					}
				}
				
				for (int k=0; k<numberOfTasks; k++) {
					//execTime += (double)jobTasksTimes[job][k][speedSlowestMachine] * (double)jobTasksProc[job][k];
					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[slowestMachine]) * (double)jobTasksProc[job][k];
				}
				
//				try {
//					// The time to compute a job in the slowest machine is computed as
//					// the time to sequentially run it in the slowest machine
//					// over the number of processors required by the job
//					execTime = Math.ceil(execTime/(double)requiredProcs(job));
//				} catch (JMException e) {
//					e.printStackTrace();
//				}
				execTime = Math.ceil(execTime/(double)CNNbProc[slowestMachine]);
				
				return (int)execTime;
			}
			
			
			/**
			 * Estimates the time to compute job in the fastest CN 
			 * available in the problem instance according to Javid's algorithm
			 * @param job
			 * @param cn
			 * @return
			 */
			public int estimateTimeFastestAvailableMachine(int job){
				double execTime = 0.0;
				
				//int numberOfTasks = jobTasksTimes[job].length;
				int numberOfTasks = jobTasksOps[job].length;
				
				int fastestMachine = 0;
				//int speedFastestMachine = CNSpeed[0]-1; // Speed goes from 1 to 10
				int speedFastestMachine = CNSpeed[0];
				int len = CNSpeed.length;
				
				for (int i=0; i<len; i++) {
					if (CNSpeed[i] > speedFastestMachine) {
						//speedFastestMachine = CNSpeed[i]-1; // Speed goes from 1 to 10
						speedFastestMachine = CNSpeed[i];
						fastestMachine = i;
					}
				}
				
				for (int k=0; k<numberOfTasks; k++) {
//					execTime += (double)jobTasksTimes[job][k][speedFastestMachine] * (double)jobTasksProc[job][k];
					//execTime += Math.ceil((double)jobTasksTimes[job][k] / (double)CNSpeed[fastestMachine]) * (double)jobTasksProc[job][k];
					execTime += Math.ceil((double)jobTasksOps[job][k] / (double)CNSpeed[fastestMachine]) * (double)jobTasksProc[job][k];
				}
				
//				try {
//					// The time to compute a job in the slowest machine is computed as
//					// the time to sequentially run it in the slowest machine
//					// over the number of processors required by the job
//					execTime = Math.ceil(execTime/(double)requiredProcs(job));
//				} catch (JMException e) {
//					e.printStackTrace();
//				}
				execTime = Math.ceil(execTime/(double)CNNbProc[fastestMachine]);
				
				return (int)execTime;
			}
		}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		FileInputStream fis = null;
		FileOutputStream fos = null;
		
		GenerateInstance gi = new GenerateInstance();
		
		if (args.length != 2) {
			System.out.println("USE: java GenerateInstance InstanceName NbInstances");
			System.exit(-1);
		}
		
		try {
			for (int inst=0; inst<(new Integer(args[1])).intValue(); inst++) {

				String instance = "";
				if (inst+1>=10)
					instance = args[0] + "-"+ new Integer(inst+1).toString()+".ssf";
				else
					instance = args[0] + "-0"+ new Integer(inst+1).toString()+".ssf";
				
				fis = new FileInputStream(instance);
				fos = new FileOutputStream(instance+"Mod");
			
				InputStreamReader isr = new InputStreamReader(fis);
				PrintWriter res = new PrintWriter(fos);
				
				BufferedReader br = new BufferedReader(isr);
				System.out.println("Reading instance: " + instance);
				String aux;
				
				aux = br.readLine(); // Avoid first line
				res.println(aux);
				aux = br.readLine();
				StringTokenizer st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f"); 
				
				String s = st.nextToken();
				res.print(s + " > ");
	//			st.nextToken();
				int numberJobs = (new Integer(st.nextToken())).intValue();
				res.println(numberJobs + ";");
				
				if (gi.verboseRd)
					System.out.println("number of jobs = " + numberJobs);
							
	//				jobPenalFunct[i] = PseudoRandom.randInt(0,NUMBER_PEN_FUNCTIONS);
	//				jobImportance[i] = PseudoRandom.randInt(1, 10);
				
				// Read the information of every job
				for (int i=0; i<numberJobs; i++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					s = st.nextToken(); // "Job"
					res.print("<\\" + s + ">");
					int id =  (new Integer(st.nextToken())).intValue(); // Job ID
					res.print(id+",");
					
					if (gi.verboseRd)
						System.out.println("Reading Job " + id);
	
					s=st.nextToken(); // User Owner
					res.print(s + ",");
					s=st.nextToken(); // Nb procs.
					if (new Integer(s).intValue() > maxNbCores )
						res.print(new Integer(maxNbCores).toString() + ",");
					else
						res.print(s + ",");
					
					s=st.nextToken(); // Job value
					res.print(s + ",");
					s=st.nextToken(); // Job importance
					res.print(s + ";");
					// Add the job Importance (from 0 to 10)
					res.print(PseudoRandom.randInt(1, 10) + ",");
					// Add the job penalization function (from 0 to NUMBER_PEN_FUNCTIONS)
					res.print(PseudoRandom.randInt(0,NUMBER_PEN_FUNCTIONS) + ";");
									
					s=st.nextToken(); // TmsToExe Job
					res.print("<\\" + s + ">");
					
					String next = st.nextToken();
					res.print(next + ">");
					next = st.nextToken();
					while (!next.equalsIgnoreCase("TmsToExe")) {
						res.print(next + ",");
						next = st.nextToken();
					}
					res.print(";<~\\TmsToExe>");
					
					// 7 parameters we do not mind
					s=st.nextToken();
					res.print(s+",");
					s=st.nextToken();
					res.print(s+";");
					for (int j=0; j<4; j++) {
						s=st.nextToken();
						res.print(s+",");
					}
					s=st.nextToken();
					res.print(s+";");
					
					s = st.nextToken(); // Job Width
					res.print(s+",");
					
					if (gi.verboseRd) 
						System.out.println("\t Width " + s);
					
					s = st.nextToken(); // Job Height
					res.print(s+";");
					
					if (gi.verboseRd) 
						System.out.println("\t Height " + s);
					
					// Read TaskLevels
					next = st.nextToken();
					res.print("<\\"+next+">");
	
					String level = st.nextToken();
					res.print(level+">");
					
					if (gi.verboseRd)
						System.out.print("\t Task levels (" + level+ "): ");
					
					int levels = new Integer(level);
					
					for (int j=0; j<levels; j++) {
						level = st.nextToken();
						res.print(level + ",");
						if (gi.verboseRd)
							System.out.print(level + ", ");
					}
					res.print(";");				
					if (gi.verboseRd)
						System.out.println();
					next = st.nextToken();
					res.print("<~" + next+ ">");
					
					// Read Tasks
					next = st.nextToken();
					res.print(next+ " > ");
					
					int numberOfTasks = (new Integer(st.nextToken())).intValue(); // Number of Tasks in this Job
					res.print(numberOfTasks + ";");
					
					if (gi.verboseRd)
						System.out.println("\t Number of tasks: " + numberOfTasks);
					
					next = st.nextToken();
					res.print("<\\" + next + ">");
					
					// Read the information for every task
					for (int j=0; j<numberOfTasks; j++){
						
						int taskId = (new Integer(st.nextToken())).intValue(); // Task ID
						res.print(taskId+",");
						// TODO: Forget about the task levels given by the instance, and compute them after the 
						//       loop when the input and output tasks for all the instances is known
	//					jobTasksLevel[id][taskId] = (new Integer(st.nextToken())).intValue(); // Task Level
						s=st.nextToken();
						res.print(s+",");
						
						s=st.nextToken(); // Processors required by the task
						
						if (gi.verboseRd) {
							System.out.println("\t Task: " + taskId);
	//						System.out.println("\t\t Level: " + jobTasksLevel[id][taskId]);
							System.out.println("\t\t Parallel Processors: " + s);
							if (new Integer(s).intValue() > maxNbCores )
								System.out.println("\t\t\t\t Parallel Processors changed to: " + maxNbCores);
						}
						if (new Integer(s).intValue() > maxNbCores )
							res.print(new Integer(maxNbCores).toString() + ";");
						else
							res.print(s + ";");
						
						// PegasusTask > Avoid this
						next = st.nextToken();
						next = st.nextToken();
						while (!next.equalsIgnoreCase("PegasusTask"))
							next = st.nextToken();
	//					next = st.nextToken();
						
						// Read Times to Execute the Task
						next = st.nextToken();
						res.print("<\\"+next+">");
						int times = (new Integer(st.nextToken())).intValue();// - 1;
						res.print(times + ">");
						s=st.nextToken(); // Avoid first 0
						res.print(s+",");
						
						for (int k=0; k<times-1; k++) { 
							s=st.nextToken();
							res.print(s+",");
							
						}
						
						
						if (gi.verboseRd)
							System.out.println();
						s=st.nextToken();
						res.print(";<~\\" + s + ">");
						
						s=st.nextToken();
						res.print(s + ",");
						
						if (gi.verboseRd) {
							System.out.println("\t\t Earliest Start time: " + s);
						}
						
						s=st.nextToken();
						res.print(s + ",");
						
						s=st.nextToken();
						res.print(s + ";");
						
						if (gi.verboseRd) {
							System.out.println("\t\t Earliest Finishing time: " + s);
						}
						
						s=st.nextToken(); // Procs
						res.print("<\\"+s+">");
						int procs = (new Integer(st.nextToken())).intValue();
						res.print(procs+">");
	
						for (int k=0; k<procs; k++) {
							s=st.nextToken();
							res.print(s+",");
						}
								
						res.print(";");
						s=st.nextToken();
						res.print("<~\\"+s+">");
						
						// Read InTasks (tasks whose output are the input of this one)
						s=st.nextToken();
						res.print("<\\"+s+">");
						
						int intasks = (new Integer(st.nextToken())).intValue();
						res.print(intasks+">");
						if (gi.verboseRd)
							System.out.print("\t\t Tasks from which I need input: (" + intasks + "): ");
	
						for (int k=0; k<intasks; k++) {
							s=st.nextToken();
							res.print(s+",");
							if (gi.verboseRd)
								System.out.print(s + ", ");
						}
						res.print(";");
						if (gi.verboseRd)
							System.out.println();
						
						s=st.nextToken();
						res.print("<~\\"+s+">");
						
						// Read OutTasks
						s=st.nextToken();
						res.print("<\\"+s+">");
						int outtasks = (new Integer(st.nextToken())).intValue();
						res.print(outtasks+">");
						if (gi.verboseRd)
							System.out.print("\t\t Tasks depending on the output: (" + outtasks + "): ");
						
						for (int k=0; k<outtasks; k++) {
							s=st.nextToken();
							res.print(s+",");
							if (gi.verboseRd)
								System.out.print(s + ", ");
						}
						res.print(";");
						if (gi.verboseRd)
							System.out.println();
						
						s=st.nextToken();
						
						res.print("<~\\"+s+">");
						
						while (!s.equalsIgnoreCase("Task"))
							s=st.nextToken();
						
						res.print("<~\\"+s+">");
						
						if (!st.hasMoreTokens())
							st = new StringTokenizer(br.readLine(), "\\<>;,~ \t\n\r\f");
						
						s=st.nextToken();
						if (!s.equalsIgnoreCase("Task"))
							while (!s.equalsIgnoreCase("Job"))
								s=st.nextToken();
						else
							res.print("<\\" + s + ">");
											
					}
					res.println("<~\\Job>");
				}
	//			s=st.nextToken();
	//			res.print("<\\"+s+">");
	//			int depnfls = (new Integer(st.nextToken())).intValue();
	//			res.print(depnfls+">");
	//			
	//			for (int k=0; k<depnfls; k++) {
	//				s=st.nextToken();
	//				res.print(s+",");
	//				if (gi.verboseRd)
	//					System.out.print(s + ", ");
	//			}
	//			res.print(";");
	//			s=st.nextToken();
	//			res.print("<~\\"+s+">");
	//			s=st.nextToken();
	//			res.println("<~\\"+s+">");
	
				// All jobs have been read at this point
				
				// Now, read the information related to the CNs
				// Go to the "Computational Nodes" section, ignoring all the intermediate data
				aux = br.readLine();
				res.println(aux);
				aux = br.readLine();
				st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
				while (!st.nextToken().equalsIgnoreCase("Computational")) {
	//				res.println(aux);
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
				}
				
				res.print("Computational " + st.nextToken() + " > ");
				
				int numberCNs = (new Integer(st.nextToken())).intValue();
				res.println(numberCNs + ";");
				
				if (gi.verboseRd)
					System.out.println("Number of CNs : " + numberCNs);
				
				
				for (int k=0; k<numberCNs; k++) {
					aux = br.readLine();
					st = new StringTokenizer(aux, "\\<>;,~ \t\n\r\f");
					s = st.nextToken();
					res.print("<\\"+s+">");
					int id = (new Integer(st.nextToken())).intValue();
					res.print(id+",");
					int NbProc = (new Integer(st.nextToken())).intValue();
					res.print(NbProc+",");
	
					//int proc = PseudoRandom.randInt(0, NUMBER_PROCESSORS-1);
					
					int proc = k; 
							
					int Speed = (new Integer(st.nextToken())).intValue(); // I don't care about the relative speed
					res.print(gi.GFLOPS[proc]+",");
					
					if (gi.verboseRd) {
						System.out.println("\t Computing Node "+id + ":");
						System.out.println("\t\t Number of Processors: "+NbProc);
						System.out.println("\t\t Speed of Processors: " +gi.GFLOPS[proc]);
					}
					
					// 3 values we do not care about
					s=st.nextToken();
					res.print(s+",");
					s=st.nextToken();
					res.print(s+",");
					s=st.nextToken();
					res.print(s+";");
					
					
					res.print(gi.NbCores[proc]+",");
					res.print(gi.EnergyIdle[proc]+",");
					res.println(gi.EnergyMax[proc]+";");
					
				}
				
				
				res.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
