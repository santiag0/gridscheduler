package jmetal.problems.jobsScheduling;

/* 
 * 
 * This is the version of SerParr that runs a real scheduler to evaluate solutions, instead of using the estimator
 * 
 * */

//import jmetal.base.Configuration.SolutionType_;
//import jmetal.base.Configuration.VariableType_;
import java.io.File;
import java.io.IOException;

import jmetal.base.Problem;
import jmetal.base.solutionType.*;
import jmetal.base.*;
import jmetal.problems.jobsScheduling.JobsScheduling_real.JobSchedInstance;
import jmetal.util.JMException;

public class SerParr_real extends JobsScheduling_real {
	/**
	 * Constructor
	 * 
	 * @param solutionType
	 *            The solution type must "Int"
	 * @throws ClassNotFoundException
	 * @throws JMException
	 * @throws IOException 
	 */
	public SerParr_real(Integer solutionNum) throws ClassNotFoundException,
			JMException {

		super();

		instanceNo_ = solutionNum.intValue();
		
		String ssfMod = path_network_ + String.format(name_, solutionNum.intValue()) + ".ssfMod"; 
		/*
		File ssfModFile = new File(ssfMod);
		if (!ssfModFile.exists()) {
			File ssfModNetworkFile = new File(path_network_
					+ String.format(name_, solutionNum.intValue())
					+ ".ssfMod");

			copyFile(ssfModNetworkFile, ssfModFile);
		}
		*/
		instance = new JobSchedInstance(ssfMod);

		instance.instSchedCNs = path_network_
				+ String.format(name_, solutionNum.intValue())
				+ ".ssfModCNsSergio";

		instance.instSchedJobs = path_network_
				+ String.format(name_, solutionNum.intValue())
				+ ".ssfModSergio.rev";
/*
		File instSchedJobsFile = new File(instance.instSchedJobs);
		if (!instSchedJobsFile.exists()) {
			File instSchedJobsNetworkFile = new File(path_network_
					+ String.format(name_, solutionNum.intValue())
					+ ".ssfModSergio.rev");

			copyFile(instSchedJobsNetworkFile, instSchedJobsFile);
		}

		File instSchedCNsFile = new File(instance.instSchedCNs);
		if (!instSchedCNsFile.exists()) {
			File instSchedCNsNetworkFile = new File(path_network_
					+ String.format(name_, solutionNum.intValue())
					+ ".ssfModCNsSergio");

			copyFile(instSchedCNsNetworkFile, instSchedCNsFile);
		}
*/
		if (verbose)
			for (int i = 0; i < instance.getNumberJobs(); i++)
				System.out.println("Job " + i + " requires "
						+ instance.requiredProcs(i) + " processors");

		numberJobs_ = instance.getNumberJobs();
		numberCNs_ = instance.getNumberCNs();

		// int numberOfTasks = 100 ;
		// int numberOfCNs = 20 ;
		//
		numberOfVariables_ = 1; // We have one single variable that is a
								// permutation of numberOfJobs + numberOfCNs
								// numbers
		length_ = new int[1];
		length_[0] = numberJobs_ + numberCNs_ - 1;

		problemName_ = name_;

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			lowerLimit_[i] = 0;
			upperLimit_[i] = numberOfVariables_;
		} // for

		// solutionType_ = Enum.valueOf( SolutionType_.class , solutionType );
		//if (solutionType.compareTo("Permutation") == 0)
			solutionType_ = new PermutationSolutionType(this);
		/*else {
			System.out.println("Error: solution type " + solutionType
					+ " invalid");
			System.exit(-1);
		}*/
	} // Small

	private String name_ = "Bernabe-5-1000-0-0-100-0-%02d";

	@Override
	public String GetName() {
		return name_;
	}

	@Override
	public void SetName(String name) {
		name_ = name;
	}
} // Small
