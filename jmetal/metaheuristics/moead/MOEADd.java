/**
 * MOEAD.java
 * @author Antonio J. Nebro
 * @version 1.0
 */
package jmetal.metaheuristics.moead;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import jmetal.base.*;
import jmetal.metaheuristics.moead.Utils;
import jmetal.util.*;

import java.util.Vector;
import jmetal.base.Algorithm;
import jmetal.base.Problem;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.util.PseudoRandom;

public class MOEADd extends Algorithm {

    int[] contributionCounter_; // contribution per crossover operator
    double[] contribution_; // contribution per crossover operator
    int currentCrossover_;
    private Problem problem_;
    /**
     * Population size
     */
    private int populationSize_;
    /**
     * Stores the population
     */
    private SolutionSet population_;
    /**
     * Z vector (ideal point)
     */
    double[] z_;
    /**
     * Lambda vectors
     */
    //Vector<Vector<Double>> lambda_ ;
    double[][] lambda_;
    /**
     * T: neighbour size
     */
    int T_;
    /**
     * Neighborhood
     */
    int[][] neighborhood_;
    /**
     * delta: probability that parent solutions are selected from neighbourhood
     */
    double delta_;
    /**
     * nr: maximal number of solutions replaced by each child solution
     */
    int nr_;
    Solution[] indArray_;
    String functionType_;
    int evaluations_;
    /**
     * Operators
     */
    Operator crossover_;
    Operator mutation_;
    Operator selection_;
    String dataDirectory_;

    /**
     * Constructor
     * @param problem Problem to solve
     */
    public MOEADd(Problem problem) {
        problem_ = problem;

        functionType_ = "_TCHE1";

    } // DMOEA

    public SolutionSet execute() throws JMException, ClassNotFoundException {
        int maxEvaluations;

        evaluations_ = 0;
        maxEvaluations = ((Integer) this.getInputParameter("maxEvaluations")).intValue();
        populationSize_ = ((Integer) this.getInputParameter("populationSize")).intValue();
        dataDirectory_ = this.getInputParameter("dataDirectory").toString();

        population_ = new SolutionSet(populationSize_);
        indArray_ = new Solution[problem_.getNumberOfObjectives()];

        T_ = 20;
        delta_ = 0.9;
        nr_ = 2;

        T_ = (int) (0.1 * populationSize_);
        delta_ = 0.9;
        nr_ = (int) (0.01 * populationSize_);

        neighborhood_ = new int[populationSize_][T_];

        z_ = new double[problem_.getNumberOfObjectives()];
        //lambda_ = new Vector(problem_.getNumberOfObjectives()) ;
        lambda_ = new double[populationSize_][problem_.getNumberOfObjectives()];

        crossover_ = operators_.get("crossover"); // default: DE crossover
        mutation_ = operators_.get("mutation");  // default: polynomial mutation
        selection_ = operators_.get("selection");  // default: polynomial mutation

        Operator crossover2 = operators_.get("crossover2"); //SBX
        Operator selection2 = operators_.get("selection2");

        int N_O = 2;

        contribution_ = new double[N_O];
        contributionCounter_ = new int[N_O];

        contribution_[0] = (double) (populationSize_ / (double) N_O)
                / (double) populationSize_;
        for (int i = 1; i < N_O; i++) {
            contribution_[i] = (double) (populationSize_ / (double) N_O)
                    / (double) populationSize_ + (double) contribution_[i - 1];
        }


        // STEP 1. Initialization
        // STEP 1.1. Compute euclidean distances between weight vectors and find T
        initUniformWeight();

        initNeighborhood();

        // STEP 1.2. Initialize population
        initPopulation();

        // STEP 1.3. Initialize z_
        initIdealPoint();

        // STEP 2. Update
        while (evaluations_ < maxEvaluations){
            
            System.out.println("Evals: " + evaluations_ + "\t C1: " + contributionCounter_[0] + "\t C2: " + contributionCounter_[1]);

            int[] permutation = new int[populationSize_];
            Utils.randomPermutation(permutation, populationSize_);

            for (int i = 0; i < populationSize_; i++) {
                //int n = permutation[i]; // or int n = i;
                int n = i; // or int n = i;
                int type;
                double rnd = PseudoRandom.randDouble();

                // STEP 2.1. Mating selection based on probability
                if (rnd < delta_) // if (rnd < realb)
                {
                    type = 1;   // neighborhood
                } else {
                    type = 2;   // whole population
                }

                SolutionSet matingPool = matingSelection(n, 2, type);

                if (evaluations_ < maxEvaluations) {
                    boolean found = false;
                    int selected = 0;
                    double rand = PseudoRandom.randDouble();
                    Solution child = null;
                    for (selected = 0; selected < N_O; selected++) {
                        if (!found && (rand <= contribution_[selected])) {
                            if (selected == 0) {/* seleccion: torneo binario, cruce: SBX */

                                Solution[] parents = new Solution[2];
                                parents[0] = (Solution) selection2.execute(population_);
                                parents[1] = population_.get(n);

                                Solution[] offSpring = (Solution[]) crossover2.execute(parents);
                                child = offSpring[0];


                            } else if (selected == 1) { /* selección: aleatoria sin repetición, cruce: DE*/

                                Solution[] parents = new Solution[3];

                                Solution[] tmpParents = (Solution[]) selection_.execute(new Object[]{matingPool, 2, -1});

                                parents[0] = tmpParents[0];
                                parents[1] = tmpParents[1];
                                parents[2] = population_.get(n);
                                child = (Solution) crossover_.execute(new Object[]{population_.get(n), parents});
                            } else {
                                System.out.println("ERROR GRAVE");
                            }

                            child.setFitness((int) selected);
                            found = true;

                            // Apply mutation
                            mutation_.execute(child);

                            // Evaluation
                            problem_.evaluate(child);

                            evaluations_++;

                            // STEP 2.3. Repair. Not necessary

                            // STEP 2.4. Update z_
                            updateReference(child);

                            // STEP 2.5. Update of solutions
                            updateProblem(child, n, type);

                        }  //if
                    }//for
                } // if
            } //for

            // CONTRIBUTION CALCULATING PHASE
            // First: reset contribution counter
            for (int i = 0; i < N_O; i++) {
                contributionCounter_[i] = 0;
            }


            // Second: deteRmine the contribution of each operator
            for (int i = 0; i < population_.size(); i++) {
                if ((int) population_.get(i).getFitness() != -1) {
                    contributionCounter_[(int) population_.get(i).getFitness()] += 1;
                }
                population_.get(i).setFitness(-1);
            }

            int minimumContribution = 2;
            int totalContributionCounter = 0;

            for (int i = 0; i < N_O; i++) {
                if (contributionCounter_[i] < minimumContribution) {
                    contributionCounter_[i] = minimumContribution;
                }
                totalContributionCounter += contributionCounter_[i];
            }

            // Third: calculating contribution
            contribution_[0] = contributionCounter_[0] * 1.0
                    / (double) totalContributionCounter;
            for (int i = 1; i < N_O; i++) {
                contribution_[i] = contribution_[i - 1] + 1.0
                        * contributionCounter_[i]
                        / (double) totalContributionCounter;
            }
            
        }//while

        return population_;
    }//execute

    /**
     * initUniformWeight
     */
    public void initUniformWeight() {
        if ((problem_.getNumberOfObjectives() == 2) && (populationSize_ < 300)) {
            for (int n = 0; n < populationSize_; n++) {
                double a = 1.0 * n / (populationSize_ - 1);
                lambda_[n][0] = a;
                lambda_[n][1] = 1 - a;
            } // for
        } // if
        else {
            String dataFileName;
            dataFileName = "W" + problem_.getNumberOfObjectives() + "D_"
                    + populationSize_ + ".dat";

            System.out.println(dataDirectory_);
            System.out.println(dataDirectory_ + "/" + dataFileName);

            try {
                // Open the file
                FileInputStream fis = new FileInputStream(dataDirectory_ + "/" + dataFileName);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);

                int numberOfObjectives = 0;
                int i = 0;
                int j = 0;
                String aux = br.readLine();
                while (aux != null) {
                    StringTokenizer st = new StringTokenizer(aux);
                    j = 0;
                    numberOfObjectives = st.countTokens();
                    while (st.hasMoreTokens()) {
                        double value = (new Double(st.nextToken())).doubleValue();
                        lambda_[i][j] = value;
                        //System.out.println("lambda["+i+","+j+"] = " + value) ;
                        j++;
                    }
                    aux = br.readLine();
                    i++;
                }
                br.close();
            } catch (Exception e) {
                System.out.println("initUniformWeight: failed when reading for file: " + dataDirectory_ + "/" + dataFileName);
                e.printStackTrace();
            }
        } // else

        //System.exit(0) ;
    } // initUniformWeight

    /**
     *
     */
    public void initNeighborhood() {
        double[] x = new double[populationSize_];
        int[] idx = new int[populationSize_];

        for (int i = 0; i < populationSize_; i++) {
            // calculate the distances based on weight vectors
            for (int j = 0; j < populationSize_; j++) {
                x[j] = Utils.distVector(lambda_[i], lambda_[j]);
                //x[j] = dist_vector(population[i].namda,population[j].namda);
                idx[j] = j;
                //System.out.println("x["+j+"]: "+x[j]+ ". idx["+j+"]: "+idx[j]) ;
            } // for

            // find 'niche' nearest neighboring subproblems
            Utils.minFastSort(x, idx, populationSize_, T_);
            //minfastsort(x,idx,population.size(),niche);

            for (int k = 0; k < T_; k++) {
                neighborhood_[i][k] = idx[k];
                //System.out.println("neg["+i+","+k+"]: "+ neighborhood_[i][k]) ;
            }
        } // for
    } // initNeighborhood

    /**
     *
     */
    public void initPopulation() throws JMException, ClassNotFoundException {
        for (int i = 0; i < populationSize_; i++) {
            Solution newSolution = new Solution(problem_);

            problem_.evaluate(newSolution);
            evaluations_++;
            population_.add(newSolution);
        } // for
    } // initPopulation

    /**
     *
     */
    void initIdealPoint() throws JMException, ClassNotFoundException {
        for (int i = 0; i < problem_.getNumberOfObjectives(); i++) {
            z_[i] = 1.0e+30;
            indArray_[i] = new Solution(problem_);
            problem_.evaluate(indArray_[i]);
            evaluations_++;
        } // for

        for (int i = 0; i < populationSize_; i++) {
            updateReference(population_.get(i));
        } // for
    } // initIdealPoint

    /**
     *
     */
    public void matingSelection(Vector<Integer> list, int cid, int size, int type) {
        // list : the set of the indexes of selected mating parents
        // cid  : the id of current subproblem
        // size : the number of selected mating parents
        // type : 1 - neighborhood; otherwise - whole population
        int ss;
        int r;
        int p;

        ss = neighborhood_[cid].length;
        while (list.size() < size) {
            if (type == 1) {
                r = PseudoRandom.randInt(0, ss - 1);
                p = neighborhood_[cid][r];
                //p = population[cid].table[r];
            } else {
                p = PseudoRandom.randInt(0, populationSize_ - 1);
            }
            boolean flag = true;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) == p) // p is in the list
                {
                    flag = false;
                    break;
                }
            }

            //if (flag) list.push_back(p);
            if (flag) {
                list.addElement(p);
            }
        }
    } // matingSelection

    /**
     *
     * @param individual
     */
    void updateReference(Solution individual) {
        for (int n = 0; n < problem_.getNumberOfObjectives(); n++) {
            if (individual.getObjective(n) < z_[n]) {
                z_[n] = individual.getObjective(n);

                indArray_[n] = individual;
            }
        }
    } // updateReference

    /**
     * @param individual
     * @param id
     * @param type
     */
    void updateProblem(Solution indiv, int id, int type) {
        // indiv: child solution
        // id:   the id of current subproblem
        // type: update solutions in - neighborhood (1) or whole population (otherwise)
        int size;
        int time;

        time = 0;

        if (type == 1) {
            size = neighborhood_[id].length;
        } else {
            size = population_.size();
        }
        int[] perm = new int[size];

        Utils.randomPermutation(perm, size);

        for (int i = 0; i < size; i++) {
            int k;
            if (type == 1) {
                k = neighborhood_[id][perm[i]];
            } else {
                k = perm[i];      // calculate the values of objective function regarding the current subproblem
            }
            double f1, f2;

            f1 = fitnessFunction(population_.get(k), lambda_[k]);
            f2 = fitnessFunction(indiv, lambda_[k]);

            if (f2 < f1) {
                population_.replace(k, new Solution(indiv));
                //population[k].indiv = indiv;
                time++;
            }
            // the maximal number of solutions updated is not allowed to exceed 'limit'
            if (time >= nr_) {
                return;
            }
        }
    } // updateProblem

    double fitnessFunction(Solution individual, double[] lambda) {
        double fitness;
        fitness = 0.0;

        if (functionType_.equals("_TCHE1")) {
            double maxFun = -1.0e+30;

            for (int n = 0; n < problem_.getNumberOfObjectives(); n++) {
                double diff = Math.abs(individual.getObjective(n) - z_[n]);

                double feval;
                if (lambda[n] == 0) {
                    feval = 0.0001 * diff;
                } else {
                    feval = diff * lambda[n];
                }
                if (feval > maxFun) {
                    maxFun = feval;
                }
            } // for

            fitness = maxFun;
        } // if
        else {
            System.out.println("MOEAD.fitnessFunction: unknown type " + functionType_);
            System.exit(-1);
        }
        return fitness;
    } // fitnessEvaluation

    public SolutionSet matingSelection(int index, int size, int type) {
        SolutionSet neighbors;
        if (type == 2) {
            neighbors = population_;
        } else {
            neighbors = new SolutionSet(T_);
            for (int i = 0; i < T_; i++) {
                neighbors.add(population_.get(neighborhood_[index][i]));
            }
        }

        return neighbors;
    }
} // MOEAD

