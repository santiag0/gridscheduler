/**
 * CellDE_main.java
 *
 * @author Juan J. Durillo
 * @author Antonio J. Nebro
 * @version 1.0
 * 
 * This algorithm is described in:
 *   J.J. Durillo, A.J. Nebro, F. Luna, E. Alba "Solving Three-Objective 
 *   Optimization Problems Using a new Hybrid Cellular Genetic Algorithm". 
 *   PPSN'08. Dortmund. September 2008. 
 */
package jmetal.metaheuristics.cellde;

import jmetal.base.*;
import jmetal.base.operator.crossover.*   ;
import jmetal.base.operator.selection.*   ;
import jmetal.experiments.settings.NSGAII_Settings;
import jmetal.experiments.settings.SELF_Settings;
import jmetal.problems.*                  ;
import jmetal.problems.LZ09.*;

import jmetal.util.Configuration;
import jmetal.util.JMException;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import jmetal.qualityIndicator.QualityIndicator;

public class SELF_main {
  public static Logger      logger_ ;      // Logger object
  public static FileHandler fileHandler_ ; // FileHandler object

  /**
   * @param args Command line arguments.
   * @throws JMException 
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three choices
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName
   *      - jmetal.metaheuristics.nsgaII.NSGAII_main problemName paretoFrontFile
   */
  public static void main(String [] args) throws 
                                 JMException, SecurityException, IOException, ClassNotFoundException {
    Problem   problem   ;         // The problem to solve
    Algorithm algorithm ;         // The algorithm to use
    Operator  selection ;
    Operator  crossover ;
    
    QualityIndicator indicators ; // Object to get quality indicators

    // Logger object and file to store log messages
//    logger_      = Configuration.logger_ ;
//    fileHandler_ = new FileHandler("SELF_main.log");
//    logger_.addHandler(fileHandler_) ;

    if (args.length != 10) {
    	System.out.println("ERROR. Try java -cp . SELF_main Problem PopSize WindowSize MinProb CR F Pc Pm DistrIndxRec DistrIndxMut");
    	System.exit(-1);
    }

    indicators = null ;
    
    int prob = new Integer(args[0]).intValue();
    
    problem = null;
    String pareto = null;
    
    switch (prob) {
    	case 1: problem = new LZ09_F1("Real");
    		pareto = "./paretoFronts/LZ07_F1.pf";
    		break;
    	case 2: problem = new LZ09_F2("Real");
			pareto = "./paretoFronts/LZ07_F2.pf";
			break;
    	case 3: problem = new LZ09_F3("Real");
			pareto = "./paretoFronts/LZ07_F3.pf";
			break;
    	case 4: problem = new LZ09_F4("Real");
			pareto = "./paretoFronts/LZ07_F4.pf";
			break;
    	case 5: problem = new LZ09_F5("Real");
			pareto = "./paretoFronts/LZ07_F5.pf";
			break;
    	case 6: problem = new LZ09_F6("Real");
			pareto = "./paretoFronts/LZ07_F6.pf";
			break;
    	case 7: problem = new LZ09_F7("Real");
			pareto = "./paretoFronts/LZ07_F7.pf";
			break;
    	case 8: problem = new LZ09_F8("Real");
			pareto = "./paretoFronts/LZ07_F8.pf";
			break;
    	case 9: problem = new LZ09_F9("Real");
			pareto = "./paretoFronts/LZ07_F9.pf";
			break;
    	
    	default: problem = new LZ09_F1("Real");
			pareto = "./paretoFronts/LZ07_F1.pf";
			break;
    }
    
    algorithm = new SELF_Settings(problem).configure(args);
    
    
    // Algorithm parameters
//    algorithm.setInputParameter("populationSize", new Integer(args[1]).intValue());
//    algorithm.setInputParameter("archiveSize",300);
//    algorithm.setInputParameter("maxEvaluations",150000);
//    algorithm.setInputParameter("feedBack", 0);
//    
//    algorithm.setInputParameter("windowSize", new Integer(args[2]).intValue());
//    algorithm.setInputParameter("minProb", new Integer(args[3]).intValue());
//    
//    // Crossover operator 
//    crossover = CrossoverFactory.getCrossoverOperator("DifferentialEvolutionCrossover");                   
//    crossover.setParameter("CR", 0.5);                   
//    crossover.setParameter("F", 0.5);
//    
//    // Add the operators to the algorithm
//    selection = SelectionFactory.getSelectionOperator("BinaryTournament") ; 
//
//    algorithm.addOperator("crossover",crossover);
//    algorithm.addOperator("selection",selection);
    
    indicators = new QualityIndicator(problem, "./paretoFronts/LZ07_F1.pf");
    
    // Execute the Algorithm
    double hv = 0.0;
    
    int runs = 30;
    
    for (int i=0; i<runs; i++) {
//	    long initTime = System.currentTimeMillis();
	    SolutionSet population = algorithm.execute();
//	    long estimatedTime = System.currentTimeMillis() - initTime;
//	    System.out.println("Total execution time: "+estimatedTime);
	    
	    hv += indicators.getHypervolume(population); 
    }
    
    hv = hv/runs;
    
    System.out.println(hv);

//    // Log messages 
//    logger_.info("Objectives values have been writen to file FUN");
//    population.printObjectivesToFile("FUN");
//    logger_.info("Variables values have been writen to file VAR");
//    population.printVariablesToFile("VAR");
//
//    if (indicators != null) {
//      logger_.info("Quality indicators") ;
//      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
//      logger_.info("GD         : " + indicators.getGD(population)) ;
//      logger_.info("IGD        : " + indicators.getIGD(population)) ;
//      logger_.info("Spread     : " + indicators.getSpread(population)) ;
//    } // if
  }//main
} // CellDE_main