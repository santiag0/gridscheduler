/**
 * CellDE.java
 * @author Juan J. Durillo, Antonio J. Nebro
 * @version 1.0
 */
package jmetal.metaheuristics.cellde;


import jmetal.base.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Comparator;
import jmetal.base.operator.comparator.*;
import jmetal.base.operator.mutation.PolynomialMutation;
import jmetal.experiments.offspring.PolynomialOffspringGenerator;
import jmetal.experiments.offspring.Offspring;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.*;
import jmetal.util.archive.CrowdingArchive;

/**
 * Class representing the MoCell algorithm
 */
public class pSteadyStateSelfAdaptiveWindowSBXPol extends Algorithm {

  //->fields
  private Problem problem_;          //The problem to solve
  int[] contributionCounter_; // contribution per crossover operator
  double[] contribution_; // contribution per crossover operator
  double total = 0.0;


  /******** THIS PIECE OF CODE HAS BEEN ADDED BY JUANJO ******************/
  int [][] angles_;
  /************************************************************************/

  int currentCrossover_;
  int[][] contributionArchiveCounter_;
  public int windowsSize = 10;
  public double mincontribution = 0.30;
  int windowsIndex = 0;

  final boolean TRAZA = true;

  public pSteadyStateSelfAdaptiveWindowSBXPol(Problem problem) {
    problem_ = problem;
  }

  /** Execute the algorithm
   * @throws JMException
   * @throws ClassNotFoundException */
  public SolutionSet execute() throws JMException, ClassNotFoundException {
    //Init the param
    int populationSize, archiveSize, maxEvaluations, evaluations;
    Operator mutationOperator, crossoverOperator, selectionOperator;
    SolutionSet currentPopulation;
    SolutionSet archive;
    //SolutionSet todas = new SolutionSet(3000000);
    SolutionSet[] neighbors;
    Neighborhood neighborhood;
    boolean change = false;
    int times = 0;

    QualityIndicator indicators; // QualityIndicator object

    Comparator dominance = new DominanceComparator();
    Comparator crowdingComparator = new CrowdingComparator();
    Distance distance = new Distance();

    //Init the params

    //Read the params
    populationSize = ((Integer) getInputParameter("populationSize")).intValue();
    archiveSize = ((Integer) getInputParameter("archiveSize")).intValue();
    maxEvaluations = ((Integer) getInputParameter("maxEvaluations")).intValue();


    indicators = (QualityIndicator) getInputParameter("indicators");
    int feedback = ((Integer) getInputParameter("feedback")).intValue();;

    //Init the variables
    currentPopulation = new SolutionSet(populationSize);
    archive = new CrowdingArchive(archiveSize, problem_.getNumberOfObjectives());
    evaluations = 0;
    //neighborhood = new Neighborhood(populationSize);
    //neighbors = new SolutionSet[populationSize];

    Offspring[] getOffspring;
    int N_O; // number of offpring objects

    getOffspring = ((Offspring[]) getInputParameter("offspringsCreators"));
    N_O = getOffspring.length;

    contribution_               = new double[N_O];
    contributionCounter_        = new int[N_O];
    contributionArchiveCounter_ = new int[N_O][windowsSize];

    contribution_[0] = (double) (populationSize / (double) N_O) / (double) populationSize;
    for (int i = 1; i < N_O; i++) {
      contribution_[i] = (double) (populationSize / (double) N_O) / (double) populationSize + (double) contribution_[i - 1];
    }


    currentCrossover_ = 0;

    int iterationsWithMinimumContribution = 0;

    //Create the initial population
    for (int i = 0; i < populationSize; i++) {
      Solution individual = new Solution(problem_);
      problem_.evaluate(individual);
      problem_.evaluateConstraints(individual);
      //todas.add(new Solution(individual));
      currentPopulation.add(individual);
      individual.setLocation(i);
      evaluations++;
    }

    for (int i = 0; i < N_O; i++) {
        for (int j = 0; j < windowsSize; j++) {
            contributionArchiveCounter_[i][j] = 0;
        }
    }

    
    // Code added by Bernab� to plot the contribution of every operator
    int generation = 0;
    int contrDE = 0;
    int contrSBX = 0;
    int contrPol = 0;
    PrintWriter DE=null, SBX=null, Pol=null;
    
//    try {
//		DE = new PrintWriter(new FileWriter("DENoDE.dat"));
//		SBX = new PrintWriter(new FileWriter("SBXNoDE.dat"));
//		Pol = new PrintWriter(new FileWriter("PolNoDE.dat"));
//	} catch (IOException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
      
    
    while (evaluations < maxEvaluations) {
      

      for (int ind = 0; ind < currentPopulation.size(); ind++) {
     //   int ind = PseudoRandom.randInt(0, populationSize-1);
        Solution individual = new Solution(currentPopulation.get(ind));

        Solution[] parents = new Solution[2];
        Solution offSpring = null;

//        neighbors[ind] = neighborhood.getEightNeighbors(currentPopulation, ind);
        //neighbors[ind].add(individual);

        boolean found = false;

        int selected = 0;
        double rnd = PseudoRandom.randDouble();
        for (selected = 0; selected < N_O; selected++) {

          if (!found && (rnd <= contribution_[selected])) {
            if ("DE".equals(getOffspring[selected].id())) {
            	offSpring = getOffspring[selected].getOffspring(currentPopulation, individual, ind) ;
            	contrDE++;
            } else if ("SBX_Polynomial".equals(getOffspring[selected].id())) {
            	offSpring = getOffspring[selected].getOffspring(currentPopulation);
            	contrSBX++;
           } else if ("Polynomial".equals(getOffspring[selected].id())) {
                //offSpring = getOffspring[selected].getOffspring(currentPopulation);
                offSpring = ((PolynomialOffspringGenerator)getOffspring[selected]).getOffspring(individual);
                contrPol++;
            }

            offSpring.setFitness((int) selected);
            currentCrossover_ = selected;
            found = true;
          } // if
        } // for

        // Evaluate individual an its constraints
        problem_.evaluate(offSpring);
        problem_.evaluateConstraints(offSpring);
        //todas.add(new Solution(offSpring));


        evaluations++;
        if (evaluations  %  25000 == 0)
          change = true;

        offSpring.setLocation(-1);
        SolutionSet ss = new SolutionSet(currentPopulation.size()+1);
        for (int i = 0; i < currentPopulation.size();i++) {
        	ss.add(currentPopulation.get(i));
        }
        ss.add(offSpring);

        Ranking rank = new Ranking(ss);
      for (int j = 0; j < rank.getNumberOfSubfronts(); j++) {
        distance.crowdingDistanceAssignment(rank.getSubfront(j),
          problem_.getNumberOfObjectives());
      }
      ss.sort(crowdingComparator);
      Solution worst = ss.get(ss.size() - 1);

      if (worst.getLocation() == -1) { //The worst is the offspring
        archive.add(new Solution(offSpring));
      } else {
        offSpring.setLocation(worst.getLocation());
        currentPopulation.replace(offSpring.getLocation(), offSpring);
        archive.add(new Solution(offSpring));
      }
    }

      for (int i = 0; i < N_O; i++) {
        contributionCounter_[i] = 0;
        contributionArchiveCounter_[i][windowsIndex] = 0;
      }



      /************************************************************************/

      for (int i = 0; i < archive.size(); i++) {
        //System.out.println(i + " = " + (int)archive.get(i).getFitness()) ;
        if ((int) archive.get(i).getFitness() != -1) {
          contributionArchiveCounter_[(int) archive.get(i).getFitness()][windowsIndex] += 1;
          //System.out.println(i + ": " + archive.get(i).getCrowdingDistance()) ;
        }
        archive.get(i).setFitness(-1);
      }
      windowsIndex = (windowsIndex + 1) % windowsSize;

      int minimumContribution             = 0;
      int totalContributionCounter        = 0;
      int totalcontributionArchiveCounter = 0;

      for (int i = 0; i < N_O; i++) {
        for (int j = 0; j < windowsSize; j++) {
        if (contributionCounter_[i] < minimumContribution) {
          contributionCounter_[i] = minimumContribution;
        }
        if (contributionArchiveCounter_[i][j] < minimumContribution) {
          contributionArchiveCounter_[i][j] = minimumContribution;
        }
        totalContributionCounter += contributionCounter_[i];
        totalcontributionArchiveCounter += contributionArchiveCounter_[i][j];
      }
      }

      boolean minimum = true;
      for (int i = 0; i < N_O;i++) {
    	  minimum = minimum && (contributionCounter_[i] == minimumContribution);
      }
      if (minimum)
    	  times ++;
//
//      if (minimum) {
//    	  times--;
//    	  if (times == 0) {
//    		  System.out.println("A cambiarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
//    		  for (int i = 0; i < N_O; i++) {
//    		//	  contributionCounter_[i] = PseudoRandom.randInt(minimumContribution,archive.size()-1);
//    		//	  contributionArchiveCounter_[i] = contributionCounter_[i];
//    		  }
//    		  times = 10;
//    	  }
//      } else {
//    	  times = 10;
//      }
//


      int aux = 0;
      for (int j = 0; j < windowsSize; j++) {
          aux += contributionArchiveCounter_[0][j];
      }
//      contribution_[0] = (double) aux/ (double) totalcontributionArchiveCounter;
//      System.out.print(contribution_[0]+" ");
//      for (int i = 1; i < N_O; i++) {
//          aux = 0;
//          for (int j = 0; j < windowsSize; j++) {
//            aux += contributionArchiveCounter_[i][j];
//          }
//        contribution_[i] = contribution_[i - 1] + (double) aux / (double) totalcontributionArchiveCounter;
//        System.out.print(((double) aux / (double) totalcontributionArchiveCounter)+" ");
//
//      }
//      System.out.println();

      double [] miContribucion = new double[N_O];
      if (totalcontributionArchiveCounter > 0)
        miContribucion[0] = (double) aux/ (double) totalcontributionArchiveCounter;
      for (int i = 1; i < N_O; i++) {
          aux = 0;
          for (int j = 0; j < windowsSize; j++) {
            aux += contributionArchiveCounter_[i][j];
          }
          if (totalcontributionArchiveCounter > 0)
            miContribucion[i] = (double) aux / (double) totalcontributionArchiveCounter;
      }
        
      for (int i = 0; i < N_O; i++) {
          if (miContribucion[i] < this.mincontribution) {
              miContribucion[i] = this.mincontribution;
          }
      }

      total = 0.0;
      for (int i = 0; i < N_O; i++) {
              total +=  miContribucion[i];
      }

      //System.out.println(total);

      if (total == 0.3) {
          //System.out.println("Hemos llegado a 0.3!!!!!");
          for (int i = 0; i < N_O; i++) {
                  miContribucion[i] = 1.0 / (double) N_O;
          }
          total = 1.0;
      } else {
          for (int i = 0; i < N_O; i++) {
                  miContribucion[i] = miContribucion[i] / total;
          }
      }

      contribution_[0] = miContribucion[0];
      System.out.print(contribution_[0]+" ");
      for (int i = 1; i < N_O; i++) {
        contribution_[i] = contribution_[i - 1] + miContribucion[i];
        //System.out.print(((double) miContribucion[i])+" ");

      }
      //System.out.print(indicators.getHypervolume(archive));
      //System.out.println();



     //Feedback!!!!
//     int realimentacion = 0;//(int)(feedback * 0.01) * populationSize;
//     for (int j = 0; j < realimentacion; j++){
//         if (archive.size() > j){
//           int r = PseudoRandom.randInt(0,currentPopulation.size()-1);
//           if (r < currentPopulation.size()){
//             Solution individual = archive.get(j);
//             individual.setLocation(r);
//             currentPopulation.replace(r,new Solution(individual));
//           }
//         }
//       }

      generation++;
      
//      DE.print(contrDE + ", ");
//      SBX.print(contrSBX + ", ");
//      Pol.print(contrPol + ", ");
//      
//      contrDE =0;
//      contrSBX=0;
//      contrPol=0;
  }
//    
//    DE.close();
//    SBX.close();
//    Pol.close();
    

    //System.out.println(evaluations);
    currentPopulation.printObjectivesToFile("malla");
    //todas.printObjectivesToFile("todas");
    //todas.printVariablesToFile("todas.var");
    //System.out.println(populationSize);
    return archive;
  }
} // CellDE2
