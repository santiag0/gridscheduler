/*
 * SBXCrossoverAndPolynomialMutation.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * This class returns a solution after applying SBX and Polynomial mutation
 */
package jmetal.experiments.offspring;

import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.base.Operator;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.util.JMException;

public class PolynomialOffspringGenerator extends Offspring {
  
  Operator mutation_;
  Operator selection_;

  public PolynomialOffspringGenerator(double mutationProbability,
                                     double distributionIndexForMutation
                                     ) throws JMException {     
    mutation_ = MutationFactory.getMutationOperator("PolynomialMutation");
    mutation_.setParameter("probability", mutationProbability);
    mutation_.setParameter("distributionIndex", distributionIndexForMutation);
    selection_ = SelectionFactory.getSelectionOperator("BinaryTournament");
    id_ = "Polynomial";
  }

  public Solution getOffspring(SolutionSet solutionSet) {
    Solution[] parents = new Solution[2];
    Solution offSpring = null;

    try {
      offSpring = new Solution((Solution) selection_.execute(solutionSet));
      mutation_.execute(offSpring);
      
      // Added by Bernab� Dorronsoro to force that all the variables will be in the considered interval
//      for (int j=0; j<offSpring.numberOfVariables(); j++) {
//    	  double value = offSpring.getDecisionVariables()[j].getValue();
//	      if (value < offSpring.getDecisionVariables()[j].getLowerBound()) {
//	        value = 2.0 * offSpring.getDecisionVariables()[j].getLowerBound() - value;
//	      }
//	      if (value > offSpring.getDecisionVariables()[j].getUpperBound()) {
//	    	  value = 2.0 * offSpring.getDecisionVariables()[j].getUpperBound() - value;
//	      }
//      }
      
    } catch (JMException ex) {
      Logger.getLogger(PolynomialOffspringGenerator.class.getName()).log(Level.SEVERE, null, ex);
    }
    return offSpring;

  } // getOffpring

    public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive) {
        return getOffspring(solutionSet);

  } // getOffpring
    
    public Solution getOffspring(Solution solution) {
    	Solution res = new Solution(solution);
    	try {
            mutation_.execute(res);
        } catch (JMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return res;
    	
    }
    
    
} // DifferentialEvolutionOffspring


