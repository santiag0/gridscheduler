/*
 * SBXCrossoverAndPolynomialMutation.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * This class returns a solution after applying SBX and Polynomial mutation
 */
package jmetal.experiments.offspring;

import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.base.Operator;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.util.JMException;

public class SBXCrossoverAndPolynomialMutation extends Offspring {

  double mutationProbability_ = 0.0;
  double crossoverProbability_ = 0.9;
  double distributionIndexForMutation_ = 20;
  double distributionIndexForCrossover_ = 20;
  Operator crossover_;
  Operator mutation_;
  Operator selection_;

  public SBXCrossoverAndPolynomialMutation(double mutationProbability,
    double crossoverProbability,
    double distributionIndexForMutation,
    double distributionIndexForCrossover) throws JMException {
    mutationProbability_ = mutationProbability;
    crossoverProbability_ = crossoverProbability;
    distributionIndexForMutation_ = distributionIndexForMutation;
    distributionIndexForCrossover_ = distributionIndexForCrossover;

    // Crossover operator
    crossover_ = CrossoverFactory.getCrossoverOperator("SBXCrossover");
    crossover_.setParameter("probability", crossoverProbability_);
    crossover_.setParameter("distributionIndex", distributionIndexForCrossover_);

    mutation_ = MutationFactory.getMutationOperator("PolynomialMutation");
    mutation_.setParameter("probability", mutationProbability_);
    mutation_.setParameter("distributionIndex", distributionIndexForMutation_);

    selection_ = SelectionFactory.getSelectionOperator("BinaryTournament");
    
    id_ = "SBX_Polynomial";
  }

  public Solution getOffspring(SolutionSet solutionSet) {
    Solution[] parents = new Solution[2];
    Solution offSpring = null;

    try {
      parents[0] = (Solution) selection_.execute(solutionSet);
      parents[1] = (Solution) selection_.execute(solutionSet);

      Solution[] children = (Solution[]) crossover_.execute(parents);
      offSpring = children[0];
      mutation_.execute(offSpring);
      //Create a new solution, using DE
    } catch (JMException ex) {
      Logger.getLogger(SBXCrossoverAndPolynomialMutation.class.getName()).log(Level.SEVERE, null, ex);
    }
    return offSpring;

  } // getOffpring

    public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive) {
    Solution[] parents = new Solution[2];
    Solution offSpring = null;

    try {
      parents[0] = (Solution) selection_.execute(solutionSet);

      if (archive.size() > 0) {
          parents[1] = (Solution)selection_.execute(archive);
      } else {
          parents[1] = (Solution)selection_.execute(solutionSet);
      }

      Solution[] children = (Solution[]) crossover_.execute(parents);
      offSpring = children[0];
      mutation_.execute(offSpring);
      
   // Added by Bernab� Dorronsoro to force that all the variables will be in the considered interval
//      for (int j=0; j<offSpring.numberOfVariables(); j++) {
//    	  double value = offSpring.getDecisionVariables()[j].getValue();
//	      if (value < offSpring.getDecisionVariables()[j].getLowerBound()) {
//	        value = 2.0 * offSpring.getDecisionVariables()[j].getLowerBound() - value;
//	      }
//	      if (value > offSpring.getDecisionVariables()[j].getUpperBound()) {
//	    	  value = 2.0 * offSpring.getDecisionVariables()[j].getUpperBound() - value;
//	      }
//      }
      
      //Create a new solution, using DE
    } catch (JMException ex) {
      Logger.getLogger(SBXCrossoverAndPolynomialMutation.class.getName()).log(Level.SEVERE, null, ex);
    }
    return offSpring;

  } // getOffpring
} // DifferentialEvolutionOffspring

