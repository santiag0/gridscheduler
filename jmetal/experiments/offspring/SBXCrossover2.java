/*
 * SBXCrossoverAndPolynomialMutation.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * This class returns a solution after applying SBX and Polynomial mutation
 */
package jmetal.experiments.offspring;

import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.base.Operator;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.util.JMException;

public class SBXCrossover2 extends Offspring {

  double crossoverProbability_ = 0.9;
  double distributionIndexForCrossover_ = 20;
  Operator crossover_;
  Operator selection_;

  public SBXCrossover2(double crossoverProbability,
    double distributionIndexForCrossover) throws JMException {
    crossoverProbability_ = crossoverProbability;
    distributionIndexForCrossover_ = distributionIndexForCrossover;

    // Crossover operator
    crossover_ = CrossoverFactory.getCrossoverOperator("SBXCrossover");
    crossover_.setParameter("probability", crossoverProbability_);
    crossover_.setParameter("distributionIndex", distributionIndexForCrossover_);

    selection_ = SelectionFactory.getSelectionOperator("BinaryTournament");
    
    id_ = "SBX_2";
  }

  public Solution getOffspring(SolutionSet solutionSet) {
    Solution[] parents = new Solution[2];
    Solution offSpring = null;

    try {
      parents[0] = (Solution) selection_.execute(solutionSet);
      parents[1] = (Solution) selection_.execute(solutionSet);

      Solution[] children = (Solution[]) crossover_.execute(parents);
      offSpring = children[0];
      //Create a new solution, using DE
    } catch (JMException ex) {
      Logger.getLogger(SBXCrossover2.class.getName()).log(Level.SEVERE, null, ex);
    }
    return offSpring;

  } // getOffpring

    public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive) {
    Solution[] parents = new Solution[2];
    Solution offSpring = null;

    try {
      parents[0] = (Solution) selection_.execute(solutionSet);

      if (archive.size() > 0) {
          parents[1] = (Solution)selection_.execute(archive);
      } else {
          parents[1] = (Solution)selection_.execute(solutionSet);
      }

      Solution[] children = (Solution[]) crossover_.execute(parents);
      offSpring = children[0];
      //Create a new solution, using DE
    } catch (JMException ex) {
      Logger.getLogger(SBXCrossover2.class.getName()).log(Level.SEVERE, null, ex);
    }
    return offSpring;

  } // getOffpring
} // DifferentialEvolutionOffspring

