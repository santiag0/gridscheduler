/**
 * DifferentialEvolutionOffspring.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * This class returns a solution after applying DE
 */

package jmetal.experiments.offspring;

import java.util.logging.Level;
import java.util.logging.Logger;
import jmetal.base.Operator;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.crossover.DifferentialEvolutionCrossover;
import jmetal.base.operator.crossover.de.DE_current_1_bin;
import jmetal.base.operator.crossover.de.DE_current_1_bin_SA;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.mutation.PolynomialMutation;
import jmetal.base.operator.selection.Selection;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.problems.LZ09.LZ09_F7;
import jmetal.util.JMException;

public class DifferentialEvolutionOffspringSA extends Offspring {
  double CR_ ;
  double F_  ;

  Operator crossover_ ;
  Operator selection_ ;
  Operator mutation_;

  public void DifferentialEvolutionOffspringSA() {

  }
  /**
   * Constructor
   * @param CR
   * @param F
   */
  public DifferentialEvolutionOffspringSA(double CR, double F, double mutationProbability, double eta_m)  {

    CR_ = CR ;
    F_  = F  ;
    try {
      // Crossover operator
      //crossover_ = CrossoverFactory.getCrossoverOperator("DifferentialEvolutionCrossover");
    	//crossover_ = new DifferentialEvolutionCrossover();
      //crossover_ = new DE_current_1_bin();
    	crossover_ = new DE_current_1_bin_SA();
    	//crossover_ = new DifferentialEvolutionX_PBX_SA();
    	//crossover_ = new DifferentialEvolutionPBXGaussian_SA();
      crossover_.setParameter("CR", CR_);
      crossover_.setParameter("F", F_);
      mutation_ = new PolynomialMutation();
      mutation_.setParameter("probability", mutationProbability);
      mutation_.setParameter("eta_m", 20.0);

      selection_ = SelectionFactory.getSelectionOperator("DifferentialEvolutionSelection");
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    id_ = "DE" ;
  }

  public Solution getOffspring(SolutionSet solutionSet, int index) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      parents = (Solution[]) selection_.execute(new Object[]{solutionSet, index});
      offSpring = (Solution)crossover_.execute(new Object[]{solutionSet.get(index), parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  public Solution getOffspring(SolutionSet solutionSet, Solution solution) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      parents = (Solution[]) selection_.execute(new Object[]{solutionSet, -1});
      offSpring = (Solution)crossover_.execute(new Object[]{solution, parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive, int index) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      if (archive.size() > 20)
        parents = (Solution[]) selection_.execute(new Object[]{archive, -1});
      else
        parents   = (Solution[]) selection_.execute(new Object[]{solutionSet, index});
      
      
        offSpring = (Solution)crossover_.execute(new Object[]{solutionSet.get(index), parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  
  
  public Solution getOffspring(SolutionSet solutionSet, Solution solution, int index) {
	    Solution[] parents = new Solution[3] ;
	    Solution offSpring = null ;

	    
	    
	    try {
	    	crossover_.setParameter("neighbors", solutionSet);
		    offSpring = (Solution) crossover_.execute(new Object[]{solution, index});		   		    
		    //mutation_.execute(offSpring);
		    
		     
	    } catch (JMException ex) {
	      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    

	    //Create a new solution, using DE
	    return offSpring ;
	  } // getOffpring
  
  
  public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive, Solution solution, int index) {
	    Solution[] parents = new Solution[3] ;
	    Solution offSpring = null ;

	    
	    
	    try {
	    	crossover_.setParameter("neighbors", solutionSet);
	    	crossover_.setParameter("archive", solutionSet);
		    offSpring = (Solution) crossover_.execute(new Object[]{solution, index});
		    mutation_.execute(offSpring);
		    //Operator mutation_ = MutationFactory.getMutationOperator("PolynomialMutation");
		    //mutation_.setParameter("probability", 1.0/ (4 * (new LZ09_F7("Real")).getNumberOfVariables()));
		    //mutation_.setParameter("distributionIndex", 20.0);
		    //mutation_.execute(offSpring);
		    
		     
	    } catch (JMException ex) {
	      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
	    }

	    //Create a new solution, using DE
	    return offSpring ;
	  } // getOffpring
  
} // DifferentialEvolutionOffspring

/*
public class DifferentialEvolutionOffspring extends Offspring {
  double CR_ ;
  double F_  ;

  Operator crossover_ ;
  Operator selection_ ;
  Operator mutation_;

  public void DifferentialEvolutionOffspring() {

  }
  /**
   * Constructor
   * @param CR
   * @param F
   
  public DifferentialEvolutionOffspring(double CR, double F, double mutationProbability, double eta_m)  {

    CR_ = CR ;
    F_  = F  ;
    try {
      // Crossover operator
      //crossover_ = CrossoverFactory.getCrossoverOperator("DifferentialEvolutionCrossover");
    	
    	
    	 crossover_ = new DifferentialEvolutionCrossover();
    	//crossover_ = new DifferentialEvolutionC2();
    
    	
    	//crossover_ = new DifferentialEvolutionX_PBX_SA();
    	//crossover_ = new DifferentialEvolutionPBXGaussian_SA();
      crossover_.setParameter("CR", CR_);
      crossover_.setParameter("F", F_);
      mutation_ = new PolynomialMutation();
      mutation_.setParameter("probability", mutationProbability);
      mutation_.setParameter("eta_m", 20.0);

      selection_ = SelectionFactory.getSelectionOperator("DifferentialEvolutionSelection");
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    id_ = "DE" ;
  }
  
  /**
   * Return on offspring from a solution set, indicating the selection operator
   * @param solutionSet
   * @return the offspring
   *
  public Solution getOffspring(SolutionSet solutionSet, int numberOfParents, int index, Selection selectionOperator) {
   Solution[] parents = new Solution[3] ;
   Solution offSpring = null ;
    try {
    	Solution[] tmpParents = (Solution[])selection_.execute(new Object[]{solutionSet, numberOfParents, -1}) ;

      parents[0] = tmpParents[0] ;
      parents[1] = tmpParents[1] ;
      parents[2] = solutionSet.get(index);

      offSpring = (Solution)crossover_.execute(new Object[]{solutionSet.get(index), parents});

    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

  	return null ;
  } ;


  public Solution getOffspring(SolutionSet solutionSet, int index) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      parents = (Solution[]) selection_.execute(new Object[]{solutionSet, index});
      offSpring = (Solution)crossover_.execute(new Object[]{solutionSet.get(index), parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  public Solution getOffspring(SolutionSet solutionSet, Solution solution) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      parents = (Solution[]) selection_.execute(new Object[]{solutionSet, -1});
      offSpring = (Solution)crossover_.execute(new Object[]{solution, parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive, int index) {
    Solution[] parents = new Solution[3] ;
    Solution offSpring = null ;

    try {
      if (archive.size() > 20)
        parents = (Solution[]) selection_.execute(new Object[]{archive, -1});
      else
        parents   = (Solution[]) selection_.execute(new Object[]{solutionSet, index});
      
      
        offSpring = (Solution)crossover_.execute(new Object[]{solutionSet.get(index), parents});
    } catch (JMException ex) {
      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
    }

    //Create a new solution, using DE
    return offSpring ;
  } // getOffpring

  
  
  public Solution getOffspring(SolutionSet solutionSet, Solution solution, int index) {
	    Solution[] parents = new Solution[3] ;
	    Solution offSpring = null ;

	    
	    
	    try {
	    	crossover_.setParameter("neighbors", solutionSet);
		    offSpring = (Solution) crossover_.execute(new Object[]{solution, index});		   		    
		    mutation_.execute(offSpring);
		    
		     
	    } catch (JMException ex) {
	      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    

	    //Create a new solution, using DE
	    return offSpring ;
	  } // getOffpring
  
  
  public Solution getOffspring(SolutionSet solutionSet, SolutionSet archive, Solution solution, int index) {
	    Solution[] parents = new Solution[3] ;
	    Solution offSpring = null ;

	    
	    
	    try {
	    	crossover_.setParameter("neighbors", solutionSet);
	    	crossover_.setParameter("archive", solutionSet);
		    offSpring = (Solution) crossover_.execute(new Object[]{solution, index});
		    mutation_.execute(offSpring);
		    //Operator mutation_ = MutationFactory.getMutationOperator("PolynomialMutation");
		    //mutation_.setParameter("probability", 1.0/ (4 * (new LZ09_F7("Real")).getNumberOfVariables()));
		    //mutation_.setParameter("distributionIndex", 20.0);
		    //mutation_.execute(offSpring);
		    
		     
	    } catch (JMException ex) {
	      Logger.getLogger(DifferentialEvolutionOffspring.class.getName()).log(Level.SEVERE, null, ex);
	    }

	    //Create a new solution, using DE
	    return offSpring ;
	  } // getOffpring
} // DifferentialEvolutionOffspring
*/
