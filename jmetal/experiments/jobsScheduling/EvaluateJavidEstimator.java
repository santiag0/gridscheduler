package jmetal.experiments.jobsScheduling;

import jmetal.experiments.*;

import java.util.logging.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import jmetal.base.Algorithm;
import jmetal.base.Problem;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.Variable;
import jmetal.base.variable.Int;
import jmetal.base.variable.Permutation;
import jmetal.experiments.settings.MOCell_Settings;
import jmetal.problems.jobsScheduling.HetroParr;
import jmetal.problems.jobsScheduling.JobsScheduling;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.experiments.util.GeneratePareto;

import jmetal.experiments.settings.CEC10.*;

/**
 * @author Bernabe Dorronsoro
 * 
 * This experiment class is made to read some solutions from the 
 * Pareto front and then evaluate them with Javid's Time Estimator
 * and print the CT of the different machines on the standard output.
 * 
 *  This file is done to look for a good time estimator
 * 
 */
public class EvaluateJavidEstimator extends ExperimentNoPareto {
  
	private static int independentRunsDf_ = 99;   // Number of independent runs per algorithm and problem
	private static int numberOfThreadsDf_ = 1;   // Number of threads to use (= number of algorithms to run in parallel)
	//private static int numberOfInstancesDF_ = 100; // Number of instances to solve per problem
	private static int numberOfInstancesDF_ = 1; // Number of instances to solve per problem
	
	public double[] procsCN = null;
	public int[] numberJobs = null;
	public double[] AvgJobsLength = null;
	public double[] AvgJobsLevel = null;
	double[] AvgTaskLength = null;
	int[] totalNbTasks=null;
	double[] AvgTaskPerLevel = null;
	
  /**
   * Configures the algorithms in each independent run
   * @param problem The problem to solve
   * @param problemIndex
   */
  public synchronized void  algorithmSettings(Problem problem, int problemIndex, Algorithm[] algorithm) {
  }
  
  // Print all solutions in the Pareto fronts into files for Sergio to validate the estimations
  // versus the real scheduler
  public void printSolutions() throws ClassNotFoundException, JMException, IOException { 
	  
	  for (int problemId = 0; problemId < problemList_.length; problemId++) {
		  
		  int sol = 0;
		  // Instantiate the problem
		  Problem problem = null;
		  try {
		      Class problemClass = Class.forName("jmetal.problems."+problemList_[problemId]);
		      Constructor [] constructors = problemClass.getConstructors();
		      int i = 0;
		      //find the constructor
		      while ((i < constructors.length) && 
		             (constructors[i].getParameterTypes().length!=1)) {
		        i++;
		      }
		      // constructors[i] is the selected one constructor
		      //Object pro = constructors[i].newInstance(params);
		      problem = (Problem)constructors[i].newInstance("Permutation");
		    }// try
		    catch(Exception e) {
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		          "Problem 'jmetal.problems.jobsScheduling."+ problemList_[problemId] + "' does not exist. "  +
		          "Please, check the problem names in jmetal/problems") ;
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		              "Error obtained: "  + e);
		      throw new JMException("Exception in jmetal.problems.jobsScheduling." + problemList_[problemId] + ".getProblem()") ;
		    } // catch      
		    
		  
		  // Read the Pareto front for every problem 
		  File pfFile = null;
		  pfFile = new File(paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  
		  // Read values from data files
		  FileInputStream fis = new FileInputStream(pfFile);
		  InputStreamReader isr = new InputStreamReader(fis);
		  BufferedReader br = new BufferedReader(isr);
		  System.out.println("Reading " + paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  String aux = br.readLine();

		  StringTokenizer st = new StringTokenizer(aux);
		  int numberObjectives = st.countTokens();
		  System.out.println("Number of objectives: " + numberObjectives);

			while (aux != null) {
				Solution solution = new Solution(numberObjectives);
				
				for (int objectivesId = 0; objectivesId < numberObjectives; objectivesId++) {
					// compose the solution with the data read
					double value = new Double(st.nextToken())
							.doubleValue();// Double.parseDouble(aux);
					// double value = Double.parseDouble(aux);
					solution.setObjective(objectivesId, value);

					System.out.print(value + " ");
				}
				System.out.println();
				
				// look for the variables for that solution
				String rFile = null;

				BufferedReader file = null;
				StringTokenizer st2;
				
				boolean found = false;
				int line = 0;
				int k = 0;
				int i=0;
				while (k<algorithmNameList_.length && !found) {
					i=0;
					while (i<independentRunsDf_ && !found){
						try {
							file = new BufferedReader(new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/FUN."+i));
							line = 0;
							rFile = file.readLine();
							
							while (rFile!=null && !found) {
								st2 = new StringTokenizer(rFile);
								String objective   = (String) st2.nextElement();
								
								if (objective.equalsIgnoreCase(new Double(solution.getObjective(0)).toString())) {
									objective   = (String) st2.nextElement();
									if (objective.equalsIgnoreCase(new Double(solution.getObjective(1)).toString())) {
										objective = (String) st2.nextElement();
										if (objective.equalsIgnoreCase(new Double(solution.getObjective(2)).toString())) {
											found = true;
										}
									}
								}
								if (!found) {
									line++;
									rFile = file.readLine();
								}
							}
						} catch (Exception e) {
							System.out.println("Error " + e);
							System.exit(-1);
						}
						i++;	
					}
					k++;
				}
				// Read the line in the corresponding VAR file
				i--;
				k--;
				try {
					file = new BufferedReader(new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/VAR."+i));
					int ln = 0;
					st2 = new StringTokenizer(rFile);
					
					while (ln<=line)
					{
						rFile = file.readLine();
						ln++;
					}
					
				} catch (Exception e) {
					System.out.println("Error " + e);
					System.exit(-1);
				}
				
//				System.out.println(rFile);
				
				st2 = new StringTokenizer(rFile);
				
				// build the solution
				
				
				Variable[] v = new Variable[1];
				int[] vars = new int[st2.countTokens()];
				int len = st2.countTokens();
				
				for (int j=0; j<len; j++) {
					vars[j] = new Integer(st2.nextToken()).intValue();
				}
				v[0] = new Permutation(vars);
				
				solution.setDecisionVariables(v);
				
				((JobsScheduling) problem).printSolutionToFile(solution, experimentBaseDirectory_ + "/data/" + "Sol_"+problemList_[problemId]+"."+sol);
					
					sol++;
				

				aux = br.readLine();
				if (aux != null)
					st = new StringTokenizer(aux);
			} // while
	  }
  }
  
public void printTimeEstimations() throws ClassNotFoundException, JMException, IOException { 
	  
	  for (int problemId = 0; problemId < problemList_.length; problemId++) {
			  
		  int sol = 0;
		  // Instantiate the problem
		  Problem problem = null;
		  try {
		      Class problemClass = Class.forName("jmetal.problems."+problemList_[problemId]);
		      Constructor [] constructors = problemClass.getConstructors();
		      int i = 0;
		      //find the constructor
		      while ((i < constructors.length) && 
		             (constructors[i].getParameterTypes().length!=1)) {
		        i++;
		      }
		      // constructors[i] is the selected one constructor
		      //Object pro = constructors[i].newInstance(params);
		      problem = (Problem)constructors[i].newInstance("Permutation");
		    }// try
		    catch(Exception e) {
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		          "Problem 'jmetal.problems.jobsScheduling."+ problemList_[problemId] + "' does not exist. "  +
		          "Please, check the problem names in jmetal/problems") ;
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		              "Error obtained: "  + e);
		      throw new JMException("Exception in jmetal.problems.jobsScheduling." + problemList_[problemId] + ".getProblem()") ;
		    } // catch      
		    
		  
		  // Read the Pareto front for every problem 
		  File pfFile = null;
		  pfFile = new File(paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  
		  // Read values from data files
		  FileInputStream fis = new FileInputStream(pfFile);
		  InputStreamReader isr = new InputStreamReader(fis);
		  BufferedReader br = new BufferedReader(isr);
		  System.out.println("Reading " + paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  String aux = br.readLine();

		  StringTokenizer st = new StringTokenizer(aux);
		  int numberObjectives = st.countTokens();
		  System.out.println("Number of objectives: " + numberObjectives);

			while (aux != null) {
				
				if (sol==1) {
				Solution solution = new Solution(numberObjectives);
				
				for (int objectivesId = 0; objectivesId < numberObjectives; objectivesId++) {
					// compose the solution with the data read
					double value = new Double(st.nextToken())
							.doubleValue();// Double.parseDouble(aux);
					// double value = Double.parseDouble(aux);
					solution.setObjective(objectivesId, value);

					System.out.print(value + " ");
				}
				System.out.println();
				
				// look for the variables for that solution
				String rFile = null;

				BufferedReader file = null;
				StringTokenizer st2;
				
				boolean found = false;
				int line = 0;
				int k = 0;
				int i=0;
				while (k<algorithmNameList_.length && !found) {
					i=0;
					while (i<independentRunsDf_ && !found){
						try {
							
							file = new BufferedReader(new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/FUN."+i));
							line = 0;
							rFile = file.readLine();
							
							while (rFile!=null && !found) {
								st2 = new StringTokenizer(rFile);
								String objective   = (String) st2.nextElement();
								
								if (objective.equalsIgnoreCase(new Double(solution.getObjective(0)).toString())) {
									objective   = (String) st2.nextElement();
									if (objective.equalsIgnoreCase(new Double(solution.getObjective(1)).toString())) {
										objective = (String) st2.nextElement();
										if (objective.equalsIgnoreCase(new Double(solution.getObjective(2)).toString())) {
											found = true;
										}
									}
								}
								if (!found) {
									line++;
									rFile = file.readLine();
								}
							}
						} catch (Exception e) {
							System.out.println("Error " + e);
							System.exit(-1);
						}
						i++;	
					}
					k++;
				}
				
				// Read the line in the corresponding VAR file
				i--;
				k--;
				System.out.println("Linea: " + line);
				try {
					FileReader fr =  new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/VAR."+i);
					file = new BufferedReader(fr);
					int ln = 0;
//					st2 = new StringTokenizer(rFile);
					
					while (ln<=line)
					{
						rFile = file.readLine();
						ln++;
					}
					
				} catch (Exception e) {
					System.out.println("Error " + e);
					System.exit(-1);
				}
				
//				System.out.println(rFile);
				
				st2 = new StringTokenizer(rFile);
				
				// build the solution
				Variable[] v = new Variable[1];
				int[] vars = new int[st2.countTokens()];
				int len = st2.countTokens();
				
				for (int j=0; j<len; j++) {
					vars[j] = new Integer(st2.nextToken()).intValue();
				}
				v[0] = new Permutation(vars);
				
				solution.setDecisionVariables(v);
				((JobsScheduling) problem).evaluate(solution);
				
				// Evaluate the completion time of the different CNs
				int initPos=0, finalPos = 0; // Initial and final positions of the jobs in a CN
				int cn = 0; // ID of a computer node
				
				//int len = solution.numberOfVariables();
				len = ((Permutation)solution.getDecisionVariables()[0]).getLength();
				int numberCNs_ = ((JobsScheduling) problem).instance.getNumberCNs();
				double CT[] = new double[numberCNs_];
//				double energy[] = new double[numberCNs_];
				for (int j=0; j<numberCNs_; j++) {
					CT[cn] = 0.0;
				}
				
				// For every CN:
		// Print test Sample Assignments for Sergio:
//		System.out.print("\n\n\n\nNEXT SOLUTION\n\n\n\n");

				double availableTime[][] = null;
				availableTime = new double[numberCNs_][];
				
				procsCN = new double[numberCNs_];
				numberJobs = new int[numberCNs_];
				AvgJobsLength = new double[numberCNs_];
				AvgJobsLevel = new double[numberCNs_];
				AvgTaskLength = new double[numberCNs_];
				totalNbTasks = new int[numberCNs_];
				AvgTaskPerLevel = new double[numberCNs_];
				
				for (int j=0; j<numberCNs_; j++) {
					int cores=((JobsScheduling) problem).instance.CNCores[j];
					int procs=((JobsScheduling) problem).instance.CNNbProc[j];
					availableTime[j] = new double[cores*procs];
					for (int h=0; h<cores; h++) {
						availableTime[j][h] = 0.0;
					}
					
					procsCN[j] = 0.0;
					numberJobs[j] = 0;
					AvgJobsLength[j] = 0.0;
					AvgJobsLevel[j] = 0.0;
					AvgTaskLength[j] = 0.0;
					totalNbTasks[j] = 0;
					AvgTaskPerLevel[j] = 0.0;
				}
				
					
				for (int j=0; j<len; j++) {
					// Get the scheduled jobs in this CN
					while ((j<len) && (((JobsScheduling) problem).isJob(v[0],j))) {
						finalPos++;
						j++;
					}
					
					// Here, we know that jobs from position initPos to finalPos are assigned to CN cn
					if (initPos != finalPos) {  // If no jobs are assigned to this CN
						// Estimate the completion time
//						CT[cn] = GetCompletionTime((Permutation)vars[0], initPos, finalPos, cn, penalizations);

//						int availableProcessors = ((JobsScheduling) problem).instance.CNNbProc[cn];
						
						ArrayList<Integer> remainingJobs = new ArrayList<Integer>();
						for (int h=initPos; h<finalPos; h++)
							remainingJobs.add(new Integer(((Permutation)v[0]).vector_[h]));
							
//						int minNumProcReq = 1;
						// Get the completion time after executing all the tasks
						while (!remainingJobs.isEmpty()) {
							// Execute all tasks for which there are resources
							Iterator<Integer> it = remainingJobs.iterator();
//							while (it.hasNext() && (availableProcessors > 0)) {
							while (it.hasNext()) {
								int job = it.next().intValue();
								int maxRequiredProc = ((JobsScheduling) problem).instance.requiredProcs(job);
								
								if (maxRequiredProc > ((JobsScheduling) problem).instance.CNCores[cn]) {
									System.out.println("WARNING: job " + job + " requires " + maxRequiredProc + " processors, while CN " + cn + " has " + ((JobsScheduling) problem).instance.CNCores[cn] + ". This job won't be taken into account.");
									it.remove();
								}
								
								else {
									procsCN[cn] += maxRequiredProc;
									numberJobs[cn]++;
									
//									double finishTime = insertInEarliestAvailableTime(availableTime, cn, (double)((JobsScheduling) problem).instance.estimateTimeAtMachineLevel(job, cn));
									
									// Javid Estimation
//									double [] penalizations = {0.0,0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
//									double finishTime = ((JobsScheduling) problem).GetCompletionTime(new Permutation(vars), initPos, finalPos, cn, penalizations);
									int finishTime = ((JobsScheduling) problem).instance.estimateTime(job, cn);
//									
//									System.out.println("Results of Javid's stimator: " + finishTime1);
									
//									int reqMachines = (int)Math.ceil((double)maxRequiredProc/(double)((JobsScheduling) problem).instance.CNCores[cn]);
									
//									double finishTime = insertInEarliestAvailableTime(availableTime, cn, estimateTime(job, cn, ((JobsScheduling) problem), reqMachines), reqMachines);
									
									// CALCULANDO EL TIEMPO NIVEL POR NIVEL
									// El ancho del grafo es el ancho del nivel m�s grande
//									double time = estimateTimeLevels(job, cn, ((JobsScheduling) problem), reqMachines, avgRequiredProc);
									AvgJobsLength[cn] += finishTime;
//									AvgJobsLevel[cn] += ((JobsScheduling) problem).instance.getLevels(job).length;
									
									for(int l=0; l<((JobsScheduling) problem).instance.jobTasksOps[job].length;l++) {
										AvgTaskLength[cn] += ((JobsScheduling) problem).instance.jobTasksOps[job][l];
										totalNbTasks[cn]++;
									}
																		
									
									
//									double finishTime = insertInEarliestAvailableTime(availableTime, cn, time, reqMachines);
									// Se considera por separado el ancho de cada nivel (como la suma de los procesadores que requieren las tareas del nivel)
//									double finishTime = insertInEarliestAvailableTime(availableTime, cn, estimateTimeInEachLevels(job, cn, ((JobsScheduling) problem)), reqMachines);
									// La tarea ocupa el procesador entero, pero durante el tiempo original
//									double finishTime = insertInEarliestAvailableTime(availableTime, cn, estimateTime(job, cn, ((JobsScheduling) problem), reqMachines), reqMachines);
	
									if (finishTime > CT[cn])
										CT[cn] = finishTime;
									
									it.remove();
								}
								
							}
						}
						// Print test Sample Assignments for Sergio:
						int numJobs=finalPos-initPos;
						System.out.print("\n" + CT[cn] + " " + numJobs + " ");
						for (int l=initPos; l<finalPos; l++)
//							System.out.print(((Permutation)vars[0]).vector_[l] + " ");
							System.out.print(vars[l] + " ");
					}
					
					System.out.println("\nThe average number of used cores for CN " + cn + " is: " + procsCN[cn]/numberJobs[cn] + ". There are " + (double)((JobsScheduling) problem).instance.CNJobsAssigned[cn].length + " tasks");
					
						
					finalPos++;
					initPos = finalPos;
					j=initPos-1;
					cn++;
				}
				
//				System.out.println("Procesadores por CN: " + borrar1 + " - " + borrar2 + " - " + borrar3 + " - " + borrar4 + " - " + borrar5 );
				
//				int suma = 0;
//				for (int b =0; b<324; b++) {
//					suma+= ((JobsScheduling) problem).instance.jobTasksProc[((Permutation)v[0]).vector_[b]][0];
//				}
//				
//				System.out.println("La suma de procesadores que requieren las primeras 324 tareas es: " + suma);
				
				// The average speed of the CNs
				double avgSpeed = 0.0;
				for (int l=0; l<((JobsScheduling) problem).instance.CNSpeed.length; l++)
					avgSpeed+=((JobsScheduling) problem).instance.CNSpeed[l];
				
				avgSpeed/=((JobsScheduling) problem).instance.CNSpeed.length;
				
				System.out.println("SOLUTION " + sol);
				for (int j=0; j<CT.length; j++) {
					procsCN[j] = procsCN[j] / (double)numberJobs[j];
				
//					System.out.println("CN" + j + ": Estimated Makespan " + CT[j] + "; with ratio: " + CT[j]*((JobsScheduling) problem).instance.CNCoresPerTasksRatio[j]);
					
//				System.out.println("Avg Speed: " + avgSpeed + "; Delta: " + Math.abs(avgSpeed-((JobsScheduling) problem).instance.CNSpeed[j]) + "; Velocidad: " + ((JobsScheduling) problem).instance.CNSpeed[j]);
//					System.out.println("CN" + j + ": Estimated Makespan " + CT[j] + "; with ratio: " + CT[j]*procsCN[j]*(1-(Math.abs(avgSpeed-((JobsScheduling) problem).instance.CNSpeed[j])/avgSpeed))+ "; Jobs: " + numberJobs[j] + "; Avg jobs length: " + AvgJobsLength[j]/numberJobs[j] + "; Avg. Task length: " + AvgTaskLength[j]/totalNbTasks[j] + "; Numero total tareas: " + totalNbTasks[j] + "; Avg processors per level: " + procsCN[j] + "; Avg number of levels: " + AvgJobsLevel[j]/numberJobs[j]);
					System.out.println("CN" + j + ": Estimated Makespan " + CT[j] + "; with ratio: " + CT[j]*procsCN[j]+ "; Jobs: " + numberJobs[j] + "; Avg jobs length: " + AvgJobsLength[j]/numberJobs[j] + "; Avg. Task length: " + AvgTaskLength[j]/totalNbTasks[j] + "; Total number of tasks: " + totalNbTasks[j] + "; Avg cores per level: " + procsCN[j] + "; Avg number of levels: " + AvgJobsLevel[j]/numberJobs[j]);
//					System.out.println("CN" + j + ": Estimated Makespan " + CT[j]*procsCN[j]+ "; Jobs: " + numberJobs[j] + "; Avg jobs length: " + AvgJobsLength[j]/numberJobs[j] + "; Avg. Task length: " + AvgTaskLength[j]/totalNbTasks[j] + "; Numero total tareas: " + totalNbTasks[j] + "; Avg cores per level: " + procsCN[j] + "; Avg number of levels: " + AvgJobsLevel[j]/numberJobs[j]);
					
					// Numero medio de tareas por nivel
				}
//				((JobsScheduling) problem).printSolutionToFile(solution, experimentBaseDirectory_ + "/data/" + "Sol_"+problemList_[problemId]+"."+sol);
				}
				sol++;

				aux = br.readLine();
				if (aux != null)
					st = new StringTokenizer(aux);
			} // while
	  }
}

/**
 * Estimates the time to compute job in CN cn 
 * @param job
 * @param cn
 * @return
 */
public double estimateTime(int job, int cn, JobsScheduling prob, int reqMachines){
	double execTime = 0.0;
	
	//int numberOfTasks = jobTasksTimes[job].length;
	int numberOfTasks = prob.instance.jobTasksOps[job].length;
	
	for (int k=0; k<numberOfTasks; k++) {
//		execTime += Math.ceil((double)prob.instance.jobTasksOps[job][k] / (double)prob.instance.CNSpeed[cn]) * (double)prob.instance.jobTasksProc[job][k]; // Speed goes from 1 to 10
		// La tarea ocupa el procesador entero, pero durante el tiempo original
		execTime += Math.ceil((double)prob.instance.jobTasksOps[job][k] / (double)prob.instance.CNSpeed[cn]) ; // Speed goes from 1 to 10
	}
	
//	execTime = Math.ceil(execTime/(double)prob.instance.CNCores[cn]);
	// Comentar la linea de abajo para : La tarea ocupa el procesador entero, pero durante el tiempo original
//	execTime = Math.ceil(execTime/((double)reqMachines*(double)prob.instance.CNCores[cn]));
	

	//	try {
//		execTime = Math.ceil(execTime/(double)requiredProcs(job));
//	} catch (JMException e) {
//		e.printStackTrace();
//	}
	return (int)execTime;
}

	  
/**
 * Estimates the time to compute job in CN cn as the sum of the time to compute every level
 * @param job
 * @param cn
 * @return
 */
public double estimateTimeLevels(int job, int cn, JobsScheduling prob, int reqMachines, int reqProcs){
	double execTime = 0.0;
	
	//int numberOfTasks = jobTasksTimes[job].length;
	int numberOfTasks = prob.instance.jobTasksOps[job].length;
	ArrayList[] levels = prob.instance.getLevels(job);
	int numberLevels = prob.instance.getLevels(job).length;
	double[] timeLevel = new double[numberLevels];
	
	// The time to compute every level is the time of the longest task in the level divided by the number of cores
	for (int i=0; i<numberLevels; i++) {
		Iterator it = levels[i].iterator();
		double time = 0;
		while (it.hasNext()) {
			double t = Math.ceil(prob.instance.jobTasksOps[job][((Integer)it.next()).intValue()]/prob.instance.CNSpeed[cn]);
			if (t>time)
				time = t;
		}
		timeLevel[i] = time;
	}
	
	for (int i=0; i<numberLevels; i++) {
//		execTime += Math.ceil((double)prob.instance.jobTasksOps[job][k] / (double)prob.instance.CNSpeed[cn]) ; // Speed goes from 1 to 10
//		execTime += (double)timeLevel[i] * (double)reqProcs / ((double)reqMachines*(double)prob.instance.CNCores[cn]);
		
		// La tarea ocupa el procesador entero, pero durante el tiempo original
		execTime += (double)timeLevel[i] ;
		
//		if (prob.instance.CNCores[cn] == 1)
//			System.out.println(timeLevel[i]);
//		System.out.println("Tarea: Tiempo " + timeLevel[i] + " Procesadores: " + reqProcs + " Nucleos del CN: " + prob.instance.CNCores[cn]);
		// Tiempo de la tarea que m�s tarda en el nivel * el n�mero de tareas del nivel / Maxima anchura de los niveles
//		execTime += (double)timeLevel[i] * (double)levels[i].size() / ((double)reqMachines*(double)prob.instance.CNCores[cn]);
	}
	
//	execTime = Math.ceil(execTime/(double)prob.instance.CNCores[cn]);
//	execTime = Math.ceil(execTime/((double)reqMachines*(double)prob.instance.CNCores[cn]));
//	try {
//		execTime = Math.ceil(execTime/(double)requiredProcs(job));
//	} catch (JMException e) {
//		e.printStackTrace();
//	}
	return (int)execTime;
}

/**
 * Estimates the time to compute job in CN cn as the sum of the time to compute every level
 * @param job
 * @param cn
 * @return
 */
public double estimateTimeInEachLevels(int job, int cn, JobsScheduling prob){
	double execTime = 0.0;
	
	//int numberOfTasks = jobTasksTimes[job].length;
	int numberOfTasks = prob.instance.jobTasksOps[job].length;
	ArrayList[] levels = prob.instance.getLevels(job);
	int numberLevels = prob.instance.getLevels(job).length;
	double[] timeLevel = new double[numberLevels];
	int [] reqProcLevel = new int[numberLevels];
	
	// The time to compute every level is the time of the longest task in the level divided by the number of cores
	for (int i=0; i<numberLevels; i++) {
		Iterator it = levels[i].iterator();
		reqProcLevel[i] = 0;
		double time = 0;
		while (it.hasNext()) {
			int task = ((Integer)it.next()).intValue();
			double t = Math.ceil(prob.instance.jobTasksOps[job][task]/prob.instance.CNSpeed[cn]);
			reqProcLevel[i] += prob.instance.jobTasksProc[job][task];
			if (t>time)
				time = t;
		}
		timeLevel[i] = time;
	}
	
	for (int i=0; i<numberLevels; i++) {
//		execTime += Math.ceil((double)prob.instance.jobTasksOps[job][k] / (double)prob.instance.CNSpeed[cn]) ; // Speed goes from 1 to 10
		int reqMachines = (int)Math.ceil(reqProcLevel[i]/(double)prob.instance.CNCores[cn]);
//		execTime += (double)timeLevel[i] / ((double)reqMachines*(double)prob.instance.CNCores[cn]);
		// Tiempo de la tarea que m�s tarda en el nivel * el n�mero de tareas del nivel / Maxima anchura de los niveles
//		execTime += (double)timeLevel[i] * (double)levels[i].size() / ((double)reqMachines*(double)prob.instance.CNCores[cn]);
		execTime += (double)timeLevel[i] ;
	}
	
//	execTime = Math.ceil(execTime/(double)prob.instance.CNCores[cn]);
//	execTime = Math.ceil(execTime/((double)reqMachines*(double)prob.instance.CNCores[cn]));
//	try {
//		execTime = Math.ceil(execTime/(double)requiredProcs(job));
//	} catch (JMException e) {
//		e.printStackTrace();
//	}
	return (int)execTime;
}

	  private double insertInEarliestAvailableTime(double[][] availableTimes, int cn, double time, int reqMachines) {
		  
		  int[] machines = new int[reqMachines];
		  double[] earliestTime = new double[reqMachines];
		  
		  for (int i=0; i<reqMachines; i++) {
			  earliestTime[i] = Double.MAX_VALUE;
			  machines[i] = Integer.MAX_VALUE;
		  }
		  
		  for (int j=0; j<reqMachines; j++) 
			  for (int i=0; i<availableTimes[cn].length; i++)
				  if(earliestTime[j]>availableTimes[cn][i]){
					  
					  //if it was assigned it before, undo the previous assignment
					  if(machines[j]<Integer.MAX_VALUE) {
						  availableTimes[cn][machines[j]]-=time;
					  }
					  earliestTime[j] = availableTimes[cn][i];
					  machines[j] = i;
					  availableTimes[cn][i] += time;
			  }
		  
//		  for (int j=0; j<reqMachines; j++)
//			  availableTimes[cn][machines[j]] += time;

		  
		  return availableTimes[cn][machines[reqMachines-1]];
	  }
  
  public static void main(String[] args) throws JMException, IOException {
	  
	  if (args.length != 0) {
			System.out.println("Error. Try: ProcessSolutionsForSergio");
			System.exit(-1);
	    } // if

	  //Integer[] params = new Integer[] {new Integer(args[0]).intValue(), new Integer(args[1]).intValue(), new Integer(args[2]).intValue(), new Integer(args[3]).intValue()};
	    
	  EvaluateJavidEstimator exp = new EvaluateJavidEstimator() ; // exp = experiment
    
    exp.experimentName_  = "JobsSchedulingStudyOneByOne" ;
    exp.algorithmNameList_   = new String[] { "MOCell" , "NSGAII" , "IBEA" , "SPEA2"} ;
    
    exp.params_ = new String[]{"Permutation"};
        
//    exp.problemList_ = new String[]{"jobsScheduling.HetroParr" , "jobsScheduling.HomoParr" , 
//    		"jobsScheduling.Mix" , "jobsScheduling.SerParr" , "jobsScheduling.SingleTask"};
    exp.problemList_ = new String[]{"jobsScheduling.SingleTask"};
//    exp.problemList_ = new String[]{"jobsScheduling.SingleTask"};

    

//    exp.paretoFrontFile_ = new String[]{"HetroParr.pf" , "HomoParr.pf" , "Mix.pf" , "SerParr.pf" , "SingleTask.pf"};
    exp.paretoFrontFile_ = new String[]{"SingleTask.pf"};
//    exp.paretoFrontFile_ = new String[]{"SingleTask.pf"};
    
    exp.indicatorList_   = new String[] {"HV", "SPREAD", "IGD", "EPSILON"} ;
    
    exp.experimentBaseDirectory_ = "/Users/bernabe/Desktop/work/trabajos/_Ongoing/Zomaya/Multiple Parallel Jobs/results/basic algorithms/" + exp.experimentName_;
    
    exp.paretoFrontDirectory_ = "/Users/bernabe/Desktop/work/trabajos/_Ongoing/Zomaya/Multiple Parallel Jobs/results/basic algorithms/" + exp.experimentName_;
    
//    exp.params_ = new Object[1];
//    exp.params_[0] = new String("Int");
    
    exp.instances_ = exp.numberOfInstancesDF_;
    
//    exp.runID_ = (new Integer(args[0])).intValue();

    // remove HV, SPREAD, IGD, and EPSILON files
    for (int i=0; i<exp.algorithmNameList_.length; i++)
    	for (int j=0; j<exp.problemList_.length; j++)
    		for (int k=0; k<exp.indicatorList_.length; k++) {
    			String f = exp.paretoFrontDirectory_+ "/data/" + 
				exp.algorithmNameList_[i] + "/" + exp.problemList_[j] +
				"/" + exp.indicatorList_[k];
    			
    			File fichero = new File(f);
    			
    			if (fichero.delete())
    				   System.out.println("File " + f + " was deleted.");
    				else
    				   System.out.println("File " + f + " could not be deleted.");
    			
    		}

    // create the Pareto front files
    for (int i=0; i< exp.paretoFrontFile_.length; i++){
    	File file = null;
    	
    		file = new File(exp.paretoFrontDirectory_+ "/" + exp.paretoFrontFile_[i]);
	    	try{
	    		file.createNewFile();	
	    	}catch(IOException ioe)
	        {
	    		System.out.println("Error while creating the empty file : " + exp.paretoFrontDirectory_+ "/" + exp.paretoFrontFile_[i] + ioe);
	    	}
    	
    }
    
    //exp.algorithmSettings_ = new Settings[numberOfAlgorithms] ;
    
    exp.independentRuns_ = independentRunsDf_ ;
    
    exp.map_.put("experimentDirectory", exp.experimentBaseDirectory_);
    exp.map_.put("algorithmNameList", exp.algorithmNameList_);
    exp.map_.put("problemList", exp.problemList_);
    exp.map_.put("indicatorList", exp.indicatorList_);
    exp.map_.put("paretoFrontDirectory", exp.paretoFrontDirectory_);
    exp.map_.put("paretoFrontFile", exp.paretoFrontFile_);
    exp.map_.put("independentRuns", exp.independentRuns_);
    // map_.put("algorithm", algorithm_);
    exp.map_.put("outputParetoFrontFile", exp.outputParetoFrontFile_);
    exp.map_.put("outputParetoSetFile", exp.outputParetoSetFile_);

    exp.map_.put("params", exp.params_); // parameters for the problem constructor
    
    exp.map_.put("instances", exp.instances_); // Number of instances to solve per problem class
//    exp.map_.put("timeEstimation", exp.time_); // For computing the run time and the run time left
    exp.map_.put("timmingFileName", exp.timmingFileName_);

//    GeneratePareto paretoFront = new GeneratePareto(exp);
//    paretoFront.run();
    
    try {
		exp.printTimeEstimations();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
//    paretoFront.computeQualityIndicators();
    
    // Generate latex tables (comment this sentence is not desired)
//    exp.generateLatexTables() ;

    // Generate  CSV files
//    exp.generateCsvFile();
    
    // GENERATE MATLAB DATA?
    
 // Configure the R scripts to be generated
//    int rows  ;
//    int columns  ;
//    String prefix ;
//    String [] problems ;
//    boolean notch ;
//
//    // Configuring scripts for ZDT
//    rows = 1 ;
//    columns = 3 ;
//    prefix = new String("JobScheduling");
//    problems = new String[]{"jobsScheduling.HetroParr" , "jobsScheduling.HomoParr" , 
//    		"jobsScheduling.Mix" , "jobsScheduling.SerParr" , "jobsScheduling.SingleTask"} ;
////    problems = new String[]{"jobsScheduling.HetroParr"};
//
//    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
//    exp.generateRWilcoxonScripts(problems, prefix) ;

//    // Configure scripts for DTLZ
////    rows = 3 ;
////    columns = 3 ;
////    prefix = new String("DTLZ");
////    problems = new String[]{"DTLZ1","DTLZ2","DTLZ3","DTLZ4","DTLZ5",
////                                    "DTLZ6","DTLZ7"} ;
////
////    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
////    exp.generateRWilcoxonScripts(problems, prefix) ;
//
//    // Configure scripts for WFG
//    rows = 3 ;
//    columns = 3 ;
//    prefix = new String("WFG");
//    problems = new String[]{"WFG1","WFG2","WFG3","WFG4","WFG5","WFG6",
//                            "WFG7","WFG8","WFG9"} ;
//
//    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
//    exp.generateRWilcoxonScripts(problems, prefix) ;
  }
} // NSGAIIStudy


