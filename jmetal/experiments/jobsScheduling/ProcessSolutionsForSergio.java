/**
 * MOCellStudy.java
 *
 * @author Bernabe Dorronsoro
 * @version 1.0
 */
package jmetal.experiments.jobsScheduling;

import jmetal.experiments.*;

import java.util.logging.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import jmetal.base.Algorithm;
import jmetal.base.Problem;
import jmetal.base.Solution;
import jmetal.base.SolutionSet;
import jmetal.base.Variable;
import jmetal.base.variable.Int;
import jmetal.base.variable.Permutation;
import jmetal.experiments.settings.MOCell_Settings;
import jmetal.problems.jobsScheduling.HetroParr;
import jmetal.problems.jobsScheduling.JobsScheduling;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.experiments.util.GeneratePareto;

import jmetal.experiments.settings.CEC10.*;

/**
 * @author Bernabe Dorronsoro
 * 
 * This experiment class is configured to merge the Pareto fronts 
 * obtained in the 30 independent runs  
 * 
 */
public class ProcessSolutionsForSergio extends ExperimentNoPareto {
  
	private static int independentRunsDf_ = 98;   // Number of independent runs per algorithm and problem
	private static int numberOfThreadsDf_ = 1;   // Number of threads to use (= number of algorithms to run in parallel)
	//private static int numberOfInstancesDF_ = 100; // Number of instances to solve per problem
	private static int numberOfInstancesDF_ = 1; // Number of instances to solve per problem
	
  /**
   * Configures the algorithms in each independent run
   * @param problem The problem to solve
   * @param problemIndex
   */
  public synchronized void  algorithmSettings(Problem problem, int problemIndex, Algorithm[] algorithm) {
//    try {
//      int numberOfAlgorithms = algorithmNameList_.length;
//
//      Properties[] parameters = new Properties[numberOfAlgorithms];
//      //Settings[] configuration = new Settings[algorithmName_.length];
//
//      for (int i = 0; i < numberOfAlgorithms; i++) {
//        parameters[i] = new Properties();
//        parameters[i].setProperty("POPULATION_SIZE", "100");
////        parameters[i].setProperty("MAX_EVALUATIONS", "500");
//        parameters[i].setProperty("MAX_EVALUATIONS", "500000");
////        parameters[i].setProperty("MAX_EVALUATIONS", "1000000");
//        parameters[i].setProperty("ARCHIVE_SIZE", "100");
//        parameters[i].setProperty("FEEDBACK", "20");
//        parameters[i].setProperty("SPECIAL_SOLUTION", "Min-min");
//        
//        parameters[i].setProperty("SELECTION", "BinaryTournament");
////        parameters[i].setProperty("SELECTION", "TournamentFour");
//        
//        parameters[i].setProperty("RECOMBINATION", "DPX");
////        parameters[i].setProperty("RECOMBINATION", "UniformCrossover");
//        parameters[i].setProperty("CROSSOVER_PROBABILITY", "0.9");
//        
//        parameters[i].setProperty("MUTATION", "RebalanceMutation");
//        parameters[i].setProperty("MUTATION_ROUNDS", "1");   // Estaba a 16  !!!!!!!!!!!!
//        parameters[i].setProperty("MUTATION_OVERLOAD_PERCENTAGE", "0.25");
//        //parameters[i].setProperty("MUTATION_POLICY", "moderate");
//        parameters[i].setProperty("MUTATION_POLICY", "simple");
//        //parameters[i].setProperty("MUTATION_MODE", "strict");
//        parameters[i].setProperty("MUTATION_MODE", "permissive");
//        parameters[i].setProperty("MUTATION_PROBABILITY", new Double(1.0/problem.getNumberOfVariables()).toString());
//        
//        parameters[i].setProperty("LOCAL_SEARCH", "LMCTSLocalSearch");
//        
//      }
//
//      if ( (paretoFrontFile_ != null) && !paretoFrontFile_[problemIndex].equals("")) {
//        for (int i = 0; i < numberOfAlgorithms; i++)
//        parameters[i].setProperty("PARETO_FRONT_FILE", paretoFrontFile_[problemIndex]);
//      } // if
// 
//      //for (int i = 0; i < numberOfAlgorithms; i++)
//      //  algorithm[i] = new NSGAII_Settings(problem).configure(parameters[i]);
//      
//      algorithm[0] = new MOCell_Settings_CEC10(problem).configure(parameters[0]);
//      algorithm[1] = new NSGAII_Settings_CEC10(problem).configure(parameters[1]);
//      algorithm[2] = new IBEA_Settings_CEC10(problem).configure(parameters[2]);
//      algorithm[3] = new MOEAD_Settings_CEC10(problem).configure(parameters[3]);
//      
//    } catch (JMException ex) {
//      Logger.getLogger(NSGAIIStudy.class.getName()).log(Level.SEVERE, null, ex);
//    }
  }
  
  // Print all solutions in the Pareto fronts into files for Sergio to validate the estimations
  // versus the real scheduler
  public void printSolutions() throws ClassNotFoundException, JMException, IOException { 
	  
	  for (int problemId = 0; problemId < problemList_.length; problemId++) {
		  
		  int sol = 0;
		  // Instantiate the problem
		  Problem problem = null;
		  try {
		      Class problemClass = Class.forName("jmetal.problems."+problemList_[problemId]);
		      Constructor [] constructors = problemClass.getConstructors();
		      int i = 0;
		      //find the constructor
		      while ((i < constructors.length) && 
		             (constructors[i].getParameterTypes().length!=1)) {
		        i++;
		      }
		      // constructors[i] is the selected one constructor
		      //Object pro = constructors[i].newInstance(params);
		      problem = (Problem)constructors[i].newInstance("Permutation");
		    }// try
		    catch(Exception e) {
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		          "Problem 'jmetal.problems.jobsScheduling."+ problemList_[problemId] + "' does not exist. "  +
		          "Please, check the problem names in jmetal/problems") ;
		      Configuration.logger_.severe("ProcessSolutionsForSergio: " +
		              "Error obtained: "  + e);
		      throw new JMException("Exception in jmetal.problems.jobsScheduling." + problemList_[problemId] + ".getProblem()") ;
		    } // catch      
		    
		  
		  // Read the Pareto front for every problem 
		  File pfFile = null;
		  pfFile = new File(paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  
		  // Read values from data files
		  FileInputStream fis = new FileInputStream(pfFile);
		  InputStreamReader isr = new InputStreamReader(fis);
		  BufferedReader br = new BufferedReader(isr);
		  System.out.println("Reading " + paretoFrontDirectory_ + paretoFrontFile_[problemId]);
		  String aux = br.readLine();

		  StringTokenizer st = new StringTokenizer(aux);
		  int numberObjectives = st.countTokens();
		  System.out.println("Number of objectives: " + numberObjectives);

			while (aux != null) {
				Solution solution = new Solution(numberObjectives);
				
				for (int objectivesId = 0; objectivesId < numberObjectives; objectivesId++) {
					// compose the solution with the data read
					double value = new Double(st.nextToken())
							.doubleValue();// Double.parseDouble(aux);
					// double value = Double.parseDouble(aux);
					solution.setObjective(objectivesId, value);

					System.out.print(value + " ");
				}
				System.out.println();
				
				// look for the variables for that solution
				String rFile = null;

				BufferedReader file = null;
				StringTokenizer st2;
				
				boolean found = false;
				int line = 0;
				int k = 0;
				int i=0;
				while (k<algorithmNameList_.length && !found) {
					i=0;
					while (i<independentRunsDf_ && !found){
						try {
							file = new BufferedReader(new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/FUN."+i));
							line = 0;
							rFile = file.readLine();
							
							while (rFile!=null && !found) {
								st2 = new StringTokenizer(rFile);
								String objective   = (String) st2.nextElement();
								
								if (objective.equalsIgnoreCase(new Double(solution.getObjective(0)).toString())) {
									objective   = (String) st2.nextElement();
									if (objective.equalsIgnoreCase(new Double(solution.getObjective(1)).toString())) {
										objective = (String) st2.nextElement();
										if (objective.equalsIgnoreCase(new Double(solution.getObjective(2)).toString())) {
											found = true;
										}
									}
								}
								if (!found) {
									line++;
									rFile = file.readLine();
								}
							}
						} catch (Exception e) {
							System.out.println("Error " + e);
							System.exit(-1);
						}
						i++;	
					}
					k++;
				}
				
				// Read the line in the corresponding VAR file
				i--;
				k--;
				try {
					file = new BufferedReader(new FileReader(experimentBaseDirectory_ + "/data/" + algorithmNameList_[k] + "/" + problemList_[problemId] + "/VAR."+i));
					int ln = 0;
//					st2 = new StringTokenizer(rFile);
					
					while (ln<=line)
					{
						rFile = file.readLine();
						ln++;
					}
					
				} catch (Exception e) {
					System.out.println("Error " + e);
					System.exit(-1);
				}
				
//				System.out.println(rFile);
				
				st2 = new StringTokenizer(rFile);
				
				// build the solution
				
				
				Variable[] v = new Variable[1];
				int[] vars = new int[st2.countTokens()];
				int len = st2.countTokens();
				
				for (int j=0; j<len; j++) {
					vars[j] = new Integer(st2.nextToken()).intValue();
				}
				v[0] = new Permutation(vars);
				
				solution.setDecisionVariables(v);
				
				((JobsScheduling) problem).printSolutionToFile(solution, experimentBaseDirectory_ + "/data/" + "Sol_"+problemList_[problemId]+"."+sol);
					
					sol++;
				

				aux = br.readLine();
				if (aux != null)
					st = new StringTokenizer(aux);
			} // while
	  }
  }
  
  public static void main(String[] args) throws JMException, IOException {
	  
	  if (args.length != 0) {
			System.out.println("Error. Try: ProcessSolutionsForSergio");
			System.exit(-1);
	    } // if

	  //Integer[] params = new Integer[] {new Integer(args[0]).intValue(), new Integer(args[1]).intValue(), new Integer(args[2]).intValue(), new Integer(args[3]).intValue()};
	    
	  ProcessSolutionsForSergio exp = new ProcessSolutionsForSergio() ; // exp = experiment
    
    exp.experimentName_  = "JobsSchedulingStudyOneByOne" ;
    exp.algorithmNameList_   = new String[] { "MOCell" , "NSGAII" , "IBEA" , "SPEA2"} ;
    
    exp.params_ = new String[]{"Permutation"};
    
//    exp.algorithmNameList_   = new String[] {"CellDE", "NSGAII", "MLS"} ;
//    exp.algorithmNameList_   = new String[] {"MLS"} ;
//    exp.algorithmNameList_   = new String[] {"CellDE", "NSGAII"} ;
    
    exp.problemList_ = new String[]{"jobsScheduling.HetroParr" , "jobsScheduling.HomoParr" , 
    		"jobsScheduling.Mix" , "jobsScheduling.SerParr" , "jobsScheduling.SingleTask"};
//    exp.problemList_ = new String[]{"jobsScheduling.HetroParr"};

    

    exp.paretoFrontFile_ = new String[]{"HetroParr.pf" , "HomoParr.pf" , "Mix.pf" , "SerParr.pf" , "SingleTask.pf"};
//    exp.paretoFrontFile_ = new String[]{"HetroParr.pf"};
    
    exp.indicatorList_   = new String[] {"HV", "SPREAD", "IGD", "EPSILON"} ;
    
    exp.experimentBaseDirectory_ = "/Users/bernabe/Desktop/work/trabajos/_Ongoing/Zomaya/Multiple Parallel Jobs/results/basic algorithms/" + exp.experimentName_;
    
    exp.paretoFrontDirectory_ = "/Users/bernabe/Desktop/work/trabajos/_Ongoing/Zomaya/Multiple Parallel Jobs/results/basic algorithms/" + exp.experimentName_;
    
//    exp.params_ = new Object[1];
//    exp.params_[0] = new String("Int");
    
    exp.instances_ = exp.numberOfInstancesDF_;
    
//    exp.runID_ = (new Integer(args[0])).intValue();

    // remove HV, SPREAD, IGD, and EPSILON files
    for (int i=0; i<exp.algorithmNameList_.length; i++)
    	for (int j=0; j<exp.problemList_.length; j++)
    		for (int k=0; k<exp.indicatorList_.length; k++) {
    			String f = exp.paretoFrontDirectory_+ "/data/" + 
				exp.algorithmNameList_[i] + "/" + exp.problemList_[j] +
				"/" + exp.indicatorList_[k];
    			
    			File fichero = new File(f);
    			
    			if (fichero.delete())
    				   System.out.println("File " + f + " was deleted.");
    				else
    				   System.out.println("File " + f + " could not be deleted.");
    			
    		}

    // create the Pareto front files
    for (int i=0; i< exp.paretoFrontFile_.length; i++){
    	File file = null;
    	
    		file = new File(exp.paretoFrontDirectory_+ "/" + exp.paretoFrontFile_[i]);
	    	try{
	    		file.createNewFile();	
	    	}catch(IOException ioe)
	        {
	    		System.out.println("Error while creating the empty file : " + exp.paretoFrontDirectory_+ "/" + exp.paretoFrontFile_[i] + ioe);
	    	}
    	
    }
    
    //exp.algorithmSettings_ = new Settings[numberOfAlgorithms] ;
    
    exp.independentRuns_ = independentRunsDf_ ;
    
    exp.map_.put("experimentDirectory", exp.experimentBaseDirectory_);
    exp.map_.put("algorithmNameList", exp.algorithmNameList_);
    exp.map_.put("problemList", exp.problemList_);
    exp.map_.put("indicatorList", exp.indicatorList_);
    exp.map_.put("paretoFrontDirectory", exp.paretoFrontDirectory_);
    exp.map_.put("paretoFrontFile", exp.paretoFrontFile_);
    exp.map_.put("independentRuns", exp.independentRuns_);
    // map_.put("algorithm", algorithm_);
    exp.map_.put("outputParetoFrontFile", exp.outputParetoFrontFile_);
    exp.map_.put("outputParetoSetFile", exp.outputParetoSetFile_);

    exp.map_.put("params", exp.params_); // parameters for the problem constructor
    
    exp.map_.put("instances", exp.instances_); // Number of instances to solve per problem class
//    exp.map_.put("timeEstimation", exp.time_); // For computing the run time and the run time left
    exp.map_.put("timmingFileName", exp.timmingFileName_);

    
    // Run the experiments
//    int numberOfThreads = numberOfThreadsDf_;
    //exp.runExperiment(numberOfThreads, params);
    
    // Since the true Pareto front is not known for this problem, we
    // generate it by merging all the obtained Pareto fronts in the experimentation
//    GenerateParetoAEDB paretoFront = new GenerateParetoAEDB(exp);
    GeneratePareto paretoFront = new GeneratePareto(exp);
    paretoFront.run();
    
    try {
		exp.printSolutions();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    paretoFront.computeQualityIndicators();
    
    // Generate latex tables (comment this sentence is not desired)
    exp.generateLatexTables() ;

    // Generate  CSV files
    exp.generateCsvFile();
    
    // GENERATE MATLAB DATA?
    
 // Configure the R scripts to be generated
    int rows  ;
    int columns  ;
    String prefix ;
    String [] problems ;
    boolean notch ;

    // Configuring scripts for ZDT
    rows = 1 ;
    columns = 3 ;
    prefix = new String("JobScheduling");
    problems = new String[]{"jobsScheduling.HetroParr" , "jobsScheduling.HomoParr" , 
    		"jobsScheduling.Mix" , "jobsScheduling.SerParr" , "jobsScheduling.SingleTask"} ;
//    problems = new String[]{"jobsScheduling.HetroParr"};

    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
    exp.generateRWilcoxonScripts(problems, prefix) ;

//    // Configure scripts for DTLZ
////    rows = 3 ;
////    columns = 3 ;
////    prefix = new String("DTLZ");
////    problems = new String[]{"DTLZ1","DTLZ2","DTLZ3","DTLZ4","DTLZ5",
////                                    "DTLZ6","DTLZ7"} ;
////
////    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
////    exp.generateRWilcoxonScripts(problems, prefix) ;
//
//    // Configure scripts for WFG
//    rows = 3 ;
//    columns = 3 ;
//    prefix = new String("WFG");
//    problems = new String[]{"WFG1","WFG2","WFG3","WFG4","WFG5","WFG6",
//                            "WFG7","WFG8","WFG9"} ;
//
//    exp.generateRBoxplotScripts(rows, columns, problems, prefix, notch = true) ;
//    exp.generateRWilcoxonScripts(problems, prefix) ;
  }
} // NSGAIIStudy


