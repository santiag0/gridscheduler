/**
 * MOCellStudy.java
 *
 * @author Bernabe Dorronsoro
 * @version 1.0
 */
package jmetal.experiments.jobsScheduling;

import jmetal.experiments.*;

import java.util.logging.Logger;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import jmetal.base.Algorithm;
import jmetal.base.Problem;
import jmetal.experiments.settings.MOCell_Settings;
import jmetal.util.JMException;
import jmetal.experiments.util.GeneratePareto;
import jmetal.experiments.settings.CEC10.*;
import jmetal.experiments.settings.jobsScheduling.MOCellSRF_Settings_JobsScheduling;
//import jmetal.experiments.settings.jobsScheduling.MOCell_Settings_JobsScheduling;
import jmetal.experiments.settings.jobsScheduling.MOCellSRF_Settings_JobsScheduling_real;
import jmetal.experiments.settings.jobsScheduling.NSGAII_Settings_JobsScheduling_real;

/**
 * @author Bernabe Dorronsoro
 * 
 *         This experiment class is configured to solve 100 instances of every
 *         scheduling problem class with NSGAII, MOCell, IBEA, and MOEA/D (100
 *         independent runs per algorithm/instance)
 */
public class JobsSchedulingStudy extends ExperimentNoPareto {

	private static int independentRunsDf_ = 5; // Number of independent runs per
												// algorithm and problem
	private int numberOfThreadsDf_ = 1; // Number of threads to use (= number of
										// algorithms to run in parallel)
	private int numberOfInstancesDF_ = 1; // Number of instances to solve per
											// problem

	private int algNum = 0;

	/**
	 * Configures the algorithms in each independent run
	 * 
	 * @param problem
	 *            The problem to solve
	 * @param problemIndex
	 */
	public synchronized void algorithmSettings(Problem problem,
			int problemIndex, Algorithm[] algorithm) {
		try {
			int numberOfAlgorithms = algorithmNameList_.length;

			Properties[] parameters = new Properties[numberOfAlgorithms];

			for (int i = 0; i < numberOfAlgorithms; i++) {
				parameters[i] = new Properties();
			} // for

			if ((paretoFrontFile_ != null)
					&& !paretoFrontFile_[problemIndex].equals("")) {
				for (int i = 0; i < numberOfAlgorithms; i++)
					parameters[i].setProperty("paretoFrontFile_",
							paretoFrontFile_[problemIndex]);
			} // if

			try {
				if (algNum == 0) {
					algorithm[0] = new MOCellSRF_Settings_JobsScheduling_real(
							problem).configure(parameters[0]);
				} else {
					algorithm[0] = new NSGAII_Settings_JobsScheduling_real(
							problem).configure(parameters[0]);
				}
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (JMException ex) {
			Logger.getLogger(NSGAIIStudy.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	public static class MultiThreadingSupport extends Thread {
		public int startingRunNumber;
		public int instanceNumber;
		public String problemName;
		public int algNum = 0;
		public JobsSchedulingStudy exp;

		public MultiThreadingSupport() {

		}

		public void run() {
			try {
				Integer[] params = new Integer[] { instanceNumber };

				exp = new JobsSchedulingStudy();
				exp.algNum = algNum;
				exp.startingRunNumber_ = startingRunNumber;
				exp.experimentName_ = exp.getClass().getSimpleName()
						+ instanceNumber;
				exp.timmingFileName_ = "TIMMINGS";

				if (algNum == 0) {
					System.out.println("ALGORITMO: MOCellSRF!!!");
					exp.algorithmNameList_ = new String[] { "MOCellSRF" };
				} else {
					System.out.println("ALGORITMO: NSGAIISRF!!!");
					exp.algorithmNameList_ = new String[] { "NSGAIISRF" };
				}

				exp.problemList_ = new String[] { "jobsScheduling."
						+ problemName + "_real" };

				exp.paretoFrontFile_ = new String[] { problemName + "_real.pf" };

				exp.indicatorList_ = new String[] { "HV", "SPREAD", "IGD",
						"EPSILON" };

				int numberOfAlgorithms = exp.algorithmNameList_.length;

				exp.experimentBaseDirectory_ = "./" + exp.experimentName_;
				exp.paretoFrontDirectory_ = exp.experimentBaseDirectory_
						+ "/paretoFronts/";
				exp.instances_ = exp.numberOfInstancesDF_;

				System.out.println(exp.paretoFrontDirectory_);

				synchronized (JobsSchedulingStudy.class) {
					File paretoDirectoryFile = new File(
							exp.paretoFrontDirectory_);
					paretoDirectoryFile.mkdirs();

					// create the Pareto front files
					for (int i = 0; i < exp.paretoFrontFile_.length; i++) {
						File file = null;
						if (exp.instances_ != 1) {
							for (int inst = 0; inst < exp.instances_; inst++) {
								file = new File(exp.paretoFrontDirectory_ + "/"
										+ exp.paretoFrontFile_[i] + "." + inst);
								try {
									file.createNewFile();
								} catch (IOException ioe) {
									System.out
											.println("Error while creating the empty file : "
													+ exp.paretoFrontDirectory_
													+ exp.paretoFrontFile_[i]
													+ ioe);
								}
							}

						} else {
							file = new File(exp.paretoFrontDirectory_ + "/"
									+ exp.paretoFrontFile_[i]);
							try {
								file.createNewFile();
							} catch (IOException ioe) {
								System.out
										.println("Error while creating the empty file : "
												+ exp.paretoFrontDirectory_
												+ exp.paretoFrontFile_[i] + ioe);
							}
						}
					}
				}

				exp.algorithmSettings_ = new Settings[numberOfAlgorithms];
				exp.independentRuns_ = JobsSchedulingStudy.independentRunsDf_;

				// Run the experiments
				int numberOfThreads = exp.numberOfThreadsDf_;
				exp.runExperiment(numberOfThreads, params);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws JMException, IOException {
		if (args.length != 6) {
			System.out
					.println("Error. Try: JobsSchedulingStudy <problem name> <instance num.> <starting run num.> <runs per thread> <thread count> <algorithm num.> ");
			System.exit(-1);
		} // if

		int instNum = new Integer(args[1]);
		int runNum = new Integer(args[2]);
		int runPerThread = new Integer(args[3]);
		int numThreads = new Integer(args[4]);
		int algNum = new Integer(args[5]);

		String problemName = args[0];

		System.out.println("=== Instance number : " + instNum);
		System.out.println("=== Starting run    : " + runNum);
		System.out.println("=== Runs per thread : " + runPerThread);
		System.out.println("=== # threads       : " + numThreads);
		System.out.println("=== Algortihm number: " + algNum);
		System.out.println("=== Problem name    : " + problemName);

		independentRunsDf_ = runPerThread;

		int currentRunNum = runNum;

		ArrayList<JobsSchedulingStudy.MultiThreadingSupport> threadList = new ArrayList<JobsSchedulingStudy.MultiThreadingSupport>();

		for (int i = 0; i < numThreads; i++) {
			JobsSchedulingStudy.MultiThreadingSupport thread = new JobsSchedulingStudy.MultiThreadingSupport();
			thread.instanceNumber = instNum;
			thread.startingRunNumber = currentRunNum;
			thread.algNum = algNum;
			thread.problemName = problemName;
			thread.start();

			threadList.add(thread);
			currentRunNum += independentRunsDf_;
		}

		for (int i = 0; i < numThreads; i++) {
			try {
				threadList.get(i).join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
} // NSGAIIStudy

