/**
 * CellDE_Settings.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * CellDE_Settings class of algorithm CellDE
 */
package jmetal.experiments.settings;

import jmetal.metaheuristics.cellde.*;
import java.util.Properties;
import jmetal.base.Algorithm;
import jmetal.base.Operator;
import jmetal.base.Problem;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.experiments.Settings;
import jmetal.experiments.offspring.DifferentialEvolutionOffspring;
import jmetal.experiments.offspring.PolynomialOffspringGenerator;
import jmetal.experiments.offspring.Offspring;
import jmetal.experiments.offspring.SBXCrossoverAndPolynomialMutation;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;

/**
 *
 * @author Antonio
 */
public class pSteadyStateSelfAdaptiveWindowDEPol_Settings extends Settings {

  public int populationSize_            = 40;

  public Algorithm algorithm = new pSteadyStateSelfAdaptiveWindowDEPol(problem_);
  int archiveSize_                      = 100;
  int maxEvaluations_                   = 150000;
  int archiveFeedback_                  = 10;
  boolean applyMutation_                = true; // Polynomial mutation
  double distributionIndexForMutation_  = 20;
  double distributionIndexForCrossover_ = 20;
  double crossoverProbability_          = 1.0;
  double mutationProbability_           = 1.0 / problem_.getNumberOfVariables();

  /**
   * Constructor
   */
  public pSteadyStateSelfAdaptiveWindowDEPol_Settings(Problem problem) {
    super(problem);
  } // CellDE_Settings

  
  /**
   * Constructor
   */
  public pSteadyStateSelfAdaptiveWindowDEPol_Settings(Problem problem, String paretoFrontFile) {
    super(problem);
    this.paretoFrontFile_ = paretoFrontFile;
  } // CellDE_Settings  
  
  /**
   * Configure the algorith with the especified parameter settings
   * @return an algorithm object
   * @throws jmetal.util.JMException
   */
  public Algorithm configure() throws JMException {
    
    Operator selection;
    Operator crossover;
    Operator mutation;

    QualityIndicator indicators;

    // Creating the problem
    //algorithm = new CellDEJuanjo(problem_);
    //algorithm = new CellDECanonico(problem_);
    

    // Algorithm parameters
    algorithm.setInputParameter("populationSize", populationSize_);
    algorithm.setInputParameter("archiveSize", archiveSize_);
    algorithm.setInputParameter("maxEvaluations", maxEvaluations_);
    algorithm.setInputParameter("feedback", archiveFeedback_);

    Offspring[] getOffspring = new Offspring[2];
    double CR, F;
    getOffspring[1] = new DifferentialEvolutionOffspring(CR = 1.0, F = 0.5, 1.0/problem_.getNumberOfVariables(),20);

    //getOffspring[0] = new DifferentialEvolutionOffspring(CR = 1.0, F = 0.5);
    //getOffspring[1] = new DifferentialEvolutionPBX_SAOffSpring(CR = 1.0, F = 0.5,problem_);
    
//    getOffspring[1] = new SBXCrossoverAndPolynomialMutation(
//      1.0 / problem_.getNumberOfVariables(), // mutation probability
//      0.9, // crossover probability
//      20, // distribution index for mutation
//      20); // distribution index for crossover
//

    getOffspring[0] = new PolynomialOffspringGenerator(1.0/problem_.getNumberOfVariables(), 20);
    //getOffspring[3]  = null;
    /*
    getOffspring[2] = new SBXCrossoverAndPolynomialMutation(
    1.0/problem_.getNumberOfVariables(), // mutation probability
    1.0, // crossover probability
    5,  // distribution index for mutation
    5) ; // distribution index for crossover

    getOffspring[2] = new DifferentialEvolutionOffspring(CR = 0.5, F = 0.5) ;
    /*
    getOffspring[2] = new DifferentialEvolutionOffspring(CR = 0.1, F = 0.75) ;
    getOffspring[3] = new DifferentialEvolutionOffspring(CR = 0.1, F = 0.1) ;
     */



    //getOffspring[2] = new SBXCrossoverAndPolynomialMutation(
    //  1/problem_.getNumberOfVariables(), // mutation probability
    //  1.0, // crossover probability
    //  20,  // distribution index for mutation
    //  200) ; // distribution index for crossover

    algorithm.setInputParameter("offspringsCreators", getOffspring);

    // Creating the indicator object
    if (!paretoFrontFile_.equals("")) {
      indicators = new QualityIndicator(problem_, paretoFrontFile_);
      algorithm.setInputParameter("indicators", indicators);
    } // if

    return algorithm;
  }
} // CellDE2_Settings

