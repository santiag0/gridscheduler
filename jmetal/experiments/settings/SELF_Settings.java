/**
 * CellDE_Settings.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 *
 * CellDE_Settings class of algorithm CellDE
 */
package jmetal.experiments.settings;

import jmetal.metaheuristics.cellde.*;
import java.util.Properties;
import jmetal.base.Algorithm;
import jmetal.base.Operator;
import jmetal.base.Problem;
import jmetal.base.operator.crossover.CrossoverFactory;
import jmetal.base.operator.mutation.MutationFactory;
import jmetal.base.operator.selection.SelectionFactory;
import jmetal.experiments.Settings;
import jmetal.experiments.offspring.DifferentialEvolutionOffspring;
import jmetal.experiments.offspring.PolynomialOffspringGenerator;
import jmetal.experiments.offspring.Offspring;
import jmetal.experiments.offspring.SBXCrossoverAndPolynomialMutation;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.JMException;

/**
 *
 * @author Antonio
 */
public class SELF_Settings extends Settings {

  public int populationSize_            = 40;

  public Algorithm algorithm = new SELF(problem_);
  
  int archiveSize_                      = 100;
  int maxEvaluations_                   = 15000;
  int archiveFeedback_                  = 10;
  boolean applyMutation_                = true; // Polynomial mutation
  double distributionIndexForMutation_  = 20;
  double distributionIndexForCrossover_ = 20;
  double crossoverProbability_          = 0.9;
  double mutationProbability_           = 1.0 / problem_.getNumberOfVariables();

  /**
   * Constructor
   */
  public SELF_Settings(Problem problem) {
    super(problem);
  } // CellDE_Settings
  
  /**
   * Constructor
   */
  public SELF_Settings(Problem problem, String paretoFrontFile) {
    super(problem);
    this.paretoFrontFile_ = paretoFrontFile;
  } // CellDE_Settings  
  
  
  public Algorithm configure() throws JMException {
	  return null;
  }

  /**
   * Configure the algorithm with the specified parameter settings
   * @return an algorithm object
   * @throws jmetal.util.JMException
   */
  public Algorithm configure(String[] args) throws JMException {
    
    Operator selection;
    Operator crossover;
    Operator mutation;

    QualityIndicator indicators;

    // Creating the problem
    //algorithm = new CellDEJuanjo(problem_);
    //algorithm = new CellDECanonico(problem_);
    
    double CR, F;
    populationSize_ = (int) new Double(args[1]).doubleValue();
    int windowSize_ = (int) new Double(args[2]).doubleValue();
    double minProb_ = new Double(args[3]).doubleValue();
    CR 				= new Double(args[4]).doubleValue();
    F				= new Double(args[5]).doubleValue();
    double Pc		= new Double(args[6]).doubleValue();
    double Pm		= new Double(args[7]).doubleValue();
    double DiC		= new Double(args[8]).doubleValue();
    double DiM		= new Double(args[9]).doubleValue();
    
    // Algorithm parameters
    algorithm.setInputParameter("populationSize", populationSize_);
    algorithm.setInputParameter("archiveSize", archiveSize_);
    algorithm.setInputParameter("maxEvaluations", maxEvaluations_);
    algorithm.setInputParameter("feedback", archiveFeedback_);
    
    algorithm.setInputParameter("minContribution", minProb_);
    algorithm.setInputParameter("windowSize", windowSize_);
    
    Offspring[] getOffspring = new Offspring[3];
    
    getOffspring[0] = new DifferentialEvolutionOffspring(CR , F , Pm ,20);
//    getOffspring[0] = new DifferentialEvolutionOffspring(CR = 1.0, F = 0.5, 1.0/problem_.getNumberOfVariables(),20);

    //getOffspring[0] = new DifferentialEvolutionOffspring(CR = 1.0, F = 0.5);
    //getOffspring[1] = new DifferentialEvolutionPBX_SAOffSpring(CR = 1.0, F = 0.5,problem_);
    
    getOffspring[1] = new SBXCrossoverAndPolynomialMutation(
    	      Pm, // mutation probability
    	      Pc, // crossover probability
    	      DiM, // distribution index for mutation
    	      DiC); // distribution index for crossover
    
//    getOffspring[1] = new SBXCrossoverAndPolynomialMutation(
//      1.0 / problem_.getNumberOfVariables(), // mutation probability
//      0.9, // crossover probability
//      20, // distribution index for mutation
//      20); // distribution index for crossover
//

    getOffspring[2] = new PolynomialOffspringGenerator(Pm, DiM);
//    getOffspring[2] = new PolynomialOffspringGenerator(1.0/problem_.getNumberOfVariables(), 20);
   //getOffspring[3]  = null;
    /*
    getOffspring[2] = new SBXCrossoverAndPolynomialMutation(
    1.0/problem_.getNumberOfVariables(), // mutation probability
    1.0, // crossover probability
    5,  // distribution index for mutation
    5) ; // distribution index for crossover

    getOffspring[2] = new DifferentialEvolutionOffspring(CR = 0.5, F = 0.5) ;
    /*
    getOffspring[2] = new DifferentialEvolutionOffspring(CR = 0.1, F = 0.75) ;
    getOffspring[3] = new DifferentialEvolutionOffspring(CR = 0.1, F = 0.1) ;
     */



    //getOffspring[2] = new SBXCrossoverAndPolynomialMutation(
    //  1/problem_.getNumberOfVariables(), // mutation probability
    //  1.0, // crossover probability
    //  20,  // distribution index for mutation
    //  200) ; // distribution index for crossover

    algorithm.setInputParameter("offspringsCreators", getOffspring);

    // Creating the indicator object
//    if (!paretoFrontFile_.equals("")) {
//      indicators = new QualityIndicator(problem_, paretoFrontFile_);
//      algorithm.setInputParameter("indicators", indicators);
//    } // if

    return algorithm;
  }
} // CellDE2_Settings

