/**
 * StandardStudy.java
 *
 * @author Antonio J. Nebro
 * @version 1.0
 */
package jmetal.experiments;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import java.util.Properties;
import jmetal.base.Algorithm;
import jmetal.base.Problem;
import jmetal.experiments.settings.GDE3_Settings;
import jmetal.experiments.settings.MOCell_Settings;
import jmetal.experiments.settings.NSGAII_Settings;
import jmetal.experiments.settings.SPEA2_Settings;
import jmetal.experiments.settings.SMPSO_Settings;
import jmetal.experiments.settings.pSteadyStateSelfAdaptiveWindow_Settings;
import jmetal.experiments.settings.pSteadyStateSelfAdaptiveWindowSACRF_Settings;
import jmetal.experiments.util.RBoxplot;
import jmetal.experiments.util.RWilcoxon;
import jmetal.util.JMException;

/**
 * @author Antonio J. Nebro
 */
public class adaptiveStudyLZ extends Experiment {

  /**
   * Configures the algorithms in each independent run
   * @param problem The problem to solve
   * @param problemIndex
   * @throws ClassNotFoundException 
   */
  public void algorithmSettings(Problem problem, int problemIndex, Algorithm[] algorithm) throws ClassNotFoundException {
    try {
      int numberOfAlgorithms = algorithmNameList_.length;

      Properties[] parameters = new Properties[numberOfAlgorithms];

      for (int i = 0; i < numberOfAlgorithms; i++) {
        parameters[i] = new Properties();
      }

      if (!paretoFrontFile_[problemIndex].equals("")) {
        for (int i = 0; i < numberOfAlgorithms; i++)
          parameters[i].setProperty("paretoFrontFile_", paretoFrontFile_[problemIndex]);
        } // if

//        algorithm[0] = new NSGAII_Settings(problem).configure(parameters[0]);
//        algorithm[1] = new MOCell_Settings(problem).configure(parameters[1]);
//        algorithm[2] = new SMPSO_Settings(problem).configure(parameters[2]);
//        algorithm[3] = new GDE3_Settings(problem).configure(parameters[3]);
        algorithm[0] = new pSteadyStateSelfAdaptiveWindow_Settings(problem).configure(parameters[0]);
        algorithm[1] = new pSteadyStateSelfAdaptiveWindowSACRF_Settings(problem).configure(parameters[1]);
      } catch (IllegalArgumentException ex) {
      Logger.getLogger(adaptiveStudy.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      Logger.getLogger(adaptiveStudy.class.getName()).log(Level.SEVERE, null, ex);
    } catch  (JMException ex) {
      Logger.getLogger(adaptiveStudy.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static void main(String[] args) throws JMException, IOException {
    adaptiveStudyLZ exp = new adaptiveStudyLZ();

    exp.experimentName_ = "AdaptiveStandardStudy_SACRF";
    exp.algorithmNameList_ = new String[]{"aSSSA", "aSSSA_SACRF"};
//    exp.algorithmNameList_ = new String[]{"NSGAII", "MOCell", "SMPSO", "GDE3", "aSSSA"};
    exp.problemList_ = new String[]{"LZ09_F1", "LZ09_F2","LZ09_F3", "LZ09_F4","LZ09_F5",
                                    //"LZ09_F6", 
    		"LZ09_F7","LZ09_F8","LZ09_F9"};
    exp.paretoFrontFile_ = new String[]{"LZ09_F1.pf","LZ09_F2.pf","LZ09_F3.pf",
                                    "LZ09_F4.pf","LZ09_F5.pf",//"LZ09_F6.pf",
                                    "LZ09_F7.pf","LZ09_F8.pf","LZ09_F9.pf"};

    exp.indicatorList_ = new String[]{"HV", "SPREAD", "EPSILON"};

    int numberOfAlgorithms = exp.algorithmNameList_.length;

    exp.experimentBaseDirectory_ = "./results/" +
                                   exp.experimentName_;
    exp.paretoFrontDirectory_ = "./data/paretoFronts";

    exp.algorithmSettings_ = new Settings[numberOfAlgorithms];

    exp.independentRuns_ = 100;

    // Run the experiments
    int numberOfThreads ;
    exp.runExperiment(numberOfThreads = 8) ;

    // Generate latex tables
    exp.generateLatexTables() ;

    // Configure the R scripts to be generated
    int rows  ;
    int columns  ;
    String prefix ;
    String [] problems ;
    boolean notch ;

        // Configure scripts for LZ
    rows = 3 ;
    columns = 3 ;
    prefix = new String("LZ");
    problems = new String[]{"LZ09_F1.pf","LZ09_F2.pf","LZ09_F3.pf",
            "LZ09_F4.pf","LZ09_F5.pf",//"LZ09_F6.pf",
            "LZ09_F7.pf","LZ09_F8.pf","LZ09_F9.pf"} ;

    RBoxplot.generateScripts(rows, columns, problems, prefix, notch=false, exp) ;
    RWilcoxon.generateScripts(problems, prefix, exp) ;
  } // main
} // StandardStudy


