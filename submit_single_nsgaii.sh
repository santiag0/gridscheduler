# Para cada instancia se realizan 30 experimentos.
for (( i=1; i<=25; i++ ))
do
	oarsub -n single_${i}_nsgaii -l /nodes=1/core=6,walltime=60:00:00 --notify="mail:santiago.iturriaga@gmail.com" "./run_param.sh SingleTask $i 0 5 6 1"
done
