# Para cada instancia se realizan 30 experimentos.
for (( i=1; i<=25; i++ ))
do
	oarsub -n homo_${i}_mocell -l /nodes=1/core=6,walltime=60:00:00 --notify="mail:santiago.iturriaga@gmail.com" "./run_param.sh HomoParr $i 0 5 6 0"
done
