TASKS=50
MACHS=1

mkdir -p moea/${TASKS}_${MACHS}

for i in {1..9}
do
    python3 convert_ruso.py inst/Bernabe-5-1000-0-0-0-100-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-0-0-100-0${i}.ssfModSergio.revCP moea/${TASKS}_${MACHS}
    python3 convert_ruso.py inst/Bernabe-5-1000-0-0-100-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-0-100-0-0${i}.ssfModSergio.revCP moea/${TASKS}_${MACHS}
    python3 convert_ruso.py inst/Bernabe-5-1000-0-100-0-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-100-0-0-0${i}.ssfModSergio.revCP moea/${TASKS}_${MACHS}
    python3 convert_ruso.py inst/Bernabe-5-1000-100-0-0-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-100-0-0-0-0${i}.ssfModSergio.revCP moea/${TASKS}_${MACHS}
    python3 convert_ruso.py inst/Bernabe-5-1000-30-30-30-10-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-30-30-30-10-0${i}.ssfModSergio.revCP moea/${TASKS}_${MACHS}
done
