#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  cplex_parser.py
#
#  Copyright 2015 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from tabulate import tabulate

def main():
    sol_file = "/inco/home01/siturria/Local/temp/res_nrg.txt"

    cn_nmach = [8,16]
    cn_ncore = [6,4]
    cn_idle = [102,76]
    cn_max = [210,214]

    #8 6 17.93 102.0 210.0
    #32 4 11.72 76.0 214.0

    total_eidle = 0.0
    total_ework = 0.0

    mach_mk = []
    mach_work = []

    for i in range(2):
        mach_mk.append([])
        mach_work.append([])

    curr_m = 1
    curr_cn = 0
    curr_mach = 0
    curr_core = 0

    aux_mach = {}

    with open(sol_file) as sol:
        m_sep = ""
        while not m_sep == "> Machine {0}".format(curr_m):
            m_sep = sol.readline().strip()
            print("{0}".format(m_sep))

        curr_mk = 0.0
        curr_work = 0.0
        curr_tasks = []

        while curr_m <= (cn_nmach[0] * cn_ncore[0] + cn_nmach[1] * cn_ncore[1]):
            m_sep = sol.readline().strip()
            print("{0}".format(m_sep))

            while not m_sep == "> Machine {0}".format(curr_m+1) and not m_sep == "":
                task_raw = m_sep.split(" ")
                t_id = int(task_raw[1].strip())
                t_start = int(task_raw[2].strip().split("=")[1].strip())
                t_end = int(task_raw[3].strip().split("=")[1].strip())

                if curr_mk < t_end:
                    curr_mk = t_end

                curr_work = curr_work + t_end - t_start
                curr_tasks.append(t_id)

                m_sep = sol.readline().strip()
                print("{0}".format(m_sep))

            curr_core = curr_core + 1

            if curr_core == cn_ncore[curr_cn]:
                mach_mk[curr_cn].append(curr_mk)
                mach_work[curr_cn].append(curr_work)

                total_eidle = total_eidle + (curr_mk * cn_idle[curr_cn])
                #total_ework = total_ework + (curr_work * (cn_max[curr_cn]-cn_idle[curr_cn]))
                total_ework = total_ework + (curr_work * ((cn_max[curr_cn]-cn_idle[curr_cn]) / cn_ncore[curr_cn]))

                curr_mach = curr_mach + 1
                curr_core = 0

                curr_mk = 0.0
                curr_work = 0.0

                if curr_mach == cn_nmach[curr_cn]:
                    curr_cn = curr_cn + 1

                    if curr_cn < 2:
                        curr_mach = 0
                        curr_core = 0

            aux_mach[curr_m] = curr_tasks
            curr_tasks = []
            curr_m = curr_m + 1

    #print(tabulate(mach_mk))
    #print(tabulate(mach_work))

    #print(mach_mk)
    #print(mach_work)

    for i in range(curr_m):
        if i in aux_mach:
            print("{0} ==> ".format(i), end="")
            print(aux_mach[i])

    print("Total EIdle = {0}".format(total_eidle))
    print("Total EMax = {0}".format(total_ework))
    print("Total energy = {0}".format(total_eidle+total_ework))

    return 0

if __name__ == '__main__':
    main()

