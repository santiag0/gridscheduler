using CP;

// Number of Tasks
int nbTaskCores = ...;
range TaskCores = 1..nbTaskCores;

int nbTasks = ...;
range Tasks = 1..nbTasks;
{int} tasks[Tasks] = ...;

// Tasks dependencies
{int} preccs[TaskCores] = ...;

// Number of Machine Cores
int nbMachineCores = ...;
range MachineCores = 1..nbMachineCores;

int nbMachines = ...;
range Machines = 1..nbMachines;
{int} machines[Machines] = ...;

int duration[TaskCores,MachineCores] = ...;

dvar interval task[t in TaskCores];
dvar interval opttask[t in TaskCores][m in MachineCores] optional size duration[t][m];
dvar sequence tool[m in MachineCores] in all(t in TaskCores) opttask[t][m];

// Minimize the makespan
minimize
    max(t in TaskCores) endOf(task[t]);
   
subject to {
    // Each job needs one unary resource of the alternative set opttask
    forall(t in TaskCores)
        alternative(task[t], all(m in MachineCores) opttask[t][m]);
       
    forall(tg in Tasks) {
        // All cores of the task must be synchronized (start and end at the same time)
        synchronize(task[first(tasks[tg])], all(tg2 in tasks[tg]) task[tg2]);
       
        // All cores of the task must be assigned to the same machine
        (sum(mg in Machines)
            ((sum(t in tasks[tg], m in machines[mg]) presenceOf(opttask[t][m]))
                == card(tasks[tg]))) == 1;   
    }           
       
    // No overlap on machines
    forall(m in MachineCores)
        noOverlap(tool[m]);
       
    // Precedence contraints
    forall (t1 in TaskCores)
        forall (t2 in preccs[t1])
            endBeforeStart(task[t2], task[t1]);
};
