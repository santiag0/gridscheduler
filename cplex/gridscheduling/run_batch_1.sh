export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib:/usr/local/cplex1251/opl/bin/x86-64_sles10_4.1/
export PATH=$PATH:/usr/lib:/usr/local/cplex1251/opl/bin/x86-64_sles10_4.1/

for i in {1..1}
do
    for obj in nrg
    do
        #oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-0-0-100-0${i}.30.dat > Bernabe-5-1000-0-0-0-100-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-0-100-0-0${i}.30.dat > Bernabe-5-1000-0-0-100-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-100-0-0-0${i}.30.dat > Bernabe-5-1000-0-100-0-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-100-0-0-0-0${i}.30.dat > Bernabe-5-1000-100-0-0-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-30-30-30-10-0${i}.30.dat > Bernabe-5-1000-30-30-30-10-0${i}.30.${obj}.log
    done
done

for i in {2..3}
do
    for obj in mk sla nrg
    do
        #oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-0-0-100-0${i}.30.dat > Bernabe-5-1000-0-0-0-100-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-0-100-0-0${i}.30.dat > Bernabe-5-1000-0-0-100-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-0-100-0-0-0${i}.30.dat > Bernabe-5-1000-0-100-0-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-100-0-0-0-0${i}.30.dat > Bernabe-5-1000-100-0-0-0-0${i}.30.${obj}.log
        oplrun gridscheduling_${obj}.mod Bernabe-5-1000-30-30-30-10-0${i}.30.dat > Bernabe-5-1000-30-30-30-10-0${i}.30.${obj}.log
    done
done
