export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib:/usr/local/cplex1251/opl/bin/x86-64_sles10_4.1/
export PATH=$PATH:/usr/lib:/usr/local/cplex1251/opl/bin/x86-64_sles10_4.1/

for i in {1..3}
do
    oplrun gridscheduling_mk.mod Bernabe-5-1000-0-0-0-100-0${i}.30.dat > Bernabe-5-1000-0-0-0-100-0${i}.30.mk.log
    oplrun gridscheduling_mk.mod Bernabe-5-1000-0-0-100-0-0${i}.30.dat > Bernabe-5-1000-0-0-100-0-0${i}.30.mk.log
    oplrun gridscheduling_mk.mod Bernabe-5-1000-0-100-0-0-0${i}.30.dat > Bernabe-5-1000-0-100-0-0-0${i}.30.mk.log
    oplrun gridscheduling_mk.mod Bernabe-5-1000-100-0-0-0-0${i}.30.dat > Bernabe-5-1000-100-0-0-0-0${i}.30.mk.log
    oplrun gridscheduling_mk.mod Bernabe-5-1000-30-30-30-10-0${i}.30.dat > Bernabe-5-1000-30-30-30-10-0${i}.30.mk.log
    
    oplrun gridscheduling_sla.mod Bernabe-5-1000-0-0-0-100-0${i}.30.dat > Bernabe-5-1000-0-0-0-100-0${i}.30.sla.log
    oplrun gridscheduling_sla.mod Bernabe-5-1000-0-0-100-0-0${i}.30.dat > Bernabe-5-1000-0-0-100-0-0${i}.30.sla.log
    oplrun gridscheduling_sla.mod Bernabe-5-1000-0-100-0-0-0${i}.30.dat > Bernabe-5-1000-0-100-0-0-0${i}.30.sla.log
    oplrun gridscheduling_sla.mod Bernabe-5-1000-100-0-0-0-0${i}.30.dat > Bernabe-5-1000-100-0-0-0-0${i}.30.sla.log
    oplrun gridscheduling_sla.mod Bernabe-5-1000-30-30-30-10-0${i}.30.dat > Bernabe-5-1000-30-30-30-10-0${i}.30.sla.log

    oplrun gridscheduling_nrg.mod Bernabe-5-1000-0-0-0-100-0${i}.30.dat > Bernabe-5-1000-0-0-0-100-0${i}.30.nrg.log
    oplrun gridscheduling_nrg.mod Bernabe-5-1000-0-0-100-0-0${i}.30.dat > Bernabe-5-1000-0-0-100-0-0${i}.30.nrg.log
    oplrun gridscheduling_nrg.mod Bernabe-5-1000-0-100-0-0-0${i}.30.dat > Bernabe-5-1000-0-100-0-0-0${i}.30.nrg.log
    oplrun gridscheduling_nrg.mod Bernabe-5-1000-100-0-0-0-0${i}.30.dat > Bernabe-5-1000-100-0-0-0-0${i}.30.nrg.log
    oplrun gridscheduling_nrg.mod Bernabe-5-1000-30-30-30-10-0${i}.30.dat > Bernabe-5-1000-30-30-30-10-0${i}.30.nrg.log
done
