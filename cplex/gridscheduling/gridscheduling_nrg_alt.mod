/*********************************************
 * OPL 12.6.0.0 Model
 * Author: santiago
 * Creation Date: Apr 9, 2015 at 12:01:36 PM
 *********************************************/
using CP;

// Number of Machines
int nbMachines = ...;
range Machines = 1..nbMachines;

// Number of Tasks
int nbTasks = ...;
range Tasks = 1..nbTasks;

int duration[Tasks,Machines] = ...;
float e_idle  [Machines] =...;
float e_max   [Machines] = ...;

// Job dependencies
{int} preccs[Tasks] = ...; 
 
int nbJobs = ...;
range Jobs = 1..nbJobs;

{int} jobTasks[Jobs] = ...; 
int deadline[Jobs] = ...;

int nbUsers = ...;
range Users = 1..nbUsers;

{int} userJobs[Users] = ...;
int userSLA[Users] = ...;

int nbTaskGroup = ...;
range TaskGroup = 1..nbTaskGroup;
{int} taskGroup[TaskGroup] = ...;

int nbMachineGroup = ...;
range MachineGroup = 1..nbMachineGroup;
{int} machineGroup[MachineGroup] = ...;

int nbCN = ...;
range CN = 1..nbCN;
{int} cn[CN] = ...;

dvar interval task[t in Tasks];
dvar interval opttask[t in Tasks][m in Machines] optional size duration[t][m];
dvar sequence tool[m in Machines] in all(t in Tasks) opttask[t][m];

execute {
	//cp.param.FailLimit = 250000;
	//cp.param.timeLimit = 50400; // 14 horas
	//cp.param.timeLimit = 25200; // 7 horas
	//cp.param.timeLimit = 18000; // 5 horas
}

// Minimize the makespan
minimize 
	sum(m in Machines) (max(t in Tasks) endOf(opttask[t][m]));

    /*sum(mg in MachineGroup) 
    	((max(t in Tasks, m in machineGroup[mg]) endOf(opttask[t][m]) * e_idle[m]) * card(machineGroup[mg])) 
    +  
    sum(m in Machines) 
    	sum(t in Tasks) (sizeOf(opttask[t][m]) * (e_max[m]-e_idle[m]));*/
    
subject to {
	// Each job needs one unary resource of the alternative set s (28)
	forall(t in Tasks)
		alternative(task[t], all(m in Machines) opttask[t][m]);
		
	forall(tg in TaskGroup) {
		synchronize(task[first(taskGroup[tg])], all(tg2 in taskGroup[tg]) task[tg2]);
		
		(sum(mg in MachineGroup) 
			((sum(t in taskGroup[tg], m in machineGroup[mg]) presenceOf(opttask[t][m])) 
				== card(taskGroup[tg]))) == 1;	
	}			
	
	/*forall(j in Jobs) {
		(sum(c in CN)
			(card(jobTasks[j]) == (sum(t in jobTasks[j], m in cn[c]) presenceOf(opttask[t][m])))) == 1; 
	}*/		
		
	// No overlap on machines
	forall(m in Machines)
		noOverlap(tool[m]);
	forall (t1 in Tasks)
		forall (t2 in preccs[t1])
			endBeforeStart(task[t2], task[t1]);
};

execute {
	writeln(task);
	
	//for (var m in Machines) {
	for (var mg in MachineGroup) {
		//writeln("=== Machine group " + mg + " =================");
		for (var m in machineGroup[mg]) {
			writeln("> Machine " + m);			
			for (var t in Tasks) {
				if (opttask[t][m].present) {
					writeln("Task " + t + " starts=" + opttask[t][m].start + " ends=" + opttask[t][m].end + " size=" + opttask[t][m].size);
				}
			}	
		}		
	}
	
	var tidle = 0;
	var tmax = 0;
	
	//for (var m in Machines) {
	for (var mg in MachineGroup) {
		var makespan;
		makespan = 0;
		
		var count;
		count = 0;
		
		for (var m in machineGroup[mg]) {
			for (var t in Tasks) {
				if (opttask[t][m].present) {
					if (makespan < opttask[t][m].end) {
						makespan = opttask[t][m].end;
					}
					
					tmax = tmax + (opttask[t][m].size * (e_max[m]-e_idle[m]));
				}
			}	
			
			count = count + 1;
		}
		
		
		tidle = tidle + (makespan * e_idle[m] * count);
	}
	
	writeln("IDLE energy = " + tidle);
	writeln("MAX energy = " + tmax);
	writeln("Total energy = " + (tidle+tmax));
};
 
