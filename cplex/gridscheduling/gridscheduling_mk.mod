/*********************************************
 * OPL 12.6.0.0 Model
 * Author: santiago
 * Creation Date: Apr 9, 2015 at 12:01:36 PM
 *********************************************/
using CP;

// Number of Machines
int nbMachines = ...;
range Machines = 1..nbMachines;

// Number of Tasks
int nbTasks = ...;
range Tasks = 1..nbTasks;

int duration[Tasks,Machines] = ...;
float e_idle  [Machines] =...;
float e_max   [Machines] = ...;

// Job dependencies
{int} preccs[Tasks] = ...; 
 
int nbJobs = ...;
range Jobs = 1..nbJobs;

{int} jobTasks[Jobs] = ...; 
int deadline[Jobs] = ...;

int nbUsers = ...;
range Users = 1..nbUsers;

{int} userJobs[Users] = ...;
int userSLA[Users] = ...;

int nbTaskGroup = ...;
range TaskGroup = 1..nbTaskGroup;
{int} taskGroup[TaskGroup] = ...;

int nbMachineGroup = ...;
range MachineGroup = 1..nbMachineGroup;
{int} machineGroup[MachineGroup] = ...;

int nbCN = ...;
range CN = 1..nbCN;
{int} cn[CN] = ...;

dvar interval task[t in Tasks];
dvar interval opttask[t in Tasks][m in Machines] optional size duration[t][m];
dvar sequence tool[m in Machines] in all(t in Tasks) opttask[t][m];

execute {
	//cp.param.FailLimit = 500000;
	//cp.param.timeLimit = 50400; // 14 horas
	//cp.param.timeLimit = 25200; // 7 horas
	cp.param.timeLimit = 18000; // 5 horas
}

// Minimize the makespan
minimize 
	max(t in Tasks) endOf(task[t]);
subject to {
	// Each job needs one unary resource of the alternative set s (28)
	forall(t in Tasks)
		alternative(task[t], all(m in Machines) opttask[t][m]);

	forall(tg in TaskGroup) {
		synchronize(task[first(taskGroup[tg])], all(tg2 in taskGroup[tg]) task[tg2]);
		
		(sum(mg in MachineGroup) 
			((sum(t in taskGroup[tg], m in machineGroup[mg]) presenceOf(opttask[t][m])) 
				== card(taskGroup[tg]))) == 1;	
	}			
	
	forall(j in Jobs) {
		(sum(c in CN)
			(card(jobTasks[j]) == (sum(t in jobTasks[j], m in cn[c]) presenceOf(opttask[t][m])))) == 1; 
	}	
		
	// No overlap on machines
	forall(m in Machines)
		noOverlap(tool[m]);	
		
	forall (t1 in Tasks)
		forall (t2 in preccs[t1])
			endBeforeStart(task[t2], task[t1]);
};

execute {
	writeln(task);
	
	//for (var m in Machines) {
	for (var mg in MachineGroup) {
		writeln("=== Machine group " + mg + " =================");
		for (var m in machineGroup[mg]) {
			writeln("> Machine " + m);			
			for (var t in Tasks) {
				if (opttask[t][m].present) {
					writeln("Task " + t + " starts=" + opttask[t][m].start + " ends=" + opttask[t][m].end + " size=" + opttask[t][m].size);
				}
			}	
		}		
	}
};
 
