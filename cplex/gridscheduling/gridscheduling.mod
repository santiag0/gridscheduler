/*********************************************
 * OPL 12.6.0.0 Model
 * Author: santiago
 * Creation Date: Apr 9, 2015 at 12:01:36 PM
 *********************************************/
using CP;

// Number of Machines
int nbMachines = ...;
range Machines = 1..nbMachines;

// Number of Tasks
int nbTasks = ...;
range Tasks = 1..nbTasks;

int duration[Tasks,Machines] = ...;
//int e_idle  [Machines] =...;
//int e_max   [Machines] = ...;

// Job dependencies
{int} preccs[Tasks] = ...; 
 
dvar interval task[t in Tasks];
dvar interval opttask[t in Tasks][m in Machines] optional size duration[t][m];
dvar sequence tool[m in Machines] in all(t in Tasks) opttask[t][m];

execute {
	//cp.param.FailLimit = 5000;
	cp.param.timeLimit = 50400;
}

// Minimize the makespan
minimize 
	//sum(j in Tasks, m in Machines) cost[j][m] * presenceOf(opttask[j][m]);
	//sum(j in Tasks, m in Machines) cost[j][m] * presenceOf(opttask[j][m]);
	max(t in Tasks) endOf(task[t]);
subject to {
	// Each job needs one unary resource of the alternative set s (28)
	forall(t in Tasks)
		alternative(task[t], all(m in Machines) opttask[t][m]);
	// No overlap on machines
	forall(m in Machines)
		noOverlap(tool[m]);
	forall (t1 in Tasks)
		forall (t2 in preccs[t1])
			endBeforeStart(task[t2], task[t1]);
};

execute {
	writeln(task);
	for (var t in Tasks) {
		writeln("Task " + t + " starts=" + task[t].start + " ends=" + task[t].end);
	}
	
	for (var m in Machines) {
		writeln("=== Machine " + m + " =================")			
		for (var t in Tasks) {
			if (opttask[t][m].present) {
				writeln("Task " + t + " starts=" + opttask[t][m].start + " ends=" + opttask[t][m].end);
			}
		}	
	}
};
 
