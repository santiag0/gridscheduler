#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  random.py
#  
#  Copyright 2015 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import random

nbMachines = 50
#nbTasks = 3000 # approx. 50 jobs
nbTasks = 1000 # approx. 17 jobs
#nbTasks = 2000 # approx. 32 jobs

def main():
	random.seed(1)
	
	print('nbMachines = {0};'.format(nbMachines));
	print('nbTasks = {0};'.format(nbTasks));

	print('duration = [');
	for t in range(nbTasks):
		print('   [', end='')
		for m in range(nbMachines):
			print('{0}, '.format(random.randint(1,25)), end='')
		print('], ')
	print('];');

	print('preccs = [');
	for t in range(nbTasks):
		print('   {', end='')
		if t > 1:
			for t2 in range(t-1):
				if random.random() < 0.10:
					print('{0}, '.format(t2+1), end='')
		print('}, ')	
	print('];');

	return 0

if __name__ == '__main__':
	main()

