#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  convert.py
#
#  Copyright 2015 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import sys
from operator import itemgetter

def convert_format(cn_file, job_file):
    maxTaskNumCores = 0
    maxMachNumCores = 0

    numMaxTasks = 50 #600
    numMaxMachines = 1 #50
    #numMaxFullTasks = 89

    aux_cn_list = []
    cn_list = []
    mach_groups = []
    task_groups = []

    numMachines = 0
    num_task_groups = 0

    with open(cn_file) as f:
        for line in f:
            #line = f.readline()
            cn = line.strip().split(' ')

            if (len(cn) >= 3):
                #<nro_máquinas_en_CN> <nro_core_en_máquinas de CN> <velocidad_de_procesador> <energy_idle> <energy_max>
                aux_cn_list.append((int(cn[0]), int(cn[1]), float(cn[2]), float(cn[3]), float(cn[4])))

    aux_cn_list = sorted(aux_cn_list, key=itemgetter(1), reverse=True)
    
    for cn in aux_cn_list:
        cn_list.append(cn)
    
        count = 1
        for i in range(cn[0]):
            new_group = []
            for j in range(cn[1]):
                new_group.append(numMachines + count)
                count = count + 1
            mach_groups.append(new_group)

        maxMachNumCores = max(maxMachNumCores, cn[1])

        numMachines = numMachines + (cn[0] * cn[1])
        if numMachines > numMaxMachines:
            break

    task_list = []
    task_map = []
    task_info = []
    job_info = []

    jidx = 0
    numTasks = 0

    with open(job_file) as f:
        for line in f:
            if numMaxTasks < numTasks and jidx > 1:
                break

            #for lidx in range(30):
            #line = f.readline()
            #print(line)

            tasks = line.strip().split('-1')
            #<tiempo_ejecucion_T0> <cores_requeridos_T0> <tarea.de.la.que.depende.T0_1><-1>
            #15 665 1 72.0 4 4 6 8 9 10 11 12 13 -1

            if len(tasks[0].strip()) > 0:
                task_map.append([])
                task_info.append([])

                for tidx in reversed(range(len(tasks))):
                    task = tasks[tidx].strip().split(' ')

                    if tidx == 0:
                        j_deadline = int(task[1])
                        job_info.append((j_deadline,))

                        e_time = float(task[3])
                        n_cores = int(task[4])

                        int_deps = []
                        for d in task[5:]:
                            int_deps.append(int(d))
                    else:
                        if len(task) > 1:
                            e_time = float(task[0])
                            n_cores = int(task[1])

                            int_deps = []
                            for d in task[2:]:
                                int_deps.append(int(d))

                    numTasks = numTasks + n_cores

                    maxTaskNumCores = max(maxTaskNumCores, n_cores)

                    task_info[jidx].append((e_time,n_cores,int_deps))
                    task_map[jidx].append(len(task_list))
                    num_task_groups = num_task_groups + 1

                    for cidx in range(n_cores):
                        task_list.append(((jidx,tidx,cidx),e_time,n_cores,[]))

                for tidx in range(len(task_info[jidx])):
                    tmap = task_map[jidx][tidx]
                    tdeps = []

                    for tdidx in range(len(task_info[jidx][tidx][2])):
                        td = task_info[jidx][tidx][2][tdidx]
                        tdmap = task_map[jidx][td]
                        for tdc in range(task_info[jidx][td][1]):
                            tdeps.append(tdmap+tdc)

                    for cidx in range(task_info[jidx][tidx][1]):
                        task_list[tmap+cidx][3].extend(tdeps)

                jidx = jidx + 1

    #print(cn_list)
    #print(job_list)

    numUsers = 3;

    if maxTaskNumCores > maxMachNumCores:
        print("Error infactibilidad")
        exit(0)

    print('nbMachines = {0};'.format(numMachines))
    print('nbTasks = {0};'.format(numTasks))
    print('nbJobs = {0};'.format(jidx))
    print('nbUsers = {0};'.format(numUsers))
    print('nbTaskGroup = {0};'.format(num_task_groups))
    print('nbMachineGroup = {0};'.format(len(mach_groups)))
    print('nbCN = {0};'.format(len(cn_list)))

    print("taskGroup = [")
    for i in range(jidx):
        for j in range(len(task_info[i])):
            print("   {", end="")
            for h in range(task_info[i][j][1]):
                print("{0}, ".format(task_map[i][j]+h+1), end="")
            print("},")
    print("];")

    print("machineGroup = [")
    for i in range(len(mach_groups)):
        print("   {", end="")
        for j in range(len(mach_groups[i])):
            print("{0}, ".format(mach_groups[i][j]), end="")
        print("},")
    print("];")

    cn_mach_idx = 1;
    print("cn = [")
    for cn in cn_list:
        print("   {", end="")
        for j in range(cn[0] * cn[1]):
            print("{0}, ".format(cn_mach_idx), end="")
            cn_mach_idx = cn_mach_idx + 1
        print("},")
    print("];")

    print("duration = [")
    for i in range(len(task_list)):
        print('   [',end='')
        for j in range(len(cn_list)):
            for jc in range(cn_list[j][0] * cn_list[j][1]):
                print(str(int(task_list[i][1]/cn_list[j][2])), end=',')
        print('],')
    print("];")

    print("preccs = [")
    for i in range(len(task_list)):
        print('   {',end='')
        for j in range(len(task_list[i][3])):
            print(task_list[i][3][j]+1, end=',')
        print('},')
    print("];")

    #{int} jobTasks[Jobs] = ...;
    print ("jobTasks = [")
    for i in range(jidx):
        print('   {',end='')
        for j in range(len(task_info[i])):
            #print(task_info[i][j][1])
            for jc in range(task_info[i][j][1]):
                print(task_map[i][j]+1+jc, end=',')
        print('},')
    print ("];")

    print ("deadline = [")
    for i in range(jidx):
        print('{0}, '.format(job_info[i][0]),end='')
    print("];")

    print('e_idle = [')
    for i in range(len(cn_list)):
        for h in range(cn_list[i][0]):
            numCores = cn_list[i][1]
            for j in range(numCores):
                print('{0},'.format(cn_list[i][3] / numCores),end='')
    print('];')

    print('e_max = [')
    for i in range(len(cn_list)):
        for h in range(cn_list[i][0]):
            numCores = cn_list[i][1]
            for j in range(numCores):
                print('{0},'.format(cn_list[i][4] / numCores),end='')
    print('];')

    #All jobs to the same user
    print('userJobs = [')
    for u in range(numUsers):
        print('   {',end='')

        for i in range(int((jidx/numUsers)*u),int((jidx/numUsers)*(u+1))):
            print('{0}, '.format(i+1),end='')

        print('}, ')
    print('];')

    #SLA for the user is the maximum number of allowed job violations
    #print('userSLA = [0];') # for 1 user with 30 jobs and a 98% SLA
    #print('userSLA = [1];') # for 1 user with 30 jobs and a 94% SLA
    #print('userSLA = [3];') # for 1 user with 30 jobs and a 90% SLA
    print('userSLA = [1,2,3];') # for 1 user with 30 jobs and a 98% SLA

def main():
    #convert_format('Hetro-Parr.ssfModCNsSergio','Hetro-Parr.ssfModSergio')

    if len(sys.argv) == 3:
        #convert_format('inst/Bernabe-5-1000-100-0-0-0-01.ssfModCNsSergioCP','inst/Bernabe-5-1000-100-0-0-0-01.ssfModSergio.revCP')
        convert_format(sys.argv[1],sys.argv[2])
    else:
        print("{0} <CN instance> <task instance>".format(sys.argv[0]))

    return 0

if __name__ == '__main__':
    main()

