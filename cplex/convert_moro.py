#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  convert_moro.py
#  
#  Copyright 2015 Santiago Iturriaga - INCO <siturria@saxo.fing.edu.uy>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
from operator import itemgetter
from os.path import basename

def convert_format(moro_file, job_file, out_dir):
    string_mach_list = []
    string_task_list = []

    maxTaskNumCores = 0
    maxMachNumCores = 0

    numMaxTasks = 50 #600
    numMaxMachines = 1 #50
    #numMaxFullTasks = 89

    aux_cn_list = []
    cn_list = []
    mach_groups = []
    task_groups = []

    numMachines = 0
    num_task_groups = 0

    with open(moro_file) as f:
        for line in f:
            #line = f.readline()
            if line.strip().startswith('<\CN>'):
                #<\CN>0,32,7.2,0,550,100;1,75.0,94.0;
                #<\CN>4,8,17.93,4,550,700;6,102.0,210.0;
                
                cn_parts = line.strip().split(';')
                cn_part1 = cn_parts[0].split(',')
                cn_part2 = cn_parts[1].split(',')
                
                nro_servers = int(cn_part1[1])
                nro_cores = int(cn_part2[0])
                cpu_speed = float(cn_part1[2])
                idle_nrg = float(cn_part2[1])
                max_nrg = float(cn_part2[2])

                #<nro_máquinas_en_CN> <nro_core_en_máquinas de CN> <velocidad_de_procesador> <energy_idle> <energy_max>
                aux_cn_list.append((nro_servers, nro_cores, cpu_speed, idle_nrg, max_nrg, line))

    aux_cn_list = sorted(aux_cn_list, key=itemgetter(1), reverse=True)

    for cn in aux_cn_list:
        cn_list.append(cn)
        string_mach_list.append(cn[5])

        count = 1
        for i in range(cn[0]):
            new_group = []
            for j in range(cn[1]):
                new_group.append(numMachines + count)
                count = count + 1
            mach_groups.append(new_group)

        maxMachNumCores = max(maxMachNumCores, cn[1])

        numMachines = numMachines + (cn[0] * cn[1])
        if numMachines > numMaxMachines:
            break

    task_list = []
    task_map = []
    task_info = []
    job_info = []

    jidx = 0
    numTasks = 0

    with open(job_file) as f:
        with open(moro_file) as mf:
            for line in f:
                if numMaxTasks < numTasks and jidx > 1:
                    break

                #for lidx in range(30):
                #line = f.readline()
                #print(line)

                tasks = line.strip().split('-1')
                string_task_list.append(line)
                #<tiempo_ejecucion_T0> <cores_requeridos_T0> <tarea.de.la.que.depende.T0_1><-1>
                #15 665 1 72.0 4 4 6 8 9 10 11 12 13 -1

                if len(tasks[0].strip()) > 0:
                    line_moro = mf.readline()
                    while not line_moro.strip().startswith('<\Job>'):
                        line_moro = mf.readline()
                    
                    task_map.append([])
                    task_info.append([])

                    for tidx in reversed(range(len(tasks))):
                        task = tasks[tidx].strip().split(' ')

                        if tidx == 0:
                            j_deadline = int(task[1])
                            job_info.append((j_deadline,line_moro))

                            e_time = float(task[3])
                            n_cores = int(task[4])

                            int_deps = []
                            for d in task[5:]:
                                int_deps.append(int(d))
                        else:
                            if len(task) > 1:
                                e_time = float(task[0])
                                n_cores = int(task[1])

                                int_deps = []
                                for d in task[2:]:
                                    int_deps.append(int(d))

                        numTasks = numTasks + n_cores

                        maxTaskNumCores = max(maxTaskNumCores, n_cores)

                        task_info[jidx].append((e_time,n_cores,int_deps))
                        task_map[jidx].append(len(task_list))
                        num_task_groups = num_task_groups + 1

                        for cidx in range(n_cores):
                            task_list.append(((jidx,tidx,cidx),e_time,n_cores,[]))

                    for tidx in range(len(task_info[jidx])):
                        tmap = task_map[jidx][tidx]
                        tdeps = []

                        for tdidx in range(len(task_info[jidx][tidx][2])):
                            td = task_info[jidx][tidx][2][tdidx]
                            tdmap = task_map[jidx][td]
                            for tdc in range(task_info[jidx][td][1]):
                                tdeps.append(tdmap+tdc)

                        for cidx in range(task_info[jidx][tidx][1]):
                            task_list[tmap+cidx][3].extend(tdeps)

                    jidx = jidx + 1

    if maxTaskNumCores > maxMachNumCores:
        print("Error infactibilidad")
        exit(0)


    #print("=================================================")
    #print("Jobs > {0};".format(len(job_info)));
    #for i in job_info:
        #print(i[1].strip())
    #print("=================================================")
    #print("Computational Nodes > {0};".format(len(cn_list)))
    #for i in range(len(cn_list)):
        #partes = cn_list[i][5].strip().split(",")
        #print(cn_list[i][5].strip().replace(partes[0]+",","<\CN>{0},".format(i)))

    with open(out_dir.strip().strip('/') + '/' + basename(moro_file), 'w') as f:
        f.write("=================================================\n")
        f.write("Jobs > {0};\n".format(len(job_info)));
        
        for i in job_info:
            f.write(i[1].strip()+"\n")

        f.write("=================================================\n")
        f.write("Computational Nodes > {0};\n".format(len(cn_list)))

        for i in range(len(cn_list)):
            partes = cn_list[i][5].strip().split(",")
            f.write(cn_list[i][5].strip().replace(partes[0]+",","<\CN>{0},".format(i))+"\n")
        
def main():
    #convert_format('Hetro-Parr.ssfModCNsSergio','Hetro-Parr.ssfModSergio')

    if len(sys.argv) == 4:
        #convert_format('inst/Bernabe-5-1000-100-0-0-0-01.ssfModCNsSergioCP','inst/Bernabe-5-1000-100-0-0-0-01.ssfModSergio.revCP')
        convert_format(sys.argv[1],sys.argv[2],sys.argv[3])
    else:
        print("{0} <moro instance> <task instance> <output file>".format(sys.argv[0]))

    return 0

if __name__ == '__main__':
    main()

