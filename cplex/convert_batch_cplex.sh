TASKS=50
MACHS=1

mkdir -p gridscheduling/${TASKS}_${MACHS}

for i in {1..9}
do
    python3 convert_cplex.py inst/Bernabe-5-1000-0-0-0-100-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-0-0-100-0${i}.ssfModSergio.revCP \
        > gridscheduling/${TASKS}_${MACHS}/Bernabe-5-1000-0-0-0-100-0${i}.30.dat   
        
    python3 convert_cplex.py inst/Bernabe-5-1000-0-0-100-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-0-100-0-0${i}.ssfModSergio.revCP \
        > gridscheduling/${TASKS}_${MACHS}/Bernabe-5-1000-0-0-100-0-0${i}.30.dat   
        
    python3 convert_cplex.py inst/Bernabe-5-1000-0-100-0-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-0-100-0-0-0${i}.ssfModSergio.revCP \
        > gridscheduling/${TASKS}_${MACHS}/Bernabe-5-1000-0-100-0-0-0${i}.30.dat   

    python3 convert_cplex.py inst/Bernabe-5-1000-100-0-0-0-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-100-0-0-0-0${i}.ssfModSergio.revCP \
        > gridscheduling/${TASKS}_${MACHS}/Bernabe-5-1000-100-0-0-0-0${i}.30.dat   

    python3 convert_cplex.py inst/Bernabe-5-1000-30-30-30-10-0${i}.ssfModCNsSergioCP inst/Bernabe-5-1000-30-30-30-10-0${i}.ssfModSergio.revCP \
        > gridscheduling/${TASKS}_${MACHS}/Bernabe-5-1000-30-30-30-10-0${i}.30.dat   
done
